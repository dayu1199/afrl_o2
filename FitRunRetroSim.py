"""
FitRunRetroSim

Created by on 6/17/22 by david

Description: Determine precision uncertainty of retroreflector measurements
by adding noise onto simulations and doing multiple fits
"""

import os
import numpy as np
import pandas as pd
import pickle
from datetime import datetime

import FitProgramRetrowBox as fithit

##################################Fitting Parameters###########################

# Fitting Options
save_files = False  # saves fits and fitted parameters in text files.
plot_results = True  # Plots time domain and TD fit converted to the frequency domain. Turn this off for many
# datafiles.
print_fit_report = True  # Prints each fit report to command window
back_remove = False  # Remove background from spectra before fitting

noise_add = True
noise_level = 0.00045 # 0.00045
noise_flat = True
iterations = 150

# Directory Info
fn_sim = 'O2simulationRetro_AP.txt'
d_save = os.path.join(r'/Users/david/Library/CloudStorage',
                      'GoogleDrive-dayu1199@colorado.edu/My Drive',
                      'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits/sim/retro')
file_tag = ''

# MFID Parameters
weight_window = [0.008, 0.5]  # default [0.005,0.5]
etalon_windows = []  # format [[0,100],[200,300]]
band_fit = [13015, 13170]
weight_flat = True  # Otherwise will do an exponential weighting

# Fitting Parameters
angle = 16.69
molec_to_fit = ['O2']  # must be a list
isotopes = [[1]]  # format [[1],[2]]
press_guess = 0.94  # atm
temp_guess = 400
vel_guess = 500
shift_guess = 0
conc_guess = 0.2095
broad_guess = 1
n_pass = 2
temp_fit = True
press_fit = False
shift_fit = True
vel_fit = True
conc_fit = False
broad_fit = False

# Background Fitting Parameters
molec_back = 'O2'
temp_back = 296.00000
shift_back = 0
press_back = 0
label_back = ''
pl_back = 38.6

# Box Fitting Parameters
conc_box = 0.0056
temp_box = 277
pres_box = 1
pl_box = 35

if not (back_remove):
    conc_back = 0
    shift_back = 0
    press_back = 0

    conc_box = 0

# Prepare save folder
now = datetime.now()
timestamp = now.strftime('%d_%m_%Y_%H%M%S')
save_path_0 = os.path.join(d_save, timestamp)
if not (os.path.isdir(save_path_0)):
    os.mkdir(save_path_0)

###############################Port in data###################################

# prepare arrays for dataframe
fn = []
vel = []
vel_unc = []
temp = []
temp_unc = []
press = []
press_unc = []
shift = []
shift_unc = []
conc = []
conc_unc = []
broad = []
broad_unc = []

# organization of fits
for i in range(iterations):
    print('iteration', i)
    save_path = os.path.join(save_path_0, str(i))
    if not (os.path.isdir(save_path)):
        os.mkdir(save_path)
    save_path = save_path

    if plot_results:
        if i < iterations-1:
            plot_results_temp = False
        else:
            plot_results_temp = True

    fitresults = fithit.FitProgram(fn_sim, save_path, file_tag,
                                   weight_window, weight_flat, etalon_windows,
                                   angle, band_fit,
                                   molec_to_fit, isotopes,
                                   press_fit, temp_fit, vel_fit,
                                   shift_fit, conc_fit, broad_fit,
                                   press_guess, temp_guess, vel_guess,
                                   shift_guess, conc_guess, broad_guess,
                                   conc_box=conc_box, temp_box=temp_box,
                                   pres_box=pres_box, pl_box=pl_box,
                                   background_remove=back_remove,
                                   molec_back=molec_back,
                                   pathlength_b=pl_back, press_back=press_back,
                                   temp_back=temp_back, conc_back=conc_back,
                                   noise_add = noise_add,
                                   noise_level = noise_level,
                                   noise_flat=noise_flat,
                                   save_files=save_files,
                                   plot_results=plot_results_temp,
                                   transUnits=False)
    vel.append(fitresults[0])
    vel_unc.append(fitresults[1])
    temp.append(fitresults[2])
    temp_unc.append(fitresults[3])
    press.append(fitresults[4])
    press_unc.append(fitresults[5])
    shift.append(fitresults[6])
    shift_unc.append(fitresults[7])
    conc.append(fitresults[8])
    conc_unc.append(fitresults[9])
    broad.append(fitresults[10])
    broad_unc.append(fitresults[11])

# Create dict of results
d_fit = {'Vel. (m/s)': vel,
         'Vel. Unc.': vel_unc,
         'Temp. (K)': temp,
         'Temp. Unc.': temp_unc,
         'Pres. (atm)': press,
         'Pres. Unc.': press_unc,
         'Shift (cm-1)': shift,
         'Shift Unc.': shift_unc,
         'Conc.': conc,
         'Conc. Unc': conc_unc,
         'Broad.': broad,
         'Broad. Unc.': broad_unc}

df_fit = pd.DataFrame.from_dict(d_fit)
if save_files:
    pickle.dump(df_fit, open(os.path.join(save_path_0, 'df_fit.p'), 'wb'))
#%% print fit results
if iterations > 1:
    if vel_fit:
        vel = np.abs(vel)
        vel_std = np.std(vel)
        vel_ave = np.mean(vel)
        print('Velocity mean is ' + str(vel_ave))
        print('Velocity uncertainty is ' + str(vel_std))
        np.savetxt(os.path.join(save_path_0, 'vel.txt'), vel)
    if conc_fit:
        conc_std = np.std(conc)
        conc_ave = np.mean(conc)
        print('Concentration mean is ' + str(conc_ave))
        print('Concentration uncertainty is ' + str(conc_std))
        np.savetxt(os.path.join(save_path_0, 'conc.txt'), conc)
    if press_fit:
        press_std = np.std(press)
        press_ave = np.mean(press)
        print('Pressure mean is ' + str(press_ave))
        print('Pressure uncertainty is ' + str(press_std))
        np.savetxt(os.path.join(save_path_0, 'press.txt'), press)
    if temp_fit:
        temp_std = np.std(temp)
        temp_ave = np.mean(temp)
        print('Temperature mean is ' + str(temp_ave))
        print('Temperature uncertainty is ' + str(temp_std))
        np.savetxt(os.path.join(save_path_0, 'temp.txt'), temp)
    if shift_fit:
        shift_std = np.std(shift)
        shift_ave = np.mean(shift)
        print('Shift mean is ' + str(shift_ave))
        print('Shift uncertainty is ' + str(shift_std))
        np.savetxt(os.path.join(save_path_0, 'shift.txt'), shift)
        # if conc_fit or press_fit:
        #     pxl_std = np.std(pxl)
        #     pxl_ave = np.mean(pxl)
        #     print('PXL mean is ' + str(pxl_ave))
        #     print('PXL uncertainty is ' + str(pxl_std))
        #     np.savetxt(os.path.join(save_path_0, 'pxl.txt'), pxl)

print('Fitting Done!')