#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Fitting program for fitting time-resolved retro data from AFRL Mar2022 campaign

Created on Fri Apr  8 10:03:04 2022

@author: david
"""

import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from openpyxl import Workbook, load_workbook
import pandas as pd
import pickle
from datetime import datetime

import FitProgramRetrowBox as fithit
##################################Fitting Parameters###########################

# Fitting Options
save_files = True  # saves fits and fitted parameters in text files.
plot_results = False  # Plots time domain and TD fit converted to the frequency domain. Turn this off for many datafiles.
print_fit_report = True  # Prints each fit report to command window
back_remove = True  # Remove background from spectra before fitting

# Directory Info
d_data = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                      r'Mar2022CampaignData/moose/031122_retro/pc_peter')
d_pc = 'pc_peter'
d_save = r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits/retro'
file_tag = ''  # remember to add a _ before
label_back = ''

indexes = [16, 17, 18]  # 16,1718 (1718 is a combination of 17 and 18)

# MFID Parameters
weight_windows = [[0.0083, 0.5],
                  [0.0083, 0.5],
                  [0.009, 0.5]]  # default [0.005,0.5]
etalon_windows = []
band_fits = [[13016.9, 13169.6],
             [13016.8, 13170.4],
             [13016.6, 13170.1]]
weight_flat = True  # Otherwise will do an exponential weighting
    
# Fitting Parameters
angle = 16.69
molec_to_fit = ['O2']  # must be a list
isotopes = [[1]]  # format [[1],[2]]
press_guesss = [0.9268, 1.191, 1.191] # atm
temp_guess = 300
vel_guess = 500
shift_guess = 0
conc_guess = 0.2095
broad_guess = 1
temp_fit = True
press_fit = False
shift_fit = True
vel_fit = True
conc_fit = False
broad_fit = False

# Background Fitting Parameters
molec_back = 'O2'
conc_back = 0.135
temp_back = 324.4
press_back = 1
pl_back = 38.6

# Box Fitting Parameters
conc_box = 0.0056
temp_box = 277
pres_box = 1
pl_box = 35

if not(back_remove):
    conc_back = 0
    shift_back = 0
    press_back = 0
    
    conc_box = 0

###############################Port in data###################################
# get files from directory
# import filename key to get filenames
d_key = r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS/MarchCampaign'
fn_key = 'FilenameKey.xlsx'
key_df = pd.read_excel(os.path.join(d_key,fn_key))

# get time for labeling fit
now = datetime.now()
timestamp = now.strftime('%d_%m_%Y_%H%M%S')
    
d_save_0 = os.path.join(d_save,timestamp)
if save_files:
    if not(os.path.isdir(d_save_0)):
        os.mkdir(d_save_0)
    
###############################Port in data###################################
# get files from directory
files = os.listdir(d_data)

i = 0
for index in indexes:
    print(index)
    weight_window = weight_windows[i]
    band_fit = band_fits[i]
    press_guess = press_guesss[i]
    i += 1

    if index == '18':
        continue
    # Grab filenames from key
    row = key_df.loc[key_df['Indexx']==index]
    vis3_daq = str(row.iloc[0]['vis3 DAQ'])
    vis3_fn = str(row.iloc[0]['vis3 fn'])
    d_vis3 = os.path.join(d_data, 'time_resolved', vis3_fn)

    n_allan = len(os.listdir(d_vis3))
    print('Fitting ' + str(n_allan) + ' spectra for ' + str(index))

    # prepare arrays for dataframe
    iteration = []
    fn = []
    vel = []
    vel_unc = []
    temp = []
    temp_unc = []
    press = []
    press_unc = []
    shift = []
    shift_unc = []
    conc = []
    conc_unc = []
    broad = []
    broad_unc = []

    # directory to save results
    d_save_1 = os.path.join(d_save_0, str(index))
    if save_files:
        if not(os.path.isdir(d_save_1)):
            os.mkdir(d_save_1)
    else:
        d_save_1 = ''
        
    for j in range(n_allan):
        print('Fitting ' + str(j+1) + ' out of ' + str(n_allan) + ' spectra')
        # perform fits
        # find vis 2 data
        vis2_found = False

        fn_retro = vis3_fn + '_trans' + str(int(j)) + '.txt'
        fn_retro = os.path.join(d_vis3, fn_retro)

        # make sure fn_y exists
        if not os.path.isfile(fn_retro):
            print('retro file not found!')
            continue

        fn.append(fn_retro)

        fitresults = fithit.FitProgram(fn_retro, d_save_1, file_tag,
                                  weight_window, weight_flat, etalon_windows,
                                  angle, band_fit,
                                  molec_to_fit, isotopes,
                                  press_fit, temp_fit, vel_fit,
                                  shift_fit, conc_fit, broad_fit,
                                  press_guess, temp_guess, vel_guess,
                                  shift_guess, conc_guess, broad_guess,
                                  conc_box=conc_box, temp_box=temp_box,
                                  pres_box=pres_box, pl_box=pl_box,
                                  background_remove=back_remove,
                                  molec_back=molec_back,
                                  pathlength_b=pl_back, press_back=press_back,
                                  temp_back=temp_back, conc_back=conc_back,
                                  save_files=False,
                                  plot_results=plot_results,
                                  transUnits=True)
        iteration.append(j)
        vel.append(fitresults[0])
        vel_unc.append(fitresults[1])
        temp.append(fitresults[2])
        temp_unc.append(fitresults[3])
        press.append(fitresults[4])
        press_unc.append(fitresults[5])
        shift.append(fitresults[6])
        shift_unc.append(fitresults[7])
        conc.append(fitresults[8])
        conc_unc.append(fitresults[9])
        broad.append(fitresults[10])
        broad_unc.append(fitresults[11])

    # Create dict of results
    d_fit = {'Iteration': iteration,
             'Fn': fn,
             'Vel. (m/s)': vel,
             'Vel. Unc.': vel_unc,
             'Temp. (K)': temp,
             'Temp. Unc.': temp_unc,
             'Pres. (atm)': press,
             'Pres. Unc.': press_unc,
             'Shift (cm-1)': shift,
             'Shift Unc.': shift_unc,
             'Conc.': conc,
             'Conc. Unc': conc_unc}

    df_fit = pd.DataFrame.from_dict(d_fit)
    if save_files:
        pickle.dump(df_fit, open(os.path.join(d_save_0,
                                              str(index)+'_df_fit.p'),'wb'))

print('Fitting Done!')