#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Comparison of smoothing methods

Created on Fri Apr 29 11:31:53 2022

@author: david
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from statsmodels.nonparametric.kernel_regression import KernelReg
import time

from packfind import find_package
find_package('pldspectrapy')
import td_support as td

#%% load data in
## directory info to find transmission file
d = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                 'Mar2022CampaignData/nist/031122_vis2/pc_conjugate')
f = '6_trans.txt'

data = np.loadtxt(os.path.join(d,f))
wvn = data[:,0]
tra = data[:,1]

#%% cut data
bd = [13028.5, 13169.5]
# optimize band for FFT
start, stop = td.bandwidth_select_td(wvn, bd)
# cut down to designated band
wvn_temp = np.array(wvn[start:stop])
tra_temp = np.array(tra[start:stop])
# transform to absorbance
abs_temp = -np.log(tra_temp)
# transform to cepstrum
cep_temp = np.fft.irfft(abs_temp)
# smooth cepstrum, use abs here as baseline can be negative or positive due
# to etalon

#%% smooth params
smooth_len = 49 # how many points for savgol to use, must be odd
smooth_deg = 4

#%% savgol smooth
tic = time.time()
cep_smooth_sav = abs(savgol_filter(cep_temp,smooth_len,smooth_deg)) # use 30 to try to
                                                # smooth over random bump
toc = time.time()                             
print('Savgol calc. time: '+str(toc-tic)+' s')

#%% convolve smooth
tic = time.time()
kernal = np.ones(smooth_len)/smooth_len
cep_smooth_con = abs(np.convolve(cep_temp, kernal, mode='same'))
toc = time.time()                             
print('Convolve calc. time: '+str(toc-tic)+' s')

#%% Kernel Reg smooth
tic = time.time()
t = np.arange(len(cep_temp))
cep_smooth_ker = abs(KernelReg(cep_temp, t, 'c'))
toc = time.time()                             
print('Kernel Reg calc. time: '+str(toc-tic)+' s')

#%% Plot comparison
plt.figure()
plt.plot(cep_temp, label='Original')
plt.plot(cemp_smooth_sav, label='Savgol')
plt.plot(cemp_smooth_con, label='Convolve')
plt.plot(cemp_smooth_ker, label='Kernel Reg')
plt.title('Smooth Comparison')
