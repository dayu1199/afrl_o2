#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 22:42:02 2022

@author: david
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
from pldspectrapy.constants import *
import td_support as td
from matplotlib.patches import Rectangle

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

hapi.db_begin('') # initialize hapi database

savedata=True

y_H2O = 0.021
y_O2 = 0.21 # mole fraction O2
P = 1 # pressure (atm)
T = 600 # Temperature (K)
V = 0 # velocity, m/s
theta = np.deg2rad(90) # degree of beam, rad
windowstart = 13000 # bandwidth start (cm-1)
windowend = 13150 # bandwidth end (cm-1)
dv = 0.00667 # frequency axis step size (cm-1)
L = 8 # path length of measurement (cm)

# adjust frequency axis to account for velocity doppler shift
wvnumspace = np.arange(windowstart,windowend,dv) # create array of frequency axis
start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,windowend]) # optimal start stop of bandwidth for fourier transfrom
wvnumspace = wvnumspace[start:stop]
nsteps = len(wvnumspace)-1
slope_doppler_d = V*np.sin(-theta)/SPEED_OF_LIGHT+1 # doppler scaling due to velocity
new_wvw_num_d = wvnumspace*slope_doppler_d # apply to frequency axis
windowend_d = np.amax(new_wvw_num_d) # new bandwidth end
windowstart_d = np.amin(new_wvw_num_d) # new bandiwdth start
dv_d = (new_wvw_num_d[-1]-new_wvw_num_d[0])/nsteps # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend_d = windowend_d.item()
windowstart_d = windowstart_d.item()
dv_d = dv_d.item()

# create absorbance model using hapi
[nu_O2,coefs_O2] = hapi.absorptionCoefficient_Voigt([(7,1,y_O2)],
            ('O2'), OmegaStep=dv_d,OmegaRange=[windowstart_d, windowend_d],
            HITRAN_units=False, Environment={'p':P,'T':T}, 
            Diluent={'self':y_O2,'air':(1-y_O2)},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2 = coefs_O2*L # get asorption (absorbance * pathlength)
lambda_O2 = 1e7/nu_O2 # transform to wavelength units

windowstart = 6900
windowend = 7188

# adjust frequency axis to account for velocity doppler shift
wvnumspace = np.arange(windowstart,windowend,dv) # create array of frequency axis
start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,windowend]) # optimal start stop of bandwidth for fourier transfrom
wvnumspace = wvnumspace[start:stop]
nsteps = len(wvnumspace)-1
slope_doppler_d = V*np.sin(-theta)/SPEED_OF_LIGHT+1 # doppler scaling due to velocity
new_wvw_num_d = wvnumspace*slope_doppler_d # apply to frequency axis
windowend_d = np.amax(new_wvw_num_d) # new bandwidth end
windowstart_d = np.amin(new_wvw_num_d) # new bandiwdth start
dv_d = (new_wvw_num_d[-1]-new_wvw_num_d[0])/nsteps # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend_d = windowend_d.item()
windowstart_d = windowstart_d.item()
dv_d = dv_d.item()

# create absorbance model using hapi
[nu_H2O,coefs_H2O] = hapi.absorptionCoefficient_Voigt([(1,1,y_H2O)],
            ('H2O_PaulLF2'), OmegaStep=dv_d,OmegaRange=[windowstart_d, windowend_d],
            HITRAN_units=False, Environment={'p':P,'T':T}, 
            Diluent={'self':y_H2O,'air':(1-y_H2O)},
            IntensityThreshold =0)#*1.5095245023408206
abs_H2O = coefs_H2O*L # get asorption (absorbance * pathlength)
lambda_H2O = 1e7/nu_H2O # transform to wavelength units


# plot absorption model
fig, ax1 = plt.subplots(figsize=(10, 4.5))
ax1.plot(nu_H2O, abs_H2O, color='blue', linewidth=1, label='$\mathregular{H_{2}O}$')  #plot absorption
ax1.plot(nu_O2, abs_O2, color='red', linewidth=1, label='$\mathregular{O_{2}}$')
ax1.set_xlabel('$\mathregular{H_{2}O}$ Wavenumber ($\mathregular{cm^{-1}}$)', color='blue')
ax1.tick_params(axis='x', colors='blue')
ax1.set_ylabel('Absorbance')
ax1.set_ylim([0, 0.1])
ax1.set_xlim([min(nu_H2O), max(nu_H2O)])
ax1.legend(frameon=False, fontsize='large')


ax2 = ax1.twiny()
ax2.plot(nu_O2, abs_O2, color='red', linewidth=1, zorder=1)  #plot absorption
ax2.set_xlabel('$\mathregular{O_{2}}$ Wavenumber ($\mathregular{cm^{-1}}$)', color='red')
ax2.tick_params(axis='x', colors='red')
ax2.set_xlim([min(nu_O2), max(nu_O2)])
ax1.xaxis.tick_top()
ax1.xaxis.set_label_position('top')
ax2.xaxis.tick_bottom()
ax2.xaxis.set_label_position('bottom')

#%% add zoom in rectangular plot
zoom_start = 13083
zoom_end = 13096
zoom_start_rel = (zoom_start - min(nu_O2))/(max(nu_O2) - min(nu_O2))
zoom_end_rel = (zoom_end - min(nu_O2))/(max(nu_O2) - min(nu_O2))
zoom_abs_height = 0.0025

ax2.add_patch(Rectangle((zoom_start, 0.000),
                        zoom_end - zoom_start, zoom_abs_height,
                        edgecolor='green',
                        fill=False,
                        lw=3,
                        zorder=2))

#%% add inset plot
ax2.add_patch(Rectangle((13002, 0.03),
                        59, 0.068,
                        edgecolor='green',
                        facecolor='white',
                        lw=1.5,
                        zorder=2))
ax3 = fig.add_axes([0.17, 0.45, 0.27, 0.3])
ax3.plot(nu_H2O, abs_H2O, color='blue', linewidth=1)  #plot absorption
ax3.set_ylim([0, zoom_abs_height])
ax3.set_xlim([zoom_start_rel*(max(nu_H2O)-min(nu_H2O))+min(nu_H2O),
              zoom_end_rel*(max(nu_H2O)-min(nu_H2O))+min(nu_H2O)])
ax3.tick_params(axis='x', colors='blue')

ax4 = ax3.twiny()
ax4.plot(nu_O2, abs_O2, color='red', linewidth=1, zorder=1)  #plot absorption
ax4.set_xlim([zoom_start, zoom_end])
ax4.tick_params(axis='x', colors='red')

ax3.xaxis.tick_top()
ax3.xaxis.set_label_position('top')
ax4.xaxis.tick_bottom()
ax4.xaxis.set_label_position('bottom')
# fig = plt.figure()
# ax1 = fig.add_subplot(111)
# ax1.plot(nu_O2, abs_O2, color='red')
# ax1.set_xlabel('O2 Wavenumber ($cm^{-1}$)')
# ax1.set_ylabel('Absorbance')
#
# ax2 = ax1.twiny()
# ax2.plot(nu_H2O,abs_H2O,color='blue') #plot absorption

if savedata:
    d_save = os.path.join(r'/Users/david/Library/CloudStorage/OneDrive-UCB-O365',
                          'Papers/2022_O2Ramjet/Figures/H2O_O2_comparison')
    fn_save = input('Filename for saving plot:')
    plt.savefig(os.path.join(d_save, fn_save), dpi=600)