"""
PlotRetrovsCrossed.py

Created by on 8/25/22 by david

Description:

"""
import numpy as np
import matplotlib.pyplot as plt

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

retro_data = np.loadtxt('O2simulationRetro_AJ.txt')
nu_retro = retro_data[:,0]
abs_retro = retro_data[:,1]

noise_retro = np.random.normal(0, 0.00044, len(abs_retro))
abs_retro += noise_retro

upstream_data = np.loadtxt('O2simulation_AJ_up.txt')
nu_up = upstream_data[:,0]
abs_up = upstream_data[:,1]

downstream_data = np.loadtxt('O2simulation_AJ_down.txt')
nu_down = downstream_data[:,0]
abs_down = downstream_data[:,1]

noise_crossed = np.random.normal(0, 0.00044, len(abs_down))
abs_down += noise_crossed
abs_up += noise_crossed

plt.figure()
plt.plot(nu_retro, abs_retro, label='Retroreflector')
plt.plot(nu_up, abs_up, label='Upstream Facing Crossed')
plt.plot(nu_down, abs_down, label='Downstream Facing Crossed')
plt.xlabel('Wavenumber ($\mathregular{cm^{-1}}$)')
plt.ylabel('Absorbance')
plt.legend()
