#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code to combine phasecorrect file 17 and 18 together

Created on Tue Mar 22 15:28:15 2022

@author: david
"""
# Package imports
import os
import time
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from packfind import find_package
find_package("pldspectrapy")
import pldspectrapy as pld
find_package("pldspectrapy1")
import pldspectrapy1 as pld1
import td_support as td

# Plot params
plt.rcParams.update({'font.family': 'Times New Roman',
                     "figure.autolayout": True,
                     "lines.linewidth": 0.8})

# Code switches and params
pclim_low = 0.12
pclim_high = 0.18
plotdata = True

# directory info
d = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                 'Mar2022CampaignData/cubig', '031122')
fname = {'11', '11_2'}  # leave off .txt
fn_save = '11combined_trans.txt'
save_folder_tag = 'pc_peter'

# Frequency axis calculation
lockfreq = 32e6
nomfreq = 13333  # Should be within the range specified by band_fit (below)

# open up directory
t0 = time.time()
files = pld.open_dir(d, recursion_levels=2, verbose=2) #######
dt = time.time() - t0
print("Opened %i files in %0.3f seconds" % (len(files), dt))
newest = ""

# create save directory
d_save = os.path.join(d, save_folder_tag)
if not os.path.isdir(d_save):
    os.mkdir(d_save)

#%% pull data
# array to hold combined data
data_hold = []
# Loop through directory and process data
for name, daq_file in files.items():
    
    # Create a couple variables used for determining processing flow
    pass_raw = 0

    if name not in fname:
        continue

    IG = pld1.open_daq_files(os.path.join(d, name))

    # Are there raw IGMs?
    if IG.data_raw is not None:
        non_empty_raw = len(IG.data_raw)
        pass_raw = 1 if (non_empty_raw > 0) else 0

    # As long as there's some IGMs go ahead and start processing!
    if pass_raw:
        newest = max(newest, name)
        print(name)

        # concatenate data
        data_hold.append(IG.data_raw.copy())
    else:
        print('No data in ' + name)

#%% combine data        
# concatenate the two datasets
# igtools opens 11_2 before 11
data = np.concatenate((data_hold[1], data_hold[0]))

#%% phase correct
trans = pld1.pc_peter(data, pclim_low, pclim_high, plot=True,
                     zoom=500)
pc_ig = np.fft.ifft(trans)

#%% create wavenumber axis
data_trans = np.loadtxt(os.path.join(r'/Volumes/GoogleDrive/My Drive',
                                     'AFRL ETHOS (DO NOT SYNC)',
                                     r'Mar2022CampaignData/moose/031122_vis3',
                                     r'pc_peter', fn_save))
x_wvn_full = data_trans[:, 0]

#%% plot data
lambda_full = 1e7/x_wvn_full 
if plotdata:
    fig,ax = plt.subplots(2,1)
    ax[0].plot(pc_ig)
    ax[0].set_xlabel('Time step')
    ax[0].set_ylabel('Power')
    ax[1].plot(lambda_full[:len(trans)],trans)
    ax[1].set_xlabel('Wavelength (cm-1)')
    ax[1].set_ylabel('Transmission')
    plt.suptitle(fn_save)
# save file
save_data = np.vstack((x_wvn_full[:len(trans)], trans))
save_data = np.transpose(save_data)

np.savetxt(os.path.join(d_save, fn_save), save_data)