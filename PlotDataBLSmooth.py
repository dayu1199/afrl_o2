#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Visualization of no baseline data using Sean's method of smoothing

Created on Fri Apr 22 15:51:48 2022

@author: david
"""

import numpy as np
import os
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from scipy.ndimage.filters import uniform_filter1d

fn_data = '6'
d = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                 r'Mar2022CampaignData/Fits/crossed/27_04_2022_161043',
                 fn_data)
# baseline free
for fn in os.listdir(d):
    if '_abs' in fn:
        data = np.loadtxt(os.path.join(d,fn))
        x_wvn = data[:,0]
        dcs = data[:,1]
        dcs_nobl = data[:,2]
    if '_fit' in fn:
        data = np.loadtxt(os.path.join(d,fn))
        model = data[:,1]
    if '_weight' in fn:
        data = np.loadtxt(os.path.join(d,fn))
        weight = data[:,1]

# smooth out the residual
# res = dcs - model
# res = savgol_filter(res,69,2)
# res = uniform_filter1d(res,71)
# binomcoefs_2 = (np.poly1d([0.5, 0.5])**2).coeffs
# res = lfilter(binomcoefs_2, 1, res)        
# data_noblsmooth = dcs - res
# bl = savgol_filter(dcs,69,2)
# data_noblsmooth = dcs - bl


# plt.figure()
# plt.plot(x_wvn,data_noblsmooth,label='Data')
# plt.plot(x_wvn,model,label='Model')
# plt.xlabel('Wavenumber (cm-1)')
# plt.ylabel('Absorbance')
# plt.title(fn)

# plt.figure()
# plt.plot(x_wvn,dcs)
# plt.plot(x_wvn,bl)
# plt.xlabel('Wavenumber (cm-1)')
# plt.ylabel('Absorbance')
# plt.title(fn)

# # plot added in model
# model_add = np.fft.rfft((1-weight)*np.fft.irfft(model))
# model_in = np.fft.rfft((weight)*np.fft.irfft(model))
# plt.figure()
# plt.plot(abs(model_add),label='Added Model')
# plt.plot(abs(model_in),label='Fitted Model')
# plt.legend()

# Look at just the weighted portion
dcs_weight = np.fft.rfft(weight*np.fft.irfft(dcs))
model_weight = np.fft.rfft(weight*np.fft.irfft(model))

plt.figure()
plt.plot(x_wvn,dcs_weight,label='Data')
plt.plot(x_wvn,model_weight,label='Model')
plt.xlabel('Wavenumber (cm-1)')
plt.ylabel('Absorbance')
