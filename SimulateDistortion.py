#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Some of the data has some of this weird distortion shit going on
I'm going to try to manually to add this distortion and fit to see if it
affects fits

To get the distortion, I'm going to smooth a real cepstrum residual
Hopefully residual will have negligible model and by smoothing I can get the
noise out

Created on Fri Apr 29 11:52:56 2022

@author: david
"""
#%% import modules
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter


from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
from pldspectrapy.constants import *
import td_support as td


#%% Spectra Parameters
y_O2 = 0.201 # mole fraction O2
P = 1.24 # pressure (atm)
T = 457.5 # Temperature (K)
V = 723.6 # velocity, m/s
theta = np.deg2rad(35) # degree of beam, rad
dv = 0.00667 # frequency axis step size (cm-1)
L = 6.8/np.cos(theta) # path length of measurement (cm)

hapi.db_begin('') # initialize hapi database

#%% Get distortion from existing fit
fn_data = '6'
d = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                 r'Mar2022CampaignData/Fits/crossed/29_04_2022_120155',
                 fn_data)
for fn in os.listdir(d):
    if '_abs' in fn:
        data = np.loadtxt(os.path.join(d,fn))
        wvnumspace = data[:,0]
    if '_cep' in fn:
        data = np.loadtxt(os.path.join(d,fn))
        dcs = data[:,0]
        fit = data[:,1]
    if '_weight' in fn:
        data = np.loadtxt(os.path.join(d,fn))
        weight = data[:,1]
res = dcs - fit
res_smooth = savgol_filter(res,31,4)
res_smooth *= weight

#%% Find noise level
noise = res_smooth - res
noise_level = np.std(noise[2000:3000])
print('Noise Level: '+str(noise_level))

#%% plot res_smooth
plt.figure()
plt.plot(res,label='Original')
plt.plot(res_smooth,label='Smoothed')
plt.title('Residual')
plt.legend()

#%% Simulate upstream facing beam
## Upstream
# adjust frequency axis to account for velocity doppler shift
nsteps = len(wvnumspace)-1
slope_doppler_u = V*np.sin(-theta)/SPEED_OF_LIGHT+1 # doppler scaling due to velocity
new_wvw_num_u = wvnumspace*slope_doppler_u # apply to frequency axis
windowend_u = np.amax(new_wvw_num_u) # new bandwidth end
windowstart_u = np.amin(new_wvw_num_u) # new bandiwdth start
dv_u = (new_wvw_num_u[-1]-new_wvw_num_u[0])/nsteps # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend_u = windowend_u.item()
windowstart_u = windowstart_u.item()
dv_u = dv_u.item()

MIDS = [(7,1,y_O2*hapi.abundance(7,1))]
# create absorbance model using hapi
[nu_u,coefs_u] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv_u,OmegaRange=[windowstart_u, windowend_u],
            HITRAN_units=False, Environment={'p':P,'T':T}, 
            Diluent={'self':y_O2,'air':(1-y_O2)},
            IntensityThreshold =0)#*1.5095245023408206
abs_u = coefs_u*L # get asorption (absorbance * pathlength)
cep_u = np.fft.irfft(abs_u)

#%% Simulate downstream facing beam
# adjust frequency axis to account for velocity doppler shift
nsteps = len(wvnumspace)-1
slope_doppler_d = V*np.sin(theta)/SPEED_OF_LIGHT+1 # doppler scaling due to velocity
new_wvw_num_d = wvnumspace*slope_doppler_d # apply to frequency axis
windowend_d = np.amax(new_wvw_num_d) # new bandwidth end
windowstart_d = np.amin(new_wvw_num_d) # new bandiwdth start
dv_d = (new_wvw_num_d[-1]-new_wvw_num_d[0])/nsteps # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend_d = windowend_d.item()
windowstart_d = windowstart_d.item()
dv_d = dv_d.item()

# create absorbance model using hapi
[nu_d,coefs_d] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv_d,OmegaRange=[windowstart_d, windowend_d],
            HITRAN_units=False, Environment={'p':P,'T':T}, 
            Diluent={'self':y_O2,'air':(1-y_O2)},
            IntensityThreshold =0)#*1.5095245023408206
abs_d = coefs_d*L # get asorption (absorbance * pathlength)
cep_d = np.fft.irfft(abs_d)

#%% add distortion to model
cep_u_dis = cep_u + res_smooth
cep_d_dis = cep_d + res_smooth

abs_u_dis = np.fft.rfft(cep_u_dis)
abs_d_dis = np.fft.rfft(cep_d_dis)

#%% plot upstream facing
plt.figure()
plt.plot(wvnumspace, np.imag(abs_u_dis), label='w/ Distortion')
plt.plot(wvnumspace, abs_u, label='Original')
plt.ylabel('Absorbance')
plt.xlabel('Wavenumber (cm-1)')
plt.title('Upstream Facing')
plt.legend()

plt.figure()
plt.plot(cep_u_dis, label='w/ Distortion')
plt.plot(cep_u, label='Original')
plt.ylabel('Absorbance')
plt.xlabel('Wavenumber (cm-1)')
plt.title('Upstream Facing')
plt.legend()


#%% plot downstream facing
plt.figure()
plt.plot(wvnumspace, abs_d_dis, label='w/ Distortion')
plt.plot(wvnumspace, abs_d, label='Original')
plt.ylabel('Absorbance')
plt.xlabel('Wavenumber (cm-1)')
plt.title('Downstream Facing')
plt.legend()

plt.figure()
plt.plot(cep_d_dis, label='w/ Distortion')
plt.plot(cep_d, label='Original')
plt.ylabel('Absorbance')
plt.xlabel('Wavenumber (cm-1)')
plt.title('Upstream Facing')
plt.legend()

#%% Save upstream data
data_u = np.vstack((wvnumspace,np.real(abs_u_dis)))
data_u = np.transpose(data_u)
np.savetxt('O2simulation_dist_up.txt', data_u)

#%% Save downstream data
data_d = np.vstack((wvnumspace,np.real(abs_d_dis)))
data_d = np.transpose(data_d)
np.savetxt('O2simulation_dist_down.txt', data_d)