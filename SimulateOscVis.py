#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Oscillation Visualization

Created on Tue Apr 26 15:08:19 2022

@author: david
"""

import matplotlib.pyplot as plt
import numpy as np

n_osc = 5000

a = np.zeros(25000)

for i in np.arange(int(len(a))):
    # only add oscillatory behavior at n_osc from ends
    stretch = 0.01
    if i<=n_osc:
        if np.mod(i,2)==0:
            a[i] = 1*np.exp(-i*stretch)
        else:
            a[i] = -1*np.exp(-i*stretch)
    elif i>(len(a)-n_osc):
        if np.mod(i,2)==0:
            a[i] = 1*np.exp((-len(a)+i)*stretch)
        else:
            a[i] = -1*np.exp((-len(a)+i)*stretch)
    

b = np.fft.rfft(a)

plt.figure()
plt.plot(a)

plt.figure()
plt.plot(abs(b))