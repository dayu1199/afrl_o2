# -*- coding: utf-8 -*-
"""
Created on Friday March 12, 2021

@author: CoburnS

Purpose: Creating a generic type processing file that will eventually be turned
into a standard option.

This will be built from loop_fitting_test.py and fitting_test_single.py
    - The core processing code will be from fitting_test_single.py because that
        was used to tune the fitting parameters.
    - The outer loop for reading through the data repository will come from
        loop_fitting_test.py since that was been verified with field data
        collected at PAO.

This code assumes that the appropriate HAPI database (local) is already available
    - Needs to be configured for molecules being fits

"""

#   Package imports

import os
import time
from copy import copy
import numpy as np
import matplotlib.pyplot as plt
from lmfit import Model

from packfind import find_package

find_package("pldspectrapy")
import pldspectrapy as pld
import pldspectrapy.pldhapi as hapi
import td_support as td
from OptimizeBL import OptimizeBL

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()
#   End package imports

#   Configurable variables -> eventually want to pull all of these out
#   and create a configuration file that sets these parameters

def Voigt(nu, nu_0, nu_d, nu_c):
    # ------------------------------------------------------------------------
    #     McLean approximation of Voigt profile to improve computation time
    #
    #     McLean, A. B., C. E. J. Mitchell, and D. M. Swanston.
    #       "Implementation of an efficient analytical approximation to the
    #       Voigt function for photoemission lineshape analysis." Journal of
    #       Electron Spectroscopy and Related Phenomena 69.2 (1994): 125-132.
    # ------------------------------------------------------------------------

    # Parameters for approximating Voigt profile (from McLean)
    C1 = (-1.2150, 1.2359, -0.3085, 0.0210)
    C2 = (-1.3509, 0.3786, 0.5906, -1.1858)
    C3 = (-1.2150, -1.2359, -0.3085, -0.0210)
    C4 = (-1.3509, -0.3786, 0.5906, 1.1858)

    a = np.sqrt(np.log(2)) * nu_c / nu_d  # Calculate Voigt 'a' parameter
    Phi_D_vo = (2 / nu_d) * np.sqrt(np.log(2) / np.pi)  # Calculate doppler line shape at vo
    w = 2 * np.sqrt(np.log(2)) * (nu - nu_0) / nu_d  # Voigt 'w' parameter

    Voigt1 = (C1[2] * (a - C1[0]) + C1[3] * (w - C1[1])) / ((a - C1[0]) ** 2 + (w - C1[1]) ** 2)
    Voigt2 = (C2[2] * (a - C2[0]) + C2[3] * (w - C2[1])) / ((a - C2[0]) ** 2 + (w - C2[1]) ** 2)
    Voigt3 = (C3[2] * (a - C3[0]) + C3[3] * (w - C3[1])) / ((a - C3[0]) ** 2 + (w - C3[1]) ** 2)
    Voigt4 = (C4[2] * (a - C4[0]) + C4[3] * (w - C4[1])) / ((a - C4[0]) ** 2 + (w - C4[1]) ** 2)

    Voigt = Voigt1 + Voigt2 + Voigt3 + Voigt4

    Phi_V = Phi_D_vo * Voigt

    return Phi_V


def spectra_K_lmfit(prefix=""):
    """
    Set up lmfit model with function hints for single absorption species
    """
    mod = Model(spectra_K, prefix=prefix)

    mod.set_param_hint("pressure", min=0, max=1)
    mod.set_param_hint("temperature", min=3, max=3500)
    mod.set_param_hint("pathlength", min=0)
    mod.set_param_hint("molefraction", min=0, max=1)
    mod.set_param_hint("shift", min=-0.2, max=0.2)
    mod.set_param_hint("broadening", min=0.1, max=100)
    pars = mod.make_params()
    # let's set up some default thermodynamics
    pars[prefix + "pressure"].value = 0.00131579*10e-7
    pars[prefix + "temperature"].value = 296
    pars[prefix + "pathlength"].value = 7.2 - 0.16*2
    pars[prefix + "molefraction"].value = 1
    pars[prefix + "shift"].value = 0
    pars[prefix + "broadening"].value = 1

    return mod, pars

def spectra_K(
    xx,
    molefraction,
    pressure,
    temperature,
    pathlength,
    shift,
    broadening,
    flip_spectrum=False,
):
    """
    Spectrum calculation for adding multiple models with composite model.

    See lmfit model page on prefix, parameter hints, composite models.

    INPUTS:
        xx -> wavenumber array (cm-1)
        molefraction
        pressure -> (atmospheres)
        temperature -> kelvin
        pathlength (centimeters)
        shift -> (cm-1) calculation relative to Hitran
        flip_spectrum -> set to True if Nyquist window is 0.5-1.0

    """
    # calculate lineshape
    nu_0 = 13042.89605  # cm-1
    M = 39.0983  # g/mol
    nu_d = nu_0 * 7.1623e-7 * (temperature/M)**(1/2)*broadening # doppler broadening
    nu_c = 2.012e-4  # probably negligible compared to doppler broadening
    # coef = Voigt(xx+shift, nu_0, nu_d, nu_c)
    coef = hapi.PROFILE_VOIGT(nu_0, nu_d / 2, nu_c / 2, xx+shift)[0]  # uses HWHM
    # strength
    A21 = 3.779e7  # A_ki
    g21 = 2
    h = 6.62607015e-34  # m2kgs-1
    k = 1.380649e-23  # m2kgs-2K-1
    f12 = 6.661e-01
    c = 299792458  # m/s
    strength = pressure*A21*g21/(100**3*c*8*np.pi*nu_0**2*k*temperature*pressure*molefraction)
    # scale by partial pressure and pathlength
    coef *= pressure*pathlength*molefraction*strength

    # time domain
    if flip_spectrum:
        absorp = np.fft.irfft(coef[::-1])
    else:
        absorp = np.fft.irfft(coef)
    return absorp


## fitting function
def FitFunc(x_wvn, y_td, bl, etalons, mod, db_name1,
            savedata=True, savename='', savename_td='', savename_freq='',
            plot_fit_td=True, plot_fit_freq=True):
    # Create the weighting function for time domain fitting
    weight = td.weight_func(len(x_wvn), bl, etalons)
    Fit = modfull.fit(
        y_td, xx=x_wvn, params=pars, weights=weight, name=db_name1
    )
    # This section sets up data for plotting so may not be needed

    # Pull time domain fit data
    y_datai = Fit.data
    fit_datai = Fit.best_fit
    weight = Fit.weights

    # Pull frequency domain fit data
    data_lessbl = np.real(
        np.fft.rfft(y_datai - (1 - weight) * (y_datai - fit_datai))
    )
    model = np.real(np.fft.rfft(fit_datai))
    residual = data_lessbl - model
                                 
    print(Fit.fit_report())
    
    print('Noise level is '+str(np.std(residual[1942:2391])))
    print('P2P is '+str(p2p[-1]))


    if plot_fit_td:
        # And plot time domain
        plt.figure()
        plt.plot(y_datai, label='Data')
        plt.plot(fit_datai, label= 'Model')
        plt.plot(y_datai - fit_datai, label='Residual')
        plt.plot(weight)
        plt.legend()
        plt.title(str(i)+' '+fname)
        plt.show()
    if plot_fit_freq:
        # And plot frequency domain
        fig, axs = plt.subplots(2, 1, sharex="col")
        axs[0].plot(x_wvn, data_lessbl, x_wvn, model)
        axs[1].plot(x_wvn, residual)
        axs[0].set_title(str(i))
        plt.figure()
        plt.plot(1/x_wvn_full[:len(trans_orig)]*1e7, trans_orig)
        plt.title(str(i))
        plt.figure()
        plt.plot(x_wvn, trans)
        plt.title(str(i)+' '+fname)
        plt.show()
        # axs[0].set_ylabel('Absorbance'); #axs[0].legend(['data','fit'])
        # axs[1].set_ylabel('Residual'); axs[1].set_xlabel('Wavenumber ($cm^{-1}$)')

    # End the plotting section

    # Section for saving data -> will probably be replaced

    if savedata:
        if not os.path.isdir(savepath):
            os.mkdir(savepath)
        # Create an output string with file information
        outinfo = (
            "====================================================" + "\n"
        )
        outinfo += "[[Data Info]]\n"
        outinfo += "\tData file\t\t\t = " + fname + "\n"
        if not(importdata):
            outinfo += "\tTotal P2P\t\t\t = " + str(daq_file.p2p_total) + "\n"
        outinfo += "\tFit region\t\t\t = " + str(band_fit) + "\n"
        outinfo += "\tBaseline\t\t\t = " + str(bl) + "\n"
        outinfo += "\tEtalons\t\t\t\t = " + str(etalons) + "\n"

        # DY - In order to save fit results to a textfile I use the following lines:
        f = open(os.path.join(savepath, savename), "a")
        if fitallan:
            f.write('pc '+str(int(i))+'\n')
        f.write(outinfo)
        f.write(Fit.fit_report() + "\n")
        # f.write('===================================================='+'\n')
        f.close()

        # Save the time domain fit arrays
        fit_outdata_td = np.array([y_datai, fit_datai, weight])
        fit_outdata_td = fit_outdata_td.T
        if fitallan:
            np.savetxt(
                os.path.join(savepath, savename_td + "_" + fname + "_" + str (int(i)) + ".txt"),
                fit_outdata_td,
                delimiter="\t",
            )
        else:
            np.savetxt(
                os.path.join(savepath, savename_td + "_" + fname + ".txt"),
                fit_outdata_td,
                delimiter="\t",
            )

        # Save the frequency domain fit arrays
        fit_outdata_freq = np.array([data_lessbl, model, x_wvn])
        fit_outdata_freq = fit_outdata_freq.T
        if not(fitallan):
            np.savetxt(
                os.path.join(savepath, savename_freq + "_" + fname + ".txt"),
                fit_outdata_freq,
                delimiter="\t",
            )
        else:
            np.savetxt(
                os.path.join(savepath, savename_freq + "_" + fname + "_" + str (int(i)) + ".txt"),
                fit_outdata_freq,
                delimiter="\t",
            )

        f = open(os.path.join(savepath, savename), "a")
        f.write(
            "====================================================" + "\n"
        )
        f.close()
        
    return Fit

if __name__ == "__main__":

    # Data repository
    # d = r'C:\Users\CoburnS\Downloads\20201125_WindClineExperiments\_processing\scan1'
    # d = r"C:\Users\CoburnS\Downloads\20201203_WindClineExperiments\scan3"
    d = os.path.join(r'/Volumes/AFRL1Beam2/031723/kcell')
    fname = '20230317094708'  # leave off .txt I think
    importdata = False
    importwvn = False
    d_import = r'/Volumes/AFRL1Beam2/031723/kcell/fit'
    fname_import = '20230317094708'
    processone = True
    fitdata = True
    optimizebl = False

    # Save info: flag, file name, directory
    savedata = False  # flag (0 = no save; 1 = save)
    process_raw = 1
    process_pc = 0
    # fit individual igs for allan deviation calculation later
    fitallan = False  # will work with only raw data
    pc_time = 60  # how much time (s) to fit for fit allan
    ig_num = 1  # how many averaged igs to process, if 0 or greater than available igs just do all

    savepath = os.path.join(d,'fit')
    savename = "fitresults.txt"
    savename_td = "fitdata_td"
    savename_freq = "fitdata_freq"
    if savedata and not os.path.exists(savepath):
        os.mkdir(savepath)
    # Plotting
    plot_fit_td = 1  # flag (0 = no plot; 1 = plot)
    plot_fit_freq = 1  # flag (0 = no plot; 1 = plot)

    # Repository for the HAPI database
    # listdir = os.path.join(curdir , 'linelists')
    linedir = ''

    # Frequency axis calculation
    lockfreq = -32e6
    nomfreq = 13333  # Should be within the range specified by band_fit (below)

    # Band range for fit
    # band_fit = [6880, 7260]  #good for HITRAN
    band_fit = [13010.2, 13182.9]  # for Paul's database

    # Create the baseline, etalon, and weight information
    bl = 500
    # Create a notch to catch etalons
    etalons = [[380,395]]  # narrow range (Paul's range)

    # HAPI/fitting parameters - will need this section for each molecule
    if fitdata:
        db_name1 = "O2"
        mod, pars = td.spectra_single_lmfit("o2")
        pars["o2mol_id"].value = 7
        pars["o2molefraction"].set(value=0.209, vary=False)  # (molefraction)
        # pars['h2opressure'].set(value = (634.7/760), vary = False)  # (atm)
        pars["o2pressure"].set(value=0.83, vary=True)  # (atm), 20201125 Exp
        # pars['h2opressure'].set(value = (0.8351), vary = False)  # (atm), 20201203 Exp
        pars["o2temperature"].set(value=296, vary=True)  # (K)
        pars["o2pathlength"].set(value=40, vary=True)  # (cm)
        pars["o2shift"].var = True  # (cm-1, but always float)

        mod2, pars2 = spectra_K_lmfit('K')
        pars2["Kpressure"].set(value=0.00131579*10e-7, vary=True)  # (atm), 20201125 Exp
        pars2["Kmolefraction"].set(vary=False)  # (molefraction)
        pars2["Ktemperature"].set(value=296, vary=False)  # (K)
        pars2["Kpathlength"].set(value=7.2-0.19*2, vary=False)  # (cm)
        pars2["Kshift"].set(vary=True)  # (cm-1, but always float)
        pars2["Kbroadening"].set(vary=True)  # (cm-1, but always float)

        modfull = mod + mod2
        pars.update(pars2)

    #   End configurable variables

    #   Start processing loop

    t0 = time.time()
    if not(importdata):
        files = pld.open_dir(d, recursion_levels=2, verbose=2)
        dt = time.time() - t0
        print("Opened %i files in %0.3f seconds" % (len(files), dt))
        newest = ""

    # simulate the absorption spectrum
    pld.db_begin(linedir)  # point HAPI (nested function in fit) to linelist file

    # Create a couple variables used for determining processing flow
    pass_pc = 0
    pass_raw = 0
    
    
    temps = []
    concs = []
    press = []
    pathlengths = []
    shifts = []
    p2p = []
    # Loop through directory and process data
    if not(importdata):
        for name, daq_file in files.items():

            if processone:
                if name != fname:
                    continue
                    print('yes')

            # Make sure there's some data
            if not daq_file.failure:

                # Are there phase corrected IGMs?
                if daq_file.data_pc is not None:
                    non_empty_pc = sum(bool(i) for i in daq_file.igs_per_pc)
                    pass_pc = 1 if (len(daq_file.data_pc) == non_empty_pc) else 0

                # Are there raw IGMs?
                if daq_file.data_raw is not None:
                    non_empty_raw = len(daq_file.data_raw)
                    pass_raw = 1 if (non_empty_raw > 0) else 0

                # As long as there's some IGMs go ahead and start processing!
                if pass_pc or pass_raw:
                    newest = max(newest, name)
                    print(name)

                    pc_lim_low = 0.11
                    pc_lim_high = 0.16
                    # Phase correction and transform to transmission
                    # daq_file.data_raw => phase correct and sum raw IGMs
                    # daq_file.data_pc => phase correct and sum PC data=
                    if fitallan:
                        if process_raw:
                            pc_num = pc_time*daq_file.dfr/daq_file.num_hwavgs
                            if ig_num == 0 or ig_num > len(daq_file.data_raw):
                                n_pc = np.ceil((len(daq_file.data_raw)/pc_num))
                            else:
                                n_pc = ig_num
                        elif process_pc:
                            pc_num = pc_time/daq_file.pc_time
                            if ig_num == 0 or ig_num > len(daq_file.data_pc):
                                n_pc = np.ceil((len(daq_file.data_pc)/pc_num))
                            else:
                                n_pc = ig_num
                    else:
                        n_pc = 1

                    for i in np.arange(n_pc):
                        print(str(int(i+1)) + ' out of ' + str(int(n_pc)) + ' fits\n')
                        if fitallan:
                            if process_raw:
                                if i == (n_pc-1):
                                    if n_pc*pc_num > len(daq_file.data_raw):
                                        modulo = len(daq_file.data_raw) % pc_num
                                        data_ig = daq_file.data_raw[int(pc_num*i):int(pc_num*i+modulo)]
                                    else:
                                        data_ig = daq_file.data_raw[int(pc_num*i):int(pc_num*(i+1))]
                                else:
                                    data_ig = daq_file.data_raw[int(pc_num*i):int(pc_num*(i+1))]
                            if process_pc:
                                if i == (n_pc-1):
                                    if n_pc*pc_num > len(daq_file.data_pc):
                                        modulo = len(daq_file.data_pc) % pc_num
                                        data_ig = daq_file.data_pc[int(pc_num*i):int(pc_num*i+modulo)]
                                    else:
                                        data_ig = daq_file.data_pc[int(pc_num*i):int(pc_num*(i+1))]
                                else:
                                    data_ig = daq_file.data_pc[int(pc_num*i):int(pc_num*(i+1))]
                        else:
                            if process_raw:
                                data_ig = daq_file.data_raw
                            elif process_pc:
                                data_ig = daq_file.data_pc

                        p2p.append((np.max(data_ig[0])-np.min(data_ig[0]))/(daq_file.num_hwavgs))

                        peak = np.argmax(abs(data_ig[0]))
                        data_ig = np.roll(data_ig, int(len(data_ig[0]) / 2) - peak)
                        data = data_ig.copy()
                        trans = pld.pc_peter(data, pc_lim_low, pc_lim_high, plot=True,
                                             zoom=500)
                        pc_ig = np.fft.ifft(trans)
                        plt.figure()
                        plt.plot(pc_ig)
                        # p2p.append((np.max(pc_ig)-np.min(pc_ig))/(pc_num*daq_file.num_hwavgs))

                        # trans = np.abs(np.fft.fft(pc_ig))

                        # Create frequency axis
                        if importwvn:
                            x_wvn_full = np.loadtxt(os.path.join(d_import,fname_import+'_xwvnfull.txt'))
                        else:
                            x_wvn_full = pld.mobile_axis2(
                                daq_file, f_opt=lockfreq, wvn_spectroscopy=nomfreq)
                                        # Convert interesting portion of spectrum to time-domain
                        if optimizebl:
                            band_fit, bl_rel = OptimizeBL(x_wvn_full, trans,
                                                          thres=5e-2, thres_range=20, thres_bl=5e-4,
                                                          weight=0.6, plot=True
                                                          )
                            bl = int(bl_rel * len(x_wvn_full) * 2)
                        start_pnt, stop_pnt = td.bandwidth_select_td(x_wvn_full, band_fit)
                        trans_orig = copy(trans)

                        if savedata:
                            if len(x_wvn_full) > len(trans_orig):
                                np.savetxt(os.path.join(savepath, name+'_trans.txt'),
                                       np.array([x_wvn_full[:len(trans_orig)], trans_orig]).T)
                            else:
                                np.savetxt(os.path.join(savepath, name+'_trans.txt'),
                                       np.array([x_wvn_full, trans_orig[:len(x_wvn_full)]]).T)

                        if len(x_wvn_full) > len(trans_orig):
                            plt.figure()
                            plt.plot(x_wvn_full[:len(trans)],trans)
                        else:
                            plt.figure()
                            plt.plot(x_wvn_full, trans_orig[:len(x_wvn_full)])

                        if start_pnt < stop_pnt:
                            # Normal setup
                            trans = trans[start_pnt:stop_pnt]
                            x_wvn = x_wvn_full[start_pnt:stop_pnt]
                            y_td = np.fft.irfft(-np.log(trans))
                        else:
                            # DCS in 0.5-1.0 portion of Nyquist window, need to flip x-axis to fit
                            trans_flipped = trans[int((daq_file.frame_length / 2)) :: -1]
                            x_wvn_flipped = x_wvn_full[::-1]
                            start_pnt, stop_pnt = td.bandwidth_select_td(
                                x_wvn_flipped, band_fit
                            )
                            x_wvn = x_wvn_flipped[start_pnt:stop_pnt]
                            trans = trans_flipped[start_pnt:stop_pnt]
                            y_td = np.fft.irfft(-np.log(trans))


                        plt.figure();
                        plt.plot(x_wvn, trans)

                        if fitdata:
                            # fit
                            tic = time.time()  # time-domain-specific fitting starts here
                            Fit = FitFunc(x_wvn, y_td, bl, etalons, mod, db_name1,
                                          savedata=savedata, savename=savename,
                                          savename_td=savename_td,
                                          savename_freq=savename_freq,
                                          plot_fit_td=plot_fit_td,plot_fit_freq=plot_fit_freq)
                            toc = time.time()  # time-domain-specific fitting starts here
                            print('Fitting time = '+str(toc-tic))
                            # store fits in arrays
                            concs.append(Fit.best_values['o2molefraction'])
                            temps.append(Fit.best_values['o2temperature'])
                            press.append(Fit.best_values['o2pressure'])
                            pathlengths.append(Fit.best_values['o2pathlength'])
                            shifts.append(Fit.best_values['o2shift'])
    else:
        i=0
        data_import = np.loadtxt(os.path.join(savepath, fname+'_trans.txt'))
        trans = data_import[:,1]
        data_ig = np.fft.irfft(trans)
        p2p.append((np.max(data_ig[0])-np.min(data_ig[0]))/50)

        x_wvn_full = data_import[:,0]

        # Convert interesting portion of spectrum to time-domain
        if optimizebl:
            band_fit, bl_rel = OptimizeBL(x_wvn_full, trans,
                                                thres=5e-2, thres_range=20, thres_bl=0.00125,
                                                weight=0.6, bd_start=[13005, 13025],
                                                bd_end=[13165, 13185], plot=True
                                                )
            bl = int(bl_rel*len(x_wvn_full)*2)
        start_pnt, stop_pnt = td.bandwidth_select_td(x_wvn_full, band_fit)

        trans_orig = copy(trans)
        if start_pnt < stop_pnt:
            # Normal setup
            trans = trans[start_pnt:stop_pnt]
            x_wvn = x_wvn_full[start_pnt:stop_pnt]
            y_td = np.fft.irfft(-np.log(trans))
        else:
            # DCS in 0.5-1.0 portion of Nyquist window, need to flip x-axis to fit
            trans_flipped = trans[:: -1]
            x_wvn_flipped = x_wvn_full[::-1]
            start_pnt, stop_pnt = td.bandwidth_select_td(
                x_wvn_flipped, band_fit
            )
            x_wvn = x_wvn_flipped[start_pnt:stop_pnt]
            trans = trans_flipped[start_pnt:stop_pnt]
            y_td = np.fft.irfft(-np.log(trans))

        tic = time.time()  # time-domain-specific fitting starts here
        Fit = FitFunc(x_wvn, y_td, bl, etalons, mod, db_name1,
                      savedata=savedata, savename=savename,
                      savename_td=savename_td,
                      savename_freq=savename_freq,
                      plot_fit_td=plot_fit_td,plot_fit_freq=plot_fit_freq)
        toc = time.time()  # time-domain-specific fitting starts here
        print('Fitting time = '+str(toc-tic))
        # store fits in arrays
        concs.append(Fit.best_values['o2molefraction'])
        temps.append(Fit.best_values['o2temperature'])
        press.append(Fit.best_values['o2pressure'])
        pathlengths.append(Fit.best_values['o2pathlength'])
        shifts.append(Fit.best_values['o2shift'])

if fitdata and savedata:
    np.savetxt(os.path.join(savepath, fname + '_xwvnfull.txt'), x_wvn_full)
    np.savetxt(os.path.join(savepath, fname+'_conc.txt'), concs)
    np.savetxt(os.path.join(savepath, fname+'_pathlength.txt'), pathlengths)
    np.savetxt(os.path.join(savepath, fname+'_temp.txt'), temps)
    np.savetxt(os.path.join(savepath, fname+'_pres.txt'), press)
    np.savetxt(os.path.join(savepath, fname+'_shift.txt'), shifts)

    print("Total total runtime %d seconds" % (time.time() - t0))
