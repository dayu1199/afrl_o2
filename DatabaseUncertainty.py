"""
DatabaseUncertainty.py

Created by on 12/8/22 by david

Description: shift parameters in database according to HITRAN uncertainty
to get database uncertainty

"""

import os
import matplotlib.pyplot as plt
import numpy as np

from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
from pldspectrapy.constants import *
import td_support as td
import FitProgram2BeamwBox as fithit
import linelist_conversions as lc
import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()


#%% Code variables
param_to_change = 5 # 0-nu, 1-sw, 2-gamma_air, 3-gamma_self, 4-n_air, 5-delta_air
                    # should probably do gamma_air and n_air together

param_key = ['nu', 'sw', 'gamma_air', 'gamma_self', 'n_air', 'delta_air']
error_type_key = [1, 2, 2, 2, 2, 1] # 1-absolute, 2-relative
abs_error_key = [99, 1, 0.1, 0.01, 0.001, 1e-4, 1e-5, 1e-6, 1e-9, 1e-10] # 99 for what I don't know what to do with
rel_error_key = [99, 99, 99, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01]
flip_sign = False  # have opposite shifts for n air and gamma air

#%% pull in existing O2 database
df_O2 = lc.par_to_df('O2.data', nu_min=13000, nu_max=13200)
df_O2['ierr'] = df_O2['ierr'].astype(str)

#%% check that there aren't any of the difficult to apply codes in ierr
i = 0
for errorcode in df_O2['ierr']:
    errorarray = np.array([int(numeric_string) for numeric_string in [*errorcode]])
    if any(np.array([1,1,1,1,1,1])==errorarray) or\
        any(np.array([99,2,2,2,2,99])==errorarray) or\
            any(np.array([99,3,3,3,3,99])==errorarray):
        print('Undefined uncertainty in feature ', int(i) )
    i += 1

#%% Change O2 database and write to new file
df_O2_up = df_O2.copy()
df_O2_down = df_O2.copy()

def get_abserror(a):
    return abs_error_key[int(a[param_to_change])]

def get_relerror(a):
    return rel_error_key[int(a[param_to_change])]

if error_type_key[param_to_change]==1: # absolute error
    # apply error
    df_O2_up[param_key[param_to_change]] = df_O2[param_key[param_to_change]] + \
                                           df_O2['ierr'].apply(get_abserror)
    df_O2_down[param_key[param_to_change]] = df_O2[param_key[param_to_change]] - \
                                           df_O2['ierr'].apply(get_abserror)
    # determined weight averaged error
    weighted_error = (df_O2['sw'] * df_O2['ierr'].apply(get_abserror)).sum() / df_O2['sw'].sum()
    print('Weighted average for ', param_key[param_to_change], ' is ', str(weighted_error))
if error_type_key[param_to_change]==2: # relative error
    if param_to_change==2 or param_to_change==4:
        # specific functions for getting gamma_air and n_air uncertainty
        def get_relerror_gamair(a):
            return rel_error_key[int(a[2])]
        def get_relerror_nair(a):
            return rel_error_key[int(a[4])]
        # modify gamma_air
        df_O2_up[param_key[2]] = df_O2[param_key[2]] * \
                                               (1 + df_O2['ierr'].apply(get_relerror_gamair))
        df_O2_down[param_key[2]] = df_O2[param_key[2]] * \
                                                 (1 - df_O2['ierr'].apply(get_relerror_gamair))
        # modify n_air
        if flip_sign:
            df_O2_up[param_key[4]] = df_O2[param_key[4]] * \
                                     (1 - df_O2['ierr'].apply(get_relerror_nair))
            df_O2_down[param_key[4]] = df_O2[param_key[4]] * \
                                       (1 + df_O2['ierr'].apply(get_relerror_nair))
        else:
            df_O2_up[param_key[4]] = df_O2[param_key[4]] * \
                                     (1 + df_O2['ierr'].apply(get_relerror_nair))
            df_O2_down[param_key[4]] = df_O2[param_key[4]] * \
                                       (1 - df_O2['ierr'].apply(get_relerror_nair))

        # determined weight averaged error
        weighted_error = (df_O2['sw'] * df_O2['ierr'].apply(get_relerror_gamair)).sum() / df_O2['sw'].sum()
        print('Weighted average for gamma_air is ', str(weighted_error))
        weighted_error = (df_O2['sw'] * df_O2['ierr'].apply(get_relerror_nair)).sum() / df_O2['sw'].sum()
        print('Weighted average for n_air is ', str(weighted_error))
    else:
        df_O2_up[param_key[param_to_change]] = df_O2[param_key[param_to_change]] * \
                                               (1 + df_O2['ierr'].apply(get_relerror))
        df_O2_down[param_key[param_to_change]] = df_O2[param_key[param_to_change]] * \
                                               (1 - df_O2['ierr'].apply(get_relerror))
        # determined weight averaged error
        weighted_error = (df_O2['sw'] * df_O2['ierr'].apply(get_relerror)).sum() / df_O2['sw'].sum()
        print('Weighted average for ', param_key[param_to_change], ' is ', str(weighted_error))

#%% Save file
# "line_mixing_flag": "%1s",
                # "gp": "%7.1f",
                # "gpp": "%7.1f",
# extra_params = {"deltap_air": "%10.3e",
#                 "delta_self": "%9.6f",
#                 "deltap_self": "%10.3e",
#                 "n_self": "%7.4f",
#                 "gamma_h2": "%6.4f",
#                 "delta_h2": "%9.6f",
#                 "deltap_h2": "%10.3e",
#                 "n_h2": "%7.4f",
#                 "gamma_co2": "%6.4f",
#                 "delta_co2": "%9.6f",
#                 "n_co2": "%7.4f",
#                 "gamma_he": "%6.4f",
#                 "delta_he": "%9.6f",
#                 "n_he": "%7.4f",
#                 "gamma_h2o": "%6.4f",
#                 "n_h2o": "%9.6f"}
extra_params = {"deltap_air": "%10.3e",
                "sd_air": "%9.6f",
                "delta_self": "%9.6f",
                "deltap_self": "%10.3e",
                "n_self": "%7.4f",
                "sd_self": "%9.6f"}
# create directory to put modified files in
save_dir = r'/Users/david/Documents/AFRLO2/linelists/ModifiedForUncertainty'
if param_to_change==2 or param_to_change==4:
    save_dir_up = os.path.join(save_dir, 'airbroad_up')
    save_dir_down = os.path.join(save_dir, 'airbroad_down')
else:
    save_dir_up = os.path.join(save_dir, param_key[param_to_change]+'_up')
    save_dir_down = os.path.join(save_dir, param_key[param_to_change]+'_down')
if not os.path.exists(save_dir_up):
    os.mkdir(save_dir_up)
if not os.path.exists(save_dir_down):
    os.mkdir(save_dir_down)

lc.df_to_par(df_O2_up, 'O2', extra_params=extra_params, save_dir=save_dir_up,
             print_name=False)
lc.df_to_par(df_O2_down, 'O2', extra_params=extra_params, save_dir=save_dir_down,
             print_name=False)

#%% Check that database are different by simulating
y_O2 = 0.21  # mole fraction O2
P = 1.37  # pressure (atm)
T = 600  # Temperature (K)
V = 875  # velocity, m/s
theta = np.deg2rad(-35)
windowstart = 13000  # bandwidth start (cm-1)
windowend = 13200  # bandwidth end (cm-1)
dv = 0.00667  # frequency axis step size (cm-1)
L = 6.81736/np.cos(theta)  # path length of measurement (cm)

# adjust frequency axis to account for velocity doppler shift
wvnumspace = np.arange(windowstart,windowend,dv) # create array of frequency axis
start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,windowend]) # optimal start stop of bandwidth for fourier transfrom
wvnumspace = wvnumspace[start:stop]
nsteps = len(wvnumspace)-1
slope_doppler = V*np.sin(theta)/SPEED_OF_LIGHT+1 # doppler scaling due to velocity
new_wvw_num = wvnumspace*slope_doppler # apply to frequency axis
windowend = np.amax(new_wvw_num) # new bandwidth end
windowstart = np.amin(new_wvw_num) # new bandiwdth start
dv = (new_wvw_num[-1]-new_wvw_num[0])/nsteps # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend = windowend.item()
windowstart = windowstart.item()
dv = dv.item()
MIDS = [(7, 1, y_O2*hapi.abundance(7, 1))]

## no error
# create absorbance model using hapi
hapi.db_begin('')
[nu, coefs_no] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv, OmegaRange=[windowstart, windowend],
            HITRAN_units=False, Environment={'p': P, 'T': T},
            Diluent={'self': y_O2-0.21/0.79*(1-y_O2),
                     'air': (1-y_O2)/0.79},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2_no = coefs_no*L  # get asorption (absorbance * pathlength)

## error down
# create absorbance model using hapi
hapi.db_begin(save_dir_down)
[nu, coefs_down] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv, OmegaRange=[windowstart, windowend],
            HITRAN_units=False, Environment={'p': P, 'T': T},
            Diluent={'self': y_O2-0.21/0.79*(1-y_O2),
                     'air': (1-y_O2)/0.79},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2_down = coefs_down*L  # get asorption (absorbance * pathlength)

## error up
# create absorbance model using hapi
hapi.db_begin(save_dir_up)
[nu, coefs_up] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv, OmegaRange=[windowstart, windowend],
            HITRAN_units=False, Environment={'p': P, 'T': T},
            Diluent={'self': y_O2-0.21/0.79*(1-y_O2),
                     'air': (1-y_O2)/0.79},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2_up = coefs_up*L  # get asorption (absorbance * pathlength)

# compare different database
plt.figure()
plt.plot(nu, abs_O2_no, label='No Error')
plt.plot(nu, abs_O2_up, label='Error Up')
plt.plot(nu, abs_O2_down, label='Error Down')
plt.legend()
plt.xlabel('Wavenumber (cm-1)')
plt.ylabel('Absorbance')
plt.title(param_key[param_to_change])

#%% Fitting Parameters
##################################Fitting Parameters###########################
# Fitting Options
save_files = False  # saves fits and fitted parameters in text files.
plot_results = True  # Plots time domain and TD fit converted to the frequency
# domain. Turn this off for many datafiles.
print_fit_report = True  # Prints each fit report to command window
file_tag = param_key[param_to_change]

# simulation spectra to use
fn_y = 'O2simulation_AJ_down.txt'
fn_p = 'O2simulation_AJ_up.txt'

# Directory Info
d_save = os.path.join(r'/Users/david/Library/CloudStorage',
                      'GoogleDrive-dayu1199@colorado.edu/My Drive',
                      'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits/sim')

# MFID Parameters
weight_window_y = [0.008, 0.5]  # default [0.005,0.5]
weight_window_p = [0.008, 0.5]  # default [0.005,0.5]
etalon_windows_y = []  # format [[0,100],[200,300]]
etalon_windows_p = []
band_fit_y = [13015.61, 13169.4]
band_fit_p = [13015.6, 13169.0]
weight_flat = True  # Otherwise will do an exponential weighting

# Fitting Parameters
molec_to_fit = ['O2']
angle_p = 34.87
angle_y = -35.01
isotopes = [[1]]  # format [[1],[2]]
temp_guess = 600
vel_guess = 875
press_guess = 1.18  # atm
shift_guess = 0
conc_guess = 0.21
broad_guess = 1
temp_fit = True
press_fit = False
shift_fit = True
vel_fit = True
conc_fit = False
broad_fit = False

#%% Fit
# Fit Regular
x_y, x_p, absflow_y, absflow_p, vel_val, vel_err, temp_val,\
temp_err, press_val, press_err, shift_val, shift_err, conc_val,\
conc_err, broad_val, broad_err = \
    fithit.FitProgram(fn_y, fn_p, '', file_tag,
                      weight_window_y, weight_window_p,
                      weight_flat, etalon_windows_y,
                      etalon_windows_p,
                      angle_y, angle_p, band_fit_y, band_fit_p,
                      molec_to_fit, isotopes,
                      press_fit, temp_fit, vel_fit,
                      shift_fit, conc_fit, broad_fit,
                      press_guess, temp_guess, vel_guess,
                      shift_guess, conc_guess, broad_guess,
                      background_remove=False, save_files=save_files,
                      plot_results=plot_results, transUnits=False,
                      noise_add=False)

# Fit up
x_y_up, x_p_up, absflow_y_up, absflow_p_up, vel_val_up, vel_err_up, temp_val_up,\
temp_err_up, press_val_up, press_err_up, shift_val_up, shift_err_up, conc_val_up,\
conc_err_up, broad_val_up, broad_err_up = \
    fithit.FitProgram(fn_y, fn_p, save_dir_up, file_tag,
                      weight_window_y, weight_window_p,
                      weight_flat, etalon_windows_y,
                      etalon_windows_p,
                      angle_y, angle_p, band_fit_y, band_fit_p,
                      molec_to_fit, isotopes,
                      press_fit, temp_fit, vel_fit,
                      shift_fit, conc_fit, broad_fit,
                      press_guess, temp_guess, vel_guess,
                      shift_guess, conc_guess, broad_guess,
                      background_remove=False, save_files=save_files,
                      plot_results=plot_results, transUnits=False,
                      noise_add=False, db_dir=save_dir_up)

# Fit down
x_y_dn, x_p_dn, absflow_y_dn, absflow_p_dn, vel_val_dn, vel_err_dn, temp_val_dn,\
temp_err_dn, press_val_dn, press_err_dn, shift_val_dn, shift_err_dn, conc_val_dn,\
conc_err_dn, broad_val_dn, broad_err_dn = \
    fithit.FitProgram(fn_y, fn_p, save_dir_down, file_tag,
                      weight_window_y, weight_window_p,
                      weight_flat, etalon_windows_y,
                      etalon_windows_p,
                      angle_y, angle_p, band_fit_y, band_fit_p,
                      molec_to_fit, isotopes,
                      press_fit, temp_fit, vel_fit,
                      shift_fit, conc_fit, broad_fit,
                      press_guess, temp_guess, vel_guess,
                      shift_guess, conc_guess, broad_guess,
                      background_remove=False, save_files=save_files,
                      plot_results=plot_results, transUnits=False,
                      noise_add=False, db_dir=save_dir_down)
print('Fitting Done!')