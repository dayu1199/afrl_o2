#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Fitting program for fitting retro data from AFRL Mar2022 campaign

Created on Fri Apr  8 10:03:04 2022

@author: david
"""

import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from openpyxl import Workbook, load_workbook
import pandas as pd
import pickle
from datetime import datetime

import FitProgramRetrowBox as fithit
##################################Fitting Parameters###########################

# Fitting Options
save_files = True  # saves fits and fitted parameters in text files.
plot_results = True  # Plots time domain and TD fit converted to the frequency domain. Turn this off for many datafiles.
print_fit_report = True  # Prints each fit report to command window
back_remove = True  # Remove background from spectra before fitting

# Directory Info
d_data = os.path.join(r'/Users/david/Library/CloudStorage',
                      'GoogleDrive-dayu1199@colorado.edu/My Drive',
                      'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData')
d_pc = 'pc_peter'
d_save = os.path.join(r'/Users/david/Library/CloudStorage',
                      'GoogleDrive-dayu1199@colorado.edu/My Drive',
                      'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits/retro')
file_tag = '' # remember to add a _ before
label_back = ''

indexes = [16, 1718]  # 16,1718 (1718 is a combination of 17 and 18)

# MFID Parameters
weight_windows = [[0.0083, 0.5],
                  [0.0083, 0.5]]  # default [0.005,0.5]
etalon_windows = []
band_fits = [[13016.9, 13169.6],
             [13014.1, 13172.2]]
weight_flat = True  # Otherwise will do an exponential weighting
    
# Fitting Parameters
angle = 16.69
molec_to_fit = ['O2']  # must be a list
isotopes = [[1]]  # format [[1],[2]]
press_guesss = [0.9268, 1.191] # atm
temp_guess = 300
vel_guess = 500
shift_guess = 0
conc_guess = 0.2095
broad_guess = 1
temp_fit = True
press_fit = False
shift_fit = True
vel_fit = True
conc_fit = False
broad_fit = False

# Background Fitting Parameters
molec_back = 'O2'
conc_back = 0.135
temp_back = 324.4
press_back = 1
pl_back = 38.6

# Box Fitting Parameters
conc_box = 0.0056
temp_box = 277
pres_box = 1
pl_box = 35

if not(back_remove):
    conc_back = 0
    shift_back = 0
    press_back = 0
    
    conc_box = 0

###############################Port in data###################################
# get files from directory
# import filename key to get filenames
d_key = os.path.join(r'/Users/david/Library/CloudStorage',
                      'GoogleDrive-dayu1199@colorado.edu/My Drive',
                     'AFRL ETHOS/MarchCampaign')
fn_key = 'FilenameKey.xlsx'
key_df = pd.read_excel(os.path.join(d_key,fn_key))
    
# organization of fits
if save_files:
    notes = input('Type down notes for this fit:')

    # workbook name
    name_tabulate = 'RetroFits_v2.xlsx'
    fn_tabulate = os.path.join(d_save,name_tabulate)
    
    # load/create workbook
    if os.path.isfile(fn_tabulate):
        wb = load_workbook(fn_tabulate)
    else:
        wb = Workbook()
    
    # get time for labeling fit
    now = datetime.now()
    timestamp = now.strftime('%d_%m_%Y_%H%M%S')
    
    # folder to save data
    d_save_0 = os.path.join(d_save,timestamp)
    if not(os.path.isdir(d_save_0)):
        os.mkdir(d_save_0)
    
###############################Port in data###################################
# get files from directory
files = os.listdir(d_data)
 
# prepare arrays for dataframe
fn = []
vel = []
vel_unc = []
temp = []
temp_unc = []
press = []
press_unc = []
shift = []
shift_unc = []
conc = []
conc_unc = []
broad = []
broad_unc = []

i = 0
for index in indexes:
    print(index)
    weight_window = weight_windows[i]
    band_fit = band_fits[i]
    press_guess = press_guesss[i]
    i += 1
    
    # Grab filenames from key
    row = key_df.loc[key_df['Indexx']==index]
    vis3_daq = str(row.iloc[0]['vis3 DAQ'])
    vis3_fn = str(row.iloc[0]['vis3 fn'])
    d_vis3 = os.path.join(d_data,vis3_daq)
    
    # find vis 3 data
    vis3_found = False
    # search through moose directory for trans file
    for folder in os.listdir(d_vis3):
        if os.path.isdir(os.path.join(d_vis3, folder)):
            for sub_folder in os.listdir(os.path.join(d_vis3, folder)):
                if os.path.isdir(os.path.join(d_vis3, folder, sub_folder)) and \
                     sub_folder == d_pc:
                    for file in os.listdir(os.path.join(d_vis3, folder,
                                                        sub_folder)):
                        if file == vis3_fn+'_trans.txt':
                            fn_retro = os.path.join(d_vis3, folder, sub_folder, file)
                            vis3_found = True
                            print(os.path.join(d_vis3, folder, sub_folder, file))
                            break
                if vis3_found:
                    break
        if vis3_found:
            break
    if not vis3_found:
        print('vis3 file not found!')
        continue
    
    # directory to save results
    if save_files:
        d_save_1 = os.path.join(d_save_0,str(index))
        if not(os.path.isdir(d_save_1)):
            os.mkdir(d_save_1)
    else:
        d_save_1 = ''
        
        
    fn.append(fn_retro)
    
    fitresults = fithit.FitProgram(fn_retro, d_save_1, file_tag,
                              weight_window, weight_flat, etalon_windows,
                              angle, band_fit,
                              molec_to_fit, isotopes, 
                              press_fit, temp_fit, vel_fit,
                              shift_fit, conc_fit, broad_fit,
                              press_guess, temp_guess, vel_guess,
                              shift_guess, conc_guess, broad_guess,
                              conc_box=conc_box, temp_box=temp_box,
                              pres_box=pres_box, pl_box=pl_box,
                              background_remove=back_remove,
                              molec_back=molec_back,
                              pathlength_b=pl_back, press_back=press_back,
                              temp_back=temp_back, conc_back=conc_back,
                              save_files=save_files,
                              plot_results=plot_results,
                              transUnits=True)
    vel.append(fitresults[0])
    vel_unc.append(fitresults[1])
    temp.append(fitresults[2])
    temp_unc.append(fitresults[3])
    press.append(fitresults[4])
    press_unc.append(fitresults[5])
    shift.append(fitresults[6])
    shift_unc.append(fitresults[7])
    conc.append(fitresults[8])
    conc_unc.append(fitresults[9])
    broad.append(fitresults[10])
    broad_unc.append(fitresults[11])
    
    if save_files:
        # load/create worksheet
        try:
            ws = wb[str(index)]
        except:
            ws = wb.create_sheet(str(index))
            # create header
            header = ['INDEX','TIME','DAQ','Fn',
                      'WEIGHT_FLAT','WEIGHT_WIN','ETA','BW',
                      'MOLECULES',
                      'VEL_FIT','VEL_VAL','VEL_ERR','VEL_GUES',
                      'TEMP_FIT','TEMP_VAL','TEMP_ERR','TEMP_GUES',
                      'PRES_FIT','PRES_VAL','PRES_ERR','PRES_GUES',
                      'SHIFT_FIT','SHIFT_VAL','SHIFT_ERR','SHIFT_GUES',
                      'CONC_FIT','CONC_VAL','CONC_ERR','CONC_GUES',
                      'BROAD_FIT','BROAD_VAL','BROAD_ERR','BROAD_GUES',
                      'BACK_REMOVE','BACK_LABEL','BACK_PRESS','BACK_TEMP',
                      'BACK_CONC',
                      'BOX_TEMP','BOX_PRES','BOX_CONC',
                      'NOTES']
            ws.append(header)
        # add data from current run
        datawrite = [str(index), timestamp, vis3_daq,
                     vis3_fn, str(weight_flat),
                     str(weight_window), str(etalon_windows),
                     str(band_fit), str(molec_to_fit),
                     str(vel_fit), vel[-1], vel_unc[-1], vel_guess,
                     str(temp_fit), temp[-1], temp_unc[-1], temp_guess,
                     str(press_fit), press[-1], press_unc[-1], press_guess,
                     str(shift_fit), shift[-1], shift_unc[-1], shift_guess,
                     str(conc_fit), str(conc[-1]), str(conc_unc[-1]),
                     str(conc_guess),
                     str(broad_fit), broad[-1], broad_unc[-1], broad_guess,
                     str(back_remove), label_back, press_back, temp_back,
                     conc_back,
                     temp_box, pres_box, conc_box, notes]
        ws.append(datawrite)
        # save workbook
        wb.save(fn_tabulate)
        
    
# Create dict of results
d_fit = {'Fn': fn,
     'Vel. (m/s)': vel,
     'Vel. Unc.': vel_unc,
     'Temp. (K)': temp,
     'Temp. Unc.': temp_unc,
     'Pres. (atm)': press,
     'Pres. Unc.': press_unc,
     'Shift (cm-1)': shift,
     'Shift Unc.': shift_unc,
     'Conc.': conc,
     'Conc. Unc': conc_unc}

df_fit = pd.DataFrame.from_dict(d_fit)
if save_files:
    pickle.dump(df_fit, open(os.path.join(d_save_0,'df_fit.p'),'wb'))

print('Fitting Done!')