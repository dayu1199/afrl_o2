#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 25 11:39:28 2021

@author: david
"""

import numpy as np
import os
import matplotlib.pyplot as plt

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

# plot retro data
d = os.path.join(
    r'/Users/david/Library/CloudStorage/GoogleDrive-dayu1199@colorado.edu/.shortcut-targets-by-id/16tkMSgflId4HArPvMXmLPmC-6LcoGXZ0/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits/retro/16_06_2022_123437/16')

# baseline free
for fn in os.listdir(d):
    if '_abs' in fn:
        data = np.loadtxt(os.path.join(d, fn))
        x_wvn = data[:, 0]
        dcs_nobl = data[:, 2]
    if '_fit' in fn:
        data = np.loadtxt(os.path.join(d, fn))
        model = data[:, 1]
fig, ax = plt.subplots()
ax.plot(x_wvn, dcs_nobl, label='Retroreflector Data', color='blue')
ax.plot(x_wvn, model, label='Retroreflector Fit', color='orange')


def closest_value(input_list, input_value):
    arr = np.asarray(input_list)

    i = (np.abs(arr - input_value)).argmin()

    return i

start = closest_value(x_wvn, 13079.48)
stop = closest_value(x_wvn, 13082.82)
noise_level = np.std(dcs_nobl[start:stop])

# plot crossed data
d = os.path.join(r'/Users/david/Library/CloudStorage/GoogleDrive-dayu1199@colorado.edu/.shortcut-targets-by-id/16tkMSgflId4HArPvMXmLPmC-6LcoGXZ0/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits/crossed/12_08_2022_095524/11combined')

# baseline free
for fn in os.listdir(d):
    if '_abs' in fn:
        data = np.loadtxt(os.path.join(d,fn))
        x_wvn = data[:,0]
        dcs_nobl = data[:,2]
    if '_fit' in fn:
        data = np.loadtxt(os.path.join(d,fn))
        model = data[:,1]

ax.plot(x_wvn,dcs_nobl, label='Crossed-Beam Data', color='green')
ax.plot(x_wvn,model, label='Crossed-Beam Fit', color='red')
ax.set_xlabel('Wavenumber ($\mathregular{cm^{-1}}$)')
ax.set_ylabel('Absorbance')
ax.set_xlim([13083,13087])
ax.set_ylim([-0.002,0.013])
ax.ticklabel_format(useOffset=False)
# ax.legend(loc='upper left')
plt.show()
# plt.title(fn)


print(noise_level)
        # if '_td' in fn:
        #     data = np.loadtxt(os.path.join(d,fn))
        #     dcs = data[:,0]
        #     model = data[:,1]
        #     res = dcs-model
        #     weight = data[:,2]
        #     plt.figure()
        #     plt.plot(dcs)
        #     plt.plot(model)
        #     plt.plot(res)
        #     plt.plot(weight)
        #     plt.title(fn)
        
