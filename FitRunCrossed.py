# -*- coding: utf-8 -*-
"""
Run Fitting for Crossed Beams for Mar2020 Campaign
vis2 downstream yellow
vis3 upstream purple

Created on Tue Sep 24 13:28:11 2019
@author: David
"""
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from openpyxl import Workbook, load_workbook
import pandas as pd
import pickle
from datetime import datetime
from OptimizeBL import OptimizeBL

from packfind import find_package
find_package('pldspectrapy')
# import pldspectrapy.pldhapi as hapi
import hapi
import FitProgram2BeamwBox as fithit
##################################Fitting Parameters###########################

# Fitting Options
save_files = True # saves fits and fitted parameters in text files.
plot_results = True  # Plots time domain and TD fit converted to the frequency domain. Turn this off for many datafiles.
print_fit_report = True  # Prints each fit report to command window
back_remove = True  # Remove background from spectra before fitting
back_interp = True
label_back = ''

# Directory Info
d_data = os.path.join(r'/Users/david/Library/CloudStorage/OneDrive-UCB-O365/AFRL/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData')
d_pc = 'pc_peter'
d_save = os.path.join(r'/Users/david/Library/CloudStorage/OneDrive-UCB-O365/AFRL/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits/crossed')
file_tag = ''  # remember to add a _ before
fn_bginter = os.path.join(r'/Users/david/Library/CloudStorage/OneDrive-UCB-O365/AFRL/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/BkgdInterpolation.xlsx')

# which indexes to fit, just test runs
indexes = [5, 6, 7, 10, '11combined']  # [5, 6, 7, 10, '11combined']

# MFID Parameters
# weight_windows_y = [[0.00872,0.5]]
# weight_windows_p = [[0.00845,0.5]]
weight_windows_y = [[0.008, 0.5],
                    [0.0084, 0.5],
                    [0.008, 0.5],
                    [0.008, 0.5],
                    [0.0082, 0.5]]  # default [0.005,0.5]
weight_windows_p = [[0.0081, 0.5],
                    [0.0083, 0.5],
                    [0.008, 0.5],
                    [0.008, 0.5],
                    [0.0085, 0.5]]  # default [0.005,0.5]
etalon_windows_y = [[[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]]]  # format [[[0.1,0.15]],[[0.22,0.25]]]
etalon_windows_p = [[[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]]]
# band_fits_y = [[13011,13166]]
# band_fits_p = [[13017,13167]]
band_fits_y = [[13016.1, 13166.7],
               [13015.2, 13168],
               [13015.2, 13169],
               [13015.2, 13170.5],
               [13015.9, 13168]]
band_fits_p = [[13016.1, 13166.9],
               [13014.3, 13167.8],
               [13014.3, 13172],
               [13015.9, 13166.1],
               [13015.1, 13168]]
weight_flat = True  # Otherwise will do an exponential weighting
    
# Fitting Parameters
# angle_p = 34.87
# angle_y = -35.1
angle_p = 36.5085
angle_y = -36.698
molec_to_fit = ['O2']  # must be a list
isotopes = [[1]] # format [[1],[2]]
press_guesss = [1.170, 1.371, 0.926, 1.176, 1.188]  # atm
conc_h2os = [0.0203, 0.0213, 0.0197, 0.0229, 0.0290]
# press_guesss = [1.189]
temp_guess = 600
vel_guess = 1500
shift_guess = 0
conc_guess = 0.21
broad_guess = 1
temp_fit = True
press_fit = False
shift_fit = True
vel_fit = True
conc_fit = False
broad_fit = True

# Background Fitting Parameters

label_back = '5 061522'
if back_interp:
    label_back = 'BG Interp'

# Box Fitting Parameters
conc_box = 0.0042336
temp_box = 295
pres_box = 1
pl_box = 35

if back_remove:
    pl_back = 10
    if back_interp:
        df_bginter = pd.read_excel(fn_bginter,
                                  sheet_name='BroadCorrect 6.1.23',
                                  header=5)
    else:
        conc_back = 0.0373
        temp_back = 399.99
        press_back = 0.20173
else:
    conc_back = 0
    shift_back = 0
    press_back = 0
    temp_back = 298
    conc_box = 0
    pl_back = 0

    
###############################Port in data###################################
# get files from directory
# import filename key to get filenames
d_key = os.path.join(r'/Users/david/Library/CloudStorage/OneDrive-UCB-O365/AFRL/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData')
fn_key = 'FilenameKey.xlsx'
df_key = pd.read_excel(os.path.join(d_key,fn_key))
 
# prepare arrays for dataframe
fn_up = []
fn_down = []
vel = []
vel_unc = []
temp = []
temp_unc = []
press = []
press_unc = []
shift = []
shift_unc = []
conc = []
conc_unc = []
conc_backs = []
temp_backs = []
pres_backs = []
# broad = []
# broad_unc = []
# pl = []
# pl_unc = []

# organization of fits
if save_files:
    notes = input('Type down notes for this fit:')
    
    # workbook name
    name_tabulate = 'CrossedFits_v3.xlsx'
    fn_tabulate = os.path.join(d_save,name_tabulate)
    
    # load/create workbook
    if os.path.isfile(fn_tabulate):
        wb = load_workbook(fn_tabulate)
    else:
        wb = Workbook()
    
    # get time for labeling fit
    now = datetime.now()
    timestamp = now.strftime('%d_%m_%Y_%H%M%S')
    
    # folder to save data
    if save_files:
        d_save_0 = os.path.join(d_save,timestamp)
        if not(os.path.isdir(d_save_0)):
            os.mkdir(d_save_0)

i = 0
hapi.db_begin('')
for index in indexes:
    print(index)
    weight_window_y = weight_windows_y[i]
    weight_window_p = weight_windows_p[i]
    etalon_window_y = etalon_windows_y[i]
    etalon_window_p = etalon_windows_p[i]
    conc_h2o = conc_h2os[i]
    band_fit_y = band_fits_y[i]
    band_fit_p = band_fits_p[i]
    press_guess = press_guesss[i]
    print(band_fit_y, band_fit_p, weight_window_y, weight_window_p)
    i += 1
    # Grab filenames from key
    row = df_key.loc[df_key['Indexx']==index]
    vis2_daq = str(row.iloc[0]['vis2 DAQ'])
    vis2_fn = str(row.iloc[0]['vis2 fn'])
    d_vis2 = os.path.join(d_data,vis2_daq)
    vis3_daq = str(row.iloc[0]['vis3 DAQ'])
    vis3_fn = str(row.iloc[0]['vis3 fn'])
    d_vis3 = os.path.join(d_data,vis3_daq)

    # if not(index==10):
    #     continue
    # find vis 2 data
    vis2_found = False
    # search through moose directory for trans file
    for folder in os.listdir(d_vis2):
        if os.path.isdir(os.path.join(d_vis2,folder)):
            for sub_folder in os.listdir(os.path.join(d_vis2,folder)):
                if os.path.isdir(os.path.join(d_vis2,folder,sub_folder)) and \
                    sub_folder==d_pc:
                    for file in os.listdir(os.path.join(d_vis2,folder,
                                                        sub_folder)):
                        if file == vis2_fn+'_trans.txt':
                            fn_y = os.path.join(d_vis2,folder,sub_folder,file)
                            vis2_found = True
                            print(os.path.join(d_vis2,folder,sub_folder,file))
                            break
                if vis2_found:
                    break
            if vis2_found:
                break
    if not vis2_found:
        print('vis2 file not found!')
        continue
    
    # find vis 3 data
    vis3_found = False
    # search through moose directory for trans file
    for folder in os.listdir(d_vis3):
        if os.path.isdir(os.path.join(d_vis3, folder)):
            for sub_folder in os.listdir(os.path.join(d_vis3,folder)):
                if os.path.isdir(os.path.join(d_vis3,folder,sub_folder)) and \
                        sub_folder==d_pc:
                    for file in os.listdir(os.path.join(d_vis3,folder,
                                                        sub_folder)):
                        if file == vis3_fn+'_trans.txt':
                            fn_p = os.path.join(d_vis3,folder,sub_folder,file)
                            vis3_found = True
                            print(os.path.join(d_vis3,folder,sub_folder,file))
                            break
                if vis3_found:
                    break
            if vis3_found:
                break
    if not vis3_found:
        print('vis3 file not found!')
        continue

    # get background parameters for bgd interp
    if back_interp:
        row = df_bginter.loc[df_bginter['Run']==index]
        conc_back = row.iloc[0]['Conc.']
        temp_back = row.iloc[0]['Temp. (K)']
        press_back = row.iloc[0]['Pres. (atm)']

    fn_down.append(fn_y)
    fn_up.append(fn_p)

    # directory to save results
    if save_files:
        d_save_1 = os.path.join(d_save_0, str(index))
        if not(os.path.isdir(d_save_1)):
            os.mkdir(d_save_1)
    else:
        d_save_1 = ''
    
    x_y, x_p, absflow_y, absflow_p, vel_val, vel_err, temp_val, temp_err, press_val, press_err, shift_val,\
        shift_err, conc_val, conc_err, broad_val, broad_err = \
            fithit.FitProgram(fn_y, fn_p, d_save_1, file_tag,
                              weight_window_y, weight_window_p, weight_flat, etalon_window_y,
                              etalon_window_p,
                              angle_y, angle_p, band_fit_y,band_fit_p,
                              molec_to_fit, isotopes, 
                              press_fit, temp_fit, vel_fit,
                              shift_fit, conc_fit, broad_fit,
                              press_guess, temp_guess, vel_guess,
                              shift_guess, conc_guess, broad_guess,
                              conc_h2o=conc_h2o,
                              conc_box=conc_box, temp_box=temp_box,
                              pres_box=pres_box, pl_box=pl_box,
                              background_remove=back_remove,
                              pathlength_b=pl_back,
                              press_back=press_back, temp_back=temp_back,
                              conc_back_y=conc_back, conc_back_p=conc_back,
                              save_files=save_files,
                              plot_results=plot_results,
                              transUnits=True)
    vel.append(vel_val)
    vel_unc.append(vel_err)
    temp.append(temp_val)
    temp_unc.append(temp_err)
    press.append(press_val)
    press_unc.append(press_err)
    shift.append(shift_val)
    shift_unc.append(shift_err)
    conc.append(conc_val[0])
    conc_unc.append(conc_err[0])
    conc_backs.append(conc_back)
    temp_backs.append(temp_back)
    pres_backs.append(press_back)
    
    if save_files:
    
        # load/create worksheet
        try:
            ws = wb[str(index)]
        except:
            ws = wb.create_sheet(str(index))
            # create header
            header = ['INDEX','TIME','DAQ Up','DAQ Down','Fn Up','Fn Down',
                      'WEIGHT_FLAT','WEIGHT_WIN','ETA_up','ETA_P','BW',
                      'MOLECULES',
                      'VEL_FIT','VEL_VAL','VEL_ERR','VEL_GUES',
                      'TEMP_FIT','TEMP_VAL','TEMP_ERR','TEMP_GUES',
                      'PRES_FIT','PRES_VAL','PRES_ERR','PRES_GUES',
                      'SHIFT_FIT','SHIFT_VAL','SHIFT_ERR','SHIFT_GUES',
                      'CONC_FIT','CONC_VAL','CONC_ERR','CONC_GUES',
                      'BROAD_FIT','BROAD_VAL','BROAD_ERR','BROAD_GUES',
                      'BACK_REMOVE','BACK_LABEL','BACK_TEMP','BACK_PRES',
                      'BACK_CONC_1','BACK_CONC_2',
                      'BOX_TEMP','BOX_PRES','BOX_CONC',
                      'TEMP_BACK','PRES_BACK','CONC_BACK','NOTES']
            ws.append(header)
        # add data from current run
        datawrite = [str(index), timestamp, vis3_daq, vis2_daq,
                     vis3_fn, vis2_fn, str(weight_flat),
                     str([weight_window_y,weight_window_p]), str(etalon_windows_p),
                     str(etalon_windows_y),
                     str([band_fit_y, band_fit_p]), str(molec_to_fit),
                     str(vel_fit), vel_val, vel_err, vel_guess,
                     str(temp_fit), temp_val, temp_err, temp_guess,
                     str(press_fit), press_val, press_err, press_guess,
                     str(shift_fit), shift_val, shift_err, shift_guess,
                     str(conc_fit), str(conc_val), str(conc_err),
                     str(conc_guess),
                     str(broad_fit), broad_val, broad_err, broad_guess,
                     str(back_remove), label_back, temp_back, press_back,
                     conc_back, conc_back,
                     temp_box, pres_box, conc_box,
                     temp_back, press_back, conc_back, notes]
        ws.append(datawrite)
        # save workbook
        wb.save(fn_tabulate)
        
# Create dict of results
d_fit = {'Fn Down': fn_down,
         'Fn Up': fn_up,
         'Vel. (m/s)': vel,
         'Vel. Unc.': vel_unc,
         'Temp. (K)': temp,
         'Temp. Unc.': temp_unc,
         'Pres. (atm)': press,
         'Pres. Unc.': press_unc,
         'Shift (cm-1)': shift,
         'Shift Unc.': shift_unc,
         'Conc.': conc,
         'Conc. Unc': conc_unc,
         'Temp. Back': temp_backs,
         'Pres. Back': pres_backs,
         'Conc. Back': conc_backs}

df_fit = pd.DataFrame.from_dict(d_fit)
if save_files:
    pickle.dump(df_fit, open(os.path.join(d_save_0,'df_fit.p'),'wb'))

print('Fitting Done!')