#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Bandwidth optimization for noisy AFRL March Campaign data

This code will go through a variety of bandwidths on a given dataset which will
bring down the baseline the quickest

Below when I say 'residual' I just mean the area underneath the absolute of
cepstrum, which in certain windows is mostly residual.

Created on Thu Apr 28 11:20:19 2022

@author: david
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from statsmodels.nonparametric.kernel_regression import KernelReg

from packfind import find_package
find_package('pldspectrapy')
import td_support as td

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

def OptimizeBL(wvn, tra,
               crit='score',
               bd_start=[13014, 13017],
               bd_end=[13166, 13173],
               bd_increment=0.1,
               smooth_method='savgol',
               band_check=[0.009, 0.030],
               thres=4e-6,
               thres_range=20,
               thres_bl=0.008,
               smooth_len=49,
               smooth_deg=4,
               mess_start=0.008,
               mess_end=0.014,
               weight=0.4,
               plot=False):
    """

    @param wvn: wavenumber axis
    @param tra: transmission spectrum
    @param crit: criteria to determine best bandwidth {'res', 'blend', 'score'}
    @param bd_start: range of where to start bandwidth
    @param bd_end: range of where to end bandwidth
    @param bd_increment: what increments of bandwidth beginnings and ends to try
    within range
    @param smooth_method: smoothing method {'savgol', 'convolve', 'kernel_reg'}
    @param band_check: band in time domain to check over
    @param thres: point where data is has no baseline in it
    @param thres_range: how many points need to be under threshold to declare bl end
    @param thres_bl: only look for baseline end after this point in cepstrum
    @param smooth_len: how many points for savgol to use, must be odd
    @param smooth_deg: degree of smoothing method
    @param mess_start: point in IG to start looking at messiness
    @param mess_end: point in IG to stop looking at messiness
    @param weight: weight to give to messiness over baseline ends
    @param plotBL: plot visualization of bandwidth optimization
    @return: the optimal bandwidth and corresponding baseline start
    """
    #%% Define bandwidths to test
    # input range of band beginnings and ends, considering power and A band limits
    # you probably want to say within 13010 and 13170


    # what increments of bandwidth beginnings and ends to try within range


    # arrays of band starts and ends as prescribed by parameters above
    bd_starts = np.arange(bd_start[0], bd_start[1], bd_increment)
    bd_ends = np.arange(bd_end[0], bd_end[1], bd_increment)

    # create array of every possible combination of bd_start and bd_end to try out
    bds = []
    for i in np.arange(len(bd_starts)):
        for j in np.arange(len(bd_ends)):
            bds.append([bd_starts[i], bd_ends[j]])
    bds = np.array(bds)

    #%% Find baseline ends of various bandwiths
    bl_ends = []
    res = []
    count = 0
    bds_actual = []
    messiness = []
    score = []  # array for weighted scores

    #%% find baseline starts and messiness for different bandwidths
    for bd in bds:
        # optimize band for FFT
        start, stop = td.bandwidth_select_td(wvn, bd)
        bds_actual.append([wvn[start], wvn[stop]])
        # cut down to designated band
        wvn_temp = np.array(wvn[start:stop])
        tra_temp = np.array(tra[start:stop])
        # transform to absorbance
        abs_temp = -np.log(tra_temp)
        # transform to cepstrum
        cep_temp = np.fft.irfft(abs_temp)
        # smooth cepstrum, use abs here as baseline can be negative or positive due
        # to etalon

        if smooth_method == 'savgol':
            cep_smooth = abs(savgol_filter(cep_temp,smooth_len,smooth_deg)) # use 30 to try to
                                                        # smooth over random bump
        elif smooth_method == 'convolve':
            kernal = np.ones(smooth_len)/smooth_len
            cep_smooth = abs(np.convolve(cep_temp, kernal, mode='same'))
        # find residual in band check range
        res.append(np.sum(cep_smooth[int(band_check[0]*len(cep_temp)):
                                         int(band_check[1]*len(cep_temp))]))
        # find point where data is consistently below threshold
        for i in np.arange(len(cep_smooth)):
            # don't include points below threshold
            if i < thres_bl*len(cep_smooth):
                continue
            # end operation if reach near the end of the cepstrum
            if i == len(cep_smooth)-thres_range:
                bl_ends.append(i/len(cep_temp))
                break
            # if next number of points all under thres than define as baseline end
            if all(cep_smooth[i:i+thres_range]<np.ones(thres_range)*thres):
                bl_ends.append(i/len(cep_temp))
                break

        # Calculate messiness
        cep_mess = cep_temp - cep_smooth
        messiness.append(np.mean(abs(cep_mess[int(mess_start*len(cep_mess)):
                                              int(mess_end*len(cep_mess))])))
        # # calculate score: norm of messiness and bl_end
        # score.append(np.linalg.norm([bl_ends[-1], messiness[-1]]))

        # print counter
        count += 1
        if np.mod(count,100) == 0:
            print('Checked '+str(count)+' / '+str(len(bds)))
    bds_actual = np.array(bds_actual)
    bl_ends = np.array(bl_ends)
    if crit == 'res':
        # %% Display optimal bandwidth
        # find bandwidth that leads to lowest residual
        res_min = np.min(res)
        res_min_arg = np.argmin(res)
        bd_resmin_actual = bds_actual[res_min_arg]
        bd_resmin = bds[res_min_arg]
        blend_resmin = bl_ends[res_min_arg]
        print('# ' + str(res_min_arg) + ' ' + str(bd_resmin) + ' ' +
              str(bd_resmin_actual) +
              ' has lowest residual: ' + str(res_min) +
              '\n' + 'with baseline end: ' + str(round(blend_resmin, 4)))

        # returns
        optimal_bd = bd_resmin
        optimal_bl = blend_resmin
    elif crit == 'blend':
        # %% find bandwidth that leads to lowest bl_end
        bl_end_min = np.min(bl_ends)
        bl_end_min_arg = np.argmin(bl_ends)
        bd_blendmin_actual = bds_actual[bl_end_min_arg]
        bd_blendmin = bds[bl_end_min_arg]
        print('# ' + str(bl_end_min_arg) + ' ' + str(bd_blendmin) + ' ' +
              str(bd_blendmin_actual) + ' has earliest baseline end: ' +
              str(bl_end_min))

        # returns
        optimal_bd = bd_blendmin
        optimal_bl = bl_end_min
    elif crit == 'score':
        #%% normalize messiness to bl end
        messiness = np.array(messiness)
        messiness = weight*np.min(bl_ends)/np.min(messiness)*messiness
        score = np.linalg.norm([bl_ends, messiness], axis=0)
        #%% find bandwidth that leads to lowest combination of bl_end and messiness
        score_min = np.min(score)
        score_min_arg = np.argmin(score)
        bd_scoremin_actual = bds_actual[score_min_arg]
        bd_scoremin = bds[score_min_arg]
        blend_scoremin = bl_ends[score_min_arg]
        print('# '+str(score_min_arg) + ' ' +str(bd_scoremin) + ' ' +
              str(bd_scoremin_actual)+' has best score: ' +
              str(round(score_min, 4)) +
              '\n' + 'with baseline end: ' + str(round(blend_scoremin, 4)))

        # returns
        optimal_bd = bd_scoremin
        optimal_bl = blend_scoremin

    #%% plot of residuals by bandstart
    # fig, ax1 = plt.subplots()
    # ax1.plot(bds_actual[:,0],res,'.')
    # ax1.set_ylabel('Residuals')
    # ax1.set_xlabel('Band Start (cm-1)')
    # ax1.xaxis.label.set_color('blue')
    # ax2 = ax1.twiny()
    # ax2.plot(bds_actual[:,1],res,'.r')
    # ax2.set_xlabel('Band End (cm-1)')
    # ax2.xaxis.label.set_color('red')

    #%% plot of bl ends by bandstart
    # fig, ax1 = plt.subplots()
    # ax1.plot(bds_actual[:,0],bl_ends,'.')
    # ax1.set_ylabel('Baseline Ends')
    # ax1.set_xlabel('Band Start (cm-1)')
    # ax1.xaxis.label.set_color('blue')
    # ax2 = ax1.twiny()
    # ax2.plot(bds_actual[:,1],bl_ends,'.r')
    # ax2.set_xlabel('Band End (cm-1)')
    # ax2.xaxis.label.set_color('red')

    #%% plot of bl ends by bandwidth
    # fig, ax1 = plt.subplots()
    # bd_widths = bds_actual[:,1] - bds_actual[:,0]
    # ax1.plot(bd_widths,bl_ends,'.')
    # ax1.set_ylabel('Baseline Ends')
    # ax1.set_xlabel('Bandwidth (cm-1)')
    # ax1.xaxis.label.set_color('blue')

    #%% plot comparison of worst res vs best res
    if plot:
        # calculate worst res cepstrum
        res_max_arg = np.argmax(res)
        start, stop = td.bandwidth_select_td(wvn, bds[res_max_arg])
        bd_resmax = np.array(bds_actual[res_max_arg])
        wvn_worst = np.array(wvn[start:stop])
        tra_worst = np.array(tra[start:stop])
        abs_worst = -np.log(tra_worst)
        cep_worst = np.fft.irfft(abs_worst)
        # if smooth_method == 'savgol':
        #     cep_worst_smooth = abs(savgol_filter(cep_worst,smooth_len,smooth_deg)) # use 30 to try to
        #                                                 # smooth over random bump
        # elif smooth_method == 'convolve':
        #     kernal = np.ones(smooth_len)/smooth_len
        #     cep_worst_smooth = abs(np.convolve(cep_worst, kernal, mode='same'))


        # calculate best res cepstrum
        res_min_arg = np.argmin(res)
        start, stop = td.bandwidth_select_td(wvn, bds[res_min_arg])
        bd_resmin = np.array(bds_actual[res_min_arg])
        wvn_best = np.array(wvn[start:stop])
        tra_best = np.array(tra[start:stop])
        abs_best = -np.log(tra_best)
        cep_best = np.fft.irfft(abs_best)
        # if smooth_method == 'savgol':
        #     cep_best_smooth = abs(savgol_filter(cep_best,smooth_len,smooth_deg)) # use 30 to try to
        #                                                 # smooth over random bump
        # elif smooth_method == 'convolve':
        #     kernal = np.ones(smooth_len)/smooth_len
        #     cep_best_smooth = abs(np.convolve(cep_best, kernal, mode='same'))

        # plot
        plt.figure()
        plt.plot(np.linspace(0,1,len(cep_worst)),cep_worst,'b',
                 label='Worst, '+str(bd_resmax.round(3)))
        plt.plot(np.linspace(0,1,len(cep_best)),cep_best,'r',
                 label='Best, '+str(bd_resmin.round(3)))
        # plt.axvspan(band_check[0],band_check[1],color='skyblue')
        plt.title('Best/Worst Bandwidth by Residual')
        plt.legend()

        #%% plot comparison of worst bl end and best bl end
        # calculate worst bl end cepstrum
        bl_end_max_arg = np.argmax(bl_ends)
        bl_end_max = np.max(bl_ends)
        bd_blendmax = np.array(bds_actual[bl_end_max_arg])
        start, stop = td.bandwidth_select_td(wvn, bds[bl_end_max_arg])
        wvn_worst = np.array(wvn[start:stop])
        tra_worst = np.array(tra[start:stop])
        abs_worst = -np.log(tra_worst)
        cep_worst = np.fft.irfft(abs_worst)
        # if smooth_method == 'savgol':
        #     cep_worst_smooth = abs(savgol_filter(cep_worst,smooth_len,smooth_deg)) # use 30 to try to
        #                                                 # smooth over random bump
        # elif smooth_method == 'convolve':
        #     kernal = np.ones(smooth_len)/smooth_len
        #     cep_worst_smooth = abs(np.convolve(cep_worst, kernal, mode='same'))

        # calculate best res cepstrum
        bl_end_min_arg = np.argmin(bl_ends)
        bl_end_min = np.min(bl_ends)
        bd_blendmin = np.array(bds_actual[bl_end_min_arg])
        start, stop = td.bandwidth_select_td(wvn, bds[bl_end_min_arg])
        wvn_best = np.array(wvn[start:stop])
        tra_best = np.array(tra[start:stop])
        abs_best = -np.log(tra_best)
        cep_best = np.fft.irfft(abs_best)
        # if smooth_method == 'savgol':
        #     cep_best_smooth = abs(savgol_filter(cep_best,smooth_len,smooth_deg)) # use 30 to try to
        #                                                 # smooth over random bump
        # elif smooth_method == 'convolve':
        #     kernal = np.ones(smooth_len)/smooth_len
        #     cep_best_smooth = abs(np.convolve(cep_best, kernal, mode='same'))

        # plot
        plt.figure()
        plt.plot(np.linspace(0,1,len(cep_worst)),cep_worst,'b',
                 label='Worst, '+str(bd_blendmax.round(3)))
        plt.plot(np.linspace(0,1,len(cep_best)),cep_best,'r',
                 label='Best, '+str(bd_blendmin.round(3)))
        plt.vlines(bl_end_max,-1,3,colors='blue',ls=':')
        plt.vlines(bl_end_min,-1,3,colors='red',ls=':')
        plt.title('Best/Worst Bandwidth by Baseline End')
        plt.legend()
        plt.show()

        #%% plot comparison of worst score and best score
        # calculate worst bl end cepstrum
        score_max_arg = np.argmax(score)
        score_max = np.max(score)
        bd_scoremax = np.array(bds_actual[score_max_arg])
        start, stop = td.bandwidth_select_td(wvn, bds[score_max_arg])
        wvn_worst = np.array(wvn[start:stop])
        tra_worst = np.array(tra[start:stop])
        abs_worst = -np.log(tra_worst)
        cep_worst = np.fft.irfft(abs_worst)
        # if smooth_method == 'savgol':
        #     cep_worst_smooth = abs(savgol_filter(cep_worst,smooth_len,smooth_deg)) # use 30 to try to
        #                                                 # smooth over random bump
        # elif smooth_method == 'convolve':
        #     kernal = np.ones(smooth_len)/smooth_len
        #     cep_worst_smooth = abs(np.convolve(cep_worst, kernal, mode='same'))

        # calculate best res cepstrum
        score_min_arg = np.argmin(score)
        score_min = np.min(score)
        bd_scoremin = np.array(bds_actual[score_min_arg])
        start, stop = td.bandwidth_select_td(wvn, bds[score_min_arg])
        wvn_best = np.array(wvn[start:stop])
        tra_best = np.array(tra[start:stop])
        abs_best = -np.log(tra_best)
        cep_best = np.fft.irfft(abs_best)
        # if smooth_method == 'savgol':
        #     cep_best_smooth = abs(savgol_filter(cep_best,smooth_len,smooth_deg)) # use 30 to try to
        #                                                 # smooth over random bump
        # elif smooth_method == 'convolve':
        #     kernal = np.ones(smooth_len)/smooth_len
        #     cep_best_smooth = abs(np.convolve(cep_best, kernal, mode='same'))

        # plot
        plt.figure()
        plt.plot(np.linspace(0,1,len(cep_worst)),cep_worst,'b',
                 label='Worst, '+str(bd_scoremax.round(3)))
        plt.plot(np.linspace(0,1,len(cep_best)),cep_best,'r',
                 label='Best, '+str(bd_scoremin.round(3)))
        plt.title('Best/Worst Bandwidth by Score')
        plt.legend()

        #%% plot messiness verse bl ends
        plt.figure()
        plt.scatter(bl_ends, messiness)
        plt.scatter(bl_ends[score_min_arg], messiness[score_min_arg], color='green')
        plt.scatter(bl_ends[score_max_arg], messiness[score_max_arg], color='red')
        plt.xlabel('Baseline Ends')
        plt.ylabel('Messiness')
        plt.show()

    return optimal_bd, optimal_bl

if __name__ == "__main__":
    # %% load data in
    ## directory info to find transmission file
    d = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                     'Mar2022CampaignData/cubig/031122/pc_peter_ig')
    f = 'boxall_trans.txt'

    data = np.loadtxt(os.path.join(d, f))
    wvn = data[:, 0]
    tra = data[:, 1]
    bd, bl = OptimizeBL(wvn, tra, plot=True)