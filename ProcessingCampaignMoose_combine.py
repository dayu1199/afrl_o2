#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code to combine phasecorrect file 17 and 18 together

Created on Tue Mar 22 15:28:15 2022

@author: david
"""
# Package imports
import os
import time
import numpy as np
import matplotlib.pyplot as plt

from packfind import find_package
find_package("pldspectrapy")
import pldspectrapy as pld
import td_support as td

# Plot params
plt.rcParams.update({'font.family': 'Times New Roman',
                     "figure.autolayout": True,
                     "lines.linewidth": 0.8})

# Code switches and params
pclim_low = 0.12
pclim_high = 0.18
savedata = True
plotdata = True
fitallan = False
ig_num = 1 # how many averaged igs to process, if 0 or greater than available
            #igsjust do all

# directory info
d = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                 'Mar2022CampaignData/moose', '031122_vis3')
fname = {'20220311212426', '20220311213143'} # leave off .txt
fn_save = '11combine_trans.txt'
save_folder_tag = 'pc_peter'

# Frequency axis calculation
lockfreq = 32e6
nomfreq = 13333  # Should be within the range specified by band_fit (below)

# open up directory
t0 = time.time()
files = pld.open_dir(d, recursion_levels=2, verbose=2) #######
dt = time.time() - t0
print("Opened %i files in %0.3f seconds" % (len(files), dt))
newest = ""



# create save directory
d_save = os.path.join(d,save_folder_tag)
if not os.path.isdir(d_save):
    os.mkdir(d_save)

#%% pull data
# array to hold combined data
data_hold = []
# Loop through directory and process data
for name, daq_file in files.items():
    
    # Create a couple variables used for determining processing flow
    pass_pc = 0
    pass_raw = 0

    if name not in fname:
        continue
        
    # Make sure there's some data
    if not daq_file.failure:
    
        # Are there raw IGMs?
        if daq_file.data_raw is not None:
            non_empty_raw = len(daq_file.data_raw)
            pass_raw = 1 if (non_empty_raw > 0) else 0
    
        # As long as there's some IGMs go ahead and start processing!
        if pass_raw:
            newest = max(newest, name)
            print(name)

            # concatenate data
            data_hold.append(daq_file.data_raw.copy())
            
            # pull wavenumber axis.
            x_wvn_full = pld.mobile_axis2(daq_file, f_opt=lockfreq,
                                          wvn_spectroscopy=nomfreq)

    else:
        print('No data in ' + name)

#%% combine data        
# concatenate the two datasets
data = np.concatenate((data_hold[0], data_hold[1]))

#%% phase correct
trans = pld.pc_peter(data, pclim_low, pclim_high, plot=True,
                     zoom=500)
pc_ig = np.fft.ifft(trans)

#%% plot data
lambda_full = 1e7/x_wvn_full 
if plotdata:
    fig,ax = plt.subplots(2,1)
    ax[0].plot(pc_ig)
    ax[0].set_xlabel('Time step')
    ax[0].set_ylabel('Power')
    ax[1].plot(lambda_full[:len(trans)],trans)
    ax[1].set_xlabel('Wavelength (cm-1)')
    ax[1].set_ylabel('Transmission')
    plt.suptitle(name)
# save file
save_data = np.vstack((x_wvn_full[:len(trans)],trans))
save_data = np.transpose(save_data)

np.savetxt(os.path.join(d_save,fn_save),save_data)

                    
    
