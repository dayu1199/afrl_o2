#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code to run fits of crossed bkgd simulations

Created on Tue May 10 14:26:02 2022

@author: david
"""

import os
import numpy as np
import pandas as pd
import pickle
from datetime import datetime

import FitProgramBkgd2BeamwBox as fithit

##################################Fitting Parameters###########################

# Fitting Options
save_files = True # saves fits and fitted parameters in text files.
plot_results = False # Plots time domain and TD fit converted to the frequency domain. Turn this off for many datafiles.
print_fit_report = True # Prints each fit report to command window 
back_remove = True # Remove background from spectra before fitting
label_back = ''
file_tag = ''

noise_add = True
noise_level = 0.0013
iterations = 1

import_boxfit = True
boxfit_d = os.path.join(r'/Users/david/Library/CloudStorage',
                        r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                        r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits',
                        r'sim/box/07_12_2022_094911')
if import_boxfit:
    with open(os.path.join(boxfit_d, 'df_fit.p'), 'rb') as f:
        df_box = pickle.load(f)
    iterations = len(df_box)
else:
    conc_guess_box = 0.0042
    temp_guess_box = 295
    pres_guess_box = 1

warm_flow = True

# Directory Info`
d_data = ''
d_save = os.path.join(r'/Users/david/Library/CloudStorage',
                      r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                      r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits',
                      r'sim/bkgdcross')

# MFID Parameters
weight_window_y = [0.0082, 0.5] # default [0.005,0.5]
weight_window_p = [0.0097, 0.5]
etalon_windows_y = [] # format [[0,100],[200,300]]
etalon_windows_p = []
band_fit_y = [13015.3, 13171.8]
band_fit_p = [13015.3, 13167.4]
weight_flat = True # Otherwise will do an exponential weighting
    
# Fitting Parameters
# Fitting Parameters
angle_p = 35
angle_y = -35
molec_to_fit = ['O2'] # must be a list
isotopes = [[1]] # format [[1],[2]]

if warm_flow:
    pres_guess_in = 0.888
    temp_guess_in = 317
    vel_guess_in = 653
    conc_guess_in = 0.21
    pres_guess_out = 0.5  # atm
    temp_guess_out = 300  # K
    conc_guess_out = 0.05
    shift_guess = 0
    broad_guess = 1
    pres_fit_in = False
    temp_fit_in = False
    vel_fit_in = False
    conc_fit_in = False
    pres_fit_out = True
    temp_fit_out = True
    conc_fit_out = True
    pl_fit_out = False
    shift_fit = True
    broad_fit = False
    pl_guess_out = 10
else:
    pres_guess_in = 0.1947 # atm
    temp_guess_in = 200 # K
    vel_guess_in = 0 # m/s
    conc_guess_in = 0.21
    pres_guess_out = 2 # atm
    temp_guess_out = 200 # K
    conc_guess_out = 0.05
    pl_guess_out = 45 # cm
    broad_guess = 1
    shift_guess = 0
    
    pres_fit_in = False
    temp_fit_in = True
    vel_fit_in = False
    conc_fit_in = False
    pres_fit_out = True
    temp_fit_out = True
    conc_fit_out = True
    pl_fit_out = False   
    shift_fit = True
    broad_fit = False
pl_box = 35
    
# Prepare save folder
now = datetime.now()
timestamp = now.strftime('%d_%m_%Y_%H%M%S')
save_path_0 = os.path.join(d_save,timestamp)
if not(os.path.isdir(save_path_0)):
    os.mkdir(save_path_0)
    
###############################Port in data###################################
 
# prepare arrays for dataframe
fn_y = []
fn_p = []
temp_in = []
temp_in_unc = []
temp_out = []
temp_out_unc = []
pres_in = []
pres_in_unc = []
pres_out = []
pres_out_unc = []
shift = []
shift_unc = []
broad = []
broad_unc = []
vel_in = []
vel_in_unc = []
pl_out = []
pl_out_unc = []
conc_in = []
conc_in_unc = []
conc_out = []
conc_out_unc = []
pxlt_out = []

# organization of fits
for i in range(iterations):
    print('iteration',i)
    fn_y = 'O2simulation_9bkgd_down.txt'
    fn_p = 'O2simulation_9bkgd_up.txt'
    save_path = os.path.join(save_path_0,str(i))
    if not(os.path.isdir(save_path)):
        os.mkdir(save_path)
    save_path = save_path

    if import_boxfit:
        conc_guess_box = df_box['Conc.'][i]
        temp_guess_box = df_box['Temp. (K)'][i]
        pres_guess_box = df_box['Pres. (atm)'][i]
    
    return_array = fithit.FitProgram(fn_y, fn_p, save_path, file_tag,
                             weight_window_y, weight_window_p, weight_flat,
                             etalon_windows_y, etalon_windows_p,
                             angle_y, angle_p, band_fit_y, band_fit_p,
                             molec_to_fit, isotopes,
                             shift_fit, shift_guess,
                             pres_fit_in, temp_fit_in, vel_fit_in,
                             conc_fit_in,
                             pres_fit_out, temp_fit_out, conc_fit_out,
                             pl_fit_out,
                             pres_guess_in, temp_guess_in, vel_guess_in,
                             conc_guess_in,
                             pres_guess_out, temp_guess_out, conc_guess_out,
                             pl_guess_out,
                             broad_fit, broad_guess,
                             conc_guess_box, temp_guess_box, pres_guess_box,
                             save_files=save_files, plot_results=plot_results,
                             print_fit_report=print_fit_report,
                             transUnits=False,
                             noise_add=noise_add, noise_level=noise_level)
    
    temp_in.append(return_array[0])
    temp_in_unc.append(return_array[1])
    temp_out.append(return_array[2])
    temp_out_unc.append(return_array[3])
    pres_in.append(return_array[4])
    pres_in_unc.append(return_array[5])
    pres_out.append(return_array[6])
    pres_out_unc.append(return_array[7])
    shift.append(return_array[8])
    shift_unc.append(return_array[9])
    broad.append(return_array[10])
    broad_unc.append(return_array[11])
    vel_in.append(return_array[12])
    vel_in_unc.append(return_array[13])
    pl_out.append(return_array[14])
    pl_out_unc.append(return_array[15])
    conc_in.append(return_array[16])
    conc_in_unc.append(return_array[17])
    conc_out.append(return_array[18][0])
    conc_out_unc.append(return_array[19])
    pxlt_out.append(pres_out[-1]*conc_out[-1]*pl_guess_out/temp_out[-1])

#%% save data
# Create dict of results
d_fit = {'Vel. In (m/s)': vel_in,
         'Vel. In Unc.': vel_in_unc,
         'Temp. In (K)': temp_in,
         'Temp. In Unc.': temp_in_unc,
         'Temp. Out (K)': temp_out,
         'Temp. Out Unc.': temp_out_unc,
         'Pres. In (atm)': pres_in,
         'Pres. In Unc.': pres_in_unc,
         'Pres. Out (atm)': pres_out,
         'Pres. Out Unc.': pres_out_unc,
         'Shift (cm-1)': shift,
         'Shift Unc.': shift_unc,
         'Conc. In': conc_in,
         'Conc. In Unc': conc_in_unc,
         'Conc. Out': conc_out,
         'Conc. Out Unc': conc_out_unc,
         'PXLT': pxlt_out}

df_fit = pd.DataFrame.from_dict(d_fit)
if save_files:
    pickle.dump(df_fit, open(os.path.join(save_path_0,'df_fit.p'),'wb'))
    if iterations > 1:
        if vel_fit_in:
            vel_std_in = np.std(vel_in)
            vel_ave_in = np.mean(vel_in)
            print('Velocity In mean is '+str(vel_ave_in))
            print('Velocity In uncertainty is '+str(vel_std_in))
            np.savetxt(os.path.join(save_path_0,'vel_in.txt'),vel_in)
        if conc_fit_in:
            conc_std_in = np.std(conc_in)
            conc_ave_in = np.mean(conc_in)
            print('Concentration In mean is '+str(conc_ave_in))
            print('Concentration In uncertainty is '+str(conc_std_in))
            np.savetxt(os.path.join(save_path_0,'conc_in.txt'),conc_in)
        if conc_fit_out:
            conc_std_out = np.std(conc_out)
            conc_ave_out = np.mean(conc_out)
            print('Concentration Out mean is '+str(conc_ave_out))
            print('Concentration Out uncertainty is '+str(conc_std_out))
            np.savetxt(os.path.join(save_path_0,'conc_out.txt'),conc_out)
        # if press_fit_in:
        #     press_std_in = np.std(press_in)
        #     press_ave_in = np.mean(press_in)
        #     print('Pressure In mean is '+str(press_ave_in))
        #     print('Pressure In uncertainty is '+str(press_std_in))
        #     np.savetxt(os.path.join(save_path_0,'press_in.txt'),press_in)
        if pres_fit_out:
            pres_std_out = np.std(pres_out)
            pres_ave_out = np.mean(pres_out)
            print('Pressure Out mean is '+str(pres_ave_out))
            print('Pressure Out uncertainty is '+str(pres_std_out))
            np.savetxt(os.path.join(save_path_0,'pres_out.txt'),pres_out)
        if temp_fit_in:
            temp_std_in = np.std(temp_in)
            temp_ave_in = np.mean(temp_in)
            print('Temperature In mean is '+str(temp_ave_in))
            print('Temperature In uncertainty is '+str(temp_std_in))
            np.savetxt(os.path.join(save_path_0,'temp_in.txt'),temp_in)
        if temp_fit_out:
            temp_std_out = np.std(temp_out)
            temp_ave_out = np.mean(temp_out)
            print('Temperature Out mean is '+str(temp_ave_out))
            print('Temperature Out uncertainty is '+str(temp_std_out))
            np.savetxt(os.path.join(save_path_0,'temp_out.txt'),temp_out)
        if shift_fit:
            shift_std = np.std(shift)
            shift_ave = np.mean(shift)
            print('Shift mean is '+str(shift_ave))
            print('Shift uncertainty is '+str(shift_std))
            np.savetxt(os.path.join(save_path_0,'shift.txt'),shift)
        if conc_fit_out or press_fit_out:
            pxlt_std_out = np.std(pxlt_out)
            pxlt_ave_out = np.mean(pxlt_out)
            print('PXLT Out mean is '+str(pxlt_ave_out))
            print('PXLT Out uncertainty is '+str(pxlt_std_out))
            np.savetxt(os.path.join(save_path_0,'pxlt_out.txt'),pxlt_out)


print('Fitting Done!')