# -*- coding: utf-8 -*-
"""
Run Fitting for Crossed Beams for Mar2020 Campaign
vis2 downstream yellow
vis3 upstream purple

Created on Tue Sep 24 13:28:11 2019
@author: David
"""
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from openpyxl import Workbook, load_workbook
import pandas as pd
import pickle
from datetime import datetime
from lmfit import Model

from OptimizeBL import OptimizeBL
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
import td_support as td
import linelist_conversions as db
import labfithelp as lab
import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()
import FitProgram2BeamwBox as fithit
##################################Fitting Parameters###########################

# Fitting Options
save_files = True  # saves fits and fitted parameters in text files.
plot_results = False  # Plots time domain and TD fit converted to the frequency domain. Turn this off for many datafiles.
print_fit_report = True  # Prints each fit report to command window
back_remove = True  # Remove background from spectra before fitting
back_interp = True
boltzmann_fit = False
boltz_threshold = 1e-24  # filters out lines in boltzmann fit below threshold
label_back = ''

# Directory Info
d_data = os.path.join(r'/Users/david/Library/CloudStorage',
                      r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                      r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData')
d_pc = 'pc_peter'
d_save = os.path.join(r'/Users/david/Library/CloudStorage',
                      r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                      r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits',
                      r'crossed')
file_tag = ''  # remember to add a _ before
fn_bginter = os.path.join(r'/Users/david/Library/CloudStorage',
                          r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                          r'AFRL ETHOS/MarchCampaign/BkgdInterpolation.xlsx')

# which indexes to fit, just test runs
indexes = [5, 6, 7, 10, '11combined']  # [5, 6, 7, 10, '11combined']

# MFID Parameters
# weight_windows_y = [[0.00872,0.5]]
# weight_windows_p = [[0.00845,0.5]]
weight_windows_y = [[0.008, 0.5],
                    [0.0084, 0.5],
                    [0.008, 0.5],
                    [0.008, 0.5],
                    [0.0082, 0.5]]  # default [0.005,0.5]
weight_windows_p = [[0.0081, 0.5],
                    [0.0083, 0.5],
                    [0.008, 0.5],
                    [0.008, 0.5],
                    [0.0085, 0.5]]  # default [0.005,0.5]
etalon_windows_y = [[[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]]]  # format [[[0.1,0.15]],[[0.22,0.25]]]
etalon_windows_p = [[[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]]]
# band_fits_y = [[13011,13166]]
# band_fits_p = [[13017,13167]]
band_fits_y = [[13016.1, 13166.7],
               [13015.2, 13168],
               [13015.2, 13169],
               [13015.2, 13170.5],
               [13015.9, 13168]]
band_fits_p = [[13016.1, 13166.9],
               [13014.3, 13167.8],
               [13014.3, 13172],
               [13015.9, 13166.1],
               [13015.1, 13168]]
weight_flat = True  # Otherwise will do an exponential weighting
    
# Fitting Parameters
angle_p = 34.87
angle_y = -35.1
molec_to_fit = ['O2']  # must be a list
isotopes = [[1]] # format [[1],[2]]
press_guesss = [1.170, 1.371, 0.926, 1.176, 1.189]  # atm
# press_guesss = [1.189]
temp_guess = 600 + np.random.rand()*20
vel_guess = 1500 + np.random.rand()*20
shift_guess = 0 + np.random.rand()/100
# temp_guess = 600
# vel_guess = 1500
# shift_guess = 0
conc_guess = 0.21
broad_guess = 1
temp_fit = True
press_fit = False
shift_fit = True
vel_fit = True
conc_fit = False
broad_fit = False

# Background Fitting Parameters

label_back = '5 061522'
if back_interp:
    label_back = 'BG Interp'

# Box Fitting Parameters
conc_box = 0.00421491
temp_box = 295
pres_box = 1
pl_box = 35

if back_remove:
    pl_back = 10
    if back_interp:
        df_bginter = pd.read_excel(fn_bginter,
                                  sheet_name='8.11.22',
                                  header=5)
    else:
        conc_back = 0.01792
        temp_back = 399.69577
        press_back = 0.20124
else:
    conc_back = 0
    shift_back = 0
    press_back = 0
    temp_back = 298
    conc_box = 0
    pl_back = 0

    
###############################Port in data###################################
# get files from directory
# import filename key to get filenames
d_key = os.path.join(r'/Users/david/Library/CloudStorage',
                     r'GoogleDrive-dayu1199@colorado.edu/My Drive/AFRL ETHOS',
                     r'MarchCampaign')
fn_key = 'FilenameKey.xlsx'
df_key = pd.read_excel(os.path.join(d_key,fn_key))
 
# prepare arrays for dataframe
fn_up = []
fn_down = []
vel = []
vel_unc = []
temp = []
temp_unc = []
press = []
press_unc = []
shift = []
shift_unc = []
conc = []
conc_unc = []
conc_backs = []
temp_backs = []
pres_backs = []
# broad = []
# broad_unc = []
# pl = []
# pl_unc = []

# organization of fits
if save_files:
    notes = input('Type down notes for this fit:')
    
    # workbook name
    name_tabulate = 'CrossedFits_v3.xlsx'
    fn_tabulate = os.path.join(d_save,name_tabulate)
    
    # load/create workbook
    if os.path.isfile(fn_tabulate):
        wb = load_workbook(fn_tabulate)
    else:
        wb = Workbook()
    
    # get time for labeling fit
    now = datetime.now()
    timestamp = now.strftime('%d_%m_%Y_%H%M%S')
    
    # folder to save data
    if save_files:
        d_save_0 = os.path.join(d_save,timestamp)
        if not(os.path.isdir(d_save_0)):
            os.mkdir(d_save_0)

#%% Fitting Procedure
i = 0
fit_results_feature = {}  # for storing data from individual feature fits
fits_plot = ['temperature', 'pressure', 'molefraction', 'shift']
temp_boltz = []
temp_boltz_unc = []
c_boltz = []
c_boltz_unc = []

for index in indexes:
    print(index)
    weight_window_y = weight_windows_y[i]
    weight_window_p = weight_windows_p[i]
    etalon_window_y = etalon_windows_y[i]
    etalon_window_p = etalon_windows_p[i]
    band_fit_y = band_fits_y[i]
    band_fit_p = band_fits_p[i]
    press_guess = press_guesss[i]
    print(band_fit_y, band_fit_p, weight_window_y, weight_window_p)
    i += 1
    # if index != 7:
    #     continue
    # Grab filenames from key
    row = df_key.loc[df_key['Indexx']==index]
    vis2_daq = str(row.iloc[0]['vis2 DAQ'])
    vis2_fn = str(row.iloc[0]['vis2 fn'])
    d_vis2 = os.path.join(d_data,vis2_daq)
    vis3_daq = str(row.iloc[0]['vis3 DAQ'])
    vis3_fn = str(row.iloc[0]['vis3 fn'])
    d_vis3 = os.path.join(d_data,vis3_daq)

    # find vis 2 data
    vis2_found = False
    # search through moose directory for trans file
    for folder in os.listdir(d_vis2):
        if os.path.isdir(os.path.join(d_vis2,folder)):
            for sub_folder in os.listdir(os.path.join(d_vis2,folder)):
                if os.path.isdir(os.path.join(d_vis2,folder,sub_folder)) and \
                    sub_folder==d_pc:
                    for file in os.listdir(os.path.join(d_vis2,folder,
                                                        sub_folder)):
                        if file == vis2_fn+'_trans.txt':
                            fn_y = os.path.join(d_vis2,folder,sub_folder,file)
                            vis2_found = True
                            print(os.path.join(d_vis2,folder,sub_folder,file))
                            break
                if vis2_found:
                    break
            if vis2_found:
                break
    if not vis2_found:
        print('vis2 file not found!')
        continue
    
    # find vis 3 data
    vis3_found = False
    # search through moose directory for trans file
    for folder in os.listdir(d_vis3):
        if os.path.isdir(os.path.join(d_vis3, folder)):
            for sub_folder in os.listdir(os.path.join(d_vis3,folder)):
                if os.path.isdir(os.path.join(d_vis3,folder,sub_folder)) and \
                        sub_folder==d_pc:
                    for file in os.listdir(os.path.join(d_vis3,folder,
                                                        sub_folder)):
                        if file == vis3_fn+'_trans.txt':
                            fn_p = os.path.join(d_vis3,folder,sub_folder,file)
                            vis3_found = True
                            print(os.path.join(d_vis3,folder,sub_folder,file))
                            break
                if vis3_found:
                    break
            if vis3_found:
                break
    if not vis3_found:
        print('vis3 file not found!')
        continue

    # get background parameters for bgd interp
    if back_interp:
        row = df_bginter.loc[df_bginter['Run']==index]
        conc_back = row.iloc[0]['Conc.']
        temp_back = row.iloc[0]['Temp. (K)']
        press_back = row.iloc[0]['Pres. (atm)']

    fn_down.append(fn_y)
    fn_up.append(fn_p)

    # directory to save results
    if save_files:
        d_save_1 = os.path.join(d_save_0, str(index))
        if not(os.path.isdir(d_save_1)):
            os.mkdir(d_save_1)
    else:
        d_save_1 = ''
    
    wvn_y, wvn_p, abs_y, abs_p,vel_val, vel_err, temp_val, temp_err, \
    press_val, press_err, shift_val, shift_err, conc_val, conc_err, \
    broad_val, broad_err = \
            fithit.FitProgram(fn_y, fn_p, d_save_1, file_tag,
                              weight_window_y, weight_window_p, weight_flat, etalon_window_y,
                              etalon_window_p,
                              angle_y, angle_p, band_fit_y,band_fit_p,
                              molec_to_fit, isotopes, 
                              press_fit, temp_fit, vel_fit,
                              shift_fit, conc_fit, broad_fit,
                              press_guess, temp_guess, vel_guess,
                              shift_guess, conc_guess, broad_guess,
                              conc_box=conc_box, temp_box=temp_box,
                              pres_box=pres_box, pl_box=pl_box,
                              background_remove=back_remove,
                              pathlength_b=pl_back,
                              press_back=press_back, temp_back=temp_back,
                              conc_back_y=conc_back, conc_back_p=conc_back,
                              save_files=save_files,
                              plot_results=plot_results,
                              transUnits=True)
    vel.append(vel_val)
    vel_unc.append(vel_err)
    temp.append(temp_val)
    temp_unc.append(temp_err)
    press.append(press_val)
    press_unc.append(press_err)
    shift.append(shift_val)
    shift_unc.append(shift_err)
    conc.append(conc_val[0])
    conc_unc.append(conc_err[0])
    conc_backs.append(conc_back)
    temp_backs.append(temp_back)
    pres_backs.append(press_back)
    
    if save_files:
    
        # load/create worksheet
        try:
            ws = wb[str(index)]
        except:
            ws = wb.create_sheet(str(index))
            # create header
            header = ['INDEX','TIME','DAQ Up','DAQ Down','Fn Up','Fn Down',
                      'WEIGHT_FLAT','WEIGHT_WIN','ETA_up','ETA_P','BW',
                      'MOLECULES',
                      'VEL_FIT','VEL_VAL','VEL_ERR','VEL_GUES',
                      'TEMP_FIT','TEMP_VAL','TEMP_ERR','TEMP_GUES',
                      'PRES_FIT','PRES_VAL','PRES_ERR','PRES_GUES',
                      'SHIFT_FIT','SHIFT_VAL','SHIFT_ERR','SHIFT_GUES',
                      'CONC_FIT','CONC_VAL','CONC_ERR','CONC_GUES',
                      'BROAD_FIT','BROAD_VAL','BROAD_ERR','BROAD_GUES',
                      'BACK_REMOVE','BACK_LABEL','BACK_TEMP','BACK_PRES',
                      'BACK_CONC_1','BACK_CONC_2',
                      'BOX_TEMP','BOX_PRES','BOX_CONC',
                      'TEMP_BACK','PRES_BACK','CONC_BACK','NOTES']
            ws.append(header)
        # add data from current run
        datawrite = [str(index), timestamp, vis3_daq, vis2_daq,
                     vis3_fn, vis2_fn, str(weight_flat),
                     str([weight_window_y,weight_window_p]), str(etalon_windows_p),
                     str(etalon_windows_y),
                     str([band_fit_y, band_fit_p]), str(molec_to_fit),
                     str(vel_fit), vel_val, vel_err, vel_guess,
                     str(temp_fit), temp_val, temp_err, temp_guess,
                     str(press_fit), press_val, press_err, press_guess,
                     str(shift_fit), shift_val, shift_err, shift_guess,
                     str(conc_fit), str(conc_val), str(conc_err),
                     str(conc_guess),
                     str(broad_fit), broad_val, broad_err, broad_guess,
                     str(back_remove), label_back, temp_back, press_back,
                     conc_back, conc_back,
                     temp_box, pres_box, conc_box,
                     temp_back, press_back, conc_back, notes]
        ws.append(datawrite)
        # save workbook
        wb.save(fn_tabulate)

    #%% Fit individual features for Boltzmann temperature
    # %% If doing boltzmann fit port in molecule data as dataframe
    # start with just big isolated features
    # TODO: try pairs of features, filter based on quantum criteria
    if boltzmann_fit:
        # get df of data file, limit to band fit
        # also limit below 13150 because features get too close up there
        # (though I could possibly split up the features by pairs as opposed to singles)
        path_O2_temp = r'linelists/temp'
        exponent = 1
        name_O2_temp = 'O2_temp_{}'.format(exponent)

        df_O2 = db.par_to_df(molec_to_fit[0]+ '.data',
                          nu_min=max([band_fit_y[0], band_fit_p[0]]),
                          nu_max=min([band_fit_y[1], band_fit_p[1], 13150]))
        # filter df_mol to only hugest biggest features
        which = df_O2.sw > boltz_threshold
        df_O2_filt = df_O2[which]

        separation = 0.5  # based on eye test

        # prepare dict for fit results
        fit_results_feature[index] = np.zeros((len(df_O2_filt),
                                               2+ 2*len(fits_plot)))
        # seperate features in data
        # TODO make applicable to both upstream and downstream spectra
        nu_feature = []
        # E_feature = []
        temp_feature = []
        temp_unc_feature = []
        pres_feature = []
        pres_unc_feature = []
        mole_feature = []
        mole_unc_feature = []
        shift_feature = []
        shift_unc_feature = []
        area_feature = []
        noise_feature = []
        pxt_feature = []

        for i_feature, nu_center in enumerate(df_O2_filt.nu):
            # print(i_feature)
            # print(nu_center)
            nu_feature.append(nu_center)
            # E_feature.append(df_O2_filt[i_feature].elower)
            #%% fit of single feature
            #consider velocity shift
            nu_left = (1 + vel_val*np.sin(np.deg2rad(angle_p))/3e8)*nu_center - separation
            nu_right = (1 + vel_val*np.sin(np.deg2rad(angle_p))/3e8)*nu_center + separation
            i_fits = td.bandwidth_select_td(wvn_p, [nu_left, nu_right],
                                            max_prime_factor=200)  # wavenumber indices of interest

            wvn_fit = wvn_p[i_fits[0]:i_fits[1]]
            abs_fit = abs_p[i_fits[0]:i_fits[1]]

            TD_fit = np.fft.irfft(abs_fit)

            # shrink the model to only include region of interest
            df_O2_iter = df_O2_filt[(df_O2_filt.nu > nu_left - 0.5) &
                                    (df_O2_filt.nu < nu_right + 0.5)]
            db.df_to_par(df_O2_iter.reset_index(), par_name=name_O2_temp,
                         save_dir=os.path.join(os.path.abspath(''), path_O2_temp),
                         print_name=False)

            hapi.db_begin(path_O2_temp)

            # model
            mod, pars = td.spectra_single_lmfit()
            pars['mol_id'].value = 7
            pars['pathlength'].set(value=6.81736/np.cos(np.deg2rad(angle_y)),
                                   vary=False) #cm, vary=False)
            pars['pressure'].set(value=press_val, vary=True,
                                 min=press_val/2, max=press_val*2)
            pars['temperature'].set(value=temp_val+temp_val*np.random.rand()/1000,
                                    vary=temp_fit, min=200, max=1000)
            pars['molefraction'].set(value=conc_val[0], vary=True,
                                     min=conc_val[0]/2, max=conc_val[0]*2)
            pars['shift'].set(value=shift_val, vary=True, min=shift_val-0.01,
                              max=shift_val+0.01)
            weight_bl = td.weight_func(len(abs_fit),
                                    round(weight_window_y[0]*len(TD_fit)*1))

            # fit individual feature
            fit = mod.fit(TD_fit, xx=wvn_fit, params=pars, weights=weight_bl,
                          name=name_O2_temp)

            temp_feature.append(fit.params['temperature'].value)
            temp_unc_feature.append(fit.params['temperature'].stderr)
            pres_feature.append(fit.params['pressure'].value)
            pres_unc_feature.append(fit.params['pressure'].stderr)
            mole_feature.append(fit.params['molefraction'].value)
            mole_unc_feature.append(fit.params['molefraction'].stderr)
            shift_feature.append(fit.params['shift'].value)
            shift_unc_feature.append(fit.params['shift'].stderr)
            pxt_feature.append(fit.params['pressure'].value*fit.params['molefraction'].value/fit.params['temperature'].value)

            #  place results in dict
            for i_results, which_results in enumerate(fits_plot):
                # save some fit results and errors for plotting later (for troubleshooting, ie pressure jumps to
                # 500 would probably be wrong
                fit_results_feature[index][i_feature, 2*i_results] = fit.params[
                    which_results].value
                fit_results_feature[index][i_feature, 2*i_results+1] = fit.params[
                    which_results].stderr


            #%% use  model conditions to find integrated area for feature
            db.df_to_par(df_O2_filt.iloc[[i_feature]].reset_index(),
                         par_name=name_O2_temp,
                         save_dir=os.path.join(os.path.abspath(''), path_O2_temp),
                         print_name=False)
            hapi.db_begin(path_O2_temp)

            # create model of one feature
            wvn_int = np.linspace(wvn_fit[0], wvn_fit[-1], 1000)
            TD_model_int = mod.eval(xx=wvn_int, params=fit.params,
                                    name=name_O2_temp)
            abs_model_int = np.real(np.fft.rfft(TD_model_int))

            if plot_results:
                plt.figure()
                abs_fit_weight = np.real(np.fft.rfft(fit.data*weight_bl))
                abs_model_weight = np.real(np.fft.rfft(fit.best_fit*weight_bl))
                plt.plot(wvn_fit, abs_fit_weight, label='data')
                plt.plot(wvn_fit, abs_model_weight, label='fit')

                plt.figure()
                plt.plot(fit.data)
                plt.plot(fit.best_fit)
                plt.plot(weight_bl)

            # add integrated area into results dict
            fit_results_feature[index][i_feature, -2] = np.trapz(abs_model_int,
                                                                 wvn_int)
            area_feature.append(np.trapz(abs_model_int, wvn_int))

            # add noise level for later noise weighting
            # we're going to noise-weight the fit so it doesn't get dominated by the noisy features at the edges
            fit_results_feature[index][i_feature, -1] = np.std(
                abs_fit - np.real(np.fft.rfft(fit.best_fit)))
            noise_feature.append(abs_fit - np.real(np.fft.rfft(fit.best_fit)))

        feature_dict = {'Nu': nu_feature,
                'Temp.': temp_feature,
                'Temp. Unc. ': temp_unc_feature,
                'Pres.': pres_feature,
                'Pres. Unc.': pres_unc_feature,
                'Molefrac': mole_feature,
                'Molefrac. Unc.': mole_unc_feature,
                'Shift': shift_feature,
                'Shift Unc.': shift_unc_feature,
                'Area': area_feature,
                'PXT': pxt_feature,
                'Noise': noise_feature
                }
        feature_df = pd.DataFrame.from_dict(feature_dict)
        #%% Find boltzmann temperature from integrated areas
        def boltzman_strength(T, nu, sw, elower, c):
            return lab.strength_T(T, elower, nu, molec_id=5) * sw * c  # unitless scaling factor c

        mod_bolt = Model(boltzman_strength, independent_vars=['nu','sw','elower'])
        mod_bolt.set_param_hint('T', value=temp_val, min=200, max=4000)

        # use ideal gas approximation for c - proportional to P*y/T, 1e22 is empirical from this data
        c_IG = 1e22 * press_val * conc_val[0] / temp_val
        mod_bolt.set_param_hint('c', value=c_IG)

        strength_estimate = boltzman_strength(temp_val, df_O2_filt.nu,
                                              df_O2_filt.sw, df_O2_filt.elower,
                                              c_IG)
        weight_noise = (max(fit_results_feature[index][:, -1])
                        - fit_results_feature[index][:, -1]) ** exponent
        # unfortunately, exponent is arbitrary, max sets range from 0-1

        weight_noise = weight_noise / max(weight_noise)
        weight_strength = strength_estimate.to_numpy()
        weight_strength = weight_strength / max(weight_strength)

        weight = (weight_noise + weight_strength)
        weight = weight / max(weight)

        result_bolt = mod_bolt.fit(fit_results_feature[index][:, -2],
                                   nu=df_O2_filt.nu, sw=df_O2_filt.sw,
                                   elower=df_O2_filt.elower,
                                   weights=weight)
        # noise weighting (focus on the good ones)

        # record boltzmann fit result
        temp_boltz.append(result_bolt.params['T'].value)
        temp_boltz_unc.append(result_bolt.params['T'].stderr)
        c_boltz.append(result_bolt.params['c'].value)
        c_boltz_unc.append(result_bolt.params['c'].stderr)

        strength_estimate *= result_bolt.params['c'].value/c_IG

        if plot_results:
            plt.figure()
            plt.plot(df_O2_filt.elower, fit_results_feature[index][:, -2],
                     color='tab:green', label='measurement', ls='none', marker='.')
            plt.plot(df_O2_filt.elower, result_bolt.best_fit,
                     color='tab:blue', ls='none', marker='.',
                     label='T = {} K (boltzmann)'.format(int(result_bolt.params['T'].value)))
            plt.plot(df_O2_filt.elower, strength_estimate,
                     color='tab:orange', ls='none', marker='.',
                     label='T = {} K (HITRAN)'.format(int(temp_val)))
            plt.plot(df_O2_filt.elower, weight/1000,
                     '.', color='black', label='weight/10', ls='none',
                     marker='.')
            plt.xlabel('E"')
            plt.legend()

            plt.figure()
            plt.plot(df_O2_filt.nu, fit_results_feature[index][:, -2],
                     color='tab:green', label='measurement', ls='none',
                     marker='.')
            plt.plot(df_O2_filt.nu, result_bolt.best_fit,
                     color='tab:blue', ls='none', marker='.',
                     label='T = {} K (boltzmann)'.format(int(result_bolt.params['T'].value)))
            plt.plot(df_O2_filt.nu, strength_estimate,
                     color='tab:orange', ls='none', marker='.',
                     label='T = {} K (HITRAN)'.format(int(temp_val)))
            plt.plot(df_O2_filt.nu, weight/1000,
                     '.', color='black', label='weight/10', ls='none',
                     marker='.')
            plt.xlabel('Wavenumber (cm-1)')
            plt.legend()

if boltzmann_fit:
    d_fit = {'Fn Down': fn_down,
             'Fn Up': fn_up,
             'Vel. (m/s)': vel,
             'Vel. Unc.': vel_unc,
             'Temp. (K)': temp,
             'Temp. Unc.': temp_unc,
             'Pres. (atm)': press,
             'Pres. Unc.': press_unc,
             'Shift (cm-1)': shift,
             'Shift Unc.': shift_unc,
             'Conc.': conc,
             'Conc. Unc': conc_unc,
             'Temp. Back': temp_backs,
             'Pres. Back': pres_backs,
             'Conc. Back': conc_backs,
             'Temp. Boltz.': temp_boltz,
             'Temp. Boltz. Unc.': temp_boltz_unc,
             'c': c_boltz,
             'c Unc.': c_boltz_unc}
else:
    d_fit = {'Fn Down': fn_down,
             'Fn Up': fn_up,
             'Vel. (m/s)': vel,
             'Vel. Unc.': vel_unc,
             'Temp. (K)': temp,
             'Temp. Unc.': temp_unc,
             'Pres. (atm)': press,
             'Pres. Unc.': press_unc,
             'Shift (cm-1)': shift,
             'Shift Unc.': shift_unc,
             'Conc.': conc,
             'Conc. Unc': conc_unc,
             'Temp. Back': temp_backs,
             'Pres. Back': pres_backs,
             'Conc. Back': conc_backs}

df_fit = pd.DataFrame.from_dict(d_fit)
if save_files:
    pickle.dump(df_fit, open(os.path.join(d_save_0,'df_fit.p'),'wb'))

df_fit.plot(y=['Vel. (m/s)'])
plt.plot([866, 875, 857, 918, 967])

df_fit.plot(y=['Pres. (atm)'])
plt.plot([1.17, 1.37, 0.93, 1.18, 1.19])

df_fit.plot(y=['Temp. (K)'])
plt.plot([586, 600, 574, 659, 731])
print('Fitting Done!')