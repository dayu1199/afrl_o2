"""
PlotInitialO2Data.py

Created by on 8/26/22 by david

Description:

"""

import os
import numpy as np
import matplotlib.pyplot as plt

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

d_data = r'/Users/david/Desktop'

data = np.loadtxt(os.path.join(d_data,'fitdata_freq_20220121164404.txt'))
wvn = data[:, 2]
abs = data[:, 0]
fit = data[:, 1]

plt.figure()
plt.plot(wvn, abs, label='Data')
plt.plot(wvn, fit, label='Fit')
# plt.legend()
# plt.ylabel('Absorbance')
# plt.xlabel('Wavenumber ($\mathregular{cm^{-1}}$)')