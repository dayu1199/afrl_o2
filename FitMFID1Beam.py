

from lmfit import Model, Parameters
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
# import hapi
import numpy as np
from math import floor
import matplotlib.pyplot as plt


def spectra(xx, press, Temp, shift, velocity, Pathlength, Broadening, Theta,
            background_remove, pathlength_b, press_back, temp_back,**kwargs):
    ''' This is the actual fitting function. In this configuration this function only works for HITRAN molecules
    Inputs:
        xx: xscaling wave in wavenumber
        press: pressure in atm
        Temp: Temperature in K
        shift: wavenumber shift in cm-1
        Pathlength: cm
        Broadening: multiple of gamma air
        **kwards: Concentrations and HITRAN molecules    '''    
    SPEEDOFLIGHT = 2.99792458e8
    conc = [];  Molecules_to_Fit =[]; molecule_number = []; isotopes = []
    
    for key, value in kwargs.items(): # Unpacks keyword arguments into molecules to fit and concentrations
        if key[:2] == 'x_':
            isos = []
            i = key.find('i')
            conc.extend([value])
            Molecules_to_Fit.append(key[2:i])   
            molecule_number.extend([getMoleculeNumber(key[2:i])])
            
            while i<len(key)-1:
                isos.extend([int(key[i+1])])
                i+=1
            isotopes.append(isos)
        elif '_b' in key:
            molec_back = key[:10]
            conc_back = value
        else:
            print('Unexpected keyword argument')


   # Spectral Fitting Section ###########################     
#    MIDS = []

    # shift xx
    xx = xx - shift
    
    if background_remove:
        step_xx = (max(xx)-min(xx))/(len(xx)-1) 
        # yellow
        MIDS_b = [(7,1,conc_back*hapi.abundance(7,1))]
        [nu_b, coefs_b] = hapi.absorptionCoefficient_Voigt(MIDS_b,
                molec_back, OmegaStep=step_xx,
                OmegaRange=[min(xx), max(xx)],HITRAN_units=False,
                Environment={'p':press_back,'T':temp_back},
                Diluent={'self':conc_back,
                         'air':(1-conc_back)}, IntensityThreshold =0)
        absorbance_b = pathlength_b*coefs_b
    #scale xx by velocity factor
    slope_doppler = velocity*np.sin(Theta)/SPEEDOFLIGHT+1
    xx_vel = xx*slope_doppler
    step = (max(xx_vel)-min(xx_vel))/(len(xx_vel)-1)      # Wavenumber resolution of data       
    total_coefs = np.zeros(len(xx))
    # print(conc, Temp, press, shift, velocity)
    for jj in range(len(Molecules_to_Fit)):
        MIDS = []
        for kk in range(len(isotopes[jj])):
            aa = ()
            aa = aa+(molecule_number[jj],)
            aa = aa+(isotopes[jj][kk],)
            aa = aa+(conc[jj]*hapi.abundance(molecule_number[jj],
                     isotopes[jj][kk]),)
            MIDS.append(aa)
        [nu,coefs] = hapi.absorptionCoefficient_Voigt(MIDS,
            (Molecules_to_Fit[jj]), OmegaStep=step,
            OmegaRange=[min(xx_vel), max(xx_vel)],HITRAN_units=False,
            Environment={'p':press,'T':Temp},
            Diluent={'self':conc[jj],
                     'air':(1-conc[jj])},
            IntensityThreshold =0)
        total_coefs += coefs
    absorbance = Pathlength*total_coefs
    if background_remove:
        absorbance += absorbance_b
    absorbance = np.fft.irfft(absorbance)
    # print(len(absorbance_y))
    print('Fitting...')
    return absorbance

    
def Fitting_Spectra(Guess_vector, Fit_vector, x_data, y_data, Molecules_to_Fit,
    molecule_numbers, isotopes, theta, weight_window, weight_flat,
    background_remove, molec_back, pathlength_b, press_back, temp_back,
    conc_back, etalonWindows={}):
    '''
    Calculates weighting wave and initializes fit parameters. Returns fit object and effective time wave
    '''
    
    # Decompose Guess and Fit vectors to initialize parameters for fit
    Temp_Guess = Guess_vector[0]
    Temp_Fit = Fit_vector[0]
    
    Pressure_Guess = Guess_vector[1]
    Pressure_Fit = Fit_vector[1]
    
    Shift_Guess = Guess_vector[2]
    Shift_Fit = Fit_vector[2]
    
    Broadening_Guess = Guess_vector[3]
    Broadening_Fit = Fit_vector[3]

    Velocity_Guess = Guess_vector[4]
    Velocity_Fit = Fit_vector[4]
    
    Pathlength_Guess = Guess_vector[5]
    Pathlength_Fit = Fit_vector[5]
    
    conc_Fit = [0]*len(Molecules_to_Fit)
    conc = [0]*len(Molecules_to_Fit)
    conc_err = [0]*len(Molecules_to_Fit)
    
    params = Parameters()

    molecule_info=[]
    for ii in range(len(Molecules_to_Fit)):
        conc_Fit[ii] = Fit_vector[6+ii]
        conc[ii] = Guess_vector[6+ii]
        molecule_info.append('x_'+Molecules_to_Fit[ii]+'i')
        for iso in isotopes[ii]:
            molecule_info[ii] += str(iso)
        params.add(molecule_info[ii], value = conc[ii], min=0.00001*conc[ii],
                   max=20*conc[ii], vary=conc_Fit[ii])
        # print(molecule_info[ii])
 
    
    #Defines bounds of fit. Make sure to change these to reflect environmental conditions
    params.add('Temp', value=Temp_Guess, min=Temp_Guess*0.5, max=295,
               vary=Temp_Fit)
    params.add('press', value=Pressure_Guess, min=Pressure_Guess*0.1,
               max=Pressure_Guess*5, vary=Pressure_Fit)
    params.add('shift', value=Shift_Guess, min=-.1, max=.1, vary=Shift_Fit)
    params.add('velocity', value=Velocity_Guess, min=-Velocity_Guess*5-1,
        max=Velocity_Guess*5+1, vary=Velocity_Fit)
    params.add('Pathlength', value = Pathlength_Guess, vary=Pathlength_Fit)
    params.add('Broadening', value = Broadening_Guess, vary=Broadening_Fit)
    params.add('Theta', value = theta, vary=False)

    #bkgd parameters
    params.add('background_remove', value=background_remove, vary=False)
    params.add(molec_back+'_b', value=conc_back, vary=False)
    params.add('pathlength_b', value=pathlength_b, vary=False)
    params.add('press_back', value=press_back, vary=False)
    params.add('temp_back', value=temp_back, vary=False)
    
    # Defines weighting window for fit
    percent_start = weight_window[0] 
    percent_stop = weight_window[1]
    y_datai = np.fft.irfft(y_data)    
    
    # Calculates weighting wave for fit
    [weight, time] =  weightWave(x_data, Temp_Guess, Pressure_Guess, conc,
        Molecules_to_Fit, molecule_numbers, percent_start, percent_stop)

    if weight_flat:
        for i in range(len(weight)):
            if weight[i] != 0:
                weight[i] = 1
    
    # Sets weighting wave to zero over etalon sections
    for a in etalonWindows:
        weight[a[0]:a[1]] = [0 for i in range(a[0],a[1])]
        weight[weight.size-a[1]:weight.size-a[0]] = [0 for i in range(weight.size-a[1],weight.size-a[0])]

    # Builds fit model
    gmodel = Model(spectra)
    
    # Runs fit, result is fit model object containing results of fit
    result = gmodel.fit(y_datai, params=params, weights=weight, xx=x_data)
    for ii in range(len(Molecules_to_Fit)):
        conc[ii] = result.best_values[molecule_info[ii]]
        conc_err[ii] = result.params[molecule_info[ii]].stderr
    print(result.fit_report)
      
    return result, time, conc, conc_err, weight, time

    
def weightWave(fwave1,T,P,molefractions, molecules, moleculenum, percent_start,percent_stop):   
    ''' Calculates exponential decay weighting wave for all molecules. Add together exponential decays weighted by each molecules concentration
    Exponential decay is calculated from the envelope portion of Ian's 2010 Time Domain paper. An average broadening (weighted by linestrength)
    is calculated from HITRAN parameters in window. Returns weighting wave and effective time wave
    '''
    
    zero_start = int(floor(fwave1.size*percent_start*2))
    zero_stop = int(floor(fwave1.size*percent_stop*2))

    fwave = fwave1*2.99792458*10**10
    c = 299792458 #m/s
    Na = 6.02E-23   #mol-1
    k = 1.38064852E-23  #m^2kgs^-2K^-1
    c2 = (6.62606957E-27)*(2.99792458E10)/(1.3806488E-16)  #cm*K
    
    ff = (np.fft.irfft(fwave))
    n = ff.size
    dstep = (fwave[1]-fwave[0])
    times =np.fft.fftfreq(n, dstep)
    times = np.fft.fftshift(times)
    time_abs = (times-times[0])
    
    ii=0
    for molecule in molecules:
        
        envelope = np.zeros((len(time_abs)))
        temp_env = np.zeros((len(time_abs)))
        chi = molefractions[ii]
        
        M = molarMass(molecule) #g/mol
        S = np.asarray(hapi.getColumn(molecule,"sw"))  #cm-1/(molec cm-2)
        gamma_self = np.asarray(hapi.getColumn(molecule,"gamma_self")) #cm-1 atm-1
        gamma_air = np.asarray(hapi.getColumn(molecule,"gamma_air")) #cm-1 atm-1
        n_air = np.asarray(hapi.getColumn(molecule,"n_air")) #unitless
        ls = np.asarray(hapi.getColumn(molecule,"nu")) #cm-1
        elower = np.asarray(hapi.getColumn(molecule,"elower")) #cm-1
        
        Q_296 = PartitionFunction(molecule,296)[0]
        QT = PartitionFunction(molecule,T)[0]
              
        linecenter = 299792458*100*ls
        gamma = 299792458*100*(gamma_air*(1-chi)*P + gamma_self*chi*P)*(296/T)**n_air
        doppler = 299792458*100*(linecenter/c)*np.sqrt(2*Na*k*T*np.log(2)/(M/1000))
        linestrength = c*100*S*(Q_296/QT)*(np.exp(-c2*elower/T)/np.exp(-c2*elower/296))*((1-np.exp(-c2*linecenter/(T*c*100))/(1-np.exp(-c2*linecenter/(296*c*100)))))
        
        #Linestrength weighted average gamma
        gamma_avg = np.average(gamma,weights=linestrength)
        doppler_avg = np.average(doppler,weights = linestrength)
        temp_env = np.exp((-1*((2*np.pi*doppler_avg*time_abs)**2)/(4*np.log(2)))+(-2.0*np.pi*gamma_avg*time_abs))
        
        envelope += temp_env/temp_env[zero_start]*chi
        
        ii+=1
                
        envelope[zero_stop:envelope.size-zero_stop] = [0 for i in range(zero_stop,envelope.size-zero_stop)]
        envelope[0:zero_start] = [0 for i in range(0,zero_start)]
        envelope[int(envelope.size/2):]=[0 for i in range(int(envelope.size/2),envelope.size)]
        
        ienvelope = np.flipud(envelope)
        envelope = envelope+ienvelope
        
        maxval = max(envelope)
        envelope = envelope/maxval

    
    return envelope, time_abs
    
    
def PartitionFunction(molecule,temperature):
    '''
    Calculates the partition sum accoring to the TIPS code released on HITRAN.org
    
    Call to TIPS is via HAPI 10/23
    
    Gamache et al. Total internal partition sums for 166 isotopologues of 51 molecules 
    important in planetary atmospheres: Application to HITRAN2016 and beyond, 
    Journal of Quantitative Spectroscopy and Radiative Transfer, 2017.
    
    Assumes isotope  =  1

    '''
    
    
    molecules = {
    'H2O':1,
    'H2O_Paul':1,
    'H2O_PaulLF':1,
    'H2O_Scott':1,
    'CO2':2,
    'O3':3,
    'N2O':4,
    'CO':5,
    'CH4':6,
    'O2':7,
    'O2_Vgt': 7,
    'NO':8,
    'SO2':9,
    'NO2':10,
    'NH3':11,
    'HNO3':12,
    'OH':13,
    'HF':14,
    'HCl':15,
    'HBr':16,
    'HI':17,
    'ClO':18,
    'OCS':19,
    'H2CO':20,
    'HOCl':21,
    'N2':22,
    'HCN':22,
    'CH3Cl':23,
    'H2O2':24,
    'C2H2':25,
    'C2H6':26,
    'PH3':27,
    'COF2':28,
    'SF6':29,
    'H2S':30,
    'HCOOH':31,
    'HO2':32,
    'O':33,
    'ClONO2':34,
    'NO+':35,
    'HOBr':36,
    'C2H4':37,
    'CH3OH':38,
    'CH3Br':39,
    'CH3CN':40,
    'CF4':41,
    'C4H2':42,
    'HC3N':43,
    'H2':44,
    'CS':45,
    'SO3':46,
    'ArH2O_Labfit':1
    }
    
    moleculenum = molecules[molecule]
    temp = [temperature]
    QT =hapi.partitionSum(moleculenum,1,temp)
    return QT
     
    
def molarMass(molecule):
    '''
    Returns the molar mass (g/mol) of molecule (string)
    '''
    masses = {
        'H2O':18.01528,
        'H2O_Paul':18.01528,
        'H2O_PaulLF':18.01528,
        'H2O_Scott':18.01528,
        'CO2':44.01,
        'O3':48,
        'N2O':44.013,
        'CO':28.01,
        'CH4':16.04,
        'O2':15.999,
        'O2_Vgt': 15.999,
        'NO':30.006,
        'SO2':64.066,
        'NO2':46.0055,
        'NH3':17.031,
        'HNO3':63.01,
        'OH':17.008,
        'HF':20.01,
        'HCl':36.46,
        'HBr':80.91,
        'HI':127.911,
        'ClO':51.4521,
        'OCS':60.07,
        'H2CO':30.031,
        'HOCl':52.46,
        'N2':28.0134,
        'HCN':27.0253,
        'CH3Cl':50.49,
        'H2O2':34.0147,
        'C2H2':26.04,
        'C2H6':30.07,
        'PH3':33.99758,
        'COF2':96.93,
        'SF6':146.06,
        'H2S':34.1,
        'HCOOH':46.03,
        'HO2':33.0,
        'O':16.0,
        'ClONO2':97.46,
        'NO+':30.006,
        'HOBr':96.91,
        'C2H4':28.05,
        'CH3OH':32.04,
        'CH3Br':94.94,
        'CH3CN':41.05,
        'CF4':88.0043,
        'C4H2':50.0587,
        'HC3N':51.048,
        'H2':2.01588,
        'CS':44.07,
        'SO3':80.062,
        'ArH2O_Labfit':18.01528
        }
    
    molarmass = masses[molecule]
    return molarmass
    
    
    
def savethings(name, mattosave):
    
    with open(name + '.txt', 'w') as f:
        jj=0
        for item in mattosave: 
            for element in item:
               f.write('{0:.20f}, '.format(element) + ' ') 
            jj+=1
            f.write('\n')   
        f.close()

    
def getMoleculeNumber(Name):
    
    HITRAN_Molecules = ['H2O','CO2','O3','N2O','CO','CH4','O2','NO','SO2','NO2','NH3','HNO3','OH','HF','HCl','HBr','HI','ClO','OCS','H2CO','HOCl','N2','HCN','CH3Cl','H2O2','C2H2','C2H6','PH3','COF2','SF6','H2S','HCOOH','HO2','O','ClONO2','NO+','HOBr','C2H4','CH3OH','CH3Br','CH3CN','CF4','C4H2','HC3N','H2','CS','SO3','ArH2O_Labfit']
    
    if Name == 'H2O_Paul':
        molecule_number = 1
    elif Name == 'H2O_PaulLF':
        molecule_number = 1
    elif Name == 'H2O_Scott':
        molecule_number = 1
    elif Name == 'O2_Vgt':
        molecule_number = 7
    else:
        try:
            molecule_number = (HITRAN_Molecules.index(Name))+1
        except ValueError:
            molecule_number=111
    
    return molecule_number