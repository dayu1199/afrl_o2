# -*- coding: utf-8 -*-
"""
Created 4/30/20 David Yun
Fit Program for signal that goes through retro
modified from FitProgram2Beam for AFRL Isolator work

PARAMETERS:
    data:
        array of data with wavenumber and transdata
    save_path:
        Where to save data, fit, weight, and report files
    file_tag:
        A special tag to end all saved files with (e.g. height30mm)
    weight_window:
        Time-domain weighting by proportion (e.g. [0.01,0.5])
    weight_flat:
        True for flat weighting, False for exponential weighting
    etalonWindows:
        where to cut out windows on downstream spectra
        (e.g. [[500,600],[10000,11000]])
    angle:
        angle of beams (in deg), single input
    band_fit:
        where to fit spectra (e.g. [6800,7100]) (in cm-1)
    Molecules_to_Fit:
        molecules to fit for (e.g. ['H2O_PaulLF'])
    isotopes:
        isotopes of molecules to fit (e.g. [[1]])
    Pressure_Fit:
        Toggle for fitting pressure, TRUE for fit, FALSE for fix
    Temp_Fit:
        Toggle for fitting temperature
    Velocity_Fit:
        Toggle for fitting velocity
    Shift_Fit:
        Toggle for fitting shift
    Conc_Fit:
        Toggle for fitting concentration, for now not able to toggle for
        individual molecules
    Broadening_Fit:
        Toggle for fitting broadening
    pathlength:
        pathlength straight across test section, code adjusts for pass and angle
    pahelength_b:
        actual path of background, no adjustment in code
    Pressure_Guess:
        Initial Pressure value
    Temp_Guess:
        Initial Temperature value
    Velocity_Guess:
        Initial Velocity value
    Shift_Guess:
        Initial shift value
    Conc_Guess:
        Intiial conc value (for now just takes one for all molecules
    Broadening_Guess:
        Initial broadening guess (just go with 1 if you don't want to mess
                                  with it')
    save_files:
        Toggle to save files
    plot_results:
        Toggle to plot spectral fit
    print_fit_report:
        Toggle to print fit results in command
        
RETURNS: Pretty self-explanatory
    Velocity
    Velocity_err
    Temperature
    Temperature_err
    Pressure
    Pressure_err
    Shift
    Shift_err
    Conc
    Conc_err
    Broadening
    Broadening_err

"""

from FitMFIDRetrowBox import Fitting_Spectra
import matplotlib.pyplot as plt
import numpy as np
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
import td_support as td
import time
import os

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

###############################################################################
##########  Output options for Fitting ########################################

def FitProgram(filename, save_path, file_tag,
               weight_window, weight_flat, etalonWindows, angle, band_fit,
               Molecules_to_Fit, isotopes,
               Pressure_Fit, Temp_Fit, Velocity_Fit, Shift_Fit, Conc_Fit,
               Broadening_Fit,
               Pressure_Guess, Temp_Guess, Velocity_Guess, Shift_Guess, 
               Conc_Guess, Broadening_Guess,
               conc_box=0, temp_box=0, pres_box=0, pl_box=0,
               pathlength=6.84276, n_pass=2,
               background_remove=False, molec_back='O2',
               pathlength_b=35.935, press_back=1, temp_back=300, conc_back=0,
               save_files=True, plot_results=True, print_fit_report=True,
               wvnUnits=True, transUnits=True,
               noise_add=False, noise_level=0.001, noise_flat=False):
    ########## Data Specific Parameters #######################################
    print(filename)
    data = np.loadtxt(filename)
    
    theta = np.deg2rad(angle)
    Pathlength = n_pass*pathlength/np.cos(theta)
    conc = [Conc_Guess]*len(Molecules_to_Fit) # Initializes concentration guess
                                                # for each molecule to fit
    Conc_Fit = [Conc_Fit]*len(Molecules_to_Fit) # True for each molecule to fit
    ###########################################################################
    
    if data[0,0]>data[-1,0]: # Flips data if it is inverted
        data = np.flipud(data) 
        print("Flipping FFT")
    print(len(data))

    start, stop = td.bandwidth_select_td(data[:,0], band_fit)
    if start < stop:
        if wvnUnits:
            x_data = data[start:stop, 0]
        else:
            x_data = data[start:stop, 0]/29979245800
    else:
        if wvnUnits:
            x_data = data[start:stop:-1, 0]
        else:
            x_data = data[start:stop:-1, 0]/29979245800
    # x_data =data[:,0]
    # Imports spectra into y_data, converts to absorbance if in trans units
    y_data = np.zeros((len(x_data), 1))
    if transUnits:
        y_data = -np.log(data[start:stop, 1])
    else:
        y_data = data[start:stop,1]

    if noise_add:
        noise = np.random.normal(0, noise_level, len(y_data))
        if not noise_flat:
            weight = ((x_data - 13100) / 38) ** 2 + 1
            noise *= weight
        y_data += noise

        # y_data[:,0] =data[:,1]
    
    # Convert to time domain
    ff = (np.fft.irfft(y_data))
    y_fit = np.zeros((ff.size, 2))

    # tic = time.clock()
    
    # Builds logic vector to imform fit of parameters to float
    Fit_vector = [Temp_Fit, Pressure_Fit, Shift_Fit, Broadening_Fit,
                  Velocity_Fit]
    Fit_vector.extend(Conc_Fit)
    
    # Builds vector of approximate parameters for starting fits
    Guess_vector = [Temp_Guess, Pressure_Guess, Shift_Guess, Broadening_Guess,
                    Velocity_Guess]
    Guess_vector.extend(conc)
    
    # Loads Molecule Data from HITRAN
    hapi.db_begin('')  # Location to store imported HITRAN data
    
    HITRAN_Molecules = ['H2O','CO2','O3','N2O','CO','CH4','O2','NO','SO2',
                        'NO2','NH3','HNO3','OH','HF','HCl','HBr','HI','ClO',
                        'OCS','H2CO','HOCl','N2','HCN','CH3Cl','H2O2','C2H2',
                        'C2H6','PH3','COF2','SF6','H2S','HCOOH','HO2','O',
                        'ClONO2','NO+','HOBr','C2H4','CH3OH','CH3Br','CH3CN',
                        'CF4','C4H2','HC3N','H2','CS','SO3','ArH2O_Labfit']
    molecule_numbers =[0]*(len(Molecules_to_Fit));
    
    if plot_results:
        plt.figure()
        plt.plot(x_data,y_data)
        plt.title('Transmission')
        plt.xlabel('Wavenumber (cm-1')
        plt.ylabel('Transmission')
    
    ii=0
    for item in Molecules_to_Fit:
        if item not in HITRAN_Molecules:
            print("Listed Molecule is not included in HITRAN", item)
        elif item == 'O2':
            print('Using preexising O2 HITRAN data in project.')
            molecule_numbers = [7]
        else:  
            molecule_numbers[ii] = (HITRAN_Molecules.index(item))+1
            isotope_ids = []
            for jj in range(len(isotopes[ii])):
                isotope_ids.append(hapi.ISO[molecule_numbers[ii],
                                            isotopes[ii][jj]][0]) 
            hapi.fetch_by_ids(item, isotope_ids,min(x_data)-1,
                              max(x_data)+1)
        ii+=1
            
    tic= time.time()  # Times each fit iteration
    if not y_data[10]:  # checks from empty FFTs and nans in data
        print("Empty FFT")
    else:
        Guess_vector = [Temp_Guess, Pressure_Guess, Shift_Guess,
                        Broadening_Guess,Velocity_Guess]
        Guess_vector.extend(conc)     
        
        # This sends data to function which compiles it for the fit
        [result,timewave,concs,concs_err,weight_wave,time_wave] = \
            Fitting_Spectra(Guess_vector, Fit_vector, x_data, y_data,
                            Molecules_to_Fit, molecule_numbers, isotopes,
                            Pathlength, theta, weight_window, weight_flat,
                            background_remove, molec_back, pathlength_b,
                            press_back, temp_back, conc_back,
                            conc_box, temp_box, pres_box, pl_box,
                            etalonWindows = etalonWindows)
        
        Velocity = result.best_values['velocity']
        Temperature = result.best_values['Temp']
        Pressure = result.best_values['press']
        Shift = result.best_values['shift']
        Broadening = result.best_values['Broadening']
        Temperature_err = result.params['Temp'].stderr
        Pressure_err = result.params['press'].stderr
        Shift_err = result.params['shift'].stderr
        Velocity_err = result.params['velocity'].stderr
        Broadening_err = result.params['Broadening'].stderr

        Conc = []
        Conc_err = []
        for jj in range(len(Molecules_to_Fit)):
            Conc.append(concs[jj])
            Conc_err.append(concs_err[jj])
    
        # Stores best fits and corresponding data in y_fit matrix          
        time_fit = result.best_fit
        time_data = result.data
        time_res = time_data - time_fit
        weight = weight_wave
        
        abs_fit = np.real(np.fft.rfft(time_fit))
        abs_data = np.real(np.fft.rfft(time_data))
        abs_datanobl = np.real(np.fft.rfft(time_data-
                                             (1-weight_wave)*time_res))
        abs_resnobl = abs_datanobl-abs_fit
        
        # taking a look at what is actually fitted
        abs_fitweight = np.real(np.fft.rfft(time_fit*weight))
        abs_dataweight = np.real(np.fft.rfft(time_data*weight))
        abs_resweight = abs_dataweight - abs_fitweight
        
        #%% create models for plotting
        # Spectral fitting Section ###########################
        # shift xx, don't velocity_in scale yet
        SPEEDOFLIGHT = 2.99792458e8
        
        xx = x_data - Shift
        step_out = (max(xx)-min(xx))/(len(xx)-1)
        
        # box Section
        MIDS = [(7,1,conc_box*hapi.abundance(7,1))]
        [nu_box,coefs_box] = hapi.absorptionCoefficient_Voigt(
            MIDS,(Molecules_to_Fit),
            OmegaStep=step_out, OmegaRange=[min(xx), max(xx)],
            HITRAN_units=False, Environment={'p':pres_box,'T':temp_box},
            Diluent={'self':conc_box-0.21/0.79*(1-conc_box),
                     'air':(1-conc_box)/0.79},
            IntensityThreshold =0)
    
        abs_box = pl_box*coefs_box
        time_box = np.fft.irfft(abs_box)
        abs_boxweight = np.real(np.fft.rfft(time_box*weight))

        # bkgd Section
        MIDS = []
        # Wavenumber resolution of data       
        for jj in range(len(Molecules_to_Fit)):
    #            MIDS = []
            for kk in range(len(isotopes[jj])):
                aa = ()
                aa = aa+(molecule_numbers[jj],)
                aa = aa+(isotopes[jj][kk],)
                aa = aa+(conc_back*hapi.abundance(molecule_numbers[jj],
                                                       isotopes[jj][kk]),)
                MIDS.append(aa)
                # print("out_p:", conc_back_p, temp_out, press_out, shift,
                #     pathlength_out_p)
                
        [nu_out,coefs_out] = hapi.absorptionCoefficient_Voigt(
            MIDS,(Molecules_to_Fit),
            OmegaStep=step_out, OmegaRange=[min(xx), max(xx)],
            HITRAN_units=False, Environment={'p':press_back,
                                             'T':temp_back},
            Diluent={'self':conc_back-0.21/0.79*(1-conc_back),
                     'air':(1-conc_back)/0.79},
            IntensityThreshold =0)
    
        abs_out = pathlength_b*coefs_out
        time_out = np.fft.irfft(abs_out)
        abs_outweight = np.real(np.fft.rfft(time_out*weight))
    
        # in Section Yellow
        MIDS = []
        slope_doppler_y = Velocity*np.sin(-theta)/SPEEDOFLIGHT+1
        xx_y = xx*slope_doppler_y
        step_y = (max(xx_y)-min(xx_y))/(len(xx_y)-1)      # Wavenumber resolution of data       
        for jj in range(len(Molecules_to_Fit)):
    #            MIDS = []
            for kk in range(len(isotopes[jj])):
                aa = ()
                aa = aa+(molecule_numbers[jj],)
                aa = aa+(isotopes[jj][kk],)
                aa = aa+(Conc[jj]*hapi.abundance(molecule_numbers[jj],
                                                    isotopes[jj][kk]),)
                MIDS.append(aa)
                # print("in_y:", conc_in, temp_in, press_in, shift, pathlength_in_y,
                      # velocity_in)
                
        [nu_y,coefs_y] = hapi.absorptionCoefficient_Voigt(
            MIDS,(Molecules_to_Fit),
            OmegaStep=step_y,OmegaRange=[min(xx_y), max(xx_y)],
            HITRAN_units=False, Environment={'p':Pressure,
                                             'T':Temperature},
            Diluent={'self':Conc[jj]-0.21/0.79*(1-Conc[jj]),
                     'air':(1-Conc[jj])/0.79}, #*1.5095245023408206
            IntensityThreshold =0)
    
        abs_y = Pathlength*coefs_y
        time_y = np.fft.irfft(abs_y)
        absweight_y = np.real(np.fft.rfft(time_y*weight))
    
        # in Section Purple
        MIDS = []
        slope_doppler_p = Velocity*np.sin(theta)/SPEEDOFLIGHT+1
        xx_p = xx*slope_doppler_p
        step_p = (max(xx_p)-min(xx_p))/(len(xx_p)-1)      # Wavenumber resolution of data       
        for jj in range(len(Molecules_to_Fit)):
    #            MIDS = []
            for kk in range(len(isotopes[jj])):
                aa = ()
                aa = aa+(molecule_numbers[jj],)
                aa = aa+(isotopes[jj][kk],)
                aa = aa+(Conc[jj]*hapi.abundance(molecule_numbers[jj],
                                                    isotopes[jj][kk]),)
                MIDS.append(aa)
                # print("in_p:", conc_in, temp_in, press_in, shift, pathlength_in_p,
                #       velocity_in)
                
        [nu_p,coefs_p] = hapi.absorptionCoefficient_Voigt(
            MIDS,(Molecules_to_Fit),
            OmegaStep=step_p,OmegaRange=[min(xx_p), max(xx_p)],
            HITRAN_units=False, Environment={'p':Pressure,
                                             'T':Temperature},
            Diluent={'self':Conc[jj]-0.21/0.79*(1-Conc[jj]),
                     'air':(1-Conc[jj])/0.79}, #*1.5095245023408206
            IntensityThreshold =0)
    
        abs_p = Pathlength*coefs_p
        time_p = np.fft.irfft(abs_p)
        absweight_p = np.real(np.fft.rfft(time_p*weight))
            
        if print_fit_report:
            print(result.fit_report(result.params))
    
        # res_y = np.copy(np.fft.rfft(result.best_fit[0:cut]))
        # res_p = np.copy(np.fft.rfft(result.best_fit[cut:]))
        toc = time.time()     
        print('Computation Time:', toc - tic)
        
        #%% plot
        ffn = os.path.basename(filename)[:-10]
        if plot_results:
            # FFT of time domain fits yellow stream
            plt.figure() 
            plt.plot(x_data,abs_datanobl, label='Data', linewidth=1)
            # plt.plot(x_data,abs_resnobl,
            #           label='Residual', linewidth=1, color='none')
            plt.plot(x_data,abs_fit, label='Total Absorbance',
                linewidth=1)
            plt.plot(x_data,abs_box, label='Box Absorbance', linewidth=1)
            plt.plot(x_data,abs_y, label='Flow Fit Down', linewidth=1)
            plt.plot(x_data,abs_p, label='Flow Fit Up', linewidth=1)
            plt.plot(x_data,abs_out, label='Optics Absorbance', linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title("No BL Fits"+ffn)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
    
            # Weighted FFT of time domain fits yellow stream
            plt.figure() 
            plt.plot(x_data,abs_dataweight, label='Data', linewidth=1)
            # plt.plot(x_data,abs_resweight,
            #          label='Residual', linewidth=1, color='none')
            plt.plot(x_data,abs_fitweight, label='Total Absorbance',
                linewidth=1)
            plt.plot(x_data,abs_boxweight, label='Box Absorbance', linewidth=1)
            plt.plot(x_data,absweight_y, label='Flow Fit Down', linewidth=1)
            plt.plot(x_data,absweight_p, label='Flow Fit Up', linewidth=1)
            plt.plot(x_data,abs_outweight, label='Optics Absorbance', linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title("Weighted Fits"+ffn)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
            
            # Time domain fit plot. Plot vs time_wave for effective time on x axis 
            # yellow
            plt.figure()
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_data, label='Data', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_res, label='Residual', linewidth=1, color='None')
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_fit,label='Total Absorbance', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_box,label='Box Absorbance', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_y,label='Flow Fit Down', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_p,label='Flow Fit Up', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_out,label='Optics Absorbance', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff)),
                     result.weights*max(time_data))
            # plt.title()
            plt.xlabel('Index')
            plt.ylabel('Magnitude')
            plt.title("Time Domain"+ffn)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)

    
        if save_files:  
            filename_report = os.path.join(save_path,
                                           ffn+file_tag +'_report.txt')
            
            f = open(filename_report,'w')
            f.write(result.fit_report())
            f.close()
            
            # save cepstrum data
            filename_save_cep = os.path.join(save_path,
                                               ffn+file_tag +'_cep.txt')   
        
            cep = np.transpose([time_data,time_fit])
            np.savetxt(filename_save_cep,cep)
        
            # save absorbance data
            filename_save_abs = os.path.join(save_path,
                                               ffn+file_tag +'_abs.txt')   
        
            abs_data = np.transpose([np.real(x_data),abs_data,abs_datanobl])
            np.savetxt(filename_save_abs,abs_data)
        
            # save res data
            filename_save_res = os.path.join(save_path,
                                               ffn+file_tag +'_res.txt')   
        
            res = np.transpose([np.real(x_data),np.real(np.fft.rfft(
                result.data)-np.fft.rfft(result.best_fit))])
            np.savetxt(filename_save_res,res)
        
            # save fit data
            filename_save_fit = os.path.join(save_path,
                                               ffn+file_tag +'_fit.txt')   
        
            fit = np.transpose([np.real(x_data),np.real(np.fft.rfft(
                result.best_fit))])
            np.savetxt(filename_save_fit,fit)
            np.savetxt(filename_save_fit,fit)
            
            #save weight vector
            filename_save_weight = os.path.join(save_path,
                                                  ffn+file_tag+'_weight.txt')   
            weight = np.transpose([time_wave,weight_wave])
            np.savetxt(filename_save_weight,weight)
    
        
        # print(weight_window)
        fit_results = [Velocity, Velocity_err, Temperature, Temperature_err, 
                       Pressure, Pressure_err, Shift, Shift_err, Conc,
                       Conc_err, Broadening, Broadening_err]
        return fit_results