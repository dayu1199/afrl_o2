#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
plot fit results from a set of fits

Created on Wed May  4 16:08:35 2022

@author: david
"""

import os
import matplotlib.pyplot as plt
import pickle
import numpy as np
import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

plot_manual = True

#%% plot manual numbers
if plot_manual:
    temp = np.array([518.6, 665.9])
    vel = np.array([204, 594])

    temp_unc = np.array([0.012, 0.017])
    vel_unc = np.array([1.12, 1.01])

    fig1, ax1 = plt.subplots()
    fig4, ax4 = plt.subplots()
    # plot velocities
    ax1.errorbar([0, 1], vel,
                 yerr=vel_unc * vel, linestyle='None', marker='o')
    ax1.set_xlabel('Run')
    ax1.set_ylabel('Velocities (m/s)')
    ax1.set_ylim([-100, 1300])
    ax1.set(xticks=[0, 1], xticklabels=['AP', 'AQ'])

    # plot temperatures
    ax4.errorbar([0, 1], temp,
                 yerr=temp_unc * temp, linestyle='None', marker='o')
    ax4.set_xlabel('Run')
    ax4.set_ylabel('Temperature (K)')
    ax4.set_ylim([200, 800])
    ax4.set(xticks=[0, 1], xticklabels=['AP', 'AQ'])
#%% Collect data
else:
    d = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                     'Mar2022CampaignData/Fits/retro/16_06_2022_123437')
    tag = '19 Bkgd Remove'

    with open(os.path.join(d,'df_fit.p'),'rb') as f:
        df_results = pickle.load(f)

    first_plot = False

    #%% Plot
    # plot velocities
    if first_plot:
        fig1, ax1 = plt.subplots()
        # fig2, ax2 = plt.subplots()
        # fig3, ax3 = plt.subplots()
        fig4, ax4 = plt.subplots()
    # df_results['Vel. (m/s)'].plot(ax=ax1,linestyle='None',marker='o')
    ax1.errorbar([0,1],abs(df_results['Vel. (m/s)']),
                  yerr=0*df_results['Vel. (m/s)'],linestyle='None',marker='o',
                  label=tag)
    ax1.set_xlabel('Run')
    ax1.set_ylabel('Velocities (m/s)')
    ax1.set_ylim([0,1000])
    ax1.set(xticks=[0,1],xticklabels=['AP','AQ'])
    ax1.legend()

    # plot concentrations
    # ax2.errorbar([0,1],df_results['Conc.'],
    #               yerr=0*df_results['Conc.'],linestyle='None',marker='o',
    #               label=tag)
    # ax2.set_xlabel('Run')
    # ax2.set_ylabel('Concentration')
    # ax2.set_ylim([0.05,0.3])
    # ax2.set(xticks=[0,1],xticklabels=['AP','AQ'])
    # ax2.legend()

    # plot pressures
    # ax3.errorbar([0,1],df_results['Pres. (atm)'],
    #               yerr=0.0*df_results['Pres. (atm)'],linestyle='None',marker='o',
    #               label=tag)
    # ax3.set_xlabel('Run')
    # ax3.set_ylabel('Pressure (atm)')
    # ax3.set_ylim([0.3,1.8])
    # ax3.set(xticks=[0,1],xticklabels=['AP','AQ'])
    # ax3.legend()

    # plot temperatures
    ax4.errorbar([0,1],df_results['Temp. (K)'],
                  yerr=0*df_results['Temp. (K)'],linestyle='None',marker='o',
                  label=tag)
    ax4.set_xlabel('Run')
    ax4.set_ylabel('Temperature (K)')
    ax4.set_ylim([200,800])
    ax4.set(xticks=[0,1],xticklabels=['AP','AQ'])
    ax4.legend()
