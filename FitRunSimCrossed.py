#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Fit Run Crossed for Simulations

Created on Wed Apr 27 10:41:26 2022

@author: david
"""
import os
import numpy as np
import pandas as pd
import pickle
from datetime import datetime

from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
import FitProgram2BeamwBox as fithit

##################################Fitting Parameters###########################

# Fitting Options
save_files = True # saves fits and fitted parameters in text files.
plot_results = False  # Plots time domain and TD fit converted to the frequency
                    # domain. Turn this off for many datafiles.
print_fit_report = True  # Prints each fit report to command window
back_remove = False  # Remove background from spectra before fitting
label_back = ''
file_tag = ''

# simulation spectra to use
fn_y = 'O2simulation_AK_down.txt'
fn_p = 'O2simulation_AK_up.txt'

noise_add = True
noise_flat = False
noise_level = 0.012
eta_center = 13099
iterations = 50
beta = 9

# bkgd fit
import_bkgdfit = False
bkgdfit_d = os.path.join(r'/Users/david/Library/CloudStorage',
                        r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                        r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits',
                        r'sim/bkgdcross/07_12_2022_101912')
boxfit_d = os.path.join(r'/Users/david/Library/CloudStorage',
                        r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                        r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits',
                        r'sim/box/07_12_2022_094911')
if import_bkgdfit:
    with open(os.path.join(bkgdfit_d, 'df_fit.p'), 'rb') as f:
        df_bkgd = pickle.load(f)
    with open(os.path.join(boxfit_d, 'df_fit.p'), 'rb') as f:
        df_box = pickle.load(f)
    iterations = len(df_bkgd)

# Directory Info
d_save = os.path.join(r'/Users/david/Library/CloudStorage/OneDrive-UCB-O365/AFRL/AFRL ETHOS (DO NOT SYNC)',
                      'Mar2022CampaignData/Fits/sim')

# MFID Parameters
weight_window_y = [0.008, 0.5]  # default [0.005,0.5]
weight_window_p = [0.008, 0.5]  # default [0.005,0.5]
etalon_windows_y = []  # format [[0,100],[200,300]]
etalon_windows_p = []
band_fit_y = [13015.61, 13169.4]
band_fit_p = [13015.6, 13169.0]
weight_flat = True  # Otherwise will do an exponential weighting
    
# Fitting Parameters
angle_p = 36.5085
angle_y = -36.698
molec_to_fit = ['O2']  # must be a list
isotopes = [[1]]  # format [[1],[2]]
temp_guess = 731
vel_guess = 967
press_guess = 1.188  # atm
shift_guess = 0
conc_guess = 0.21
broad_guess = 1
temp_fit = True
press_fit = False
shift_fit = True
vel_fit = True
conc_fit = False
broad_fit = False

# Background Fitting Parameters
molec_back = 'O2'
pathlength_b = 10
temp_back = 309
conc_back_y = 0.0187
conc_back_p = 0.0187
shift_back = 0
press_back = 0.633
label_back = ''

conc_box = 0.0042
temp_box = 295
pres_box = 1

if not(back_remove):
    conc_back_y = 0
    conc_back_p = 0
    shift_back = 0
    press_back = 0
    
# Prepare save folder
now = datetime.now()
timestamp = now.strftime('%d_%m_%Y_%H%M%S')
save_path_0 = os.path.join(d_save,  timestamp)
if not(os.path.isdir(save_path_0)):
    os.mkdir(save_path_0)
    
###############################Port in data###################################
 
# prepare arrays for dataframe
fn_up = []
fn_down = []
vel = []
vel_unc = []
temp = []
temp_unc = []
press = []
press_unc = []
shift = []
shift_unc = []
conc = []
conc_unc = []
# broad = []
# broad_unc = []
# pl = []
# pl_unc = []
hapi.db_begin('')

# organization of fits
for i in range(iterations):
    print('iteration',i)
    save_path = os.path.join(save_path_0,str(i))
    if not(os.path.isdir(save_path)):
        os.mkdir(save_path)
    save_path = save_path

    if import_bkgdfit:
        conc_guess_box = df_box['Conc.'][i]
        temp_guess_box = df_box['Temp. (K)'][i]
        pres_guess_box = df_box['Pres. (atm)'][i]
        conc_back_y = df_bkgd['Conc. Out'][i]
        conc_back_p = df_bkgd['Conc. Out'][i]
        press_back = df_bkgd['Pres. Out (atm)'][i]
        temp_back = df_bkgd['Temp. Out (K)'][i]
    
    x_y, x_p, absflow_y, absflow_p, vel_val, vel_err, temp_val, temp_err,\
        press_val, press_err, shift_val,\
        shift_err, conc_val, conc_err, broad_val, broad_err = \
            fithit.FitProgram(fn_y, fn_p, save_path, file_tag,
                              weight_window_y, weight_window_p,
                              weight_flat, etalon_windows_y,
                              etalon_windows_p,
                              angle_y, angle_p, band_fit_y, band_fit_p,
                              molec_to_fit, isotopes, 
                              press_fit, temp_fit, vel_fit,
                              shift_fit, conc_fit, broad_fit,
                              press_guess, temp_guess, vel_guess,
                              shift_guess, conc_guess, broad_guess,
                              background_remove=back_remove,
                              pathlength_b=pathlength_b,
                              press_back=press_back, temp_back=temp_back,
                              conc_back_y=conc_back_y, conc_back_p=conc_back_p,
                              conc_box=conc_box, temp_box=temp_box, pres_box=pres_box,
                              save_files=save_files,
                              plot_results=plot_results,
                              transUnits=False,
                              noise_add=noise_add, noise_level=noise_level,
                              noise_flat=noise_flat,
                              eta_center=eta_center, beta=beta)
    vel.append(vel_val)
    vel_unc.append(vel_err)
    temp.append(temp_val)
    temp_unc.append(temp_err)
    press.append(press_val)
    press_unc.append(press_err)
    shift.append(shift_val)
    shift_unc.append(shift_err)
    conc.append(conc_val[0])
    conc_unc.append(conc_err[0])

# Create dict of results
d_fit = {'Vel. (m/s)': vel,
         'Vel. Unc.': vel_unc,
         'Temp. (K)': temp,
         'Temp. Unc.': temp_unc,
         'Pres. (atm)': press,
         'Pres. Unc.': press_unc,
         'Shift (cm-1)': shift,
         'Shift Unc.': shift_unc,
         'Conc.': conc,
         'Conc. Unc': conc_unc}

df_fit = pd.DataFrame.from_dict(d_fit)
if save_files:
    pickle.dump(df_fit, open(os.path.join(save_path_0,'df_fit.p'),'wb'))
    if iterations > 1:
        if vel_fit:
            vel_std = np.std(vel)
            vel_ave = np.mean(vel)
            print('Velocity mean is '+str(vel_ave))
            print('Velocity uncertainty is '+str(vel_std))
            np.savetxt(os.path.join(save_path_0,'vel.txt'),vel)
        if conc_fit:
            conc_std = np.std(conc)
            conc_ave = np.mean(conc)
            print('Concentration mean is '+str(conc_ave))
            print('Concentration uncertainty is '+str(conc_std))
            np.savetxt(os.path.join(save_path_0,'conc.txt'),conc)
        if press_fit:
            press_std = np.std(press)
            press_ave = np.mean(press)
            print('Pressure mean is '+str(press_ave))
            print('Pressure uncertainty is '+str(press_std))
            np.savetxt(os.path.join(save_path_0,'press.txt'),press)
        if temp_fit:
            temp_std = np.std(temp)
            temp_ave = np.mean(temp)
            print('Temperature mean is '+str(temp_ave))
            print('Temperature uncertainty is '+str(temp_std))
            np.savetxt(os.path.join(save_path_0,'temp.txt'),temp)
        if shift_fit:
            shift_std = np.std(shift)
            shift_ave = np.mean(shift)
            print('Shift mean is '+str(shift_ave))
            print('Shift uncertainty is '+str(shift_std))
            np.savetxt(os.path.join(save_path_0,'shift.txt'),shift)


print('Fitting Done!')