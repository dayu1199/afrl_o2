# -*- coding: utf-8 -*-
"""
Created on Thu May 18 17:29:30 2017

@author: Nazanin H
"""

import numpy as np
import numpy.fft as fft
import matplotlib.pyplot as plt
import os
import time
#from scipy.optimize import curve_fit
#import cmath


def ret_poly(x,a,b,c):
    return (np.polyval([a,b,c],x))

ts = time.time()               
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
k= 240000
ind =91001
iii=0
c=2.99792458e8


filename ='F:/Nazanin data/180116/Big window-cavity-test'
num_points=os.path.getsize(filename)/8  
num_chunckss=num_points/k
num_chuncks =10
print(num_chunckss)


ff64n = np.dtype(np.float64)
ff64f = ff64n.newbyteorder(">")
f1 = open(filename, 'rb')

  
IGavg=np.zeros(k/2/5,dtype=complex)   

frep1 = 200.044306e6
frep2 = 200.043472e6
f_stop_filter  = c/1500e-9 # 183.879834974741e12    
f_start_filter = f_stop_filter-(k/2)*frep1 

freq   = np.linspace(f_start_filter,f_stop_filter,k/2+1) /1e12

#data = np.fromfile(f1, dtype=ff64f, count=100*k)
 
jj=0
for i in range(num_chuncks):
        data = np.fromfile(f1, dtype=ff64f, count= k)
#        
##        if not i% 100 == 0:
###             continue
               
        ffts = fft.fft(data)[k/2:k+1]       
        fftss = ffts[4::5]
        freqq = freq [4::5]
        phi   = np.angle(fftss) 
#        phi_u = np.zeros(np.size(phi))
       

#        if i==0:
#             phi_u = np.unwrap(phi,discont=1.0*np.pi)
#             z_ref = np.polyfit(freqq[1005:2575],phi_u[1005:2575],2) 
#             phi_cor = phi_u - np.polyval(z_ref,freqq)                
#                   
#        else:
#            r,phi_u = np.vectorize(cmath.polar)(fftss)
#            phi_u = phi_cor
#            fftss = r*np.exp(-1j*phi_u)    
#            IGaligned = fft.ifft(fftss)    
          
                            
        phi_u = np.unwrap(phi,discont=1.0*np.pi)
        z_ref = np.polyfit(freqq[16000:20000],phi_u[16000:20000],3) 
        phi_cor = phi_u - np.polyval(z_ref,freqq) 
        IGaligned = fft.ifft(fftss*np.exp(-1j*np.polyval(z_ref,freqq)))
        IGavg += IGaligned
        
        
#        plt.figure(3)
#        plt.plot(np.abs(fft.fft(IGaligned)))     
#        plt.figure()
#        plt.plot(freqq,phi_u/np.pi,'.',freqq,phi_cor/np.pi,freqq,np.polyval(z_ref,freqq)/np.pi,freqq,phi/np.pi+7)
        
        
        #freq_mins = [0,]
        #freq_maxs = [182.18,181.6,180.18]
        #Delta = 0.05
#        freq_ignore = [182.18,181.6,180.18]
#        Delta = 0.1
#        freq_start = 0
#        for freqhole in freq_ignore:
#            inds, = np.nonzero((freqq>freq_start)*(freqq<freqhole - Delta))
#            phi_u[inds] = np.unwrap(phi[inds])
#            inds, = np.nonzero((freqq>freqhole-Delta)*(freqq<freqhole+Delta))
#            phi_u[inds] = phi[inds] - (phi[inds[0]] - np.unwrap([phi_u[inds[0]-1],phi[inds[0]]])[1])
#            freq_start = freqhole + Delta
        #z_ref,v = np.polyfit(freqq[1005:2300],phi_u[1005:2300],2,full=False,cov=True)   #2400:551
        
        
#        z_ref, v = curve_fit(ret_poly,freqq[1005:2300],phi_u[1005:2300],p0=[0,0,0])
#        phi_cor = phi_u - np.polyval(z_ref,freqq)
#        IGaligned = fft.ifft((fftss)*np.exp(-1j*np.polyval(z_ref,freqq)))
        
        
        #print(np.sum(np.abs(phi_cor[1005:2300])))
#        if np.sum(np.abs(phi_cor[1005:2300])) < 2000.:
#            IGaligned = fft.ifft((fftss)*np.exp(-1j*np.polyval(z_ref,freqq)))
#            #plt.figure()
#            #plt.plot(freqq,phi_u/np.pi,'.',freqq,phi_cor/np.pi,freqq,np.polyval(z_ref,freqq)/np.pi,freqq,phi/np.pi+7)
#            IGavg += IGaligned
#        else:
#            print(i)
#            jj += 1       
#plt.plot(freqq,phi_u/np.pi,'.',freqq,phi_cor/np.pi,freqq,np.polyval(z_ref,freqq)/np.pi,freqq,phi/np.pi+7)           
        
        
IGavg= IGavg/num_chuncks
fftavg = (np.abs(fft.fft((IGavg))))
phase= np.angle(fft.fft(IGavg))
f1.close()

wavelength=2.99792458e8/(freqq)/1e3 # -(10.37-0.68+0.33)

#f_start=2.99792458e8/wavelength[k/2-1]
#freq=np.linspace(f_start,(k/2-1)*frep1+f_start,k/20)

#plt.figure(1)
#plt.plot(np.real(IGavg))
#
plt.figure(1)
plt.xlabel('Frequency(THz)')
plt.ylabel(' Intensity (a.u.)')
plt.grid(axis=u'both')
plt.plot(freqq,fftavg)
 
print("it took me %.2f" %(time.time()-ts))
#%% Absorbance
#
#fit_param=np.polyfit(wavelength[1416:1736],-np.log(fftavg[1416:1736]),12)
#baseline=np.polyval(fit_param,wavelength[1416:1736])
#plt.figure(3)
#plt.plot(wavelength[1416:1736],-np.log(fftavg[1416:1736]),wavelength[1416:1736],baseline)
#plt.figure(4)
#plt.plot(wavelength[1416:1736],-np.log(fftavg[1416:1736])-baseline)

#%%
#wavelength=2.99792458e8/freqq/1e3
#CH4_filename='C:/Users/Administrator/Documents/Data bases/SpectraPlotSimulations/CH4,x=.000002,T=296K,P=1.0016atm,L=20000cm,simNum2.csv'   #this is with correct consenteration, path length
#CH4_data=np.loadtxt(CH4_filename, delimiter=",")   #skiprows=1, usecol=[2,3,4]
#CH4_freq= 2.99792458e8/(1e7/CH4_data[:,0]*1e-9)/1e12
#
#Ethane_filename='C:/Users/Administrator/Documents/Data bases/Ethane/NIST_C2H6_resampled.txt'# This data needs to be corrected for our pathlength and concenteration factor of ~2000
#Ethane_data=np.loadtxt(Ethane_filename)
#Ethane_freq_filename='C:/Users/Administrator/Documents/Data bases/Ethane/eth_freq.txt'
#Ethane_freq=np.loadtxt(Ethane_freq_filename)/1e12# 2.9992458e8/(1e7/Ethane_data[:,0]*1e-9)/1e12
#
#
#Water_filename='C:/Users/Administrator/Documents/Data bases/SpectraPlotSimulations/H2O,x=3e-3,T=300K,P=1atm,L=200000cm,simNum0.csv'# for 1km and 3e-3
#Water_data=np.loadtxt(Water_filename, delimiter=",")
#Water_freq= 2.9992458e8/(1e7/Water_data[:,0]*1e-9)/1e12
#
#
#plt.figure(6)
###plt.plot(Water_freq,Water_data[:,1]+2.5,'b',freqq ,fftavg*2-3.5,'r')
##plt.plot(CH4_freq,CH4_data[:,1]*100+2.5,'g', Ethane_freq,Ethane_data*200+2.5,'y',Water_freq,Water_data[:,1]*2+2.5,'b',freqq ,fftavg*2,'r')
#plt.plot(CH4_freq,CH4_data[:,1]*100,'g', freqq,fftavg)

#%%  Save to file     

#S= np.column_stack((freqq,phase))
#np.savetxt('C:/Users/Administrator/Documents/SCOUT/Data/CEDC-no sweeping/170102/11sec_avg_file5_phase', S ) 
