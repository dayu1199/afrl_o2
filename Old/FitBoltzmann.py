"""
FitBoltzmann

Created by on 8/12/22 by david

Description: Fit the Boltzman distribution of O2 lines

"""

import os
import numpy as np
import time
import matplotlib.pyplot as plt

from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
import td_support as td

# nonuniform-temperature code
from packfind import find_package
find_package('spectralfit_nonuniform')
import spectralfit_nonuniform as ebin
from length_bin import Snorm2Tx

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

import pickle

def FitNonUni(x_wvn, trans, fn, d_save,
              temp_uni, pres_uni, conc_uni, pl,
              conc_correction, temp_correction,
              lsq_on=True,
              uniform_on=True,
              import_L1=False, L1='',
              import_L2=False, L2='',
              corner_thrsh=0.3,
              reg_weight=10. ** (np.arange(0, -5, -0.1)),
              band_fit=[6880, 7188], bl=0.02, etalons=[],
              Elist =[0, 200, 500, 750, 1020, 1220, 1520, 1950, 2220, 2520],
              savedata=True, savedir='',
              linedir=r'/Users/david/Documents/UCF_Project/H2O_Scott.data',
              ls_refine=False, two_T=False, tmax=1500):
    '''
    '''
    # Set timer
    t0 = time.time()

    # Initialize the E"-binning object.
    fn_output = os.path.join(d_save, fn + '_ebin')
    N = ebin.EbinHapi(fn_output + 'new', bandwidth=band_fit)
    N.trans_spectrum = trans
    N.x_wvn = x_wvn
    N.bl = bl
    N.etalons = etalons

    # linelist
    N.linelist_path = linedir

    # Nominal path-average parameters
    conc_uni *= conc_correction
    temp_uni *= temp_correction
    N.t_celsius = temp_uni - 273.15
    N.p_torr = pres_uni * 760
    N.chi = conc_uni
    N.pathlength = pl

    # E" delineations between bins
    print('Default E" boundaries: E" =', N.Elist)
    N.Elist = Elist
    print('Actual E"-bin boundaries: E" =', N.Elist)

    # Now the actual fit
    N.fit_snorm();

    # output S(E") fit
    ebins = N.ebin_xVals
    snorm = N.snorm_out

    plt.figure()
    plt.plot(ebins, snorm, '+', label='float areas')
    plt.plot(N.ebin_xVals, N.snorm_out, 'x', label='float areas, width, shift')
    plt.xlabel('Lower-state energy E" (cm$^{-1}$)')
    plt.ylabel('Normalized linestrength')
    plt.legend()
    plt.title('E"-bin output')
    plt.show()

    # Optional refinement step\
    if ls_refine:
        scipy_results2 = N.fit_snorm_width();
        plt.close()
        scipy_results3 = N.fit_snorm_width_shift()
        print('Snorm(E")', N.snorm_out)
        print('Pressure(E")', N.p_rat)
        print('Shift(E")', N.shift)

        # show change in S(E") due to lineshape-floating
        print('Normalized linestrength plot before/after lineshape float')
        plt.figure()
        plt.plot(ebins, snorm, '+', label='float areas')
        plt.plot(N.ebin_xVals, N.snorm_out, 'x', label='float areas, width, shift')
        plt.xlabel('Lower-state energy E" (cm$^{-1}$)')
        plt.ylabel('Normalized linestrength')
        plt.legend()
        plt.title('E"-bin output After Refinement')

    # relative uncertainties
    unc_rel = N.snorm_uc / N.snorm_out
    print('Relative Uncertainties of EBins: ' + str(unc_rel))

    # two temeprature distribution
    if two_T:
        t1, t2, x1, x2 = N.fit_temperature(two_t_fit=True)
        print(t1, t2, x1, x2)

    ## Fit temperature distribution with lsq start condition
    if lsq_on:
        if not (import_L1):
            # if there is no saved L1, run the fit
            L1 = Snorm2Tx(N)
            L1.start_condition = 'lsq'
            L1.reg_weight = reg_weight
            L1.corner_thrsh = corner_thrsh
            plot_handles = L1.length_bin(tmax_override=tmax)
        else:
            L1.corner_thrsh = corner_thrsh
            plot_handles = L1.plot_fit(plot_limits=True)
        # L.save_fit()
        if savedata:
            # save plot as image
            plot_handles[0].savefig(os.path.join(savedir, 'NonUniFitPlotL1.png'))

        # Plot best fit temperature distribution

        L1.tx = np.sort(L1.tx, axis=0)
        tx_best1 = L1.tx[:, L1.corner_all]
        pxl_best1 = L1.pxl[L1.corner]
        # cost function
        cost1 = L1.res[L1.corner] + L1.reg_weight[L1.corner] * L1.reg[L1.corner]
        print('For LSQ Start:')
        print('Best-fit temperature array', tx_best1)
        print('Pxl column density stronger than E"-bin guess by %.3f' % pxl_best1)
        print('Best-fit curvature: ' + str(round(L1.reg[L1.corner], 2)))
        print('Best-fit residual: ' + str(round(L1.res[L1.corner], 2)))
        print('Best-fit cost function: ' + str(cost1))
        print('Reg Weights: ' + str(L1.reg_weight[L1.corner_all]))

        # SVD of temperature distribution
        L1.nbins = 16
        l = L1.get_l2()
        ltl = np.transpose(l) @ l
        u, s, v = np.linalg.svd(ltl)
        u_basis = u[:, -1:-6:-1]  # first 6 basis vectors
        basis = np.linalg.lstsq(u_basis, L1.tx[:, L1.corner])[0]
        plt.figure();
        for i in range(1, 6):
            plt.plot(u_basis[:, :i] @ basis[:i])
            plt.title('LSQ Start Basis')
            plt.savefig(os.path.join(savedir, 'LSQBasisPlot.png'))

    if uniform_on:
        ## Fit temperature distribution with uniform start condition
        if not (import_L2):
            # if there is no saved L2, run the fit
            L2 = Snorm2Tx(N)
            L2.start_condition = 'uniform'
            L2.reg_weight = reg_weight
            L2.corner_thrsh = corner_thrsh
            plot_handles = L2.length_bin(tmax_override=tmax)
        else:
            L2.corner_thrsh = corner_thrsh
            plot_handles = L2.plot_fit(plot_limits=True)
        # L.save_fit()
        if savedata:
            # save plot as image
            plot_handles[0].savefig(os.path.join(savedir, 'NonUniFitPlotL2.png'))
        # Plot best fit temperature distribution

        L2.tx = np.sort(L2.tx, axis=0)
        tx_best2 = L2.tx[:, L2.corner_all]
        pxl_best2 = L2.pxl[L2.corner]
        # cost function
        cost2 = L2.res[L2.corner] + L2.reg_weight[L2.corner] * L2.reg[L2.corner]
        print('For Uniform Start:')
        print('Best-fit temperature array', tx_best2)
        print('Pxl column density stronger than E"-bin guess by %.3f' % pxl_best2)
        print('Best-fit curvature: ' + str(round(L2.reg[L2.corner], 2)))
        print('Best-fit residual: ' + str(round(L2.res[L2.corner], 2)))
        print('Best-fit cost function: ' + str(cost2))
        print('Reg Weights: ' + str(L2.reg_weight[L2.corner_all]))

        # SVD of temperature distribution
        L2.nbins = 16
        l = L2.get_l2()
        ltl = np.transpose(l) @ l
        u, s, v = np.linalg.svd(ltl)
        u_basis = u[:, -1:-6:-1]  # first 6 basis vectors
        basis = np.linalg.lstsq(u_basis, L2.tx[:, L2.corner])[0]
        plt.figure();
        for i in range(1, 6):
            plt.plot(u_basis[:, :i] @ basis[:i])
            plt.title('Uniform Start Basis')
            plt.savefig(os.path.join(savedir, 'UniformBasisPlot.png'))

    if lsq_on and uniform_on:
        tx_accept = np.concatenate((L1.tx[:, L1.corner_all], L2.tx[:, L2.corner_all]), axis=1)
    elif lsq_on:
        tx_accept = L1.tx[:, L1.corner_all]
    elif uniform_on:
        tx_accept = L2.tx[:, L2.corner_all]

    uc_min = np.min(tx_accept, axis=1)
    uc_max = np.max(tx_accept, axis=1)

    print("Total nonuniform fit processing runtime %d seconds" %
          (time.time() - t0))

    plt.figure()
    plt.plot(np.sort(tx_accept))
    plt.xlabel('Length bin')
    plt.ylabel('Temperature (K)')
    plt.title('Best-fit temperature distributions')
    plt.savefig(os.path.join(savedir, 'NonUniFitPlotCombined.png'))

    if savedata:
        print('Saving data to\n', savedir)

        # save nonuni objects
        pickle.dump(N, open(os.path.join(savedir, 'N.p'), 'wb'))

        # save fit report
        f = open(os.path.join(savedir, 'NonUniFitReport.txt'), 'w')
        f.write(fn + '\n')
        f.write('Correct conc by : ' + str(conc_correction) + '\n')
        f.write('Correct temp by : ' + str(temp_correction) + '\n')
        f.write('Corner Threshold: ' + str(corner_thrsh) + '\n')
        f.write('E"-bin boundaries: E" = ' + str(N.Elist) + '\n')
        f.write('Relative uncertainties of E"Bins: ' + str(unc_rel))
        # save Print LSQ Data
        if lsq_on:
            f.write('\n\nL1 LSQ Start Results\n')
            f.write('Best-fit temperature array ' + str(tx_best1) + '\n')
            f.write('Pxl column density stronger than E"-bin guess by %.3f\n' % pxl_best1)
            f.write('Best-fit curvature: ' + str(round(L1.reg[L1.corner], 2)) + '\n')
            f.write('Best-fit residual: ' + str(round(L1.res[L1.corner], 2)) + '\n')
            f.write('Best-fit cost function: ' + str(round(cost1, 2)) + '\n')
            f.write('Reg Weights: ' + str(L1.reg_weight[L1.corner_all]) + '\n')
            # save max temps and min temps
            t_core1 = []
            t_wall1 = []
            for c in L1.corner_all:
                print(c)
                t_core1.append(max(L1.tx[:, c]))
                t_wall1.append(min(L1.tx[:, c]))
            f.write('Core Temps: ' + str(t_core1) + '\n')
            f.write('Wall Temps: ' + str(t_wall1) + '\n')
            pickle.dump(L1, open(os.path.join(savedir, 'L1.p'), 'wb'))

        # save Print Uniform Data
        if uniform_on:
            f.write('\n\nL2 Uniform Start Results\n')
            f.write('Best-fit temperature array ' + str(tx_best2) + '\n')
            f.write('Pxl column density stronger than E"-bin guess by %.3f\n' % pxl_best2)
            f.write('Best-fit curvature: ' + str(round(L2.reg[L2.corner], 2)) + '\n')
            f.write('Best-fit residual: ' + str(round(L2.res[L2.corner], 2)) + '\n')
            f.write('Best-fit cost function: ' + str(round(cost2, 2)) + '\n')
            f.write('Reg Weights: ' + str(L2.reg_weight[L2.corner_all]))
            # save max temps and min temps
            t_core2 = []
            t_wall2 = []
            for c in L2.corner_all:
                print(c)
                t_core2.append(max(L2.tx[:, c]))
                t_wall2.append(min(L2.tx[:, c]))
            f.write('Core Temps: ' + str(t_core2) + '\n')
            f.write('Wall Temps: ' + str(t_wall2) + '\n')
            pickle.dump(L2, open(os.path.join(savedir, 'L2.p'), 'wb'))

        # save combined solution
        f.write('\nCombined results\n')
        f.write('Best-fit temperature arrays' + str(tx_accept) + '\n')
        # save max temps and min temps
        t_core3 = []
        t_wall3 = []
        for arr in tx_accept:
            t_core3.append(max(arr))
            t_wall3.append(min(arr))
        f.write('Core Temps: ' + str(t_core3) + '\n')
        f.write('Wall Temps: ' + str(t_wall3) + '\n')

        # combined soluation
        f.close()

    return N, L1, L2, tx_accept

if __name__ == '__main__':
    # import/save params
    savedata = True
    lsq_on = True
    uniform_on = True
    import_L1 = False
    import_L2 = False
    if not (savedata):
        print('Not Saving Results')

    # directory info for uni fit
    d_fit = os.path.join(r'/Users/david/Library/CloudStorage',
                         r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                         r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits',
                         r'crossed/12_08_2022_095524')
    label = '7'
    d_uni = os.path.join(d_fit, label)
    fn_results_uni = label + '_report.txt'

    # directory info for upstream (moose)
    d_data_u = os.path.join(r'/Users/david/Library/CloudStorage',
                            r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                            r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData',
                            r'moose/031122_vis3/pc_peter')
    fn_u = '20220311204327_trans.txt'

    # directory info for downstream (nist)
    d_data_d = os.path.join(r'/Users/david/Library/CloudStorage',
                            r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                            r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData',
                            r'nist/031122_vis2/pc_peter')
    fn_d = '7_trans.txt'

    # create nonuni folder
    d_nonuni = os.path.join(d_uni, 'NonUniFit')
    if not(os.path.exists(d_nonuni)):
        os.makedirs(d_nonuni)

    # import/save params
    savedata = True
    lsq_on = True
    uniform_on = True
    import_L1 = False
    import_L2 = False
    if not (savedata):
        print('Not Saving Results')

    # fitting parameters
    conc_correction = 2.5
    temp_correction = 1.0
    corner_thrsh = 0.3
    reg_weight = 10. ** (np.arange(-0.05, -5, -0.05))
    pl = 4.5
    tmax = 1500
    molec_fit = 'O2'

    # import previous nonuniform fits
    if lsq_on:
        if import_L1:
            with open(os.path.join(d_uni, 'NonUniFit', 'L1.p'), 'rb') as fn:
                L1 = pickle.load(fn)
        else:
            L1 = ''
    else:
        L1 = ''

    if uniform_on:
        if import_L2:
            with open(os.path.join(d_uni, 'NonUniFit', 'L2.p'), 'rb') as fn:
                L2 = pickle.load(fn)
        else:
            L2 = ''
    else:
        L2 = ''

    # pull in information from uniform fit
    fn_read = os.path.join(d_uni, fn_results_uni)
    f = open(fn_read)
    for line in f:
        contents = line.split()
        if 'x_' + molec_fit + 'i1:' in line:
            conc_uni = float(contents[1])
        elif 'Temp:' in line:
            temp_uni = float(contents[1])
        elif 'press:' in line:
            pres_uni = float(contents[1])
        elif 'conc_back_p:' in line:
            conc_back_u = float(contents[1])
        elif 'conc_back_y:' in line:
            conc_back_d = float(contents[1])
        elif 'press_back:' in line:
            press_back = float(contents[1])
        elif 'temp_back:' in line:
            temp_back = float(contents[1])
        elif 'pathlength_b:' in line:
            pl_back = float(contents[1])
        elif 'conc_box:' in line:
            conc_box = float(contents[1])
        elif 'temp_box:' in line:
            temp_box = float(contents[1])
        elif 'temp_box:' in line:
            temp_box = float(contents[1])
        elif 'pres_box:' in line:
            pres_box = float(contents[1])
        elif 'pl_box:' in line:
            pl_box = float(contents[1])
        elif 'shift:' in line:
            shift = float(contents[1])

    # get trans and x_wvn data for upstream data
    data_u = np.loadtxt(os.path.join(d_data_u, fn_u))
    trans_u = data_u[:, 1]
    x_wvn_u = data_u[:, 0]


    # get trans and x_wvn data for downstream data
    data_d = np.loadtxt(os.path.join(d_data_d, fn_d))
    trans_d = data_d[:, 1]
    x_wvn_d = data_d[:, 0]

    print('Correct conc by : ' + str(conc_correction))
    print('Correct temp by : ' + str(temp_correction))
    print('Tmax set to ' + str(tmax))

    # cut down data first
    start_u, stop_u = td.bandwidth_select_td(x_wvn_u, [13014.3, 13172])
    x_wvn_u = x_wvn_u[start_u:stop_u]
    trans_u = trans_u[start_u:stop_u]
    abs_u = -np.log(trans_u)

    # create and subtract bkgd
    hapi.db_begin('')
    xx = x_wvn_u - shift
    step_xx = (max(xx) - min(xx)) / (len(xx) - 1)
    # box
    MIDS_box = [(7, 1, conc_box * hapi.abundance(7, 1))]
    [nu_box, coefs_box] = hapi.absorptionCoefficient_SDVoigt(MIDS_box,
                       'O2', OmegaStep=step_xx,
                       OmegaRange=[min(xx), max(xx)], HITRAN_units=False,
                       Environment={'p': pres_box, 'T': temp_box},
                       Diluent={'self': conc_box - 0.21 / 0.79 * (1 - conc_box),
                                'air': (1 - conc_box) / 0.79},
                       IntensityThreshold=0)
    abs_box = pl_box * coefs_box
    # bkgd
    MIDS_b = [(7, 1, conc_back_u * hapi.abundance(7, 1))]
    [nu_back, coefs_back] = hapi.absorptionCoefficient_SDVoigt(MIDS_b,
                   'O2', OmegaStep=step_xx,
                   OmegaRange=[min(xx), max(xx)], HITRAN_units=False,
                   Environment={'p': press_back, 'T': temp_back},
                   Diluent={
                       'self': conc_back_u - 0.21 / 0.79 * (1 - conc_back_u),
                       'air': (1 - conc_back_u) / 0.79},
                   IntensityThreshold=0)
    abs_back = pl_back * coefs_back

    abs_u -= abs_box
    abs_u -= abs_back
    trans = np.exp(-abs_u)

    # nonuniform fit of upstream data
    N, L1, L2, tx_accept = FitNonUni(x_wvn_u, trans_u, label, d_nonuni,
                                     temp_uni, pres_uni, conc_uni, pl,
                                     conc_correction, temp_correction,
                                     lsq_on=lsq_on,
                                     uniform_on=uniform_on,
                                     import_L1=import_L1, L1=L1,
                                     import_L2=import_L2, L2=L2,
                                     corner_thrsh=corner_thrsh,
                                     reg_weight=reg_weight,
                                     band_fit=[13040, 13160],
                                     bl=0.008, etalons=[[515, 530]],
                                     savedata=savedata, savedir=d_nonuni,
                                     Elist=[0, 50, 150, 300, 500, 700, 1000],
                                     linedir=r'/Users/david/Documents/AFRLO2/O2.data',
                                     ls_refine=False, two_T=False, tmax=tmax)

# Elist=[0, 25, 50, 100, 150, 200, 300, 400, 500, 600, 700, 800, 1000],
