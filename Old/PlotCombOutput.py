# -*- coding: utf-8 -*-
"""
Look at spectrum from csv file

Created on Thu Apr  1 14:08:54 2021

@author: David
"""
from sys import platform
import os
import csv
import numpy as np
import matplotlib.pyplot as plt

if platform == 'darwin':
    dr = r'/Users/davidyun/Documents/AFRL_O2/OutputSpectra'
elif platform == 'win32':
    dr = r'C:\Users\David\Google Drive\O2'
fn = 'DCSv5.4 Osc.csv'

i = 0
x = []
y = []
with open(os.path.join(dr,fn)) as csvfile:
    spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in spamreader:
        i += 1
        if row[0] == '[EndOfFile]':
            break
        elif i > 72:
            x_temp, y_temp = row[0].split(';')
            x.append(float(x_temp))
            y.append(float(y_temp))
            
plt.figure()            
plt.plot(x,y)
plt.xlabel('Wavelength (nm)')
plt.title(fn)

#transform to frequency
c = 299792458
x = np.asarray(x)
f = c/(x*1e-9)
y = np.asarray(y)
y = np.flip(y)
f = np.flip(f)
plt.figure()
plt.plot(f/1e12,y)
plt.xlabel('Frequency (THz)')
plt.title(fn)

# create pulse
y_t = np.fft.irfft(y)
df = f[1]-f[0]
dt = 1/df/len(y_t)
t = np.arange(len(y_t))*dt
y_t = y_t*np.exp(1j*f[0]*t)
# roll y_t to center
y_t = abs(y_t)

plt.figure()
plt.plot(t*1e15,y_t)
plt.xlabel('Time (fs)')
plt.title(fn)
