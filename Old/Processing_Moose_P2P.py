# -*- coding: utf-8 -*-
"""
Created on Friday March 12, 2021

@author: CoburnS

Purpose: Creating a generic type processing file that will eventually be turned
into a standard option.

This will be built from loop_fitting_test.py and fitting_test_single.py
    - The core processing code will be from fitting_test_single.py because that
        was used to tune the fitting parameters.
    - The outer loop for reading through the data repository will come from
        loop_fitting_test.py since that was been verified with field data
        collected at PAO.

This code assumes that the appropriate HAPI database (local) is already available
    - Needs to be configured for molecules being fits

"""

#   Package imports

import os
import time
from copy import copy
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from packfind import find_package

find_package("pldspectrapy")
import pldspectrapy as pld
import td_support as td

#   End package imports

#   Configurable variables -> eventually want to pull all of these out
#   and create a configuration file that sets these parameters

# Plot formatting - this will need to be optional
plt.rcParams.update({"figure.autolayout": True, "lines.linewidth": 0.8})

# Data repository
# d = r'C:\Users\CoburnS\Downloads\20201125_WindClineExperiments\_processing\scan1'
# d = r"C:\Users\CoburnS\Downloads\20201203_WindClineExperiments\scan3"
d = r'/Volumes/My Passport/O2Tests/110321_HardwareAvgTests'
fname = '20211102111848' # leave off .txt I think
processone = False
fitdata = False

# Save info: flag, file name, directory
savedata = True  # flag (0 = no save; 1 = save)
# fit individual igs for allan deviation calculation later
fitallan = True
pc_num = 1 # how many pcs to average together for fit allan
ig_num = 100 # how many averaged igs to process, if 0 or greater than available igsjust do all

savepath = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)/Data/Preliminary/110221_RetroPurgeTests')
savename = "fitresults.txt"
savename_td = "fitdata_td"
savename_freq = "fitdata_freq"
if not os.path.exists(savepath):
    os.mkdir(savepath)

# Plotting
plot_fit_td = 1  # flag (0 = no plot; 1 = plot)
plot_fit_freq = 1  # flag (0 = no plot; 1 = plot)

# Repository for the HAPI database
# listdir = os.path.join(curdir , 'linelists')
linedir = ''

# Frequency axis calculation
lockfreq = 32e6
nomfreq = 13333  # Should be within the range specified by band_fit (below)

# Band range for fit
# band_fit = [6880, 7260]  #good for HITRAN
band_fit = [13050.2, 13150]  # for Paul's database

# Create the baseline, etalon, and weight information
bl = 240
# Create a notch to catch etalons
etalons = []  # narrow range (Paul's range)

# HAPI/fitting parameters - will need this section for each molecule
if fitdata:
    db_name1 = "O2"
    mod, pars = td.spectra_single_lmfit("o2")
    pars["o2mol_id"].value = 7
    pars["o2molefraction"].set(value=0.209, vary=False)  # (molefraction)
    # pars['h2opressure'].set(value = (634.7/760), vary = False)  # (atm)
    pars["o2pressure"].set(value=0.83, vary=False)  # (atm), 20201125 Exp
    # pars['h2opressure'].set(value = (0.8351), vary = False)  # (atm), 20201203 Exp
    pars["o2temperature"].set(value=296, vary=False)  # (K)
    pars["o2pathlength"].set(value=60, vary=True)  # (cm)
    pars["o2shift"].var = True  # (cm-1, but always float)
    
    modfull = mod

#   End configurable variables

#   Start processing loop
name_array = []
hwd_avg_array = []
p2p1_array = []
p2p2_array = []
p2p3_array = []
p2p4_array = []
p2p5_array = []
p2p6_array = []
p2p7_array = []
p2p8_array = []
p2p9_array = []
p2p10_array = []
p2pavg_array = []
p2pstd_array = []
if __name__ == "__main__":

    t0 = time.time()
    files = pld.open_dir(d, recursion_levels=2, verbose=2)
    dt = time.time() - t0
    print("Opened %i files in %0.3f seconds" % (len(files), dt))
    newest = ""

    # simulate the absorption spectrum
    pld.db_begin(linedir)  # point HAPI (nested function in fit) to linelist file

    # Create a couple variables used for determining processing flow
    pass_pc = 0
    pass_raw = 0
    
    # Loop through directory and process data
    for name, daq_file in files.items():
        
        if processone:
            if name != fname:
                continue
            
        # Make sure there's some data
        if not daq_file.failure:

            # Are there phase corrected IGMs?
            if daq_file.data_pc is not None:
                non_empty_pc = sum(bool(i) for i in daq_file.igs_per_pc)
                pass_pc = 1 if (len(daq_file.data_pc) == non_empty_pc) else 0

            # Are there raw IGMs?
            if daq_file.data_raw is not None:
                non_empty_raw = len(daq_file.data_raw)
                pass_raw = 1 if (non_empty_raw > 0) else 0

            # As long as there's some IGMs go ahead and start processing!
            if pass_pc or pass_raw:
                newest = max(newest, name)
                print(name)
                
                daq_file.pc_lim_low = 0.18
                daq_file.pc_lim_high = 0.23
                # Phase correction and transform to transmission
                # daq_file.data_raw => phase correct and sum raw IGMs
                # daq_file.data_pc => phase correct and sum PC data=
                if fitallan:
                    if ig_num == 0 or ig_num > len(daq_file.data_raw):
                        n_pc = np.ceil((len(daq_file.data_raw)/pc_num))
                    else:
                        n_pc = ig_num
                else:
                    n_pc = 1
                
                temps = []
                concs = []
                press = []
                pathlengths = []
                shifts = []
                p2p = []
                
                name_array.append(name)
                hwd_avg_array.append(daq_file.num_hwavgs)
                for i in np.arange(n_pc):
                    print(str(int(i+1)) + ' out of ' + str(int(n_pc)) + ' fits\n')
                    if fitallan:
                        if i == (n_pc-1):
                            if n_pc*pc_num > len(daq_file.data_raw):
                                modulo = len(daq_file.data_raw) % pc_num
                                data_ig = daq_file.data_raw[int(pc_num*i):int(pc_num*i+modulo)]
                        data_ig = daq_file.data_raw[int(pc_num*i):int(pc_num*(i+1))]
                    else:
                        data_ig = daq_file.data_raw
                    
                    p2p.append((np.max(data_ig[0])-np.min(data_ig[0]))/(daq_file.num_hwavgs))
                    
                    if i==0:
                        p2p1_array.append((np.max(data_ig[0])-np.min(data_ig[0]))/(daq_file.num_hwavgs))
                    elif i==1:
                        p2p2_array.append((np.max(data_ig[0])-np.min(data_ig[0]))/(daq_file.num_hwavgs))
                    elif i==2:
                        p2p3_array.append((np.max(data_ig[0])-np.min(data_ig[0]))/(daq_file.num_hwavgs))
                    elif i==3:
                        p2p4_array.append((np.max(data_ig[0])-np.min(data_ig[0]))/(daq_file.num_hwavgs))
                    elif i==4:
                        p2p5_array.append((np.max(data_ig[0])-np.min(data_ig[0]))/(daq_file.num_hwavgs))
                    elif i==5:
                        p2p6_array.append((np.max(data_ig[0])-np.min(data_ig[0]))/(daq_file.num_hwavgs))
                    elif i==6:
                        p2p7_array.append((np.max(data_ig[0])-np.min(data_ig[0]))/(daq_file.num_hwavgs))
                    elif i==7:
                        p2p8_array.append((np.max(data_ig[0])-np.min(data_ig[0]))/(daq_file.num_hwavgs))
                    elif i==8:
                        p2p9_array.append((np.max(data_ig[0])-np.min(data_ig[0]))/(daq_file.num_hwavgs))
                    elif i==9:
                        p2p10_array.append((np.max(data_ig[0])-np.min(data_ig[0]))/(daq_file.num_hwavgs))
                    
                    pc_ig = pld.pc_truncated(
                        data_ig,
                        daq_file.pc_lim_low,
                        daq_file.pc_lim_high,
                        daq_file.frame_length,
                    )
                    
                    # p2p.append((np.max(pc_ig)-np.min(pc_ig))/(pc_num*daq_file.num_hwavgs))
                    
                    trans = np.abs(np.fft.fft(pc_ig))
    
                    # Create frequency axis
                    x_wvn_full = pld.mobile_axis(
                        daq_file, f_opt=lockfreq, wvn_spectroscopy=nomfreq
                    )
    
                    tic = time.time()  # time-domain-specific fitting starts here
    
                    # Convert interesting portion of spectrum to time-domain
                    start_pnt, stop_pnt = td.bandwidth_select_td(x_wvn_full, band_fit)
                    trans_orig = copy(trans)
                    if start_pnt < stop_pnt:
                        # Normal setup
                        trans = trans[start_pnt:stop_pnt]
                        x_wvn = x_wvn_full[start_pnt:stop_pnt]
                        y_td = np.fft.irfft(-np.log(trans))
                    else:
                        # DCS in 0.5-1.0 portion of Nyquist window, need to flip x-axis to fit
                        trans_flipped = trans[int((daq_file.frame_length / 2)) :: -1]
                        x_wvn_flipped = x_wvn_full[::-1]
                        start_pnt, stop_pnt = td.bandwidth_select_td(
                            x_wvn_flipped, band_fit
                        )
                        x_wvn = x_wvn_flipped[start_pnt:stop_pnt]
                        trans = trans_flipped[start_pnt:stop_pnt]
                        y_td = np.fft.irfft(-np.log(trans))
                        
                    if fitdata:
                        # Create the weighting function for time domain fitting
                        weight = td.weight_func(len(x_wvn), bl, etalons)
        
                        # Here we need to insert some functionality for determining
                        # a temperature and pressure value for use in the fitting
                        # parameters
        
                        # Run the time domain fitting
                        # Note that the h2oname parameter will need to be updated
                        # - could probably pull that out into the configuration file
                        # as well
                        Fit = modfull.fit(
                            y_td, xx=x_wvn, params=pars, weights=weight, name=db_name1
                        )
                        #                Fit = modfull.fit(y_td, xx = x_wvn, params = pars,
                        #                                  weights = weight, h2oname = 'H2O')
        
                        # This section sets up data for plotting so may not be needed
        
                        # Pull time domain fit data
                        y_datai = Fit.data
                        fit_datai = Fit.best_fit
                        weight = Fit.weights
        
                        # Pull frequency domain fit data
                        data_lessbl = np.real(
                            np.fft.rfft(y_datai - (1 - weight) * (y_datai - fit_datai))
                        )
                        model = np.real(np.fft.rfft(fit_datai))
                        residual = data_lessbl - model
                                                     
                        print(Fit.fit_report())
                        
                        print('Noise level is '+str(np.std(residual[1942:2391])))
                        print('P2P is '+str(p2p[-1]))
                        
                        # store fits in arrays
                        concs.append(Fit.best_values['o2molefraction'])
                        temps.append(Fit.best_values['o2temperature'])
                        press.append(Fit.best_values['o2pressure'])
                        pathlengths.append(Fit.best_values['o2pathlength'])
                        shifts.append(Fit.best_values['o2shift'])
                        
                        # # Optional plotting
                        # if fitallan and i == (n_pc-1):
                        #     plot_fit_td = 1
                        #     plot_fit_freq = 1
                        #     plt.figure()
                        #     plt.plot(1/x_wvn_full*1e7, trans_orig[:len(x_wvn_full)])
                        #     plt.figure()
                        #     plt.plot(x_wvn, trans)
                        # elif not(fitallan):
                            # plot_fit_td = 1
                        #     plot_fit_freq = 1
                        # else:
                        #     plot_fit_td = 0
                        #     plot_fit_freq = 0
                            
                        if plot_fit_td:
                            # And plot time domain
                            plt.figure()
                            plt.plot(y_datai, label='Data')
                            plt.plot(fit_datai, label= 'Model')
                            plt.plot(y_datai - fit_datai, label='Residual')
                            plt.plot(weight)
                            plt.legend()
                            plt.title(str(i))
                        if plot_fit_freq:
                            # And plot frequency domain
                            fig, axs = plt.subplots(2, 1, sharex="col")
                            axs[0].plot(x_wvn, data_lessbl, x_wvn, model)
                            axs[1].plot(x_wvn, residual)
                            axs[0].set_title(str(i))
                            plt.figure()
                            plt.plot(1/x_wvn_full*1e7, trans_orig[:len(x_wvn_full)])
                            plt.title(str(i))
                            plt.figure()
                            plt.plot(x_wvn, trans)
                            plt.title(str(i))
                            # axs[0].set_ylabel('Absorbance'); #axs[0].legend(['data','fit'])
                            # axs[1].set_ylabel('Residual'); axs[1].set_xlabel('Wavenumber ($cm^{-1}$)')
        
                        # End the plotting section
        
                        # Section for saving data -> will probably be replaced
        
                        if savedata:
                            # Create an output string with file information
                            outinfo = (
                                "====================================================" + "\n"
                            )
                            outinfo += "[[Data Info]]\n"
                            outinfo += "\tData file\t\t\t = " + name + "\n"
                            outinfo += "\tTotal P2P\t\t\t = " + str(daq_file.p2p_total) + "\n"
                            outinfo += "\tFit region\t\t\t = " + str(band_fit) + "\n"
                            outinfo += "\tBaseline\t\t\t = " + str(bl) + "\n"
                            outinfo += "\tEtalons\t\t\t\t = " + str(etalons) + "\n"
        
                            # DY - In order to save fit results to a textfile I use the following lines:
                            f = open(os.path.join(savepath, savename), "a")
                            if fitallan:
                                f.write('pc '+str(int(i))+'\n')
                            f.write(outinfo)
                            f.write(Fit.fit_report() + "\n")
                            # f.write('===================================================='+'\n')
                            f.close()
        
                            # Save the time domain fit arrays
                            fit_outdata_td = np.array([y_datai, fit_datai, weight])
                            fit_outdata_td = fit_outdata_td.T
                            if fitallan:
                                np.savetxt(
                                    os.path.join(savepath, savename_td + "_" + name + "_" + str (int(i)) + ".txt"),
                                    fit_outdata_td,
                                    delimiter="\t",
                                )
                            else:
                                np.savetxt(
                                    os.path.join(savepath, savename_td + "_" + name + ".txt"),
                                    fit_outdata_td,
                                    delimiter="\t",
                                )
        
                            # Save the frequency domain fit arrays
                            fit_outdata_freq = np.array([data_lessbl, model, x_wvn])
                            fit_outdata_freq = fit_outdata_freq.T
                            if fitallan:
                                np.savetxt(
                                    os.path.join(savepath, savename_freq + "_" + name + ".txt"),
                                    fit_outdata_freq,
                                    delimiter="\t",
                                )
                            else:
                                np.savetxt(
                                    os.path.join(savepath, savename_freq + "_" + name + "_" + str (int(i)) + ".txt"),
                                    fit_outdata_freq,
                                    delimiter="\t",
                                )
        
                            f = open(os.path.join(savepath, savename), "a")
                            f.write(
                                "====================================================" + "\n"
                            )
                            f.close()
        
                        # End saving section
                    
                p2pavg_array.append(np.mean(p2p))
                p2pstd_array.append(np.std(p2p))
                if fitdata and savedata:
                    np.savetxt(os.path.join(savepath, name+'_conc.txt'), concs)
                    np.savetxt(os.path.join(savepath, name+'_pathlength.txt'), pathlengths)
                    np.savetxt(os.path.join(savepath, name+'_temp.txt'), temps)
                    np.savetxt(os.path.join(savepath, name+'_pres.txt'), press)
                    np.savetxt(os.path.join(savepath, name+'_shift.txt'), shifts)

    print("Total total runtime %d seconds" % (time.time() - t0))
    
    p2p_dict = {
        'name': name_array,
        '# hardware averages': hwd_avg_array,
        'p2p average': p2pavg_array,
        'p2p std. dev': p2pstd_array,
        'p2p 1': p2p1_array,
        'p2p 2': p2p2_array,
        'p2p 3': p2p3_array,
        'p2p 4': p2p4_array,
        'p2p 5': p2p5_array,
        'p2p 6': p2p6_array,
        'p2p 7': p2p7_array,
        'p2p 8': p2p8_array,
        'p2p 9': p2p9_array,
        'p2p 10': p2p10_array}
    p2p_df = pd.DataFrame.from_dict(p2p_dict)
    
    plt.figure()
    plt.errorbar(hwd_avg_array, p2pavg_array, yerr=p2pstd_array,
                 linestyle='None', marker='.', markersize=8, capsize=4)