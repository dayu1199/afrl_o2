#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Check python igor pc function 
pc_linear in igtools pldspectrapy1

Created on Fri Oct  8 16:15:15 2021

@author: david
"""
import os
import numpy as np
import matplotlib.pyplot as plt

from packfind import find_package
find_package("pldspectrapy1")
import pldspectrapy1 as pld1

# directory info
d = r'/Volumes/GoogleDrive/My Drive/IgorCode/DavidProcedures/Data_Temp'
fn = '10'

# pc params
pc_lim_low=0.12
pc_lim_high=0.18
num_pcs=150

# import spectra
# Phase Correct IGs
IG = pld1.open_daq_files(os.path.join(d, fn))
#%% Igor PC
# output1 = pld1.pc_igor(IG, num_pcs=num_pcs, pc_lim_low=pc_lim_low,
                        # pc_lim_high=pc_lim_high, thrsh=1000)
#%% Truncate PC
output2 = pld1.pc_truncated(IG, pc_lim_low=pc_lim_low,
                            pc_lim_high=pc_lim_high)
#%% Traditional PC
# output3 = pld1.Spectra(IG, num_pcs=len(IG), pc_lim_low=pc_lim_low,
                       # pc_lim_high=pc_lim_high)
#%% Linear PC
# output4 = pld1.pc_linearflatten(IG, pc_lim_low=pc_lim_low, pc_lim_high=pc_lim_high)
#%% Conjugate PC
output5 = pld1.pc_conjugate(IG)
#%% Peter PC
data = IG.data_raw.copy()
output6 = pld1.pc_peter(data, pc_lim_low, pc_lim_high, zoom=500)

# %%
# import igor PC
# igor = np.loadtxt('/Volumes/GoogleDrive/My Drive/Misc/woo.txt',skiprows=1)
# igor_pc = igor

# out1 = abs(np.fft.rfft(output1))
out2 = abs(np.fft.rfft(output2))
# out3 = abs(output3.data[0])
# out4 = abs(np.fft.fft(output4))
out5 = output5
out6 = output6
# outigor = igor_pc


plt.figure()
# plt.plot(out1/max(out1[5000:]),label='Pyt Linear')
plt.plot(out2/max(out2[5000:]),label='Pyt Truncate')
# plt.plot(out3/max(out3[5000:]),label='Pyt Trad')
# plt.plot(out4/max(out4[5000:]),label='Pyt Linear Naz')
plt.plot(out5/max(out5[5000:]),label='Pyt Conjugate')
plt.plot(out6/max(out6[5000:]),label='Pyt Peter')
# plt.plot(outigor/max(outigor[5000:]),label='Igor Linear')
plt.legend()