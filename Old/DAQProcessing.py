# -*- coding: utf-8 -*-
"""
Created on Friday March 12, 2021

@author: CoburnS

Purpose: Creating a generic type processing file that will eventually be turned
into a standard option.

This will be built from loop_fitting_test.py and fitting_test_single.py
    - The core processing code will be from fitting_test_single.py because that
        was used to tune the fitting parameters.
    - The outer loop for reading through the data repository will come from
        loop_fitting_test.py since that was been verified with field data
        collected at PAO.

This code assumes that the appropriate HAPI database (local) is already available
    - Needs to be configured for molecules being fits

"""

#   Package imports

import os
import time
import numpy as np
import matplotlib.pyplot as plt

from packfind import find_package

find_package("pldspectrapy")
import pldspectrapy as pld
import td_support as td
plt.rcParams.update({"figure.autolayout": True, "lines.linewidth": 0.8})

# directory where data is
d = r'F:\UCF\lab_tests\RefChannel\15m'
# data filename
fname = '' # leave off .txt
processone = True # process just one file, False if you want to process all
                    # data in directory
                    
# where to save data
d_save = r'F:\UCF\lab_tests\RefChannel\15m\FitResults'

# Frequency axis calculation
lockfreq = 32e6
nomfreq = 13333  # change for o2

# open files in data files in directory
files = pld.open_dir(d, recursion_levels=2, verbose=2)
dt = time.time() - t0
print("Opened %i files in %0.3f seconds" % (len(files), dt))
newest = ""

# Create a couple variables used for determining processing flow
pass_pc = 0
pass_raw = 0

# Loop through directory and process data
for name, daq_file in files.items():
    
    if processone:
        if name != fname:
            continue
        
    # Make sure there's some data
    if not daq_file.failure:

        # Are there phase corrected IGMs?
        if daq_file.data_pc is not None:
            non_empty_pc = sum(bool(i) for i in daq_file.igs_per_pc)
            pass_pc = 1 if (len(daq_file.data_pc) == non_empty_pc) else 0

        # Are there raw IGMs?
        if daq_file.data_raw is not None:
            non_empty_raw = len(daq_file.data_raw)
            pass_raw = 1 if (non_empty_raw > 0) else 0

        # As long as there's some IGMs go ahead and start processing!
        if pass_pc or pass_raw:
            newest = max(newest, name)

            # Phase correction and transform to transmission
            # daq_file.data_raw => phase correct and sum raw IGMs
            # daq_file.data_pc => phase correct and sum PC data
            pc_ig = pld.pc_truncated(
                daq_file.data_raw,
                daq_file.pc_lim_low,
                daq_file.pc_lim_high,
                daq_file.frame_length,
            )
            # convert IG to transmission
            trans = np.abs(np.fft.fft(pc_ig))
            
            # convert transmission to absorbance
            absorbance = -np.log(trans)
            
            # Create frequency axis
            x_wvn = pld.mobile_axis(
                daq_file, f_opt=lockfreq, wvn_spectroscopy=nomfreq
            )
            
            absorbance_data = np.column_stack((x_wvn,absorbance))
            np.savetxt(os.path.join(d_save,name+'_abs.txt'), absorbance_data)