# -*- coding: utf-8 -*-
"""
Run Fitting Program

Parameters to define before fitting:
    beam_mode: 1,2
    beam1_channel: 'yellow_crossed','purple_crossed','white_h2o_channel','green'
    scan = 1,2,3,4,5
    
TODO: fix shift back
    add background remove capability
    add ability to do specialized fits

Created on Tue Sep 24 13:28:11 2019
@author: David
"""
import os
import sys
import numpy as np
import FitProgramRetro_Annulus as fithit
from datetime import datetime
################################Change this###################################
fn = 'data_RDEsimAnnu_abs.txt' # text file name of data
                    #data should be in format of column 1: freq (cm-1), absorbance
data_0 = np.loadtxt('data_RDEsimAnnu_abs.txt')
save_dir = r'C:\Users\David\Google Drive\RDE\FitData'
file_tag = '' # tag to add to file name 
# Add noise?
absorbance_noise = 0.000
iterations = 1

## Fitting Parameters
# geometry
angle = 0
pathlength = 0 # total pathlength of beam
# molecules
molec_to_fit = ['O2'] # must be a list
isotopes = [[1]] # format [[1],[2]]
# initial fit guess
press_guess = 0.8 # atm
temp_guess = 600
vel_guess = 1000
shift_guess = 0
conc_guess = 0.25
broad_guess = 1
# What to fit for
temp_fit = True
press_fit = True
shift_fit = True
vel_fit = True
conc_fit = True
broad_fit = False

##############################################################################
save_files = True # saves fits and fitted parameters in text files.
plot_results = True # Plots time domain and TD fit converted to the frequency domain. Turn this off for many datafiles.
print_fit_report = True # Prints each fit report to command window 
back_remove = False # Remove background from spectra before fitting
# MFID Parameters
weight_window = [0.004,0.5] # default [0.005,0.5]
etalon_windows = [] # format [[0,100],[200,300]]
band_fit = [12820,13333]
weight_flat = True # Otherwise will do an exponential weighting


# Background Fitting Parameters
temp_back = 296.00000
shift_back = 0
press_back = 0
label_back = ''

if not(back_remove):
    conc_back = 0
    shift_back = 0
    press_back = 0
    
# Prepare save folder
now = datetime.now()
timestamp = now.strftime('%d_%m_%Y_%H%M%S')
save_path_0 = os.path.join(save_dir,fn+'_'+timestamp)
if not(os.path.isdir(save_path_0)):
    os.mkdir(save_path_0)    
    
vel = []
vel_er = []
temp = []
temp_er = []
press = []
press_er = []
shift = []
shift_er = []
conc = []
conc_er = []
broad = []
broad_er = []


for i in range(iterations):
        print('iteration',i)
        data = np.zeros(data_0.shape)
        save_path = os.path.join(save_path_0,str(i))
        if not(os.path.isdir(save_path)):
            os.mkdir(save_path)
        save_path = save_path+'//'
        noise = np.random.normal(0,absorbance_noise,len(data[:,1]))
        data[:,0] = data_0[:,0]
        data[:,1] = data_0[:,1]+noise
        # run fit module
        vel_val, vel_err, temp_val, temp_err, press_val, press_err, shift_val,\
            shift_err, conc_val, conc_err, broad_val, broad_err = \
                fithit.FitProgram(data, save_path, file_tag, weight_window,
                                  weight_flat, etalon_windows, angle, band_fit,
                                  molec_to_fit, isotopes, 
                                  press_fit, temp_fit, vel_fit,
                                  shift_fit, conc_fit, broad_fit,
                                  press_guess, temp_guess, vel_guess,
                                  shift_guess, conc_guess, broad_guess,
                                  plot_results=plot_results)
        vel.append(vel_val)
        vel_er.append(vel_err)
        temp.append(temp_val)
        temp_er.append(temp_err)
        press.append(press_val)
        press_er.append(press_err)
        shift.append(shift_val)
        shift_er.append(shift_er)
        conc.append(conc_val)
        # conc_er.append(conc_er)

if iterations > 1:
    if vel_fit:
        vel_std = np.std(vel)
        print('Velocity uncertainty is '+str(vel_std))
        np.savetxt(os.path.join(save_path_0,'vel.txt'),vel)
    if conc_fit:
        conc_std = np.std(conc)
        print('Concenctration uncertainty is '+str(conc_std))
        np.savetxt(os.path.join(save_path_0,'conc.txt'),conc)
    if press_fit:
        press_std = np.std(press)
        print('Pressure uncertainty is '+str(press_std))
        np.savetxt(os.path.join(save_path_0,'press.txt'),press)
    if temp_fit:
        temp_std = np.std(temp)
        print('Temperature uncertainty is '+str(temp_std))
        np.savetxt(os.path.join(save_path_0,'temp.txt'),temp)
    if shift_fit:
        shift_std = np.std(shift)
        print('Shift uncertainty is '+str(shift_std))
        np.savetxt(os.path.join(save_path_0,'shift.txt'),shift)

print('Fitting Done!')