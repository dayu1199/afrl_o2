#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Program for preparing retro data for BACKGROUND fitting

Created on Mon Apr 11 10:38:35 2022

@author: david
"""
import os
import matplotlib.pyplot as plt
import numpy as np
import time

from FitMFIDBkgdRetro import Fitting_Spectra
import td_support as td
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi

def FitProgram(filename, save_path, file_tag, weight_window, weight_flat,
               etalon_windows, angle, band_fit, molecules_to_fit, isotopes,
               shift_fit, shift_guess,
               pressure_fit_in, temp_fit_in, velocity_fit_in, conc_fit_in,
               pressure_fit_out, temp_fit_out, conc_fit_out, pathlength_fit_out,
               pressure_guess_in, temp_guess_in, velocity_guess_in,
               conc_guess_in,
               pressure_guess_out, temp_guess_out, conc_guess_out,
               pathlength_guess_out,
               broadening_fit, broadening_guess,
               pathlength_in=6.84276, n_pass=2,
               save_files=True, plot_results=True, print_fit_report=True,
               wvnUnits=True, transUnits=True):
    
    ###########################################################################
    theta = angle/360*2*np.pi # convert deg to rad

    pathlength_in = n_pass*pathlength_in/np.cos(theta)

    conc_in = [conc_guess_in]*len(molecules_to_fit) # Initializes concentration
                                            # guess for each molecule to fit
    conc_out = [conc_guess_out]*len(molecules_to_fit)
    
    conc_fit_in = [conc_fit_in]*len(molecules_to_fit) # True for each molecule
                                                        # to fit
    conc_fit_out = [conc_fit_out]*len(molecules_to_fit)
    
    # ------------- Time Domain Specific Parameters ---------------------------    
    ###########################################################################    
    print(filename)
    
    # Loads in file
    data = np.loadtxt(filename)
    
    if data[0,0]>data[-1,0]: # Flips data if it is inverted
        data = np.flipud(data) 
        print("Flipping FFT")
    
    print(len(data))
    if (len(data)!=len(data)):
            print('Data have inconsistent sizes')
    
    # Imports frequency wave for data, converts to wavenumber if in frequency
    # units
    start, stop = td.bandwidth_select_td(data[:,0], band_fit)
    if start < stop:
        if wvnUnits:
            x_data = data[start:stop, 0]
        else:
            x_data = data[start:stop, 0]/29979245800
    else:
        if wvnUnits:
            x_data = data[start:stop:-1, 0]
        else:
            x_data = data[start:stop:-1, 0]/29979245800
    
    # Imports spectra into y_data, converts to absorbance if in transmission
    # units
    y_data = np.zeros((len(x_data),1))
    if transUnits:
        y_data[:,0] = -np.log(data[start:stop,1])
    else:
        y_data[:,0] = data[start:stop,1]
    
    # Convert to time domain
    ff = (np.fft.irfft(y_data[:,0]))
    y_fit = np.zeros((ff.size,(len(data[0])-1)*2))
    
    # Builds logic vector to imform fit of parameters to float
    fit_vector = [temp_fit_in, temp_fit_out, pressure_fit_in, pressure_fit_out, 
        shift_fit, broadening_fit, velocity_fit_in, pathlength_fit_out]
    fit_vector.extend(conc_fit_in)
    fit_vector.extend(conc_fit_out)
    
    # Builds vector of approximate parameters for starting fits
    guess_vector = [temp_guess_in, temp_guess_out, pressure_guess_in,
        pressure_guess_out, shift_guess,
        broadening_guess, velocity_guess_in, pathlength_guess_out]
    guess_vector.extend(conc_in)
    guess_vector.extend(conc_out)
    
    # Loads Molecule Data from HITRAN
    hapi.db_begin("")  # Location to store imported HITRAN data
    
    HITRAN_Molecules = ['H2O','CO2','O3','N2O','CO','CH4','O2','NO','SO2',
                        'NO2','NH3','HNO3','OH','HF','HCl','HBr','HI','ClO',
                        'OCS','H2CO','HOCl','N2','HCN','CH3Cl','H2O2','C2H2',
                        'C2H6','PH3','COF2','SF6','H2S','HCOOH','HO2','O',
                        'ClONO2','NO+','HOBr','C2H4','CH3OH','CH3Br','CH3CN',
                        'CF4','C4H2','HC3N','H2','CS','SO3','ArH2O_Labfit']
    molecule_numbers =[0]*(len(molecules_to_fit))
    
    ii=0
    for item in molecules_to_fit:
        if item not in HITRAN_Molecules:
            print("Listed Molecule is not included in HITRAN", item)
        else:  
            molecule_numbers[ii] = (HITRAN_Molecules.index(item))+1
            isotope_ids = []
            for jj in range(len(isotopes[ii])):
                isotope_ids.append(hapi.ISO[molecule_numbers[ii],
                                            isotopes[ii][jj]][0]) 
                
            hapi.fetch_by_ids(item, isotope_ids,min(x_data)-1,
                              max(x_data)+1) 
        ii+=1

    tic= time.time()  # Times each fit iteration
    if not y_data[10]:  # checks from empty FFTs and nans in data
        print("Empty FFT")
    else:
        # This sends data to function which compiles it for the fit
        [result, time_wave, conc_in, conc_err_in, conc_out,
         conc_err_out, weight_wave] =\
            Fitting_Spectra(guess_vector,
                fit_vector, x_data, y_data[:,0],
                molecules_to_fit, molecule_numbers, isotopes,
                pathlength_in, theta,
                weight_window, weight_flat,
                etalon_windows=etalon_windows)
                                      
                                      
        # Stores fitted parameters and one sigma uncertainties
        Temperature_In = result.best_values['temp_in']
        Temperature_Out = result.best_values['temp_out']
        Pressure_In = result.best_values['press_in']
        Pressure_Out = result.best_values['press_out']
        Shift = result.best_values['shift']
        Shift_err = result.params['shift'].stderr
        Broadening = result.best_values['broadening']
        Broadening_err = result.params['broadening'].stderr
        Temperature_In_err = result.params['temp_in'].stderr
        Temperature_Out_err = result.params['temp_out'].stderr
        Pressure_In_err = result.params['press_in'].stderr
        Pressure_Out_err = result.params['press_out'].stderr
        Velocity_In = result.best_values['velocity_in']
        Velocity_In_err = result.params['velocity_in'].stderr
        Pathlength_Out = result.best_values['pathlength_out']
        Pathlength_Out_err = result.params['pathlength_out'].stderr
        
        Conc_In = []
        Conc_In_err = []
        Conc_Out = []
        Conc_Out_err = []
        for jj in range(len(molecules_to_fit)):
            Conc_In.append(conc_in[jj])
            Conc_In_err.append(conc_err_in[jj])
            Conc_Out.append(conc_out)
            Conc_Out_err.append(conc_err_out[jj])
    
        # Stores best fits and corresponding data in y_fit matrix             
        time_fit = result.best_fit
        time_data = result.data
        time_res = time_data - time_fit
        
        abs_fit = np.real(np.fft.rfft(time_fit))
        abs_data = np.real(np.fft.rfft(time_data))
        abs_datanobl = np.real(np.fft.rfft(time_data-
                                             (1-weight_wave)*time_res))
        abs_resnobl = abs_datanobl-abs_fit
        
        if print_fit_report:
            print(result.fit_report(result.params))

        toc = time.time()     
        print('Computation Time:', toc - tic)
        
        ffn = os.path.basename(filename)[:-10]
        if plot_results:
            # FFT of time domain fits yellow stream
            plt.figure() 
            plt.plot(x_data,abs_datanobl, label='Data', linewidth=1)
            plt.plot(x_data,abs_resnobl,
                     label='Residual', linewidth=1)
            plt.plot(x_data,abs_fit, label='Fit',
                linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title("Yellow Stream"+ffn)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)

    
            # Time domain fit plot. Plot vs time_wave for effective time on x axis 
            # yellow
            plt.figure()
            plt.plot(time_data, label='Data', linewidth=1)
            plt.plot(time_res,
                     label='Residual', linewidth=1)
            plt.plot(time_fit,label='Fit', linewidth=1) 
            plt.plot(result.weights*max(time_data))
            # plt.title()
            plt.xlabel('Index')
            plt.ylabel('Magnitude')
            plt.title("Yellow Stream"+ffn)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
    
        if save_files:  
            filename_report = os.path.join(save_path,
                                           ffn+file_tag +'_report.txt')
            
            f = open(filename_report,'w')
            f.write(result.fit_report())
            f.close()
            
            # save cepstrum data
            filename_save_cep = os.path.join(save_path,
                                               ffn+file_tag +'_cep.txt')
            cep_save = np.transpose([time_data,time_fit])
            np.savetxt(filename_save_cep,cep_save)
        
            # save absorbance data
            filename_save_abs = os.path.join(save_path,
                                               ffn+file_tag +'_abs.txt')
        
            abs_save = np.transpose([np.real(x_data),abs_data,abs_datanobl])
            np.savetxt(filename_save_abs,abs_save)

            # save res data
            filename_save_res = os.path.join(save_path,
                                               ffn+file_tag +'_res.txt')   
        
            res_save = np.transpose([np.real(x_data),np.real(np.fft.rfft(
                result.data)-np.fft.rfft(result.best_fit))])
            np.savetxt(filename_save_res,res_save)
        
            # save fit data
            filename_save_fit = os.path.join(save_path,
                                               ffn+file_tag +'_fit.txt')   
        
            fit_save = np.transpose([np.real(x_data),np.real(np.fft.rfft(
                result.best_fit))])
            np.savetxt(filename_save_fit,fit_save)
            
            #save weight vector
            filename_save_weight = os.path.join(save_path,
                                                  ffn+file_tag+'_weight.txt')   
            weight_save = np.transpose([time_wave,weight_wave])
            np.savetxt(filename_save_weight,weight_save)
    
        print(weight_window)
        return_array = [Temperature_In, Temperature_In_err, Temperature_Out, \
            Temperature_Out_err, Pressure_In, Pressure_In_err, \
            Pressure_Out, Pressure_Out_err, Shift, Shift_err, \
            Broadening, Broadening_err,\
            Velocity_In, Velocity_In_err, 
            Pathlength_Out, Pathlength_Out_err,\
            Conc_In, Conc_In_err, Conc_Out, Conc_Out_err]
        return return_array

