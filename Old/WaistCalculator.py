#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Calculate waist sizes in doubline setup

Created on Thu Sep  2 09:48:32 2021

@author: david
"""

import numpy as np

def WaistDivergence(w_0, lambd, z):
    z_R = np.pi*w_0**2/lambd
    w = w_0*(1+(z/z_R)**2)**(1/2)
    return w

def WaistOriginal(w, lambd, z):
    c = (lambd*z/np.pi)**2
    b = -w**2
    a = 1
    w_0_plu = (-b+(b**2-4*a*c)**(1/2))/(-2*a)
    w_0_min = (-b-(b**2-4*a*c)**(1/2))/(-2*a) 
    plu_pos = False
    min_pos = False
    if w_0_plu > 0:
        w_0 = w_0_plu**(1/2)
        plu_pos = True
    if w_0_min > 0:
        w_0 = w_0_min**(1/2)
        min_pos = True
    if plu_pos and min_pos:
        print('Both solutions to quad. eq. are positive')
    return w_0

## initial params
w_0 = 9e-5 # fiber core size
f_0 = 11e-3 # focal length of collimating lens
lambd_ir = 1430e-9 # IR center wavelength

## Waist after first collimation
w_1 = WaistDivergence(w_0, lambd_ir, f_0)
print('Waist after IR collimation is ' + str(round(w_1/w_0,1)) + 'x')

## waist at PPLN
f_1 = 50e-3 # focusing lens size
w_2 = WaistOriginal(w_1, lambd_ir, f_1)
print('Waist on PPLN is ' + str(round(w_2/w_0,1)) + 'x')