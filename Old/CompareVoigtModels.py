"""
CompareVoigtModels

Compare hapi's hartmann-tran voigt to mclean

Created by on 4/28/23 by david

Description:

"""
import numpy as np
import matplotlib.pyplot as plt
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

def McLeanVoigt(nu, nu_0, nu_d, nu_c):
    # ------------------------------------------------------------------------
    #     McLean approximation of Voigt profile to improve computation time
    #
    #     McLean, A. B., C. E. J. Mitchell, and D. M. Swanston.
    #       "Implementation of an efficient analytical approximation to the
    #       Voigt function for photoemission lineshape analysis." Journal of
    #       Electron Spectroscopy and Related Phenomena 69.2 (1994): 125-132.
    # ------------------------------------------------------------------------

    # Parameters for approximating Voigt profile (from McLean)
    C1 = (-1.2150, 1.2359, -0.3085, 0.0210)
    C2 = (-1.3509, 0.3786, 0.5906, -1.1858)
    C3 = (-1.2150, -1.2359, -0.3085, -0.0210)
    C4 = (-1.3509, -0.3786, 0.5906, 1.1858)

    a = np.sqrt(np.log(2)) * nu_c / nu_d  # Calculate Voigt 'a' parameter
    Phi_D_vo = (2 / nu_d) * np.sqrt(np.log(2) / np.pi)  # Calculate doppler line shape at vo
    w = 2 * np.sqrt(np.log(2)) * (nu - nu_0) / nu_d  # Voigt 'w' parameter

    Voigt1 = (C1[2] * (a - C1[0]) + C1[3] * (w - C1[1])) / ((a - C1[0]) ** 2 + (w - C1[1]) ** 2)
    Voigt2 = (C2[2] * (a - C2[0]) + C2[3] * (w - C2[1])) / ((a - C2[0]) ** 2 + (w - C2[1]) ** 2)
    Voigt3 = (C3[2] * (a - C3[0]) + C3[3] * (w - C3[1])) / ((a - C3[0]) ** 2 + (w - C3[1]) ** 2)
    Voigt4 = (C4[2] * (a - C4[0]) + C4[3] * (w - C4[1])) / ((a - C4[0]) ** 2 + (w - C4[1]) ** 2)

    Voigt = Voigt1 + Voigt2 + Voigt3 + Voigt4

    Phi_V = Phi_D_vo * Voigt

    return Phi_V

# parameters
temperature = 290
pressure = 2.8e-09
pathlength = 6.82
molefraction = 1
xx = np.arange(13040,13046,0.00667)

nu_0 = 13042.8955  # cm-1
M = 39.0983  # g/mol
nu_d = nu_0 * 7.1623e-7 * (temperature/M)**(1/2) # doppler broadening
nu_c = 2.012e-4  # probably negligible compared to doppler broadening

# get Mclean shape
coef_mclean = McLeanVoigt(xx, nu_0, nu_d, nu_c)  # uses FWHM
coef_voigt = hapi.PROFILE_VOIGT(nu_0, nu_d/2, nu_c/2, xx)[0]  # uses HWHM

# strength
A21 = 3.779e7  # A_ki
g21 = 2
h = 6.62607015e-34  # m2kgs-1
k = 1.380649e-23  # m2kgs-2K-1
f12 = 6.661e-01
c = 299792458  # m/s
strength = pressure*A21*g21/(100**3*c*8*np.pi*nu_0**2*k*temperature*pressure*molefraction)
# scale by partial pressure and pathlength
coef_mclean *= pressure*pathlength*molefraction*strength
coef_voigt *= pressure*pathlength*molefraction*strength
coef_res = coef_voigt - coef_mclean

plt.figure()
plt.plot(xx, coef_mclean, label='McLean')
plt.plot(xx, coef_voigt, label='Hapi')
plt.plot(xx, coef_res, label='Residual')
plt.legend()
