# -*- coding: utf-8 -*-
"""
4.10.20
David Yun

Fitting Program for background parameters from Feb 2020 AFRL isolator work
TODO:
    Fitting for individual backgrounds

#############################################################################
Created on Fri Aug 17 10:25:23 2018
@author: Amanda Makowiecki

---------------- General Comments ----------------------------------
Data Format:
    Data should be text files with the first column being optical frequency and the next columns being frequency spectra.
    wvnUnits and transUnits parameters should be updated to reflect the units of optical frequency and frequency spectra.
    
Fitting Variables:
    Possible fitted parameters are temperature, pressure, concentrations, lineshift, broadening
    Broadening is simply a scaled air broadening eg. if broadening is 2 then broadening is equivalent to twice gamma air
    
Variables:
    The lengths of Pressure_Guess and Temp_Guess should be the same as the number of files being fit. The length of conc
    should be the same length as the number of files being fit x the number of molecules being fit.


-----------------Time Domain Specific Parameters --------------------
Weight Window:
    The weight_window defines where in the time domain signal to begin and end weighting.
    The input is proportion of points relative to full length of wave and the weighting is mirrored across the center of the wave
    For example a weight window starting with 0.002 will ignore the first and last 0.2% of data points in the time domain.
    FOr clean spectra 0.002 is a good choice, but this parameter should be informed by looking at the residual signal from a time
    domain fit. 
    When combined with the weighting function aa stop point for weighting a generally not needed and therefore the second weighting window
    parameter is set to 0.5. In very low pressure conditions, this can be decreased to minimize noise contribution at high frequencies.

Etalon Windows:
    In the time domain etalons manifest as a delta function. They can be easily viewed as spikes in the data and residual of a time domain fit.
    The etalon_wndows parameter species the start and stop points in the time domain signal where the etalon lies. Only etalons on the first half
    of the time domain window need to be defined here. 
"""
import os
import matplotlib.pyplot as plt
import numpy as np
import time

from FitMFIDBkgd2Beam import Fitting_Spectra
import td_support as td
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi

def FitProgram(filename_y, filename_p, save_path, file_tag,
               weight_window_y, weight_window_p, weight_flat,
               etalon_windows_y, etalon_windows_p,
               angle_y, angle_p, band_fit_y, band_fit_p, molecules_to_fit,
               isotopes, shift_fit, shift_guess,
               pressure_fit_in, temp_fit_in, velocity_fit_in, conc_fit_in,
               pressure_fit_out, temp_fit_out, conc_fit_out, pathlength_fit_out,
               pressure_guess_in, temp_guess_in, velocity_guess_in,
               conc_guess_in,
               pressure_guess_out, temp_guess_out, conc_guess_out,
               pathlength_guess_out,
               broadening_fit, broadening_guess,
               pathlength_in=6.81736,
               save_files=True, plot_results=True, print_fit_report=True,
               wvnUnits=True, transUnits=True,
               noise_add=False, noise_level=0.001):
    
    ###########################################################################
    theta_y = angle_y/360*2*np.pi # convert deg to rad
    theta_p= angle_p/360*2*np.pi

    pathlength_in_y = pathlength_in/np.cos(theta_y) #cm
    pathlength_in_p = pathlength_in/np.cos(theta_p)

    conc_in = [conc_guess_in]*len(molecules_to_fit) # Initializes concentration
                                            # guess for each molecule to fit
    conc_out = [conc_guess_out]*len(molecules_to_fit)
    
    conc_fit_in = [conc_fit_in]*len(molecules_to_fit) # True for each molecule
                                                        # to fit
    conc_fit_out = [conc_fit_out]*len(molecules_to_fit)
    
    # ------------- Time Domain Specific Parameters ---------------------------    
    ###########################################################################    
    print(filename_y)
    print(filename_p)
    
    # Loads in file
    data_y = np.loadtxt(filename_y) 
    data_p = np.loadtxt(filename_p)
    
    if data_y[0,0]>data_y[-1,0]: # Flips data if it is inverted
        data_y = np.flipud(data_y) 
        print("Flipping FFT_y")
    if data_p[0,0]>data_p[-1,0]: # Flips data if it is inverted
        data_p = np.flipud(data_p) 
        print("Flipping FFT_p")
    
    print(len(data_y))
    print(len(data_p))
    if (len(data_y)!=len(data_p)):
            print('Data have inconsistent sizes')
    
    # Imports frequency wave for data, converts to wavenumber if in frequency
    # units
    start_y, stop_y = td.bandwidth_select_td(data_y[:,0], band_fit_y)
    start_p, stop_p = td.bandwidth_select_td(data_p[:,0], band_fit_p)
    if start_y < stop_p:
        if wvnUnits:
            x_data_y = data_y[start_y:stop_y, 0]
            x_data_p = data_p[start_p:stop_p, 0]
        else:
            x_data_y = data_y[start_y:stop_y, 0]/29979245800
            x_data_p = data_p[start_p:stop_p, 0]/29979245800
    else:
        if wvnUnits:
            x_data_y = data_y[start_y:stop_y:-1, 0]
            x_data_p = data_p[start_p:stop_p:-1, 0]
        else:
            x_data_y = data_y[start_y:stop_y:-1, 0]/29979245800
            x_data_p = data_p[start_p:stop_p:-1, 0]/29979245800
    
    # Imports spectra into y_data, converts to absorbance if in transmission
    # units
    y_data_y = np.zeros((len(x_data_y),1))
    y_data_p = np.zeros((len(x_data_p),1)) 
    if transUnits:
        y_data_y = -np.log(data_y[start_y:stop_y,1])
        y_data_p = -np.log(data_p[start_p:stop_p,1])
    else:
        y_data_y = data_y[start_y:stop_y,1]
        y_data_p = data_p[start_p:stop_p,1]
    
    if noise_add:
        noise_y = np.random.normal(0,noise_level,len(y_data_y))
        weight_y = np.cosh((x_data_y-13095)*np.pi/105)
        noise_y *= weight_y
        y_data_y += noise_y
        noise_p = np.random.normal(0,noise_level,len(y_data_p))
        weight_p = np.cosh((x_data_p-13095)*np.pi/105)
        noise_p *= weight_p
        y_data_p += noise_p  
    
    # Convert to time domain
    ff_y = (np.fft.irfft(y_data_y))
    ff_p = (np.fft.irfft(y_data_p))
    y_fit_y = np.zeros((ff_y.size,(len(data_y[0])-1)*2))
    y_fit_p = np.zeros((ff_p.size,(len(data_p[0])-1)*2))
    
    # Builds logic vector to imform fit of parameters to float
    fit_vector = [temp_fit_in, temp_fit_out, pressure_fit_in, pressure_fit_out, 
        shift_fit, broadening_fit, velocity_fit_in, pathlength_fit_out]
    fit_vector.extend(conc_fit_in)
    fit_vector.extend(conc_fit_out)
    
    # Builds vector of approximate parameters for starting fits
    guess_vector = [temp_guess_in, temp_guess_out, pressure_guess_in,
        pressure_guess_out, shift_guess,
        broadening_guess, velocity_guess_in, pathlength_guess_out]
    guess_vector.extend(conc_in)
    guess_vector.extend(conc_out)
    
    # Loads Molecule Data from HITRAN
    hapi.db_begin("")  # Location to store imported HITRAN data
    
    HITRAN_Molecules = ['H2O','CO2','O3','N2O','CO','CH4','O2','NO','SO2',
                        'NO2','NH3','HNO3','OH','HF','HCl','HBr','HI','ClO',
                        'OCS','H2CO','HOCl','N2','HCN','CH3Cl','H2O2','C2H2',
                        'C2H6','PH3','COF2','SF6','H2S','HCOOH','HO2','O',
                        'ClONO2','NO+','HOBr','C2H4','CH3OH','CH3Br','CH3CN',
                        'CF4','C4H2','HC3N','H2','CS','SO3','ArH2O_Labfit']
    molecule_numbers =[0]*(len(molecules_to_fit))
    
    ii=0
    for item in molecules_to_fit:
        if item not in HITRAN_Molecules:
            print("Listed Molecule is not included in HITRAN", item)
        else:  
            molecule_numbers[ii] = (HITRAN_Molecules.index(item))+1
            isotope_ids = []
            for jj in range(len(isotopes[ii])):
                isotope_ids.append(hapi.ISO[molecule_numbers[ii],
                                            isotopes[ii][jj]][0]) 
                
            hapi.fetch_by_ids(item, isotope_ids,min(x_data_y)-1,
                              max(x_data_y)+1) 
        ii+=1
    
    if plot_results:
        plt.figure()
        plt.plot(x_data_y,data_y[start_y:stop_y,1])
        plt.title('Absorbance')
        plt.xlabel('Wavenumber (cm-1')
        plt.ylabel('Transmission')

    tic= time.time()  # Times each fit iteration
    if not y_data_y[10] or not y_data_p[10]:  # checks from empty FFTs and nans in data
        print("Empty FFT")
    else:
        # This sends data to function which compiles it for the fit
        [result, time_wave, conc_in, conc_err_in, conc_out,
         conc_err_out, weight_wave] =\
            Fitting_Spectra(guess_vector,
                fit_vector, x_data_y, y_data_y, x_data_p, y_data_p,
                molecules_to_fit, molecule_numbers, isotopes,
                pathlength_in_y, pathlength_in_p, theta_y, theta_p,
                weight_window_y, weight_window_p, weight_flat,
                etalon_windows_y=etalon_windows_y,
                etalon_windows_p=etalon_windows_p)
                                      
                                      
        # Stores fitted parameters and one sigma uncertainties
        Temperature_In = result.best_values['temp_in']
        Temperature_Out = result.best_values['temp_out']
        Pressure_In = result.best_values['press_in']
        Pressure_Out = result.best_values['press_out']
        Shift = result.best_values['shift']
        Shift_err = result.params['shift'].stderr
        Broadening = result.best_values['broadening']
        Broadening_err = result.params['broadening'].stderr
        Temperature_In_err = result.params['temp_in'].stderr
        Temperature_Out_err = result.params['temp_out'].stderr
        Pressure_In_err = result.params['press_in'].stderr
        Pressure_Out_err = result.params['press_out'].stderr
        Velocity_In = result.best_values['velocity_in']
        Velocity_In_err = result.params['velocity_in'].stderr
        Pathlength_Out = result.best_values['pathlength_out']
        Pathlength_Out_err = result.params['pathlength_out'].stderr
        
        Conc_In = []
        Conc_In_err = []
        Conc_Out = []
        Conc_Out_err = []
        for jj in range(len(molecules_to_fit)):
            Conc_In.append(conc_in[jj])
            Conc_In_err.append(conc_err_in[jj])
            Conc_Out.append(conc_out)
            Conc_Out_err.append(conc_err_out[jj])
    
        # Stores best fits and corresponding data in y_fit matrix 
        cut = len(ff_y)               
        time_fit_y = result.best_fit[:cut]
        time_fit_p = result.best_fit[cut:]
        time_data_y = result.data[:cut]
        time_data_p = result.data[cut:]
        time_res_y = time_data_y - time_fit_y
        time_res_p = time_data_p - time_fit_p
        weight_y = weight_wave[:cut]
        weight_p = weight_wave[cut:]
        
        abs_fit_y = np.real(np.fft.rfft(time_fit_y))
        abs_fit_p = np.real(np.fft.rfft(time_fit_p))
        abs_data_y = np.real(np.fft.rfft(time_data_y))
        abs_data_p = np.real(np.fft.rfft(time_data_p))
        abs_datanobl_y = np.real(np.fft.rfft(time_data_y-
                                             (1-weight_wave[:cut])*time_res_y))
        abs_datanobl_p = np.real(np.fft.rfft(time_data_p-
                                             (1-weight_wave[cut:])*time_res_p))
        abs_resnobl_y = abs_datanobl_y-abs_fit_y
        abs_resnobl_p = abs_datanobl_p-abs_fit_p
        
        # taking a look at what is actually fitted
        abs_fitweight_y = np.fft.rfft(time_fit_y*weight_y)
        abs_fitweight_p = np.fft.rfft(time_fit_p*weight_p)
        abs_dataweight_y = np.fft.rfft(time_data_y*weight_y)
        abs_dataweight_p = np.fft.rfft(time_data_p*weight_p)
        abs_resweight_y = abs_dataweight_y - abs_fitweight_y
        abs_resweight_p = abs_dataweight_p - abs_fitweight_p
        
        if print_fit_report:
            print(result.fit_report(result.params))
    
        # res_y = np.copy(np.fft.rfft(result.best_fit[0:cut]))
        # res_p = np.copy(np.fft.rfft(result.best_fit[cut:]))
        toc = time.time()     
        print('Computation Time:', toc - tic)
        
        ffn_y = os.path.basename(filename_y)[:-10]
        ffn_p = os.path.basename(filename_p)[:-10]
        if plot_results:
            # FFT of time domain fits yellow stream
            plt.figure() 
            plt.plot(x_data_y,abs_datanobl_y, label='Data', linewidth=1)
            plt.plot(x_data_y,abs_resnobl_y,
                     label='Residual', linewidth=1)
            plt.plot(x_data_y,abs_fit_y, label='Fit',
                linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title("Yellow Stream"+ffn_y)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
    
            # FFT of time domain fits purple stream
            plt.figure() 
            plt.plot(x_data_p,abs_datanobl_p, label='Data', linewidth=1)
            plt.plot(x_data_p,abs_resnobl_p,
                     label='Residual', linewidth=1)
            plt.plot(x_data_p,abs_fit_p, label='Fit',
                linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title("Purple Stream"+ffn_p)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
            
            # Weighted FFT of time domain fits yellow stream
            plt.figure() 
            plt.plot(x_data_y,abs_dataweight_y, label='Data', linewidth=1)
            plt.plot(x_data_y,abs_resweight_y,
                     label='Residual', linewidth=1)
            plt.plot(x_data_y,abs_fitweight_y, label='Fit',
                linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title("Yellow Stream"+ffn_y)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
            
            # Weighted FFT of time domain fits yellow stream
            plt.figure() 
            plt.plot(x_data_p,abs_dataweight_p, label='Data', linewidth=1)
            plt.plot(x_data_p,abs_resweight_p,
                     label='Residual', linewidth=1)
            plt.plot(x_data_p,abs_fitweight_p, label='Fit',
                linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title("Purple Stream"+ffn_y)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
    
            # Time domain fit plot. Plot vs time_wave for effective time on x axis 
            # yellow
            plt.figure()
            plt.plot(time_data_y, label='Data', linewidth=1)
            plt.plot(time_res_y,
                     label='Residual', linewidth=1)
            plt.plot(time_fit_y,label='Fit', linewidth=1) 
            plt.plot(result.weights[1:cut]*max(time_data_y))
            # plt.title()
            plt.xlabel('Index')
            plt.ylabel('Magnitude')
            plt.title("Yellow Stream"+ffn_y)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
    
            # purple
            plt.figure()
            plt.plot(time_data_p, label='Data', linewidth=1)
            plt.plot(time_res_p,
                     label='Residual', linewidth=1)
            plt.plot(time_fit_p,label='Fit', linewidth=1) 
            plt.plot(result.weights[cut+1:]*max(time_data_p))
            # plt.title()
            plt.xlabel('Index')
            plt.ylabel('Magnitude')
            plt.title("Purple Stream"+ffn_p)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show()
    
        if save_files:  
            filename_report = os.path.join(save_path,
                                           ffn_y+file_tag +'_report.txt')
            
            f = open(filename_report,'w')
            f.write(result.fit_report())
            f.close()
            
            # save cepstrum data
            filename_save_y_cep = os.path.join(save_path,
                                               ffn_y+file_tag +'_cep.txt')   
            filename_save_p_cep = os.path.join(save_path,
                                               ffn_p+file_tag +'_cep.txt')
        
            y_cep = np.transpose([time_data_y,time_fit_y])
            p_cep = np.transpose([time_data_p,time_fit_p])
            np.savetxt(filename_save_y_cep,y_cep)
            np.savetxt(filename_save_p_cep,p_cep)
        
            # save absorbance data
            filename_save_y_abs = os.path.join(save_path,
                                               ffn_y+file_tag +'_abs.txt')   
            filename_save_p_abs = os.path.join(save_path,
                                               ffn_p+file_tag +'_abs.txt')
        
            y_abs = np.transpose([np.real(x_data_y),abs_data_y,abs_datanobl_y])
            p_abs = np.transpose([np.real(x_data_p),abs_data_p,abs_datanobl_p])
            np.savetxt(filename_save_y_abs,y_abs)
            np.savetxt(filename_save_p_abs,p_abs)
        
            # save res data
            filename_save_y_res = os.path.join(save_path,
                                               ffn_y+file_tag +'_res.txt')   
            filename_save_p_res = os.path.join(save_path,
                                               ffn_p+file_tag +'_res.txt')
        
            y_res = np.transpose([np.real(x_data_y),np.real(np.fft.rfft(
                result.data[0:cut])-np.fft.rfft(result.best_fit[0:cut]))])
            p_res = np.transpose([np.real(x_data_p),np.real(np.fft.rfft(
                result.data[cut:])-np.fft.rfft(result.best_fit[cut:]))])
            np.savetxt(filename_save_y_res,y_res)
            np.savetxt(filename_save_p_res,p_res)
        
            # save fit data
            filename_save_y_fit = os.path.join(save_path,
                                               ffn_y+file_tag +'_fit.txt')   
            filename_save_p_fit = os.path.join(save_path,
                                               ffn_p+file_tag +'_fit.txt')
        
            y_fit = np.transpose([np.real(x_data_y),np.real(np.fft.rfft(
                result.best_fit[0:cut]))])
            p_fit = np.transpose([np.real(x_data_p),np.real(np.fft.rfft
                (result.best_fit[cut:]))])
            np.savetxt(filename_save_y_fit,y_fit)
            np.savetxt(filename_save_p_fit,p_fit)
            
            #save weight vector
            filename_save_y_weight = os.path.join(save_path,
                                                  ffn_y+file_tag+'_weight.txt')   
            filename_save_p_weight = os.path.join(save_path,
                                                  ffn_p+file_tag+'_weight.txt')
            y_weight = np.transpose([time_wave[0:cut],weight_wave[0:cut]])
            p_weight = np.transpose([time_wave[cut:],weight_wave[cut:]])
            np.savetxt(filename_save_y_weight,y_weight)
            np.savetxt(filename_save_p_weight,p_weight)
    
        print(weight_window_y)
        print(weight_window_p) 
        return_array = [Temperature_In, Temperature_In_err, Temperature_Out, \
            Temperature_Out_err, Pressure_In, Pressure_In_err, \
            Pressure_Out, Pressure_Out_err, Shift, Shift_err, \
            Broadening, Broadening_err,\
            Velocity_In, Velocity_In_err, 
            Pathlength_Out, Pathlength_Out_err,\
            Conc_In, Conc_In_err, Conc_Out, Conc_Out_err]
        return return_array