# -*- coding: utf-8 -*-
"""
Created 4/30/20 David Yun
Fit Program for signal that goes through retro
modified from FitProgram2Beam for AFRL Isolator work

PARAMETERS:
    data:
        array of data with wavenumber and transdata
    save_path:
        Where to save data, fit, weight, and report files
    file_tag:
        A special tag to end all saved files with (e.g. height30mm)
    weight_window:
        Time-domain weighting by proportion (e.g. [0.01,0.5])
    weight_flat:
        True for flat weighting, False for exponential weighting
    etalonWindows:
        where to cut out windows on downstream spectra
        (e.g. [[500,600],[10000,11000]])
    angle:
        angle of beams (in deg), single input
    band_fit:
        where to fit spectra (e.g. [6800,7100]) (in cm-1)
    Molecules_to_Fit:
        molecules to fit for (e.g. ['H2O_PaulLF'])
    isotopes:
        isotopes of molecules to fit (e.g. [[1]])
    Pressure_Fit:
        Toggle for fitting pressure, TRUE for fit, FALSE for fix
    Temp_Fit:
        Toggle for fitting temperature
    Velocity_Fit:
        Toggle for fitting velocity
    Shift_Fit:
        Toggle for fitting shift
    Conc_Fit:
        Toggle for fitting concentration, for now not able to toggle for
        individual molecules
    Broadening_Fit:
        Toggle for fitting broadening
    pathlength:
        pathlength straight across test section, code adjusts for pass and angle
    pahelength_b:
        actual path of background, no adjustment in code
    Pressure_Guess:
        Initial Pressure value
    Temp_Guess:
        Initial Temperature value
    Velocity_Guess:
        Initial Velocity value
    Shift_Guess:
        Initial shift value
    Conc_Guess:
        Intiial conc value (for now just takes one for all molecules
    Broadening_Guess:
        Initial broadening guess (just go with 1 if you don't want to mess
                                  with it')
    save_files:
        Toggle to save files
    plot_results:
        Toggle to plot spectral fit
    print_fit_report:
        Toggle to print fit results in command
        
RETURNS: Pretty self-explanatory
    Velocity
    Velocity_err
    Temperature
    Temperature_err
    Pressure
    Pressure_err
    Shift
    Shift_err
    Conc
    Conc_err
    Broadening
    Broadening_err

"""

from FitMFIDRetrowBox import Fitting_Spectra
import matplotlib.pyplot as plt
import numpy as np
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
import td_support as td
import time
import os

###############################################################################
##########  Output options for Fitting ########################################

def FitProgram(filename, save_path, file_tag,
               weight_window, weight_flat, etalonWindows, angle, band_fit,
               Molecules_to_Fit, isotopes,
               Pressure_Fit, Temp_Fit, Velocity_Fit, Shift_Fit, Conc_Fit,
               Broadening_Fit,
               Pressure_Guess, Temp_Guess, Velocity_Guess, Shift_Guess, 
               Conc_Guess, Broadening_Guess,
               pathlength=6.84276, n_pass=2,
               background_remove=False, molec_back='O2',
               pathlength_b=35.935, press_back=1, temp_back=300, conc_back=0,
               save_files=True, plot_results=True, print_fit_report=True,
               wvnUnits=True, transUnits=True):
    ########## Data Specific Parameters #######################################
    print(filename)
    data = np.loadtxt(filename)
    
    theta = angle/180*np.pi
    Pathlength = n_pass*pathlength/np.cos(theta)
    conc = [Conc_Guess]*len(Molecules_to_Fit) # Initializes concentration guess
                                                # for each molecule to fit
    Conc_Fit = [Conc_Fit]*len(Molecules_to_Fit) # True for each molecule to fit
    ###########################################################################
    
    if data[0,0]>data[-1,0]: # Flips data if it is inverted
        data = np.flipud(data) 
        print("Flipping FFT")
    print(len(data))

    start, stop = td.bandwidth_select_td(data[:,0], band_fit)
    if start < stop:
        if wvnUnits:
            x_data = data[start:stop, 0]
        else:
            x_data = data[start:stop, 0]/29979245800
    else:
        if wvnUnits:
            x_data = data[start:stop:-1, 0]
        else:
            x_data = data[start:stop:-1, 0]/29979245800
    # x_data =data[:,0]
    # Imports spectra into y_data, converts to absorbance if in trans units
    y_data = np.zeros((len(x_data), 1))
    if transUnits:
        y_data[:,0] =-np.log(data[start:stop,1])
    else:
        y_data[:,0] =data[start:stop,1]

    # y_data[:,0] =data[:,1]
    
    # Convert to time domain
    ff = (np.fft.irfft(y_data[:,0]))
    y_fit = np.zeros((ff.size, 2))

    # tic = time.clock()
    
    # Builds logic vector to imform fit of parameters to float
    Fit_vector = [Temp_Fit, Pressure_Fit, Shift_Fit, Broadening_Fit,
                  Velocity_Fit]
    Fit_vector.extend(Conc_Fit)
    
    # Builds vector of approximate parameters for starting fits
    Guess_vector = [Temp_Guess, Pressure_Guess, Shift_Guess, Broadening_Guess,
                    Velocity_Guess]
    Guess_vector.extend(conc)
    
    # Loads Molecule Data from HITRAN
    hapi.db_begin('')  # Location to store imported HITRAN data
    
    HITRAN_Molecules = ['H2O','CO2','O3','N2O','CO','CH4','O2','NO','SO2',
                        'NO2','NH3','HNO3','OH','HF','HCl','HBr','HI','ClO',
                        'OCS','H2CO','HOCl','N2','HCN','CH3Cl','H2O2','C2H2',
                        'C2H6','PH3','COF2','SF6','H2S','HCOOH','HO2','O',
                        'ClONO2','NO+','HOBr','C2H4','CH3OH','CH3Br','CH3CN',
                        'CF4','C4H2','HC3N','H2','CS','SO3','ArH2O_Labfit']
    molecule_numbers =[0]*(len(Molecules_to_Fit));
    
    if plot_results:
        plt.figure()
        plt.plot(x_data,y_data[:,0])
        plt.title('Transmission')
        plt.xlabel('Wavenumber (cm-1')
        plt.ylabel('Transmission')
    
    ii=0
    for item in Molecules_to_Fit:
        if item not in HITRAN_Molecules:
            print("Listed Molecule is not included in HITRAN", item)
        else:  
            molecule_numbers[ii] = (HITRAN_Molecules.index(item))+1
            isotope_ids = []
            for jj in range(len(isotopes[ii])):
                isotope_ids.append(hapi.ISO[molecule_numbers[ii],
                                            isotopes[ii][jj]][0]) 
            hapi.fetch_by_ids(item, isotope_ids,min(x_data)-1,
                              max(x_data)+1)
        ii+=1
            
    tic= time.time()  # Times each fit iteration
    if not y_data[10]:  # checks from empty FFTs and nans in data
        print("Empty FFT")
    else:
        Guess_vector = [Temp_Guess, Pressure_Guess, Shift_Guess,
                        Broadening_Guess,Velocity_Guess]
        Guess_vector.extend(conc)     
        
        # This sends data to function which compiles it for the fit
        [result,timewave,concs,concs_err,weight_wave,time_wave] = \
            Fitting_Spectra(Guess_vector, Fit_vector, x_data, y_data[:,0],
                            Molecules_to_Fit, molecule_numbers, isotopes,
                            Pathlength, theta, weight_window, weight_flat,
                            background_remove, molec_back, pathlength_b,
                            press_back, temp_back, conc_back,
                            etalonWindows = etalonWindows)
        
        Velocity = result.best_values['velocity']
        Temperature = result.best_values['Temp']
        Pressure = result.best_values['press']
        Shift = result.best_values['shift']
        Broadening = result.best_values['Broadening']
        Temperature_err = result.params['Temp'].stderr
        Pressure_err = result.params['press'].stderr
        Shift_err = result.params['shift'].stderr
        Velocity_err = result.params['velocity'].stderr
        Broadening_err = result.params['Broadening'].stderr

        Conc = []
        Conc_err = []
        for jj in range(len(Molecules_to_Fit)):
            Conc.append(concs[jj])
            Conc_err.append(concs_err[jj])
    
        # Stores best fits and corresponding data in y_fit matrix                
        time_fit = result.best_fit
        time_data = result.data
        time_res = time_data - time_fit
        
        abs_fit = np.real(np.fft.rfft(time_fit))
        abs_data = np.real(np.fft.rfft(time_data))
        abs_res = abs_data - abs_fit
        abs_datanobl = np.real(np.fft.rfft(time_data-(1-weight_wave)*time_res))
        abs_resnobl = abs_datanobl-abs_fit
        
        if print_fit_report:
            print(result.fit_report(result.params))
    
        res = np.copy(np.fft.rfft(result.best_fit))  # Initializing residual wave
        toc = time.time()     
        print('Computation Time:', toc - tic)
        
        ffn = os.path.basename(filename)[:-10]
        
        if plot_results: 
            # No baseline fits
            plt.figure() 
            plt.plot(x_data,abs_datanobl, label='Data', linewidth=1)
            plt.plot(x_data,abs_resnobl,
                     label='Residual', linewidth=1)
            plt.plot(x_data,abs_fit, label='Fit',
                linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title("Freq Domain" + ffn)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
            
            # FFT of time domain fits yellow stream
            plt.figure() 
            plt.plot(x_data,abs_data, label='Data', linewidth=1)
            plt.plot(x_data,abs_res,
                     label='Residual', linewidth=1)
            plt.plot(x_data,abs_fit, label='Fit',
                linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title(ffn)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
        
            # Time domain fit plot. Plot vs timewave for effective time on x axis 
            # yellow
            plt.figure()
            plt.plot(time_data, label='Data', linewidth=1)
            plt.plot(time_res, label='Residual',
                     linewidth=1)
            plt.plot(time_fit, label='Fit', linewidth=1) 
            plt.plot(weight_wave*max(time_data))
            plt.title('Time Domain' + ffn)
            plt.xlabel('Index')
            plt.ylabel('Magnitude')
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show()
        
        if save_files:  
            filename_report = os.path.join(save_path,
                                           ffn+file_tag+'_report.txt')
            
            f = open(filename_report,'w')
            f.write(result.fit_report())
            f.close()
            
            # save cepstrum data
            filename_save_cep = os.path.join(save_path,
                                               ffn+file_tag +'_cep.txt')
        
            cep = np.transpose([time_data,time_fit])
            np.savetxt(filename_save_cep,cep)
        
            # save absorbance data
            filename_save_abs = os.path.join(save_path,
                                             ffn+file_tag+'_abs.txt')   
        
            abs_data = np.transpose([np.real(x_data),abs_data,abs_datanobl])
            np.savetxt(filename_save_abs,abs_data)
        
            # save res data
            filename_save_res = os.path.join(save_path,
                                             ffn+file_tag+'_res.txt')   
        
            res_data = np.transpose([np.real(x_data),np.real(np.fft.rfft(
                result.data)-np.fft.rfft(result.best_fit))])
            np.savetxt(filename_save_res,res_data)
            
            # save fit data
            filename_save_fit = os.path.join(save_path,
                                             ffn+file_tag+'_fit.txt')   
        
            fit_data = np.transpose([np.real(x_data),np.real(np.fft.rfft(
                result.best_fit))])
            np.savetxt(filename_save_fit,fit_data)
            
            #save weight vector
            filename_save_weight = os.path.join(save_path,
                                                ffn+file_tag+'_weight.txt')   
            weight_data = np.transpose([time_wave,weight_wave])
            np.savetxt(filename_save_weight,weight_data)
        
        # print(weight_window)
        fit_results = [Velocity, Velocity_err, Temperature, Temperature_err, 
                       Pressure, Pressure_err, Shift, Shift_err, Conc,
                       Conc_err, Broadening, Broadening_err]
        return fit_results