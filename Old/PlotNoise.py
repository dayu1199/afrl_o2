#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  4 00:48:58 2021

@author: david
"""
import os
import pandas as pd
import matplotlib.pyplot as plt

d = r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS/Data/Noise'
fn = 'NoiseMeasurements.xlsx'

df = pd.read_excel(os.path.join(d,fn),
                   )

plt.figure()
plt.loglog(df['Time (s)'].tolist(), df['6.5 V'].tolist(), label = '6.5 V',
           linestyle='None',marker='x')
plt.loglog(df['Time (s)'].tolist(), df['2.6 V'].tolist(), label = '2.6 V',
           linestyle='None',marker='x')
plt.loglog(df['Time (s)'].tolist(), df['1.0 V'].tolist(), label = '1.0 V',
           linestyle='None',marker='x')
plt.loglog(df['Time (s)'].tolist(), df['0.6 V'].tolist(), label = '0.6 V',
           linestyle='None',marker='x')
plt.ylabel('Aborbance Noise')
plt.xlabel('Time (s)')
plt.legend()