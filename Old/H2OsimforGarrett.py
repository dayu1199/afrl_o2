#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 22:42:02 2022

@author: david
"""
import numpy as np
import matplotlib.pyplot as plt
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
from pldspectrapy.constants import *
import td_support as td

hapi.db_begin('') # initialize hapi database

y_H2O = 0.4
P = 2.5 # pressure (atm)
T = 2500 # Temperature (K)
V =  0 # velocity, m/s
theta = np.deg2rad(12.5) # degree of beam, rad
dv = 0.00667 # frequency axis step size (cm-1)
L = 2 # path length of measurement (cm)

windowstart = 6900
windowend = 7200

# adjust frequency axis to account for velocity doppler shift
wvnumspace = np.arange(windowstart,windowend,dv) # create array of frequency axis
start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,windowend]) # optimal start stop of bandwidth for fourier transfrom
wvnumspace = wvnumspace[start:stop]
nsteps = len(wvnumspace)-1
slope_doppler_d = V*np.sin(-theta)/SPEED_OF_LIGHT+1 # doppler scaling due to velocity
new_wvw_num_d = wvnumspace*slope_doppler_d # apply to frequency axis
windowend_d = np.amax(new_wvw_num_d) # new bandwidth end
windowstart_d = np.amin(new_wvw_num_d) # new bandiwdth start
dv_d = (new_wvw_num_d[-1]-new_wvw_num_d[0])/nsteps # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend_d = windowend_d.item()
windowstart_d = windowstart_d.item()
dv_d = dv_d.item()

# create absorbance model using hapi
[nu_H2O,coefs_H2O] = hapi.absorptionCoefficient_Voigt([(1,1,y_H2O)],
            ('H2O_PaulLF2'), OmegaStep=dv_d,OmegaRange=[windowstart_d, windowend_d],
            HITRAN_units=False, Environment={'p':P,'T':T}, 
            Diluent={'self':y_H2O,'air':(1-y_H2O)},
            IntensityThreshold =0)#*1.5095245023408206
abs_H2O = coefs_H2O*L # get asorption (absorbance * pathlength)
lambda_H2O = 1e7/nu_H2O # transform to wavelength units


# plot absorption model
plt.figure()
plt.plot(nu_H2O,abs_H2O,color='blue') #plot absorption
