#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 23:24:38 2022

@author: david
"""
import os
import numpy as np
import matplotlib.pyplot as plt

d_cross = r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/MooseDAQ/031122_vis3/fit'
fn_cross = '20220311204327'

for fn in os.listdir(d_cross):
    if fn_cross and 'fitdata_freq' in fn:
        data = np.loadtxt(os.path.join(d_cross,fn))
        data_lessbl_cross = data[:,0]
        model_cross = data[:,1]
        xwvn_cross = data[:,2]
        
d_retro = r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/MooseDAQ/031122_retro/fit'
fn_retro = '20220311234509'

for fn in os.listdir(d_retro):
    if fn_retro and 'fitdata_freq' in fn:
        data = np.loadtxt(os.path.join(d_retro,fn))
        data_lessbl_retro = data[:,0]
        model_retro = data[:,1]
        xwvn_retro = data[:,2]

plt.figure()
plt.plot(xwvn_retro,data_lessbl_retro,color='blue',label='Retro Setup')
plt.plot(xwvn_cross,data_lessbl_cross,color='red',label='Crossed Setup')
plt.ylabel('Absorbance')
plt.xlabel('Wavenumber (cm-1)')
plt.legend()

