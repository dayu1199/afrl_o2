#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 13 10:52:10 2022

@author: david
"""

import os
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

dr = r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)/Data/RFSA_PhotodetectorSaturation/Jan2022'

fns = []
freq = []
power = []
volts = []

plt.figure()

toplot = ['04V',
          '08V',
          '15V',
          '35V']
for label in toplot:
    for fn in os.listdir(dr):
        if '.CSV' in fn and label in fn:
            fns.append(fn[:-4])
            data = pd.read_csv(os.path.join(dr,fn),skiprows=56,usecols=[0,1],
                               header=0,names=['Freq.','Pow.'])
            freq.append(np.array(data['Freq.'].tolist()))
            power.append(np.array(data['Pow.'].tolist()))
            volts.append(int(fn[:2])/10)
            
            plt.plot(freq[-1]*1e-6,power[-1],label=str(volts[-1])+' V')
plt.legend()
plt.xlabel('Frequency (MHz)')        
