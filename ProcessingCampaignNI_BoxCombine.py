#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code to combine phasecorrect file 17 and 18 together

Created on Tue Mar 22 15:28:15 2022

@author: david
"""
#%% Package imports
import os
import time
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from packfind import find_package
import time

find_package("pldspectrapy")
import pldspectrapy as pld
find_package("pldspectrapy1")
import pldspectrapy1 as pld1
import td_support as td

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

#%% parameters
# Code switches and params
pclim_low = 0.12
pclim_high = 0.18
plotdata = True
savedata = True

# directory info
d = os.path.join(r'/Users/david/Library/CloudStorage',
                 r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                 r'AFRL ETHOS (DO NOT SYNC)',
                 r'Mar2022CampaignData/cubig/031122')
fn_save_all = 'boxall_trans.txt'
save_folder_tag = 'pc_peter_ig'

# Frequency axis calculation
lockfreq = 32e6
nomfreq = 13333  # Should be within the range specified by band_fit (below)

# '11' has weird etalon
indexes = ['6', '7', '8', '9', '10', '11_2',
         '12', '13', '14', '15', '16', '17', '18', '19']

# get filename key info
d_key = os.path.join(r'/Users/david/Library/CloudStorage',
                     r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                     r'AFRL ETHOS/MarchCampaign')
fn_key = 'FilenameKey.xlsx'
key_df = pd.read_excel(os.path.join(d_key,fn_key))
d_moose = os.path.join(r'/Users/david/Library/CloudStorage',
                       r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                       r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/moose')

# create save directory
d_save = os.path.join(d, save_folder_tag)
if not os.path.isdir(d_save):
    os.mkdir(d_save)

#%% open up directory #this is just to get the file names
t0 = time.time()
files = pld.open_dir(d, recursion_levels=2, verbose=2)
dt = time.time() - t0
print("Opened %i files in %0.3f seconds" % (len(files), dt))
newest = ""

#%% pull data
igs = []
x_wvns = []
daq_name = 'cubig'

#%% pull pc igs and wvnums for individual files
for name, daq_file in files.items():
    if name in indexes:
        print(name)

        # savename
        fn_save = name + '_ig.txt'
        IG = pld1.open_daq_files(os.path.join(d, name))
        # Are there raw IGMs?
        if IG.data_raw is not None:
            non_empty_raw = len(IG.data_raw)
            pass_raw = 1 if (non_empty_raw > 0) else 0

        # As long as there's some IGMs go ahead and start processing!
        if pass_raw:
            # search for column with both correct daq and filename
            row = key_df.loc[(key_df['vis2 DAQ'] == daq_name) & \
                             (key_df['vis2 fn'] == name)]
            if len(row) == 0:
                row = key_df.loc[(key_df['box DAQ'] == daq_name) & \
                                 (key_df['box fn'] == name)]
            if len(row) == 0:
                print('No moose axis found for this file. Used default axis')
            else:
                fn_axis = str(row.iloc[0]['vis3 fn']) + '_trans.txt'
                axis_found = False
                # search through moose directory for freq axis
                for folder in os.listdir(d_moose):
                    for sub_folder in os.listdir(os.path.join(d_moose, folder)):
                        if os.path.isdir(os.path.join(d_moose, folder, sub_folder)):
                            for file in os.listdir(os.path.join(d_moose, folder,
                                                                sub_folder)):
                                if file == fn_axis:
                                    data_trans = np.loadtxt(os.path.join(d_moose,
                                                                         folder,
                                                                         sub_folder,
                                                                         file))
                                    x_wvns.append(data_trans[:, 0])
                                    axis_found = True
                                    break
                        if axis_found:
                            break
                    if axis_found:
                        break
                if not axis_found:
                    print('Axis not Found!!')

            # check if already processed
            if os.path.exists(os.path.join(d_save, fn_save)):
                print('IG already processed in ' + d_save)
                igs.append(np.loadtxt(os.path.join(d_save, fn_save),
                                      dtype='complex'))
                continue

            tic = time.time()
            data = IG.data_raw.copy()
            ig = pld1.pc_peter(data, pclim_low, pclim_high, plot=plotdata,
                                  zoom=500, igout=True)
            toc = time.time()
            print('Phase correcting '+name+' took ' + str(toc - tic) + ' s')
            if savedata:
                np.savetxt(os.path.join(d_save, fn_save), ig)

            igs.append(ig)
    else:
        print('No data in '+name)

#%% average wavenumber axis
x_wvn_full = np.mean(x_wvns, axis=0)

#%% phase correct all igs into one
tic = time.time()
igs = np.array(igs)
# # take out 11 which has osillatory behavior
# igs0 = np.delete(igs, (7), axis=0)
trans = pld1.pc_peter(igs, pclim_low, pclim_high, plot=True,
                     zoom=500)
pc_ig = np.fft.irfft(trans)
toc = time.time()
print('Phase correcting data took '+str(toc-tic)+' s')

#%% plot data
lambda_full = 1e7/x_wvn_full 
if plotdata:
    fig,ax = plt.subplots(2,1)
    ax[0].plot(pc_ig)
    ax[0].set_xlabel('Time step')
    ax[0].set_ylabel('Power')
    ax[1].plot(lambda_full[:len(trans)], trans)
    ax[1].set_xlabel('Wavelength (cm-1)')
    ax[1].set_ylabel('Transmission')
# save file
if savedata:
    save_data = np.vstack((x_wvn_full[:len(trans)], trans))
    save_data = np.transpose(save_data)

    np.savetxt(os.path.join(d_save, fn_save_all), save_data)

#%% plot data for analysis

start, stop = td.bandwidth_select_td(x_wvn_full, [13016, 13170])

ffts = pld1.FFT(igs)
transs = ffts.__abs__()[:, int(len(ffts[0])/2):]
transs0 = transs[:, start:stop]
igs_cut = np.fft.irfft(transs0)
abss = -np.log(transs0)
ceps = np.fft.irfft(abss)
weight = np.zeros(len(ceps[0]))
weight_start = int(0.009*len(weight))
weight_ind = np.array([*range(len(weight))])
weight[weight_ind>weight_start] = 1
weight[weight_ind>(len(weight)-weight_start)] = 0
ceps_weight = ceps*weight
abss_weight = np.fft.rfft(ceps_weight)

trans0 = trans[start:stop]
pc_ig = np.fft.irfft(trans0)
abs0 = -np.log(trans0)
cep = np.fft.irfft(abs0)
cep_weight = cep*weight
abs_weight = np.fft.rfft(cep_weight)

plt.figure()
i = 0
for tran in transs0:
    plt.plot(tran, label=indexes[i])
    i += 1
    if i > 5:
        break
plt.plot(trans0, label='All')
plt.legend()
plt.title('Transmission')

plt.figure()
i = 0
for ce in ceps:
    plt.plot(ce, label=indexes[i])
    i += 1
    if i > 5:
        break
plt.plot(cep, label='All')
plt.legend()
plt.title('Cepstra')

plt.figure()
i = 0
for ig in igs_cut:
    plt.plot(ig-(np.mean(ig[6000:7000])-np.mean(igs_cut[0, 6000:7000])),
                 label=indexes[i])
    i += 1
    if i > 5:
        break
plt.plot(pc_ig-(np.mean(pc_ig[6000:7000])-np.mean(igs_cut[0, 6000:7000])),
         label='All')
plt.legend()
plt.title('IG')

plt.figure()
i = 0
for ab in abss:
    plt.plot(ab, label=indexes[i])
    i += 1
    if i > 5:
        break
plt.plot(abs0, label='All')
plt.legend()
plt.title('Absorbance')

plt.figure()
i = 0
for ab in abss_weight:
    plt.plot(ab, label=indexes[i])
    i += 1
    if i > 5:
        break
plt.plot(abs_weight, label='All')
plt.legend()
plt.title('Weighted Absorbance')

noise_levels = np.std(abss_weight[:, 15700:16100], axis=1)
noise_level = np.std(abs_weight[15700:16100])
