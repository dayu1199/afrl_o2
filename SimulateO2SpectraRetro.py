#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot O2 Spectra
Created on Tue Sep  7 09:26:39 2021

@author: david
"""
import numpy as np
import matplotlib.pyplot as plt
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
from pldspectrapy.constants import *
import td_support as td
import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

#%% define parameters
save_data = True

add_noise = False
noise_level = 0.00045

y_O2 = 0.2095  # mole fraction O2
P = 1.19  # pressure (atm)
T = 731.3  # Temperature (K)
V = 967.3 # velocity, m/s
theta = np.deg2rad(16.69)  # degree of beam, rad
windowstart = 13010  # bandwidth start (cm-1)
windowend = 13180  # bandwidth end (cm-1)
dv = 0.00667  # frequency axis step size (cm-1)
L = 2 * (6.84276/np.cos(theta)) # path length of measurement (cm)
                        # -2 passes for each angle using 25.4mm retro

# create x-axis
wvnumspace = np.arange(windowstart,windowend,dv) # create frequency axis
start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,windowend])
wvnumspace = wvnumspace[start:stop]
nsteps = len(wvnumspace)-1

hapi.db_begin('')
MIDS = [(7, 1, y_O2*hapi.abundance(7, 1))]

#%% downstream facing spectra
# create temporary x axis with doppler shift 
slope_doppler_d = V*np.sin(-theta)/SPEED_OF_LIGHT+1  # doppler scaling
                                                    # due to velocity
new_wvw_num_d = wvnumspace*slope_doppler_d  # apply to frequency axis
windowend_d = np.amax(new_wvw_num_d)  # new bandwidth end
windowstart_d = np.amin(new_wvw_num_d)  # new bandiwdth start
dv_d = (new_wvw_num_d[-1]-new_wvw_num_d[0])/nsteps  # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend_d = windowend_d.item()
windowstart_d = windowstart_d.item()
dv_d = dv_d.item()
# create absorbance model using hapi
[nu, coefs] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv_d, OmegaRange=[windowstart_d, windowend_d],
            HITRAN_units=False, Environment={'p': P, 'T': T},
            Diluent={'self': y_O2 - 0.21 / 0.79 * (1 - y_O2),
                      'air': (1 - y_O2) / 0.79},
            IntensityThreshold =0)
abs_O2_d = coefs*L

#%% upstream facing spectra
# create temporary x axis with doppler shift 
slope_doppler_u = V*np.sin(theta)/SPEED_OF_LIGHT+1  # doppler scaling
                                                    # #due to velocity
new_wvw_num_u = wvnumspace*slope_doppler_u  # apply to frequency axis
windowend_u = np.amax(new_wvw_num_u)  # new bandwidth end
windowstart_u = np.amin(new_wvw_num_u)  # new bandiwdth start
dv_u = (new_wvw_num_u[-1]-new_wvw_num_u[0])/nsteps  # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend_u = windowend_u.item()
windowstart_u = windowstart_u.item()
dv_u = dv_u.item()
# create absorbance model using hapi
[nu_u, coefs_u] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv_u,OmegaRange=[windowstart_u, windowend_u],
            HITRAN_units=False, Environment={'p': P, 'T': T},
             Diluent={'self': y_O2 - 0.21 / 0.79 * (1 - y_O2),
                      'air': (1 - y_O2) / 0.79},
            IntensityThreshold =0)
abs_O2_u = coefs_u*L

#%% Combine Spectra
#Summing the two spectra
abs_O2_total = abs_O2_d + abs_O2_u

#%% create noise
noise = np.random.normal(0, noise_level, len(abs_O2_u))
weight = ((wvnumspace-13100)/38)**2+1
noise *= weight

#%% Plot Spectra
# lambda_O2 = 1e7/wvnumspace  # transform to wavelength units
# plot absorption model
plt.figure()  # initialize plotting figure

# plotting combined retro absorbtion
if add_noise:
    plt.plot(wvnumspace, abs_O2_total+noise, label='Total')
plt.plot(wvnumspace, abs_O2_total,label='Total')
# plotting both absorption angles (dotted)
plt.plot(wvnumspace, abs_O2_d,
         label='Downstream Facing', c='black', ls='dashed')
plt.plot(wvnumspace, abs_O2_u,
         label='Upstream Facing', c='red', ls='dashed')
plt.legend()  # add legend
# plt.savefig('Combined_Data_Theta=35.png')

#%% Save Data
data = np.vstack((wvnumspace, abs_O2_total))
data = np.transpose(data)
if save_data:
    np.savetxt("O2simulationRetro_AQ.txt", data)
