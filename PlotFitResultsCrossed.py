#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
plot fit results from a set of fits

Created on Wed May  4 16:08:35 2022

@author: david
"""

import os
import matplotlib.pyplot as plt
import pickle
import numpy as np

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()
# plt.rcParams.update({"figure.autolayout": True, "lines.linewidth": 0.8,
#                      'font.size': 22})
plot_manual = True

#%% plot manual data
if plot_manual:
    temp = np.array([553, 576, 584, 628, 784])
    vel = np.array([808, 827, 939, 1004, 1008])

    # temp_SD = np.array([579, 583, 571, 613, 777])
    # vel_SD = np.array([740, 752, 828, 885, 916])

    temp_1D = np.array([586, 600, 574, 659, 731])
    vel_1D = np.array([866, 875, 857, 918, 967])

    temp_unc = np.array([0.025, 0.035, 0.033, 0.042, 0.033])
    vel_unc = np.array([0.098, 0.097, 0.086, 0.111, 0.100])
    # temp_unc = np.array([0.025, 0.033, 0.034, 0.042, 0.03])
    # vel_unc = np.array([0.069, 0.083, 0.135, 0.126, 0.094])
    #
    # temp_unc = 0.05
    # vel_unc = 0.03


    fig1, ax1 = plt.subplots()
    fig4, ax4 = plt.subplots()
    # plot velocities
    ax1.errorbar([0, 1, 2, 3, 4], vel,
                 yerr=vel_unc * vel, linestyle='None', marker='o',
                 label='DCS')
    # ax1.errorbar([0, 1, 2, 3, 4], vel_SD,
    #              yerr=vel_unc * vel_SD, linestyle='None', marker='o',
    #              label='SD Voigt')
    ax1.scatter([0, 1, 2, 3, 4], vel_1D, label='1D', color='red')
    ax1.set_xlabel('Run')
    ax1.set_ylabel('Velocities (m/s)')
    ax1.set_ylim([0, 1200])
    ax1.set(xticks=[0, 1, 2, 3, 4], xticklabels=['AB', 'AF', 'AG', 'AI', 'AK'])
    ax1.legend()

    # plot temperatures
    ax4.errorbar([0, 1, 2, 3, 4], temp,
                 yerr=temp_unc * temp, linestyle='None', marker='o',
                 label='DCS')
    # ax4.errorbar([0, 1, 2, 3, 4], temp_SD,
    #              yerr=temp_unc * temp_SD, linestyle='None', marker='o',
    #              label='SD Voigt')
    ax4.scatter([0, 1, 2, 3, 4], temp_1D, label='1D', color='red')
    ax4.set_xlabel('Run')
    ax4.set_ylabel('Temperature (K)')
    ax4.set_ylim([0, 900])
    ax4.set(xticks=[0, 1, 2, 3, 4], xticklabels=['AB', 'AF', 'AG', 'AI', 'AK'])
    ax4.legend()
#%% Collect data
else:
    d = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                     'Mar2022CampaignData/Fits/crossed/24_06_2022_115315')
    tag = '12 Bkgd Remove'

    with open(os.path.join(d,'df_fit.p'),'rb') as f:
        df_results = pickle.load(f)

    first_plot = True

    #%% Plot
    # plot velocities
    if first_plot:
        fig1, ax1 = plt.subplots()
        fig2, ax2 = plt.subplots()
        fig3, ax3 = plt.subplots()
        fig4, ax4 = plt.subplots()
    # df_results['Vel. (m/s)'].plot(ax=ax1,linestyle='None',marker='o')
    ax1.errorbar([0,1,2,3,4],df_results['Vel. (m/s)'],
                  yerr=0*df_results['Vel. (m/s)'],linestyle='None',marker='o',
                  label=tag)
    ax1.set_xlabel('Run')
    ax1.set_ylabel('Velocities (m/s)')
    ax1.set_ylim([200,950])
    ax1.set(xticks=[0,1,2,3,4],xticklabels=['AB','AF','AG','AJ','AK'])
    ax1.legend()

    # plot concentrations
    ax2.errorbar([0,1,2,3,4],df_results['Conc.'],
                  yerr=0*df_results['Conc.'],linestyle='None',marker='o',
                  label=tag)
    ax2.set_xlabel('Run')
    ax2.set_ylabel('Concentration')
    ax2.set_ylim([0.05,0.3])
    ax2.set(xticks=[0,1,2,3,4],xticklabels=['AB','AF','AG','AJ','AK'])
    ax2.legend()

    # plot pressures
    ax3.errorbar([0,1,2,3,4],df_results['Pres. (atm)'],
                  yerr=0.0*df_results['Pres. (atm)'],linestyle='None',marker='o',
                  label=tag)
    ax3.set_xlabel('Run')
    ax3.set_ylabel('Pressure (atm)')
    ax3.set_ylim([0.3,1.8])
    ax3.set(xticks=[0,1,2,3,4],xticklabels=['AB','AF','AG','AJ','AK'])
    ax3.legend()

    # plot temperatures
    ax4.errorbar([0,1,2,3,4],df_results['Temp. (K)'],
                  yerr=0*df_results['Temp. (K)'],linestyle='None',marker='o',
                  label=tag)
    ax4.set_xlabel('Run')
    ax4.set_ylabel('Temperature (K)')
    ax4.set_ylim([200,800])
    ax4.set(xticks=[0,1,2,3,4],xticklabels=['AB','AF','AG','AJ','AK'])
    ax4.legend()

