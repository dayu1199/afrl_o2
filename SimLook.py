"""
Look at sim results
"""
import pickle
import pandas as pd
import seaborn as sns
import os

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

label_pre = '06_12_2022_140724'
label_post = '06_12_2022_140845'
d = os.path.join(r'/Users/david/Library/CloudStorage',
                 'GoogleDrive-dayu1199@colorado.edu/My Drive',
                 'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits/sim')

with open(os.path.join(d, label_pre, 'df_fit.p'), 'rb') as f:
    df_fit_pre = pickle.load(f)

with open(os.path.join(d, label_post, 'df_fit.p'), 'rb') as f:
    df_fit_post = pickle.load(f)

df_fit = pd.concat([df_fit_pre, df_fit_post])

df_stat = df_fit.describe()
df_stat_pre = df_fit_pre.describe()
df_stat_post = df_fit_post.describe()

sns.displot(data=df_fit['Vel. (m/s)'])
sns.displot(data=df_fit['Temp. (K)'])