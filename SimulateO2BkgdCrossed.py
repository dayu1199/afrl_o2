#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Simulate o2 spectra in crossed bkgd runs

Created on Tue May 10 13:50:09 2022

@author: david
"""
#%% import modules

import numpy as np
import matplotlib.pyplot as plt
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
from pldspectrapy.constants import *
import td_support as td

# add O2 spectra
hapi.db_begin('') # initialize hapi database

#%% simulate the inside section
y_O2 = 0.21 # mole fraction O2
P = 0.888 # pressure (atm)
T = 317 # Temperature (K)
V = 653 # velocity, m/s
theta_u = np.deg2rad(34.87) # degree of beam, rad
theta_d = np.deg2rad(35.1) # degree of beam, rad
windowstart = 13010 # bandwidth start (cm-1)
windowend = 13180 # bandwidth end (cm-1)
dv = 0.00667 # frequency axis step size (cm-1)
L = 6.8/np.cos(theta) # path length of measurement (cm)

## Upstream
# adjust frequency axis to account for velocity doppler shift
wvnumspace = np.arange(windowstart,windowend,dv) # create array of frequency axis
start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,windowend]) # optimal start stop of bandwidth for fourier transfrom
wvnumspace = wvnumspace[start:stop]
nsteps = len(wvnumspace)-1
slope_doppler_u = V*np.sin(theta_u)/SPEED_OF_LIGHT+1 # doppler scaling due to velocity
new_wvw_num_u = wvnumspace*slope_doppler_u # apply to frequency axis
windowend_u = np.amax(new_wvw_num_u) # new bandwidth end
windowstart_u = np.amin(new_wvw_num_u) # new bandiwdth start
dv_u = (new_wvw_num_u[-1]-new_wvw_num_u[0])/nsteps # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend_u = windowend_u.item()
windowstart_u = windowstart_u.item()
dv_u = dv_u.item()

MIDS = [(7,1,y_O2*hapi.abundance(7,1))]
# create absorbance model using hapi
[nu_u,coefs_u] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv_u,OmegaRange=[windowstart_u, windowend_u],
            HITRAN_units=False, Environment={'p':P,'T':T},
            Diluent={'self': y_O2 - 0.21 / 0.79 * (1 - y_O2),
                     'air': (1 - y_O2) / 0.79},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2_u_in = coefs_u*L # get asorption (absorbance * pathlength)

## Downstream
# adjust frequency axis to account for velocity doppler shift
wvnumspace = np.arange(windowstart,windowend,dv) # create array of frequency axis
start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,windowend]) # optimal start stop of bandwidth for fourier transfrom
wvnumspace = wvnumspace[start:stop]
nsteps = len(wvnumspace)-1
slope_doppler_d = V*np.sin(-theta_d)/SPEED_OF_LIGHT+1 # doppler scaling due to velocity
new_wvw_num_d = wvnumspace*slope_doppler_d # apply to frequency axis
windowend_d = np.amax(new_wvw_num_d) # new bandwidth end
windowstart_d = np.amin(new_wvw_num_d) # new bandiwdth start
dv_d = (new_wvw_num_d[-1]-new_wvw_num_d[0])/nsteps # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend_d = windowend_d.item()
windowstart_d = windowstart_d.item()
dv_d = dv_d.item()

# create absorbance model using hapi
[nu_d,coefs_d] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv_d,OmegaRange=[windowstart_d, windowend_d],
            HITRAN_units=False, Environment={'p':P,'T':T},
            Diluent={'self': y_O2 - 0.21 / 0.79 * (1 - y_O2),
                     'air': (1 - y_O2) / 0.79},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2_d_in = coefs_d*L # get asorption (absorbance * pathlength)

#%% simulate the outside section
y_O2 = 0.0169 # mole fraction O2
P = 0.916 # pressure (atm)
T = 250.09 # Temperature (K)
windowstart = 13010 # bandwidth start (cm-1)
windowend = 13180 # bandwidth end (cm-1)
dv = 0.00667 # frequency axis step size (cm-1)
L = 10

# adjust frequency axis to account for velocity doppler shift
wvnumspace = np.arange(windowstart,windowend,dv) # create array of frequency axis
start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,windowend]) # optimal start stop of bandwidth for fourier transfrom
wvnumspace = wvnumspace[start:stop]
nsteps = len(wvnumspace)-1
windowstart = np.amin(wvnumspace) # new bandiwdth start
windowend = np.amax(wvnumspace) # new bandwidth end
dv = (wvnumspace[-1]-wvnumspace[0])/nsteps # new freq. axis step size

MIDS = [(7,1,y_O2*hapi.abundance(7,1))]
# create absorbance model using hapi
[nu,coefs] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv,OmegaRange=[windowstart, windowend],
            HITRAN_units=False, Environment={'p':P,'T':T},
            Diluent={'self': y_O2 - 0.21 / 0.79 * (1 - y_O2),
                     'air': (1 - y_O2) / 0.79},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2_out = coefs*L # get asorption (absorbance * pathlength)

##% simulate the box section
# adjust frequency axis to account for velocity doppler shift
P = 1
T = 295
y_O2 = 0.0042
# windowstart = 13010 # bandwidth start (cm-1)
# windowend = 13180 # bandwidth end (cm-1)
# dv = 0.00667 # frequency axis step size (cm-1)
L = 35
# create absorbance model using hapi
MIDS = [(7,1,y_O2*hapi.abundance(7,1))]
[nu,coefs] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv,OmegaRange=[windowstart, windowend],
            HITRAN_units=False, Environment={'p':P,'T':T},
            Diluent={'self': y_O2 - 0.21 / 0.79 * (1 - y_O2),
                     'air': (1 - y_O2) / 0.79},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2_box = coefs*L # get asorption (absorbance * pathlength)

#%% combine all sections
abs_O2_u = abs_O2_u_in + abs_O2_out + abs_O2_box
abs_O2_d = abs_O2_d_in + abs_O2_out + abs_O2_box

#%% Make Noise Vector
add_noise = True
if add_noise:
    beta = 9
    center = 13095
    sigma = 105
    subbotin = np.exp(-(abs(wvnumspace - center) / sigma) ** beta)
    # normalize to 0.5
    subbotin *= 0.95 / max(subbotin)
    subbotin = 1 - subbotin
    weight = subbotin.copy()
    # calculate an etalon
    etalon = 0.15 * np.cos((wvnumspace - 13099) * 2 * np.pi / 11) + 0.8
    weight *= etalon
    # add slight parabola
    parabola = ((wvnumspace - 13100) / 60) ** 2 + 1
    parabola /= min(parabola)
    weight *= parabola
    noise_level = 0.017
    noise_u = np.random.normal(0,noise_level,len(abs_O2_u))
    noise_d = np.random.normal(0,noise_level,len(abs_O2_d))
    noise_u *= weight
    noise_d *= weight

#%% Plot Data
# plot absorption model
lambda_O2 = 1e7/wvnumspace # transform to wavelength units
plt.figure() # initialize plotting figure
# plt.plot(nu,abs_O2+noise,label='O2') #plot absorption
plt.figure()
plt.title('Upstream')
if add_noise:
  plt.plot(wvnumspace, abs_O2_u+noise_u)
plt.plot(wvnumspace, abs_O2_u, label='Total')
plt.plot(wvnumspace, abs_O2_u_in, label='Flow')
plt.plot(wvnumspace, abs_O2_out, label='Optics')
plt.plot(wvnumspace, abs_O2_box, label='Free-Space')
plt.legend()  # add legend
plt.xlim([13017, 13165])

plt.figure()
plt.title('Downstream')
if add_noise:
    plt.plot(wvnumspace, abs_O2_d + noise_d)
plt.plot(wvnumspace, abs_O2_d, label='Total')
plt.plot(wvnumspace, abs_O2_d_in, label='Flow')
plt.plot(wvnumspace, abs_O2_out, label='Optics')
plt.plot(wvnumspace, abs_O2_box, label='Free-Space')
plt.legend()  # add legend
plt.xlim([13017, 13165])

# time_O2 = np.fft.irfft(abs_O2_u)
# if add_noise:
#     time_noise = np.fft.irfft(noise_u)
#
# plt.figure()
# plt.plot(time_noise+time_O2)
# plt.plot(time_O2)

#%% Save Data
# save data
data = np.vstack((wvnumspace,abs_O2_u))
data = np.transpose(data)
np.savetxt('O2simulation_9bkgd_up.txt',data)

data = np.vstack((wvnumspace,abs_O2_d))
data = np.transpose(data)
np.savetxt('O2simulation_9bkgd_down.txt',data)