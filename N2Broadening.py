#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Exploring how to get to just N2 broadening

Created on Mon May 23 10:24:23 2022

@author: david
"""
#%% import modules
import matplotlib.pyplot as plt

from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi

plt.rcParams.update({"figure.autolayout": True, "lines.linewidth": 0.8,
                     'font.size': 22,'font.family':'serif',
                     'font.serif':'Times'})

#%% fetch O2
hapi.db_begin('')

# hapi.fetch_by_ids('O2',[36,37,38],13000,13200, ParameterGroups=['160-char', 'SDVoigt'])

#%% get broadening data
nu,gamma_air,gamma_self = hapi.getColumns('O2',['nu','gamma_air','gamma_self'])
gamma_N2 = (gamma_air-0.21*gamma_self)/0.79

air_o2_factor = gamma_air/gamma_self

#%% plot gammas
plt.figure()
plt.scatter(nu,gamma_self,label='O2')
plt.scatter(nu,gamma_air,label='Air')
plt.scatter(nu,gamma_N2,label='N2')
plt.legend()

plt.title('O2-X Broadening')
plt.xlabel('Wavenumber ($cm^{-1}$)')
plt.ylabel('Gamma')

#%% plot ratio of gammas
plt.figure()
plt.scatter(nu,gamma_self/gamma_N2)

plt.xlabel('Wavenumber ($cm^{-1}$)')
plt.ylabel('gamma_self/gamma_n2')