#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
plot fit results for box fits

Created on Wed May 11 10:22:33 2022

@author: david
"""

import os
import matplotlib.pyplot as plt
import pickle
import seaborn as sns

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

# plt.rcParams.update({"figure.autolayout": True, "lines.linewidth": 0.8,
#                      'font.size': 22})

#%% Collect data
d = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                 'Mar2022CampaignData/Fits/box/15_06_2022_160013')

with open(os.path.join(d,'df_fit.p'),'rb') as f:
    df_results = pickle.load(f)

#%% Plot
# plot velocities
# fig1, ax1 = plt.subplots()
# df_results['Vel. (m/s)'].plot(ax=ax1,linestyle='None',marker='o')
# # ax1.errorbar([0,1,2,3,4],df_results['Vel. (m/s)'],
#               # yerr=0.05*df_results['Vel. (m/s)'],linestyle='None',marker='o')
# ax1.set_xlabel('Run')
# ax1.set_ylabel('Velocities (m/s)')
# ax1.set_ylim([200,950])
# ax1.set(xticks=[0,1,2,3,4],xticklabels=['AB','AF','AG','AI','AK'])

# plot concentrations
fig2, ax2 = plt.subplots()
df_results['Conc.'].plot(ax=ax2,linestyle='None',marker='o')
# ax2.errorbar([0,1,2,3,4],df_results['Conc.'],
              # yerr=0*df_results['Conc.'],linestyle='None',marker='o')
ax2.set_xlabel('Run')
ax2.set_ylabel('Concentration')
ax2.set_ylim([0.00,0.15])
# ax2.set(xticks=[0,1,2,3,4],xticklabels=['AB','AF','AG','AI','AK'])

# plot pressures
fig3, ax3 = plt.subplots()
df_results['Pres. (atm)'].plot(ax=ax3,linestyle='None',marker='o')
# ax3.errorbar([0,1,2,3,4],df_results['Pres. (atm)'],
              # yerr=0.11*df_results['Pres. (atm)'],linestyle='None',marker='o')
ax3.set_xlabel('Run')
ax3.set_ylabel('Pressure (atm)')
ax3.set_ylim([0,6])
# ax3.set(xticks=[0,1,2,3,4],xticklabels=['AB','AF','AG','AI','AK'])

# plot temperatures
fig4, ax4 = plt.subplots()
df_results['Temp. (K)'].plot(ax=ax4,linestyle='None',marker='o')
# ax4.errorbar([0,1,2,3,4],df_results['Temp. (K)'],
              # yerr=0.084*df_results['Temp. (K)'],linestyle='None',marker='o')
ax4.set_xlabel('Run')
ax4.set_ylabel('Temperature (K)')
ax4.set_ylim([200,800])
# ax4.set(xticks=[0,1,2,3,4],xticklabels=['AB','AF','AG','AI','AK'])

#%% Find averages
conc = df_results['Conc.']
plt.figure()
sns.boxplot(df_results['Conc.'])

pres = df_results['Pres. (atm)']
plt.figure()
sns.boxplot(df_results['Pres. (atm)'])

conc = df_results['Temp. (K)']
plt.figure()
sns.boxplot(df_results['Temp. (K)'])
