#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot O2 Spectra
Created on Tue Sep  7 09:26:39 2021

@author: david
"""
#%% import modules
import numpy as np
import matplotlib.pyplot as plt
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
from pldspectrapy.constants import *
import td_support as td

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

#%% Params
add_bkgd = False
add_noise = True
noise_level = 0.009
label = 'AB'
if add_bkgd:
    label += 'wbkgd'
savedata = True
# add O2 spectra
hapi.db_begin('')  # initialize hapi database

y_O2 = 0.21  # mole fraction O2
P = 1.171  # pressure (atm)
T = 586  # Temperature (K)
V = 866  # velocity, m/s
theta_u = np.deg2rad(36.5085)  # degree of beam, rad
theta_d = np.deg2rad(-36.698)
windowstart = 13000  # bandwidth start (cm-1)
windowend = 13200  # bandwidth end (cm-1)
dv = 0.00667  # frequency axis step size (cm-1)
L_u = 6.81736/np.cos(theta_u)  # path length of measurement (cm)
L_d = 6.81736/np.cos(theta_d)  # path length of measurement (cm)

#%% Simmulate Upstream
# adjust frequency axis to account for velocity doppler shift
wvnumspace = np.arange(windowstart,windowend,dv) # create array of frequency axis
start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,windowend]) # optimal start stop of bandwidth for fourier transfrom
wvnumspace = wvnumspace[start:stop]
nsteps = len(wvnumspace)-1
slope_doppler_u = V*np.sin(theta_u)/SPEED_OF_LIGHT+1 # doppler scaling due to velocity
new_wvw_num_u = wvnumspace*slope_doppler_u # apply to frequency axis
windowend_u = np.amax(new_wvw_num_u) # new bandwidth end
windowstart_u = np.amin(new_wvw_num_u) # new bandiwdth start
dv_u = (new_wvw_num_u[-1]-new_wvw_num_u[0])/nsteps # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend_u = windowend_u.item()
windowstart_u = windowstart_u.item()
dv_u = dv_u.item()

MIDS = [(7, 1, y_O2*hapi.abundance(7, 1))]
# create absorbance model using hapi
[nu_u,coefs_u] = hapi.absorptionCoefficient_Voigt(MIDS,
            ('O2'), OmegaStep=dv_u, OmegaRange=[windowstart_u, windowend_u],
            HITRAN_units=False, Environment={'p': P, 'T': T},
            Diluent={'self': y_O2,
                     'air': (1-y_O2)},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2_u = coefs_u*L_u  # get asorption (absorbance * pathlength)

abs_total_u = np.sum(abs_O2_u)


#%% Simulate Downstream
# adjust frequency axis to account for velocity doppler shift
wvnumspace = np.arange(windowstart,windowend,dv) # create array of frequency axis
start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,windowend]) # optimal start stop of bandwidth for fourier transfrom
wvnumspace = wvnumspace[start:stop]
nsteps = len(wvnumspace)-1
slope_doppler_d = V*np.sin(theta_d)/SPEED_OF_LIGHT+1 # doppler scaling due to velocity
new_wvw_num_d = wvnumspace*slope_doppler_d # apply to frequency axis
windowend_d = np.amax(new_wvw_num_d) # new bandwidth end
windowstart_d = np.amin(new_wvw_num_d) # new bandiwdth start
dv_d = (new_wvw_num_d[-1]-new_wvw_num_d[0])/nsteps # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend_d = windowend_d.item()
windowstart_d = windowstart_d.item()
dv_d = dv_d.item()

# create absorbance model using hapi
[nu_d, coefs_d] = hapi.absorptionCoefficient_Voigt(MIDS,
            ('O2'), OmegaStep=dv_d, OmegaRange=[windowstart_d, windowend_d],
            HITRAN_units=False, Environment={'p': P, 'T': T},
            Diluent={'self': y_O2,
                     'air': (1-y_O2)},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2_d = coefs_d*L_d  # get asorption (absorbance * pathlength)

#%% Simulate Background
if add_bkgd:
    # Optics
    y_O2 = 0.0187  # mole fraction O2
    P = 0.634  # pressure (atm)
    T = 309.1  # Temperature (K)
    L = 10

    # adjust frequency axis to account for velocity doppler shift
    wvnumspace = np.arange(windowstart, windowend, dv)  # create array of frequency axis
    start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,
                                                      windowend])  # optimal start stop of bandwidth for fourier transfrom
    wvnumspace = wvnumspace[start:stop]
    nsteps = len(wvnumspace) - 1
    windowstart = np.amin(wvnumspace)  # new bandiwdth start
    windowend = np.amax(wvnumspace)  # new bandwidth end
    dv = (wvnumspace[-1] - wvnumspace[0]) / nsteps  # new freq. axis step size

    MIDS = [(7, 1, y_O2 * hapi.abundance(7, 1))]
    # create absorbance model using hapi
    [nu, coefs] = hapi.absorptionCoefficient_SDVoigt(MIDS,
                                                     ('O2'), OmegaStep=dv, OmegaRange=[windowstart, windowend],
                                                     HITRAN_units=False, Environment={'p': P, 'T': T},
                                                     Diluent={'self': y_O2,
                                                              'air': (1 - y_O2)},
                                                     IntensityThreshold=0)  # *1.5095245023408206
    abs_O2_out = coefs * L  # get asorption (absorbance * pathlength)

    # Box
    # adjust frequency axis to account for velocity doppler shift
    P = 1
    T = 295
    y_O2 = 0.0042
    L = 35
    MIDS = [(7, 1, y_O2 * hapi.abundance(7, 1))]
    # create absorbance model using hapi
    [nu, coefs] = hapi.absorptionCoefficient_SDVoigt(MIDS,
                                                     ('O2'), OmegaStep=dv, OmegaRange=[windowstart, windowend],
                                                     HITRAN_units=False, Environment={'p': P, 'T': T},
                                                     Diluent={'self': y_O2,
                                                              'air': (1 - y_O2)},
                                                     IntensityThreshold=0)  # *1.5095245023408206
    abs_O2_box = coefs * L  # get asorption (absorbance * pathlength)

    abs_O2_u += abs_O2_out + abs_O2_box
    abs_O2_d += abs_O2_out + abs_O2_box

#%% create noise
# calculate a subbotin distribution, flat-top guassian, Beta degree of flatness
beta = 8
center = 13094
sigma = 98
subbotin = np.exp(-(abs(wvnumspace-center)/sigma)**beta)
# normalize to 0.5
subbotin *= 0.95/max(subbotin)
subbotin = 1 - subbotin
weight = subbotin.copy()
# calculate an etalon
etalon_center = 13097
etalon_amp = 0.18
etalon = etalon_amp*np.cos((wvnumspace-etalon_center)*2*np.pi/11) + 0.8
weight *= etalon
# add slight parabola
parabola = ((wvnumspace-13100)/60)**2+1
parabola /= min(parabola)
weight *= parabola
noise = np.random.normal(0, noise_level, len(abs_O2_u))

noise *= weight
plt.figure(); plt.plot(subbotin); plt.plot(etalon);
plt.plot(parabola); plt.plot(weight)

#%% plot absorption model
lambda_O2 = 1e7/wvnumspace # transform to wavelength units
# plt.figure() # initialize plotting figure
# plt.plot(nu,abs_O2+noise,label='O2') #plot absorption
plt.figure()
plt.title('Upstream')
if add_noise:
  plt.plot(wvnumspace, abs_O2_u+noise, label='Model w/ Noise')
plt.plot(wvnumspace, abs_O2_u, label='Total')
if add_bkgd:
    plt.plot(wvnumspace, abs_O2_out, label='Optics')
    plt.plot(wvnumspace, abs_O2_box, label='Free-Space')

plt.legend()  # add legend
plt.xlim([13017, 13165])


plt.figure()
plt.title('Downstream')
if add_noise:
    plt.plot(wvnumspace, abs_O2_d + noise, label='Model w/ Noise')
plt.plot(wvnumspace, abs_O2_d, label='Total')
if add_bkgd:
    plt.plot(wvnumspace, abs_O2_out, label='Optics')
    plt.plot(wvnumspace, abs_O2_box, label='Free-Space')

plt.legend()  # add legend
plt.xlim([13017, 13165])

time_O2 = np.fft.irfft(abs_O2_u)
time_noise = np.fft.irfft(noise)

# plt.figure()
# plt.plot(time_noise+time_O2)
# plt.plot(time_O2)

#%% noise level
def closest_value(input_list, input_value):
    arr = np.asarray(input_list)
    i = (np.abs(arr - input_value)).argmin()
    return i
start = closest_value(wvnumspace, 13079.48)
stop = closest_value(wvnumspace, 13082.82)
noise_level = np.std(noise[start:stop])
print('Noise level = '+str(noise_level))

#%% save data

# save data
if savedata:
    data = np.vstack((wvnumspace, abs_O2_u))
    data = np.transpose(data)
    np.savetxt('O2simulation_'+label+'_up.txt', data)

    data = np.vstack((wvnumspace, abs_O2_d))
    data = np.transpose(data)
    np.savetxt('O2simulation_'+label+'_down.txt', data)
