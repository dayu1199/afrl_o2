#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot OceanOptics4000 Spectra

Created on Sun May 30 21:27:03 2021

@author: davidyun
"""
from sys import platform
import os
import numpy as np
import matplotlib.pyplot as plt
import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi

if platform == 'darwin':
    dr = r'/Volumes/USB DRIVE/102821_filterspectra'
elif platform == 'win32':
    dr = r'C:\Users\David\Google Drive\O2\May30_PT_firstpass_FP_1_BP_1_1'
fn = '102821_spectrathroughfilter.txt'

ttl_array = []
lambda_array = []
int_array = []

line_count = 0
with open(fn,'r') as f:
    for line in f.readlines():
        if line_count >= 14:
            data = line.split()
            lambda_array.append(float(data[0]))
            int_array.append(float(data[1]))
        line_count += 1
lambda_array = np.array(lambda_array)
int_array = np.array(int_array)

plt.figure(1)
# add O2 spectra
hapi.db_begin('')
# hapi.fetch_by_ids('O2',7,1,10000,16666,ParameterGroups=[160-char','SDVoigt'])

plt.plot(lambda_array, int_array, label='DCS Laser Intensity')
y_O2 = 0.2
P = 1
T = 300
windowstart = 11000
windowend = 15000
dv = 0.00667
L = 10

[nu,coefs] = hapi.absorptionCoefficient_Voigt([(7,1,y_O2)],
            ('O2'), OmegaStep=dv,OmegaRange=[windowstart, windowend],
            HITRAN_units=False, Environment={'p':P,'T':T},
            Diluent={'self':y_O2,'air':(1-y_O2)},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2 = coefs*L
lambda_O2 = 1e7/nu
plt.plot(lambda_O2,-abs_O2*6000000,label='O2 A-Band')


# plt.ylabel('Intensity')
plt.ytic
plt.xlabel('Wavelength(nm)')
plt.legend()
