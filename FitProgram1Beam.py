# -*- coding: utf-8 -*-
"""
Created on Fri Aug 17 10:25:23 2018
@author: Amanda Makowiecki

---------------- General Comments ----------------------------------
Data Format:
    Data should be text files with the first column being optical frequency and the next columns being frequency spectra.
    wvnUnits and transUnits parameters should be updated to reflect the units of optical frequency and frequency spectra.
    
Fitting Variables:
    Possible fitted parameters are temperature, pressure, concentrations, lineshift, broadening
    Broadening is simply a scaled air broadening eg. if broadening is 2 then broadening is equivalent to twice gamma air
    
Variables:
    The lengths of Pressure_Guess and Temp_Guess should be the same as the number of files being fit. The length of conc
    should be the same length as the number of files being fit x the number of molecules being fit.


-----------------Time Domain Specific Parameters --------------------
Weight Window:
    The weight_window defines where in the time domain signal to begin and end weighting.
    The input is proportion of points relative to full length of wave and the weighting is mirrored across the center of the wave
    For example a weight window starting with 0.002 will ignore the first and last 0.2% of data points in the time domain.
    FOr clean spectra 0.002 is a good choice, but this parameter should be informed by looking at the residual signal from a time
    domain fit. 
    When combined with the weighting function aa stop point for weighting a generally not needed and therefore the second weighting window
    parameter is set to 0.5. In very low pressure conditions, this can be decreased to minimize noise contribution at high frequencies.

Etalon Windows:
    In the time domain etalons manifest as a delta function. They can be easily viewed as spikes in the data and residual of a time domain fit.
    The etalonWindows parameter species the start and stop points in the time domain signal where the etalon lies. Only etalons on the first half
    of the time domain window need to be defined here. 

"""
import os
from FitMFID1Beam import Fitting_Spectra, savethings
import matplotlib.pyplot as plt
import numpy as np
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
# import hapi
import td_support as td
import time
from scipy.signal import savgol_filter

def noise_weight(x_data):
    beta = 9
    center = 13092
    sigma = 100
    subbotin = np.exp(-(abs(x_data - center) / sigma) ** beta)
    # normalize to 0.5
    subbotin *= 0.95 / max(subbotin)
    subbotin = 1 - subbotin
    weight = subbotin.copy()
    # calculate an etalon
    etalon = 0.05 * np.cos((x_data - 13105) * 2 * np.pi / 11) + 0.8
    weight *= etalon
    # add slight parabola
    parabola = ((x_data - 13100) / 60) ** 2 + 1
    parabola /= min(parabola)
    weight *= parabola
    return weight

###############################################################################
##########  Ouput options for Fitting #########################################

def FitProgram(filename, dir_pull, save_path, file_tag,
               weight_window, weight_flat, etalonWindows, angle,
               band_fit,
               Molecules_to_Fit, isotopes,
               Pressure_Fit, Temp_Fit, Velocity_Fit, Shift_Fit, Conc_Fit,
               Broadening_Fit, Pathlength_Fit,
               Pressure_Guess, Temp_Guess, Velocity_Guess, Shift_Guess, 
               Conc_Guess, Broadening_Guess, Pathlength_Guess,
               background_remove=False, molec_back='O2',
               pathlength_b=35.935, press_back=1, temp_back=300, conc_back=0,
               save_files=True, plot_results=True, print_fit_report=True,
               wvnUnits=True, transUnits=True,
               noise_add=False, noise_level=0.001):
    ########## Data Specific Parameters #######################################
    print(filename)
    
    theta_path = angle/360*2*np.pi    
    Pathlength_Guess = Pathlength_Guess/np.cos(theta_path) #cm
    conc = [Conc_Guess]*len(Molecules_to_Fit) # Initializes concentration guess
                                                # for each molecule to fit
    ######### Select which Parameters to Fit  #################################
    Conc_Fit = [Conc_Fit]*len(Molecules_to_Fit) # True for each molecule to fit
    ###########################################################################

    # Loads in file
    data = np.loadtxt(os.path.join(dir_pull,filename)) 
    
    if data[0,0]>data[-1,0]: # Flips data if it is inverted
        data = np.flipud(data) 
        print("Flipping FFT")
    print(len(data))

    start, stop = td.bandwidth_select_td(data[:,0], band_fit)
    if start < stop:
        if wvnUnits:
            x_data = data[start:stop, 0]
        else:
            x_data = data[start:stop, 0]/29979245800
    else:
        if wvnUnits:
            x_data = data[start:stop:-1, 0]
        else:
            x_data = data[start:stop:-1, 0]/29979245800
            
    # Imports spectra into y_data, converts to absorbance if in trans units
    y_data = np.zeros((len(x_data), len(data[0])-1))   
    if transUnits:
        y_data =-np.log(data[start:stop,1])
    else:
        y_data =data[start:stop,1]
        
    if noise_add:
        noise = np.random.normal(0,noise_level,len(y_data))
        weight = noise_weight(x_data)
        noise *= weight
        y_data += noise
    
    # convert to time domain
    ff = (np.fft.irfft(y_data))
    y_fit = np.zeros((ff.size, (len(data[0])-1)*2))
        
    tic = time.time()
    
    # Builds logic vector to imform fit of parameters to float
    Fit_vector = [Temp_Fit, Pressure_Fit, Shift_Fit, Broadening_Fit,
                  Velocity_Fit,Pathlength_Fit]
    Fit_vector.extend(Conc_Fit)
    
    # Builds vector of approximate parameters for starting fits
    Guess_vector = [Temp_Guess, Pressure_Guess, Shift_Guess, Broadening_Guess,
                    Velocity_Guess, Pathlength_Guess]
    Guess_vector.extend(conc)
    
    # Loads Molecule Data from Hitran
    hapi.db_begin('')  # Location to store imported HITRAN data
     
    HITRAN_Molecules = ['H2O','CO2','O3','N2O','CO','CH4','O2','NO','SO2',
                        'NO2','NH3','HNO3','OH','HF','HCl','HBr','HI','ClO',
                        'OCS','H2CO','HOCl','N2','HCN','CH3Cl','H2O2','C2H2',
                        'C2H6','PH3','COF2','SF6','H2S','HCOOH','HO2','O',
                        'ClONO2','NO+','HOBr','C2H4','CH3OH','CH3Br','CH3CN',
                        'CF4','C4H2','HC3N','H2','CS','SO3','ArH2O_Labfit']
    molecule_numbers =[0]*(len(Molecules_to_Fit)); pp=0
        
    ii=0
    for item in Molecules_to_Fit:
         if item not in HITRAN_Molecules:
             print("Listed Molecule is not included in HITRAN", item)
         elif item == 'O2' or item == 'O2_Vgt':
             print('Using preexising O2 HITRAN data in project.')
         else:  
             molecule_numbers[ii] = (HITRAN_Molecules.index(item))+1
             isotope_ids = []
             for jj in range(len(isotopes[ii])):
                 isotope_ids.append(hapi.ISO[molecule_numbers[ii],
                                             isotopes[ii][jj]][0])
             hapi.fetch_by_ids(item, isotope_ids,min(x_data)-1,
                               max(x_data)+1)
         ii+=1
            
    if plot_results:
        plt.figure()
        plt.plot(x_data,y_data)
        plt.title('Transmission')
        plt.xlabel('Wavenumber (cm-1')
        plt.ylabel('Transmission')
        
    tic= time.time()  # Times each fit iteration
    if not y_data[10] or y_data[100]>1000:  # checks from empty FFTs and nans in data
        print("Empty FFT")
    else:
        # This sends data to function which compiles it for the fit
        [result,timewave,concs,concs_err,weight_wave,time_wave] = \
            Fitting_Spectra(Guess_vector, Fit_vector, x_data, y_data,
                            Molecules_to_Fit, molecule_numbers, isotopes,
                            theta_path, weight_window, weight_flat,
                            background_remove, molec_back, pathlength_b,
                            press_back, temp_back, conc_back,
                            etalonWindows = etalonWindows,)

        Velocity = result.best_values['velocity']
        Temperature = result.best_values['Temp']
        Pressure = result.best_values['press']
        Shift = result.best_values['shift']
        Broadening = result.best_values['Broadening']
        Pathlength = result.best_values['Pathlength']
        Temperature_err = result.params['Temp'].stderr
        Pressure_err = result.params['press'].stderr
        Shift_err = result.params['shift'].stderr
        Velocity_err = result.params['velocity'].stderr
        Broadening_err = result.params['Broadening'].stderr
        Pathlength_err = result.params['Pathlength'].stderr
        
        Conc = []
        Conc_err = []
        for jj in range(len(Molecules_to_Fit)):
            Conc.append(concs[jj])
            Conc_err.append(concs_err[jj])
    
        # Stores best fits and corresponding data in y_fit matrix                
        time_fit = result.best_fit
        time_data = result.data
        time_res = time_data - time_fit
        weight = weight_wave
        
        abs_fit = np.real(np.fft.rfft(time_fit))
        abs_data = np.real(np.fft.rfft(time_data))
        # abs_res = abs_data - abs_fit
        # abs_res = savgol_filter(abs_res,81,2)
        # abs_datanobl = abs_data - abs_res
        abs_datanobl = np.real(np.fft.rfft(time_data-(1-weight_wave)*time_res))
        abs_resnobl = abs_datanobl-abs_fit
        
        # taking a look at what is actually fitted
        abs_fitweight = np.fft.rfft(time_fit*weight)
        abs_dataweight = np.fft.rfft(time_data*weight)
        abs_resweight = abs_dataweight - abs_fitweight
        
        if print_fit_report:
            print(result.fit_report(result.params))
    
        res = np.copy(np.fft.rfft(result.best_fit))  # Initializing residual wave
        toc = time.time()     
        print('Computation Time:', toc - tic)
    
        ffn = os.path.basename(filename)[:-10]
        if plot_results: 
            # No baseline fits
            plt.figure() 
            plt.plot(x_data,abs_datanobl, label='Data', linewidth=1)
            plt.plot(x_data,abs_resnobl,
                     label='Residual', linewidth=1)
            plt.plot(x_data,abs_fit, label='Fit',
                linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title("Freq Domain" + filename)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
            
            # Weighted FFT of time domain fits yellow stream
            plt.figure() 
            plt.plot(x_data,abs_dataweight, label='Data', linewidth=1)
            plt.plot(x_data,abs_resweight,
                     label='Residual', linewidth=1)
            plt.plot(x_data,abs_fitweight, label='Fit',
                linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title("Yellow Stream"+ffn)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
        
            # Time domain fit plot. Plot vs timewave for effective time on x axis 
            # yellow
            plt.figure()
            plt.plot(time_data, label='Data', linewidth=1)
            plt.plot(time_res, label='Residual',
                     linewidth=1)
            plt.plot(time_fit, label='Fit', linewidth=1) 
            plt.plot(weight_wave*max(time_data))
            plt.title('Time Domain' + filename)
            plt.xlabel('Index')
            plt.ylabel('Magnitude')
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show()
        
        if save_files:  
            filename_report = os.path.join(save_path,
                                           filename[:-10]+file_tag+'_report.txt')
            
            f = open(filename_report,'w')
            f.write(result.fit_report())
            f.close()
            
            # save cepstrum data
            filename_save_cep = os.path.join(save_path,
                                               filename[:-10]+file_tag +'_cep.txt')
        
            cep = np.transpose([time_data,time_fit])
            np.savetxt(filename_save_cep,cep)
        
            # save absorbance data
            filename_save_abs = os.path.join(save_path,
                                             filename[:-10]+file_tag+'_abs.txt')   
        
            abs_data = np.transpose([np.real(x_data),abs_data,abs_datanobl])
            np.savetxt(filename_save_abs,abs_data)
        
            # save res data
            filename_save_res = os.path.join(save_path,
                                             filename[:-10]+file_tag+'_res.txt')   
        
            res_data = np.transpose([np.real(x_data),np.real(np.fft.rfft(
                result.data)-np.fft.rfft(result.best_fit))])
            np.savetxt(filename_save_res,res_data)
            
            # save fit data
            filename_save_fit = os.path.join(save_path,
                                             filename[:-10]+file_tag+'_fit.txt')   
        
            fit_data = np.transpose([np.real(x_data),np.real(np.fft.rfft(
                result.best_fit))])
            np.savetxt(filename_save_fit,fit_data)
            
            #save weight vector
            filename_save_weight = os.path.join(save_path,
                                                filename[:-10]+file_tag+'_weight.txt')   
            weight_data = np.transpose([time_wave,weight_wave])
            np.savetxt(filename_save_weight,weight_data)
        
        # print(weight_window)
        fit_results = [Velocity, Velocity_err, Temperature, Temperature_err, 
                       Pressure, Pressure_err, Shift, Shift_err, Conc,
                       Conc_err, Broadening, Broadening_err,
                       Pathlength, Pathlength_err]
        return fit_results