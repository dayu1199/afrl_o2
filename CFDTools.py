# -*- coding: utf-8 -*-
"""
Functions used to analyze high-res CFD provided by AFRL

Created on Wed Feb 17 11:24:10 2021

@author: David
"""
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pandas as pd
from scipy import interpolate
from math import radians

from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
from pldspectrapy.constants import *

# plt.rcParams.update({'figure.autolayout': True,'font.size':11,
#                      'font.family':'Times New Roman',
#                      'lines.linewidth':.8})

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

## function to find nearest value in an array to a specified array
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


def ExtractHeightSlice(height,dr,fn,label,axial=456.2e-3,
                       deg=35):
    """
    Extract arrays of values corresponding to a certain height from CFD

    Parameters
    ----------
    height : float
        HEIGHT FROM BOTTOM OF ISOLATOR (m)
    dr : string
        DIRECTORY WHERE CFD IS
    fn : string
        CFD FILENAME

    Returns
    -------
    array of property values at height
    u = streamwise velocity (m/s)
    v = radial velocity (m/s)
    w = spanwise velocity (m/s)
    p = pressure (Pa)
    T = temperature (K)
    z = z coordinates of CFD points
    x = x coordinates of CFD points
    """
    # read in lines and create dictionary
    headers = ['X','Y','Z','P','T','U','V','W','R','M','mf_O2','mf_H2O','mf_CH4','mf_CO2','mf_N2']
    cfdvalues = []
    line_count = 0
    with open(os.path.join(dr,fn)) as f:
        for line in f:
            line_count += 1
            if line_count > 21:
                # don't read lines that aren't close to height to save on memory
                if float(line.split()[2])<height-0.0005 or float(line.split()[2])>height+0.0005:
                    continue
                # also cut out everything before x=0 or after x=0.7
                # if float(line.split()[0])<0 or float(line.split()[0])>0.7:
                #     continue
                cfdvalues.append(np.asfarray(line.split()))
    cfd_df = pd.DataFrame.from_records(cfdvalues, columns=headers)

    # convert mass fractions to mole fractions
    M_O2 = 31.998e-3  # kg/mol
    M_H2O = 18.01528e-3
    M_CH4 = 16.04e-3
    M_CO2 = 44.01e-3
    M_N2 = 28.02e-3
    cfd_df['M_tot'] = 1 / (cfd_df['mf_O2'] / M_O2 + cfd_df['mf_H2O'] / M_H2O +
                           cfd_df['mf_CH4'] / M_CH4 + cfd_df['mf_CO2'] / M_CO2 +
                           cfd_df['mf_N2'] / M_N2)
    cfd_df['X_O2'] = cfd_df['mf_O2'] * cfd_df['M_tot'] / M_O2
    cfd_df['X_H2O'] = cfd_df['mf_H2O'] * cfd_df['M_tot'] / M_H2O
    cfd_df['X_CH4'] = cfd_df['mf_CH4'] * cfd_df['M_tot'] / M_CH4
    cfd_df['X_CO2'] = cfd_df['mf_CO2'] * cfd_df['M_tot'] / M_CO2
    cfd_df['X_N2'] = cfd_df['mf_N2'] * cfd_df['M_tot'] / M_N2

    z = find_nearest(cfd_df['Z'], height)
        
    slice_z = cfd_df.loc[cfd_df['Z']==z]
    slice_z.sort_values(by=['X', 'Y'], inplace=True)
    
    x = []
    x_row = []
    i = 0
    # i_stop = [] # use this variable if you want to get the uneven slices in the
        #other variables as well
    for val in slice_z['X']:
        if i>0:
            if val!=x_row[-1]:
                x.append(x_row)
                x_row = []
                x_row.append(val)
                # i_stop.append(i)
                i = 1
            else:
                x_row.append(val)
                i += 1
        else:
            x_row.append(val)
            i += 1
    x.append(x_row)
    
    # get rid of the uneven axial slices, i.e. get rid of any row that does not
    # have the same # of entries as the first row
    n_col = len(x[0])
    # find where the rows start getting uneven and cut out everything after
    i = 0
    i_cut = 0
    for list_x in x:
        if len(list_x)!=n_col:
            i_cut = i
            break
        i+=1
    if i_cut != 0:
        del x[i:]
    
    y = []
    y_row = []
    i = 0
    for val in slice_z['Y']:
        if i_cut != 0:
            if len(y) == i_cut:
                break
        i += 1
        y_row.append(val)
        if i == n_col:
            y.append(y_row)
            y_row = []
            i = 0
    
    p = []
    p_row = []
    i = 0
    for val in slice_z['P']:
        if i_cut != 0:
            if len(p) == i_cut:
                break
        i += 1
        p_row.append(val)
        if i == n_col:
            p.append(p_row)
            p_row = []
            i = 0
    u = []
    u_row = []
    i = 0
    for val in slice_z['U']:
        if i_cut != 0:
            if len(u) == i_cut:
                break
        i += 1
        u_row.append(val)
        if i == n_col:
            u.append(u_row)
            u_row = []
            i = 0
    v = []
    v_row = []
    i = 0
    for val in slice_z['V']:
        if i_cut != 0:
            if len(v) == i_cut:
                break
        i += 1
        v_row.append(val)
        if i == n_col:
            v.append(v_row)
            v_row = []
            i = 0
    w = []
    w_row = []
    i = 0
    for val in slice_z['W']:
        if i_cut != 0:
            if len(w) == i_cut:
                break
        i += 1
        w_row.append(val)
        if i == n_col:
            w.append(w_row)
            w_row = []
            i = 0
    T = []
    T_row = []
    i = 0
    for val in slice_z['T']:
        if i_cut != 0:
            if len(T) == i_cut:
                break
        i += 1
        T_row.append(val)
        if i == n_col:
            T.append(T_row)
            T_row = []
            i = 0
    xo2 = []
    xo2_row = []
    i = 0
    for val in slice_z['X_O2']:
        if i_cut != 0:
            if len(xo2) == i_cut:
                break
        i += 1
        xo2_row.append(val)
        if i == n_col:
            xo2.append(xo2_row)
            xo2_row = []
            i = 0
    xh2o = []
    xh2o_row = []
    i = 0
    for val in slice_z['X_H2O']:
        if i_cut != 0:
            if len(xh2o) == i_cut:
                break
        i += 1
        xh2o_row.append(val)
        if i == n_col:
            xh2o.append(xh2o_row)
            xh2o_row = []
            i = 0
    xco2 = []
    xco2_row = []
    i = 0
    for val in slice_z['X_CO2']:
        if i_cut != 0:
            if len(xco2) == i_cut:
                break
        i += 1
        xco2_row.append(val)
        if i == n_col:
            xco2.append(xco2_row)
            xco2_row = []
            i = 0
    x = np.asarray(x)
    y = np.asarray(y)
    p = np.asarray(p)
    u = np.asarray(u)
    w = np.asarray(w)
    T = np.asarray(T)
    xh2o = np.asarray(xh2o)
    xco2 = np.asarray(xco2)
    # remove last row (boundary) from array
    # z = z[:-1,:]
    # x = x[:-1,:]
    # p = p[:-1,:]
    # u = u[:-1,:]
    # w = w[:-1,:]
    # T = T[:-1,:]
    # xh2o = xh2o[:-1,:]
    # xco2 = xco2[:-1,:]
    # define interpolation points
    n_int= 100
    theta = deg*np.pi/180
    y_up1 = np.linspace(0,0.03408,num=n_int)
    y_down1 = np.flip(y_up1)
    x_up1 = np.linspace(axial,axial+0.03408*np.tan(theta),num=n_int)
    x_down1 = np.linspace(axial-0.03408*np.tan(theta),axial,num=n_int)
    x1 = np.append(x_down1[:-1],x_up1)
    y1 = np.append(y_down1[:-1],y_up1)
    
    # z_up2 = np.linspace(0,0.05,num=n_int)
    # z_down2 = np.flip(z_up2)
    # x_up2 = np.linspace(240e-3,240e-3+0.05*np.tan(theta),num=n_int)
    # x_down2 = np.linspace(240e-3-0.05*np.tan(theta),240e-3,num=n_int)
    # x2 = np.append(x_down2[:-1],x_up2[:-1])
    # z2 = np.append(z_down2[:-1],z_up2[:-1])
    
    # z_up3 = np.linspace(0,0.05,num=n_int)
    # z_down3 = np.flip(z_up2)
    # x_up3 = np.linspace(260e-3,260e-3+0.05*np.tan(theta),num=n_int)
    # x_down3 = np.linspace(260e-3-0.05*np.tan(theta),260e-3,num=n_int)
    # x3 = np.append(x_down3[:-1],x_up3[:-1])
    # z3 = np.append(z_down3[:-1],z_up3[:-1])
    
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4)
    fig.suptitle((label+ ' Height '+str(height)+' m'))
    # plot tomographic
    h1 = ax1.pcolormesh(y,x,p/101325)
    ax1.set_ylabel('Axial (m)')
    ax1.set_xlabel('Transverse (m)')
    ax1.set_title('Pressure (atm)')
    # ax1.plot(z3,x3,'g.')
    # ax1.plot(z2,x2,'.',color='orange')
    ax1.plot(y1,x1,'b.')
    cbar = fig.colorbar(h1, ax=ax1, extend='both',label='Pressure (atm)',
                        orientation='horizontal')

    h2 = ax2.pcolormesh(y, x, T)
    # ax3.set_ylabel('Axial (m)')
    ax2.set_xlabel('Transverse (m)')
    ax2.set_title('Temperature')
    ax2.set(yticklabels=[])
    # ax3.plot(z3,x3,'g.')
    # ax3.plot(z2,x2,'.',color='orange')
    ax2.plot(y1, x1, 'b.')
    cbar = fig.colorbar(h2, ax=ax2, extend='both', label='Temperature (K)',
                        orientation='horizontal')

    h3 = ax3.pcolormesh(y,x,u)
    # ax2.set_ylabel('Axial (m)')
    ax3.set_xlabel('Transverse (m)')
    ax3.set_title('Streamwise Velocity')
    ax3.set(yticklabels=[])
    # ax2.plot(z3,x3,'g.')
    # ax2.plot(z2,x2,'.',color='orange')
    ax3.plot(y1,x1,'b.')
    cbar = fig.colorbar(h3, ax=ax3, extend='both',label='Velocity (m/s)',
                        orientation='horizontal')

    
    # h4 = ax4.pcolormesh(y,x,v)
    # # ax2.set_ylabel('Axial (m)')
    # ax4.set_xlabel('Transverse (m)')
    # ax4.set_title(' Radial Velocity')
    # ax4.set(yticklabels=[])
    # # ax2.plot(z3,x3,'g.')
    # # ax2.plot(z2,x2,'.',color='orange')
    # ax4.plot(y1,x1,'b.')
    # cbar = fig.colorbar(h4, ax=ax4, extend='both',label='Velocity (m/s)',
    #                     orientation='horizontal')

    h4 = ax4.pcolormesh(y, x, xo2)
    # ax2.set_ylabel('Axial (m)')
    ax4.set_xlabel('Transverse (m)')
    ax4.set_title('X_O2')
    ax4.set(yticklabels=[])
    # ax2.plot(z3,x3,'g.')
    # ax2.plot(z2,x2,'.',color='orange')
    ax4.plot(y1, x1, 'b.')
    cbar = fig.colorbar(h4, ax=ax4, extend='both', label='O2 Mole Fraction',
                        orientation='horizontal')
    
    return x,y,u,w,p,T,xo2

# def ExtractAxialSlice(axial,dr,fn,label):
#     """
#
#     Parameters
#     ----------
#     axial : float
#         axial position, distance from nozzle exit (m)
#     dr : string
#         DIRECTORY WHERE CFD IS
#     fn : string
#         CFD FILENAME
#     label : string
#         PLOT LABEL
#
#     Returns
#     -------
#     z : SPANWISE
#         DESCRIPTION.
#     y : TYPE
#         DESCRIPTION.
#     u : TYPE
#         DESCRIPTION.
#     w : TYPE
#         DESCRIPTION.
#     p : TYPE
#         DESCRIPTION.
#     T : TYPE
#         DESCRIPTION.
#
#     """
#
#     headers = ['X','Y','Z','P','T','U','V','W','R','M','X_O2','X_H2O','X_CH4','X_CO2','X_N2']
#     cfdvalues = []
#     f = open(os.path.join(dr,fn))
#     line_count = 0
#     for line in f:
#         line_count += 1
#         if line_count > 21:
#             # also cut out everything before x=0 or after x=0.7
#             if float(line.split()[0])<axial-0.001 or float(line.split()[0])>axial+0.001:
#                 continue
#             cfdvalues.append(np.asfarray(line.split()))
#     cfd_df = pd.DataFrame.from_records(cfdvalues, columns=headers)
#     print(max(cfd_df['T']))
#     x = find_nearest(cfd_df['X'],axial)
#     slice_x = cfd_df.loc[cfd_df['X']==x]
#
#     slice_x.sort_values(by=['Y','Z'], inplace=True)
#
#     y = []
#     y_row = []
#     i = 0
#     i_stop = [] # use this variable if you want to get the uneven slices in the
#         # other variables as well
#     for val in slice_x['Y']:
#         if i>0:
#             if val!=y_row[-1]:
#                 y.append(y_row)
#                 y_row = []
#                 y_row.append(val)
#                 i_stop.append(i)
#                 i = 1
#             else:
#                 y_row.append(val)
#                 i += 1
#         else:
#             y_row.append(val)
#             i += 1
#     y.append(y_row)
#     i_stop.append(i)
#
#     # get rid of the uneven axial slices, i.e. get rid of any row that does not
#     # have the same # of entries as the first row
#     n_col = len(y[-1])
#     # find where the rows start getting uneven and cut out everything after
#     i = 0
#     i_cut = 0
#     for list_y in y:
#         if len(list_y)==n_col:
#             i_cut = i
#             break
#         i+=1
#     if i_cut != 0:
#         del y[:i_cut]
#
#     z = []
#     z_row = []
#     i = 0
#     j = 0
#     for val in slice_x['Z']:
#         i += 1
#         z_row.append(val)
#         if i == i_stop[j]:
#             z.append(z_row)
#             z_row = []
#             i = 0
#             j += 1
#     if i_cut != 0:
#         del z[:i_cut]
#
#     p = []
#     p_row = []
#     i = 0
#     j = 0
#     for val in slice_x['P']:
#         i += 1
#         p_row.append(val)
#         if i == i_stop[j]:
#             p.append(p_row)
#             p_row = []
#             i = 0
#             j += 1
#     if i_cut != 0:
#         del p[:i_cut]
#
#     u = []
#     u_row = []
#     i = 0
#     j = 0
#     for val in slice_x['U']:
#         i += 1
#         u_row.append(val)
#         if i == i_stop[j]:
#             u.append(u_row)
#             u_row = []
#             i = 0
#             j += 1
#     if i_cut != 0:
#         del u[:i_cut]
#
#     w = []
#     w_row = []
#     i = 0
#     j = 0
#     for val in slice_x['W']:
#         i += 1
#         w_row.append(val)
#         if i == i_stop[j]:
#             w.append(w_row)
#             w_row = []
#             i = 0
#             j += 1
#     if i_cut != 0:
#         del w[:i_cut]
#
#     T = []
#     T_row = []
#     i = 0
#     j = 0
#     for val in slice_x['T']:
#         i += 1
#         T_row.append(val)
#         if i == i_stop[j]:
#             T.append(T_row)
#             T_row = []
#             i = 0
#             j += 1
#     if i_cut != 0:
#         del T[:i_cut]
#     z = np.asarray(z)
#     y = np.asarray(y)
#     p = np.asarray(p)
#     u = np.asarray(u)
#     w = np.asarray(w)
#     T = np.asarray(T)
#     # remove last row (boundary) from array
#     z = z[:-1,:]
#     y = y[:-1,:]
#     p = p[:-1,:]
#     u = u[:-1,:]
#     w = w[:-1,:]
#     T = T[:-1,:]
#
#
#     fig, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4)
#     fig.suptitle((label+' Axial '+str(axial)+' m'))
#     # plot tomographic
#     h1 = ax1.pcolormesh(z,y,p)
#     ax1.set_ylabel('Axial (m)')
#     ax1.set_xlabel('Height (m)')
#     ax1.set_title('Pressure')
#     cbar = fig.colorbar(h1, ax=ax1, extend='both',label='Pressure (K)',
#                         orientation='horizontal')
#
#     h2 = ax2.pcolormesh(z,y,u)
#     # ax2.set_ylabel('Axial (m)')
#     ax2.set_xlabel('Transverse (m)')
#     ax2.set_title('Streamwise Velocity')
#     ax2.set(yticklabels=[])
#     cbar = fig.colorbar(h2, ax=ax2, extend='both',label='Velocity (m/s)',
#                         orientation='horizontal')
#
#
#     h3 = ax3.pcolormesh(z,y,T)
#     # ax3.set_ylabel('Axial (m)')
#     ax3.set_xlabel('Transverse (m)')
#     ax3.set_title('Temperature')
#     ax3.set(yticklabels=[])
#     cbar = fig.colorbar(h3, ax=ax3, extend='both',label='Temperature (K)',
#                         orientation='horizontal')
#
#
#     h4 = ax4.pcolormesh(z,y,w)
#     # ax2.set_ylabel('Axial (m)')
#     ax4.set_xlabel('Transverse (m)')
#     ax4.set_title(' Spanwise Velocity')
#     ax4.set(yticklabels=[])
#     cbar = fig.colorbar(h4, ax=ax4, extend='both',label='Velocity (m/s)',
#                         orientation='horizontal')
#     return z,y,u,w,p,T
#
# def ExtractAxialSlice_lowres(axial,dr,fn,label):
#     """
#
#     Parameters
#     ----------
#     axial : float
#         axial position, distance from nozzle exit (m)
#     dr : string
#         DIRECTORY WHERE CFD IS
#     fn : string
#         CFD FILENAME
#
#     Returns
#     -------
#     z : SPANWISE
#         DESCRIPTION.
#     y : TYPE
#         DESCRIPTION.
#     u : TYPE
#         DESCRIPTION.
#     w : TYPE
#         DESCRIPTION.
#     p : TYPE
#         DESCRIPTION.
#     T : TYPE
#         DESCRIPTION.
#
#     """
#
#     headers = ['X','Y','Z','P','T','U','V','W','R','M','X_O2','X_H2O','X_CH4','X_CO2','X_N2']
#     cfdvalues = []
#     f = open(os.path.join(dr,fn))
#     j = 0
#     read = True
#     while read:
#         line = f.readline()
#         if line[0:4]=='ZONE':
#             for i in range(4):
#                 throw = f.readline()
#                 # print(throw)
#             #read slice
#             parse = True
#             while parse:
#                 line = f.readline()
#                 params = str.split(line)
#                 #stop at end of slice
#                 if len(params) < 5:
#                     parse = False
#                 else:
#                     cfdvalues.append(np.asfarray(line.split()))
#             j += 1
#             print(j)
#             # read slices 1-5.
#             if j == 6:
#                 read = False
#     cfd_df = pd.DataFrame.from_records(cfdvalues, columns=headers)
#     x = find_nearest(cfd_df['X'],axial)
#     slice_x = cfd_df.loc[cfd_df['X']==x]
#
#     slice_x.sort_values(by=['Y','Z'], inplace=True)
#
#     y = []
#     y_row = []
#     i = 0
#     i_stop = [] # use this variable if you want to get the uneven slices in the
#         # other variables as well
#     for val in slice_x['Y']:
#         if i>0:
#             if val!=y_row[-1]:
#                 y.append(y_row)
#                 y_row = []
#                 y_row.append(val)
#                 i_stop.append(i)
#                 i = 1
#             else:
#                 y_row.append(val)
#                 i += 1
#         else:
#             y_row.append(val)
#             i += 1
#     y.append(y_row)
#     i_stop.append(i)
#
#     # get rid of the uneven axial slices, i.e. get rid of any row that does not
#     # have the same # of entries as the first row
#     n_col = len(y[-1])
#     # find where the rows start getting uneven and cut out everything after
#     i = 0
#     i_cut = 0
#     for list_y in y:
#         if len(list_y)==n_col:
#             i_cut = i
#             break
#         i+=1
#     if i_cut != 0:
#         del y[:i_cut]
#
#     z = []
#     z_row = []
#     i = 0
#     j = 0
#     for val in slice_x['Z']:
#         i += 1
#         z_row.append(val)
#         if i == i_stop[j]:
#             z.append(z_row)
#             z_row = []
#             i = 0
#             j += 1
#     if i_cut != 0:
#         del z[:i_cut]
#
#     p = []
#     p_row = []
#     i = 0
#     j = 0
#     for val in slice_x['P']:
#         i += 1
#         p_row.append(val)
#         if i == i_stop[j]:
#             p.append(p_row)
#             p_row = []
#             i = 0
#             j += 1
#     if i_cut != 0:
#         del p[:i_cut]
#
#     u = []
#     u_row = []
#     i = 0
#     j = 0
#     for val in slice_x['U']:
#         i += 1
#         u_row.append(val)
#         if i == i_stop[j]:
#             u.append(u_row)
#             u_row = []
#             i = 0
#             j += 1
#     if i_cut != 0:
#         del u[:i_cut]
#
#     w = []
#     w_row = []
#     i = 0
#     j = 0
#     for val in slice_x['W']:
#         i += 1
#         w_row.append(val)
#         if i == i_stop[j]:
#             w.append(w_row)
#             w_row = []
#             i = 0
#             j += 1
#     if i_cut != 0:
#         del w[:i_cut]
#
#     T = []
#     T_row = []
#     i = 0
#     j = 0
#     for val in slice_x['T']:
#         i += 1
#         T_row.append(val)
#         if i == i_stop[j]:
#             T.append(T_row)
#             T_row = []
#             i = 0
#             j += 1
#     if i_cut != 0:
#         del T[:i_cut]
#     z = np.asarray(z)
#     y = np.asarray(y)
#     p = np.asarray(p)
#     u = np.asarray(u)
#     w = np.asarray(w)
#     T = np.asarray(T)
#     # remove last row (boundary) from array
#     z = z[:-1,:]
#     y = y[:-1,:]
#     p = p[:-1,:]
#     u = u[:-1,:]
#     w = w[:-1,:]
#     T = T[:-1,:]
#
#
#     fig, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4)
#     fig.suptitle((label+' Axial '+str(axial)+' m'))
#     # plot tomographic
#     h1 = ax1.pcolormesh(z,y,p)
#     ax1.set_ylabel('Axial (m)')
#     ax1.set_xlabel('Height (m)')
#     ax1.set_title('Pressure')
#     cbar = fig.colorbar(h1, ax=ax1, extend='both',label='Pressure (K)',
#                         orientation='horizontal')
#
#     h2 = ax2.pcolormesh(z,y,u)
#     # ax2.set_ylabel('Axial (m)')
#     ax2.set_xlabel('Transverse (m)')
#     ax2.set_title('Streamwise Velocity')
#     ax2.set(yticklabels=[])
#     cbar = fig.colorbar(h2, ax=ax2, extend='both',label='Velocity (m/s)',
#                         orientation='horizontal')
#
#
#     h3 = ax3.pcolormesh(z,y,T)
#     # ax3.set_ylabel('Axial (m)')
#     ax3.set_xlabel('Transverse (m)')
#     ax3.set_title('Temperature')
#     ax3.set(yticklabels=[])
#     cbar = fig.colorbar(h3, ax=ax3, extend='both',label='Temperature (K)',
#                         orientation='horizontal')
#
#
#     h4 = ax4.pcolormesh(z,y,w)
#     # ax2.set_ylabel('Axial (m)')
#     ax4.set_xlabel('Transverse (m)')
#     ax4.set_title(' Spanwise Velocity')
#     ax4.set(yticklabels=[])
#     cbar = fig.colorbar(h4, ax=ax4, extend='both',label='Velocity (m/s)',
#                         orientation='horizontal')
#     return z,y,u,w,p,T
#
# def ExtractTransverseSlice(transverse,dr,fn,label):
#     """
#
#     Parameters
#     ----------
#     transverse : float
#         transverse position, distance from plane of symmetry (m)
#     dr : string
#         DIRECTORY WHERE CFD IS
#     fn : string
#         CFD FILENAME
#     label : string
#         PLOT LABEL
#
#     Returns
#     -------
#     x : SPANWISE
#         DESCRIPTION.
#     y : TYPE
#         DESCRIPTION.
#     u : TYPE
#         DESCRIPTION.
#     w : TYPE
#         DESCRIPTION.
#     p : TYPE
#         DESCRIPTION.
#     T : TYPE
#         DESCRIPTION.
#
#     """
#
#     headers = ['X','Y','Z','P','T','U','V','W','R','M','X_O2','X_H2O','X_CH4','X_CO2','X_N2']
#     cfdvalues = []
#     f = open(os.path.join(dr,fn))
#     line_count = 0
#     for line in f:
#         line_count += 1
#         if line_count > 21:
#             # also cut out everything before x=0 or after x=0.7
#             if float(line.split()[0])<0 or float(line.split()[0])>0.7:
#                 continue
#             elif float(line.split()[2])<transverse-0.001 or \
#                 float(line.split()[2])>transverse+0.001:
#                 continue
#             cfdvalues.append(np.asfarray(line.split()))
#     cfd_df = pd.DataFrame.from_records(cfdvalues, columns=headers)
#     z = find_nearest(cfd_df['Z'],transverse)
#     slice_z = cfd_df.loc[cfd_df['Z']==z]
#
#     slice_z.sort_values(by=['X','Y'], inplace=True)
#
#     x = []
#     x_row = []
#     i = 0
#     # i_stop = [] # use this variable if you want to get the uneven slices in the
#         #other variables as well
#     for val in slice_z['X']:
#         if i>0:
#             if val!=x_row[-1]:
#                 x.append(x_row)
#                 x_row = []
#                 x_row.append(val)
#                 # i_stop.append(i)
#                 i = 1
#             else:
#                 x_row.append(val)
#                 i += 1
#         else:
#             x_row.append(val)
#             i += 1
#     x.append(x_row)
#
#     # get rid of the uneven axial slices, i.e. get rid of any row that does not
#     # have the same # of entries as the first row
#     n_col = len(x[0])
#     # find where the rows start getting uneven and cut out everything after
#     i = 0
#     i_cut = 0
#     for list_x in x:
#         if len(list_x)!=n_col:
#             i_cut = i
#             break
#         i+=1
#     if i_cut != 0:
#         del x[i:]
#
#     y = []
#     y_row = []
#     i = 0
#     for val in slice_z['Y']:
#         if i_cut != 0:
#             if len(z) == i_cut:
#                 break
#         i += 1
#         y_row.append(val)
#         if i == n_col:
#             y.append(y_row)
#             y_row = []
#             i = 0
#
#     p = []
#     p_row = []
#     i = 0
#     for val in slice_z['P']:
#         if i_cut != 0:
#             if len(p) == i_cut:
#                 break
#         i += 1
#         p_row.append(val)
#         if i == n_col:
#             p.append(p_row)
#             p_row = []
#             i = 0
#     u = []
#     u_row = []
#     i = 0
#     for val in slice_z['U']:
#         if i_cut != 0:
#             if len(u) == i_cut:
#                 break
#         i += 1
#         u_row.append(val)
#         if i == n_col:
#             u.append(u_row)
#             u_row = []
#             i = 0
#     w = []
#     w_row = []
#     i = 0
#     for val in slice_z['W']:
#         if i_cut != 0:
#             if len(w) == i_cut:
#                 break
#         i += 1
#         w_row.append(val)
#         if i == n_col:
#             w.append(w_row)
#             w_row = []
#             i = 0
#     T = []
#     T_row = []
#     i = 0
#     for val in slice_z['T']:
#         if i_cut != 0:
#             if len(T) == i_cut:
#                 break
#         i += 1
#         T_row.append(val)
#         if i == n_col:
#             T.append(T_row)
#             T_row = []
#             i = 0
#     xh2o = []
#     xh2o_row = []
#     i = 0
#     for val in slice_z['X_H2O']:
#         if i_cut != 0:
#             if len(xh2o) == i_cut:
#                 break
#         i += 1
#         xh2o_row.append(val)
#         if i == n_col:
#             xh2o.append(xh2o_row)
#             xh2o_row = []
#             i = 0
#     xco2 = []
#     xco2_row = []
#     i = 0
#     for val in slice_z['X_CO2']:
#         if i_cut != 0:
#             if len(xco2) == i_cut:
#                 break
#         i += 1
#         xco2_row.append(val)
#         if i == n_col:
#             xco2.append(xco2_row)
#             xco2_row = []
#             i = 0
#     x = np.asarray(x)
#     y = np.asarray(y)
#     p = np.asarray(p)
#     u = np.asarray(u)
#     w = np.asarray(w)
#     T = np.asarray(T)
#     xh2o = np.asarray(xh2o)
#     xco2 = np.asarray(xco2)
#
#     # calculate mass flux
#     M_h2o = 0.01801 # kg/mol
#     M_air = 0.02897 # kg/mol
#     R = 8.314 # J K-1 mol-1
#     m_dot = (xh2o*M_h2o+(1-xh2o)*M_air)*p*u/(R*T)
#     # remove last row (boundary) from array
#     # x = x[:-1,:]
#     # y = y[:-1,:]
#     # p = p[:-1,:]
#     # u = u[:-1,:]
#     # w = w[:-1,:]
#     # T = T[:-1,:]
#     # xh2o = xh2o[:-1,:]
#     # xco2 = xco2[:-1,:]
#     # define interpolation points
#     # n_int= 100
#     # deg = 35
#     # theta = deg*np.pi/180
#     # z_up1 = np.linspace(0,0.05,num=n_int)
#     # z_down1 = np.flip(z_up1)
#     # x_up1 = np.linspace(axial,axial+0.05*np.tan(theta),num=n_int)
#     # x_down1 = np.linspace(axial-0.05*np.tan(theta),axial,num=n_int)
#     # x1 = np.append(x_down1[:-1],x_up1[:-1])
#     # z1 = np.append(z_down1[:-1],z_up1[:-1])
#
#
#
#     fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1)
#     fig.suptitle((label+ ' Transverse '+str(transverse)+' m'))
#     # plot tomographic
#     h1 = ax1.pcolormesh(x,y,p)
#     ax1.set_ylabel('Height (m)')
#     # ax1.set_title('Pressure')
#     ax1.set(xticklabels=[])
#     # ax1.plot(z3,x3,'g.')
#     # ax1.plot(z2,x2,'.',color='orange')
#     # ax1.plot(z1,x1,'b.')
#     cbar = fig.colorbar(h1, ax=ax1, extend='both',label='Pressure (K)',
#                         orientation='vertical')
#
#     h2 = ax2.pcolormesh(x,y,u)
#     # ax2.set_ylabel('Axial (m)')
#     ax2.set_ylabel('Height (m)')
#     # ax2.set_title('Streamwise Velocity')
#     ax2.set(xticklabels=[])
#     # ax2.plot(z3,x3,'g.')
#     # ax2.plot(z2,x2,'.',color='orange')
#     # ax2.plot(z1,x1,'b.')
#     cbar = fig.colorbar(h2, ax=ax2, extend='both',label='U Velocity (m/s)',
#                         orientation='vertical')
#
#
#     h3 = ax3.pcolormesh(x,y,T)
#     # ax3.set_ylabel('Axial (m)')
#     ax3.set_ylabel('Height (m)')
#     # ax3.set_title('Temperature')
#     ax3.set(xticklabels=[])
#     # ax3.plot(z3,x3,'g.')
#     # ax3.plot(z2,x2,'.',color='orange')
#     # ax3.plot(z1,x1,'b.')
#     cbar = fig.colorbar(h3, ax=ax3, extend='both',label='Temperature (K)',
#                         orientation='vertical')
#
#
#     h4 = ax4.pcolormesh(x,y,w)
#     # ax2.set_ylabel('Axial (m)')
#     ax4.set_ylabel('Height (m)')
#     ax4.set_xlabel('Axial (m)')
#     # ax4.set_title(' Spanwise Velocity')
#     # ax2.plot(z3,x3,'g.')
#     # ax2.plot(z2,x2,'.',color='orange')
#     # ax4.plot(z1,x1,'b.')
#     cbar = fig.colorbar(h4, ax=ax4, extend='both',label='W Velocity (m/s)',
#                         orientation='vertical')
#
#     plt.figure()
#     plt.pcolormesh(x,y,p/101325,cmap='plasma')
#     plt.ylabel('Height (m)')
#     plt.xlabel('Axial (m)')
#
#     plt.title(('Pressure along Transverse '+str(transverse)+' m'))
#     plt.colorbar(extend='both',label='Pressure (atm)',
#                         orientation='vertical')
#     pos = 0.220
#     width = 0.1014*np.tan(np.deg2rad(12.5))
#     xy = [pos-width/2,0]
#     rect = mpatches.Rectangle(xy,width,0.039,fill=False,
#                               color='green',hatch = '...')
#     plt.gca().add_patch(rect)
#
#     pos = 0.240
#     width = 0.1014*np.tan(np.deg2rad(12.5))
#     xy = [pos-width/2,0]
#     rect = mpatches.Rectangle(xy,width,0.039,fill=False,
#                               color='blue',hatch = '...')
#     plt.gca().add_patch(rect)
#
#     pos = 0.260
#     width = 0.1014*np.tan(np.deg2rad(12.5))
#     xy = [pos-width/2,0]
#     rect = mpatches.Rectangle(xy,width,0.039,fill=False,
#                               color='red',hatch = '...')
#     plt.gca().add_patch(rect)
#
#     plt.figure()
#     plt.pcolormesh(x[:,1:-1],y[:,1:-1],m_dot[:,1:-1], cmap='plasma')
#     plt.ylabel('Height (m)')
#     plt.xlabel('Axial (m)')
#
#     plt.title(('Mass Flux along Transverse '+str(transverse)+' m'))
#     plt.colorbar(extend='both',label='Mass Flux (kg m-2 s-1)',
#                         orientation='vertical')
#     # pos = 0.220
#     # width = 0.1014*np.tan(np.deg2rad(12.5))
#     # xy = [pos-width/2,0]
#     # rect = mpatches.Rectangle(xy,width,0.039,fill=False,
#     #                           color='black',hatch = '...')
#     # plt.gca().add_patch(rect)
#     #
#     # pos = 0.240
#     # width = 0.1014*np.tan(np.deg2rad(12.5))
#     # xy = [pos-width/2,0]
#     # rect = mpatches.Rectangle(xy,width,0.039,fill=False,
#     #                           color='blue',hatch = '...')
#     # plt.gca().add_patch(rect)
#     #
#     # pos = 0.260
#     # width = 0.1014*np.tan(np.deg2rad(12.5))
#     # xy = [pos-width/2,0]
#     # rect = mpatches.Rectangle(xy,width,0.039,fill=False,
#     #                           color='red',hatch = '...')
#     # plt.gca().add_patch(rect)
#
#     plt.figure()
#     plt.pcolormesh(x[:,1:-1], y[:,1:-1], u[:,1:-1], cmap='plasma')
#     plt.ylabel('Height (m)')
#     plt.xlabel('Axial (m)')
#
#     plt.title(('Velocity along Transverse ' + str(transverse) + ' m'))
#     plt.colorbar(extend='both', label='Velocity (m/s)',
#                  orientation='vertical')
#
#     return x,y,u,w,p,T,xh2o,xco2

def InterpolationPoints(x,y,m,axial_pos,deg=35,n_int=100):
    """
    Find simple averaged value of property along LOS

    Parameters
    ----------
    x : array
        AXIAL COORDINATES OF CFD POINTS (m)
    y : array
        RADIAL COORDINATES OF CFD POINTS (m)
    m : array
        PROPERTY VALUES OF CFD POINTS
    axial_pos : float
        AXIAL POSITION THAT BEAMS ARE CENTERED ON (m)
    deg : float
        ANGLE OF BEAMS NORMAL TO FLOW (deg)
    n_int : int
        NUMBER OF POINTS TO MAKE ALONG LOS BEFORE AVERAGING

    Returns
    -------
    xy: float array
        x, y coordinates on the path
    m_int : float array
        interpolated property values along both paths
    m_LOS_simple : float
        simple LOS value of property along path
    """
    
    # create interpolation function
    f = interpolate.interp2d(y[0,:],x[:,0],m, bounds_error=True)
    # create array of interpolated values for each beam
    y_up = np.linspace(0,0.03408,num=n_int)
    y_down = np.flip(y_up)
    theta = radians(deg)
    x_up = np.linspace(axial_pos,axial_pos+0.03408*np.tan(theta),num=n_int)
    x_down = np.linspace(axial_pos-0.03408*np.tan(theta),axial_pos,num=n_int)
    xy_up = np.transpose(np.concatenate([[x_up],[y_up]]))
    xy_down = np.transpose(np.concatenate([[x_down],[y_down]]))
    m_up = []
    for xu,yu in xy_up:
        m_up.append(f(yu,xu))
    m_up = np.asarray(m_up)
    m_down = []
    for xd,yd in xy_down:
        m_down.append(f(yd,xd))
    m_down = np.asarray(m_down)
    # take out the last value of each. each length segment only gets one
    # corresponding value
    xy = np.concatenate((xy_down[:-1],xy_up),axis=0)
    m_int = np.append(m_down[:-1],m_up[:-1])
    m_LOS_simple = np.mean(m_int)
    return xy, m_int, m_LOS_simple

def LOSSpectralSimulation(dr,fn,height,axial, deg=35, n_int=200,v_start=13015,
                          v_stop=13170,dv=0.00667,label='', mol_to_sim='O2'):
    """
    Creates upstream and downstream spectra based on LOS through scramjet

    Parameters
    ----------
    dr : TYPE
        DESCRIPTION.
    fn : TYPE
        DESCRIPTION.
    height : TYPE
        DESCRIPTION.
    axial : TYPE
        DESCRIPTION.
    deg : TYPE, optional
        DESCRIPTION. The default is 35.
    n_int : TYPE, optional
        DESCRIPTION. The default is 200.
    v_start : TYPE, optional
        DESCRIPTION. The default is 6863.
    v_stop : TYPE, optional
        DESCRIPTION. The default is 7278.
    dv : TYPE, optional
        DESCRIPTION. The default is 0.00667.
    label : TYPE, optional
        DESCRIPTION. The default is ''.

    Returns
    -------
    transmission_data_p : TYPE
        DESCRIPTION.
    transmission_data_y : TYPE
        DESCRIPTION.

    """
    # Get height slice
    x,y,u,w,p,T,xo2 = ExtractHeightSlice(height, dr, fn, label,
                                               axial=axial, deg=deg)
    # Get property values along line of sight
    xy, u_int, u_LOS_simple = InterpolationPoints(x, y, u, axial, deg=deg,
                                                  n_int=n_int)
    xy, w_int, w_LOS_simple = InterpolationPoints(x, y, w, axial, deg=deg,
                                                  n_int=n_int)
    xy, p_int, p_LOS_simple = InterpolationPoints(x, y, p, axial, deg=deg,
                                                  n_int=n_int)
    xy, T_int, T_LOS_simple = InterpolationPoints(x, y, T, axial, deg=deg,
                                                  n_int=n_int)
    xy, xo2_int, xo2_LOS_simple = InterpolationPoints(x, y, xo2, axial,
                                                        deg=deg, n_int=n_int)
    # xz, xco2_int, xco2_LOS_simple = InterpolationPoints(x, z, xco2, axial,
    #                                                     deg=deg, n_int=n_int)
    
    ## simulate spectra based on LOS
    wvnumspace = np.arange(v_start,v_stop,dv)
    nsteps = len(wvnumspace)-1
    hapi.db_begin('')
    absorbance_p = []
    absorbance_y = []
    for ii in range(len(u_int)):
        print(ii, ' out of ', (len(u_int)-1))
        # define path length
        # length of interpolation segment (z2-z1)**2+(x2-x1)**2, cm
        dL = (((xy[ii+1,1]-xy[ii,1])**2+(xy[ii+1,0]-xy[ii,0])**2)**(1/2))*100
        
        # characterize dL chunk using properties at beginning of chunk
        T = T_int[ii]
        P = p_int[ii]*9.86923e-6 # convert to atm
        O2mol = xo2_int[ii]
        u = u_int[ii]
        w = w_int[ii]
        
        # take into account w contribution
        theta = np.deg2rad(deg)
        # p upstream
        scale_p = 1+(u*np.sin(theta)+w*np.cos(theta))/SPEED_OF_LIGHT
        wvw_num_p = wvnumspace*scale_p
        windowend_p = np.amax(wvw_num_p)
        windowstart_p = np.amin(wvw_num_p)
        dv_p = (wvw_num_p[-1]-wvw_num_p[0])/nsteps
        
        MIDS = [(7,1,O2mol*hapi.abundance(7,1))]

        [nu_O2_p,coefs_O2_p] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            (mol_to_sim), OmegaStep=dv_p,OmegaRange=[windowstart_p, windowend_p],
            HITRAN_units=False, Environment={'p':P,'T':T},
            Diluent={'self': O2mol - 0.21 / 0.79 * (1 - O2mol),
                     'air': (1 - O2mol) / 0.79},
            IntensityThreshold =0)
        absorbance_O2_p = dL*coefs_O2_p
        absorbance_p.append(absorbance_O2_p)
        
        # y upstream
        scale_y = 1+(u*np.sin(-theta)+w*np.cos(-theta))/SPEED_OF_LIGHT
        wvw_num_y = wvnumspace*scale_y
        windowend_y = np.amax(wvw_num_y)
        windowstart_y = np.amin(wvw_num_y)
        dv_y = (wvw_num_y[-1]-wvw_num_y[0])/nsteps

        [nu_O2_y, coefs_O2_y] = hapi.absorptionCoefficient_SDVoigt(MIDS,
               (mol_to_sim), OmegaStep=dv_y,
               OmegaRange=[windowstart_y, windowend_y],
               HITRAN_units=False, Environment={'p': P, 'T': T},
               Diluent={'self': O2mol - 0.21 / 0.79 * (1 - O2mol),
                        'air': (1 - O2mol) / 0.79},
               IntensityThreshold=0)
        absorbance_O2_y = dL * coefs_O2_y
        absorbance_y.append(absorbance_O2_y)
    
    absorbance_tot_p = np.sum(absorbance_p, axis = 0)
    transmission_p = np.exp(-1*absorbance_tot_p)
    transmission_data_p = np.column_stack((wvnumspace,transmission_p))
    
    absorbance_tot_y = np.sum(absorbance_y, axis = 0)
    transmission_y = np.exp(-1*absorbance_tot_y)
    transmission_data_y = np.column_stack((wvnumspace,transmission_y))
    
    plt.figure()
    plt.plot(wvnumspace,absorbance_tot_p,'b',label='Downstream')
    plt.plot(wvnumspace,absorbance_tot_y,'r',label='Upstream')
    plt.legend()
    plt.show()
    
    return transmission_data_p, transmission_data_y

def LOSSpectralSimulation_Uniform(dr,fn,height,axial,deg=35, n_int=200,v_start=13015,
                          v_stop=13170,dv=0.00667,label='', mol_to_sim='O2'):
    """
    Creates UNIFORM upstream and downstream spectra based on LOS through
    scramjet

    Parameters
    ----------
    dr : TYPE
        DESCRIPTION.
    fn : TYPE
        DESCRIPTION.
    height : TYPE
        DESCRIPTION.
    axial : TYPE
        DESCRIPTION.
    deg : TYPE, optional
        DESCRIPTION. The default is 35.
    n_int : TYPE, optional
        DESCRIPTION. The default is 200.
    v_start : TYPE, optional
        DESCRIPTION. The default is 6863.
    v_stop : TYPE, optional
        DESCRIPTION. The default is 7278.
    dv : TYPE, optional
        DESCRIPTION. The default is 0.00667.
    label : TYPE, optional
        DESCRIPTION. The default is ''.

    Returns
    -------
    transmission_data_p : TYPE
        DESCRIPTION.
    transmission_data_y : TYPE
        DESCRIPTION.

    """
    # Get height slice
    x, y, u, w, p, T, xo2 = ExtractHeightSlice(height, dr, fn, label,
                                               axial=axial, deg=deg)
    # Get property values along line of sight
    xy, u_int, u_LOS_simple = InterpolationPoints(x, y, u, axial, deg=deg,
                                                  n_int=n_int)
    xy, w_int, w_LOS_simple = InterpolationPoints(x, y, w, axial, deg=deg,
                                                  n_int=n_int)
    xy, p_int, p_LOS_simple = InterpolationPoints(x, y, p, axial, deg=deg,
                                                  n_int=n_int)
    xy, T_int, T_LOS_simple = InterpolationPoints(x, y, T, axial, deg=deg,
                                                  n_int=n_int)
    xy, xo2_int, xo2_LOS_simple = InterpolationPoints(x, y, xo2, axial,
                                                      deg=deg, n_int=n_int)
    
    ## simulate spectra based on LOS
    wvnumspace = np.arange(v_start, v_stop, dv)
    nsteps = len(wvnumspace)-1
    hapi.db_begin('')
    absorbance_p = []
    absorbance_y = []

    
    # characterize dL chunk using properties at beginning of chunk
    T = T_LOS_simple
    P = p_LOS_simple*9.86923e-6 # convert to atm
    O2mol = xo2_LOS_simple
    u = u_LOS_simple
    w = w_LOS_simple
    
    # take into account w contribution
    theta = np.deg2rad(deg)
    L = 6.81736 / np.cos(theta)
    # p upstream
    scale_p = 1+(u*np.sin(theta)+w*np.cos(theta))/SPEED_OF_LIGHT
    wvw_num_p = wvnumspace*scale_p
    windowend_p = np.amax(wvw_num_p)
    windowstart_p = np.amin(wvw_num_p)
    dv_p = (wvw_num_p[-1]-wvw_num_p[0])/nsteps

    MIDS = [(7, 1, O2mol*hapi.abundance(7, 1))]

    [nu_O2_p, coefs_O2_p] = hapi.absorptionCoefficient_SDVoigt(MIDS,
       (mol_to_sim), OmegaStep=dv_p,
       OmegaRange=[windowstart_p, windowend_p],
       HITRAN_units=False, Environment={'p': P, 'T': T},
       Diluent={'self': O2mol - 0.21 / 0.79 * (1 - O2mol),
                'air': (1 - O2mol) / 0.79},
       IntensityThreshold=0)
    absorbance_p = L * coefs_O2_p
    
    # y upstream
    scale_y = 1+(u*np.sin(-theta)+w*np.cos(-theta))/SPEED_OF_LIGHT
    wvw_num_y = wvnumspace*scale_y
    windowend_y = np.amax(wvw_num_y)
    windowstart_y = np.amin(wvw_num_y)
    dv_y = (wvw_num_y[-1]-wvw_num_y[0])/nsteps

    [nu_O2_y, coefs_O2_y] = hapi.absorptionCoefficient_SDVoigt(MIDS,
       (mol_to_sim), OmegaStep=dv_y,
       OmegaRange=[windowstart_y, windowend_y],
       HITRAN_units=False, Environment={'p': P, 'T': T},
       Diluent={'self': O2mol - 0.21 / 0.79 * (1 - O2mol),
                'air': (1 - O2mol) / 0.79},
       IntensityThreshold=0)
    absorbance_y = L * coefs_O2_y
    
    transmission_p = np.exp(-1*absorbance_p)
    transmission_data_p = np.column_stack((wvnumspace,transmission_p))
    
    transmission_y = np.exp(-1*absorbance_y)
    transmission_data_y = np.column_stack((wvnumspace,transmission_y))
    
    plt.figure()
    plt.plot(wvnumspace,absorbance_p,'b',label='Downstream')
    plt.plot(wvnumspace,absorbance_y,'r',label='Upstream')
    plt.legend()
    plt.show()
    
    return transmission_data_p, transmission_data_y
    