# -*- coding: utf-8 -*-
"""
GUI to phase correct IGS.
Allows user to cut out IGS where timing jumps occur and define optimal phase
correction reference limits.


In order to run this GUI write code like this:
from PhaseCorrectGUI import *

dir_raw = 'E:\\20200211\\bad_phase_correct_forSean\\purple_crossed\\scan1'
fn_raw = '20042_testAJ_purple'
dir_log = 'E:\\20200211\\yellow_crossed\\scan1'
fn_log = '20200211182822.log'
dir_push = 'E:\\20200211\\pc_new\\purple_crossed'
fn_push = '20042_testAJ_purple_trans.txt'
dir_pull = 'E:\\20200211\\bad_phase_correct_forSean\\purple_crossed'+\
    '\\scan1\\'
    
app = QtGui.QApplication(sys.argv)
main = Window(app, dir_raw, fn_raw, dir_push, dir_log, fn_log)
main.show()
app.exec_()
    

You must define the relevant directories and filenames for GUI to work.

See PhaseCorrectMany as an example.

Created on Thu Mar 12 14:50:53 2020

@author: David
"""

# external modules
import os
import numpy as np
from PyQt5.QtCore import Qt
import PyQt5.QtWidgets as QtGui
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

# lab modules
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy as pld

class MplCanvas(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)

class Window(QtGui.QDialog):
    def __init__(self, app, dir_raw, fn_raw, dir_push, fn_push, dir_log,
                 fn_log, jump_buffer=2, parent=None):
        '''
        Initialize GUI with widgets.

        Parameters
        ----------
        app : TYPE, optional
            DESCRIPTION: define as app = QtGui.QApplication(sys.argv)
            
        dir_raw: STRING
            DESCRIPTION: directory of raw IG file
            
        fn_raw: STRING
            DESCRIPTION: filename of raw IG file
            
        dir_push: STRING
            DESCRIPTION: directory to save phase-correcteddata
            
        fn_raw: STRING
            DESCRIPTION: filename to save phase-corrected data
            
        dir_log: STRING
            DESCRIPTION: directory where log data is. This code assumes that
                        VC707 was used
                        
        fn_log: STRING
            DESCRIPTION: filename where log data is. This code assumes that
                        VC707 was used
                        
        jump_buffer: INTEGER
            DESCRIPTION: How many IGS to cut around where timing jump occurs

        Returns
        -------
        None.

        '''
        super(Window, self).__init__(parent)
        
        # initial parameters
        self.pc_lim_low = 0.32
        self.pc_lim_high = 0.4
        self.saved = False
        self.jump_buffer = jump_buffer
        self.goto_setPCLim = True
        self.app = app
        self.dir_push = dir_push
        self.fn_push = fn_push
        
        # pull in data from IG:
        self.fn_raw = fn_raw
        path_raw = os.path.join(dir_raw,self.fn_raw)
        self.IG = pld.open_daq_files(path_raw)
        self.data = self.IG.data_raw
                
        # pull in frequency axis
        if not(fn_log==''):
            self.axis_add = True
            path_log = os.path.join(dir_log,fn_log)
            log = pld.DAQFilesVC707(path_log)
            self.x_wvn = pld.mobile_axis(log,-25e6)
        else:
            self.axis_add = False
            print('NO FREQUENCY AXIS PROVIDED.')
        
        ## Define all widgets
        # a figure instance to plot on
        self.figure = Figure()
        
        # label of filename being looked at 
        self.label_title = QtGui.QLabel(self.fn_raw)
        
        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = MplCanvas(self)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        
        ## findJump specific widgets
        # add a slider that determines which IG you are looking at 
        self.slider_IG = QtGui.QSlider(Qt.Horizontal)
        self.slider_IG.setMinimum(0)
        self.slider_IG.setMaximum(len(self.data)-1)
        self.slider_IG.setValue(round(len(self.data)/2))
        self.slider_IG.valueChanged.connect(self.valueChange_IG)
        self.num_IG = self.slider_IG.value()
        
        # Give instructions to find Jump
        self.label_findjump = QtGui.QLabel('Scroll through IGs and determine'+
                                           ' if there are any timing jumps.')
        
        # display IG num
        self.label_IG = QtGui.QLabel('IG # '+str(self.num_IG))
        
        # Button to indicate a jump found
        self.button_jump = QtGui.QPushButton('Found Jump')
        self.button_jump.clicked.connect(self.jumpFound)
        
        # Button to indicate there are no jumps 
        self.button_nojump = QtGui.QPushButton('No Jump Found')
        self.button_nojump.clicked.connect(self.jumpNotFound)
        
        ## setPCLim specific widgets
        # label to give instructions
        self.label_PCLimInstr = QtGui.QLabel('Make sure noise spikes in the '+
                                             'spectra dont fall in the phase '+
                                             'correction reference region')
        
        # Slider to scroll spectrum on pc lim GUI
        self.slider_spec = QtGui.QSlider(Qt.Horizontal)
        self.label_spec = QtGui.QLabel('')
        
        # Slider to set pc lim low
        self.slider_pclim1 = QtGui.QSlider(Qt.Horizontal)
        self.label_pclim1 = QtGui.QLabel('')
        
        # Slider to set pc lim high
        self.slider_pclim2 = QtGui.QSlider(Qt.Horizontal)
        self.label_pclim2 = QtGui.QLabel('')
        
        # button to change redo PC Lim
        self.button_PCAll = QtGui.QPushButton('Set PC Ref. Limits and '+
                                              'Phase Correct All')
        self.button_PCAll.clicked.connect(self.defPCLim)
        
        ## phaseCorrectAll
        # label that ask Question 
        self.label_PC = QtGui.QLabel('Is phase correction satisfatory?'+
                                     ' (Dashes indicate PC ref. region)')
        
        # Button to indicate that PC is good
        self.button_PCGood = QtGui.QPushButton('Yes')
        self.button_PCGood.clicked.connect(self.saveData)
        
        # button to iniicate that PC is bad
        self.button_PCBad = QtGui.QPushButton('No')
        self.button_PCBad.clicked.connect(self.cutJump)
        
        ## saveData specific widgets
        # label indicating save successful and the save path
        self.label_save = QtGui.QLabel('')
        
        # button to close program after saving
        self.button_saveconfirm = QtGui.QPushButton('OK')
        self.button_saveconfirm.clicked.connect(self.saveConfirm)
        
        # Set Initial window to find jump
        self.findJump()
             
    def closeEvent(self, parent=None):
        '''
        Make sure code closes when GUI closes

        Parameters
        ----------
        parent : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        None.

        '''
        super(Window, self).closeEvent(parent)
        if not(self.saved):
            print(self.fn_push+' NOT SAVED.')
        print("Window closed")
        self.app.quit()
        
    def cutJump(self):
        '''
        Cuts out the IGs around the jump

        Returns
        -------
        None.

        '''
        # get jump value
        self.index_jump = self.slider_IG.value()
        
        if self.jump_found:
            print('DELETING JUMP IGS AND PHASE CORRECTING BY 1')
        else:
            print('PHASE CORRECTING BY 1')
        
        # Initial phase correctino in groups of 1
        self.s = pld.Spectra(self.IG, num_pcs=1, pc_lim_low=self.pc_lim_low,
                             pc_lim_high=self.pc_lim_high)
        
        # cut out jump IGs if indicated
        if self.jump_found:
            self.cut_low = self.index_jump-self.jump_buffer
            self.cut_high = self.index_jump+self.jump_buffer+1
            self.s.data = np.delete(self.s.data,
                                    np.s_[self.cut_low:self.cut_high],0)
        
        # change GUI to set PC Lim
        if self.goto_setPCLim:
            self.goto_setPCLim = False
            self.setPCLim()
        
    def defPCLim(self):
        '''
        Redefine PC limits

        Returns
        -------
        None.

        '''
        
        if self.num_pclim1 < self.num_pclim2:
            self.pc_lim_low = self.num_pclim1/(2*len(self.s.data[0]))
            self.pc_lim_high = self.num_pclim2/(2*len(self.s.data[0]))
        elif self.num_pclim1 > self.num_pclim2:
            self.pc_lim_low = self.num_pclim2/(2*len(self.s.data[0]))
            self.pc_lim_hight = self.num_pclim1/(2*len(self.s.data[0]))
        elif self.num_pclim1 == self.num_pclim2:
            self.pc_lim_low = self.num_pclim1/(2*len(self.s.data[0]))
            self.pc_lim_high = self.num_pclim2/(2*len(self.s.data[0]))
            
        self.phaseCorrectAll()
        
    def findJump(self):
        '''
        Change GUI to find jump in IGs if there is any

        Returns
        -------
        None.

        '''
        # set the layout
        self.layout = QtGui.QVBoxLayout()
        self.layout.addWidget(self.label_title)
        self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.canvas)
        self.layout.addWidget(self.label_findjump)
        self.layout.addWidget(self.label_IG)
        self.layout.addWidget(self.slider_IG)
        self.layout.addWidget(self.button_jump)
        self.layout.addWidget(self.button_nojump)
        self.setLayout(self.layout)
        
        # # plot the first IG
        self.plotIG()
        
    def jumpFound(self):
        '''
        Indicate that jump has been found

        Returns
        -------
        None.

        '''
        self.jump_found = True
        self.cutJump()
        
    def jumpNotFound(self):
        '''
        Indicate that jump has not been found

        Returns
        -------
        None.

        '''
        self.jump_found = False
        self.cutJump()
        
    def phaseCorrectAll(self):
        '''
        Phase correction method and changes GUI to display result

        Returns
        -------
        None.

        '''
        # get rid of irrelevant widgets from previous
        self.slider_IG.setParent(None)
        self.label_IG.setParent(None)
        self.button_jump.setParent(None)
        self.button_nojump.setParent(None)
        self.slider_spec.setParent(None)
        self.label_PCLimInstr.setParent(None)
        self.label_spec.setParent(None)
        self.slider_pclim1.setParent(None)
        self.label_pclim1.setParent(None)
        self.slider_pclim2.setParent(None)
        self.label_pclim2.setParent(None)
        self.button_PCAll.setParent(None)
        
        # redo IG processing with new pc_lim and phase average further
        self.cutJump()
        self.goto_setPCLim = True # reset this variable in case we need to pc
                                    # again
        self.s.phase_average_further(match_peaks=True)
        
        # change GUI to ask whether PC is good or not
        self.layout.addWidget(self.label_PC)
        self.layout.addWidget(self.button_PCGood)
        self.layout.addWidget(self.button_PCBad)
        self.setLayout(self.layout)
        
        self.plotPC()
        # self.close()

    def plotIG(self, setLim=False):
        '''
        Plot IGs in GUI

        Returns
        -------
        None.

        '''

        # discards the old graph
        self.canvas.axes.cla()
        
        # plot data
        self.canvas.axes.plot(self.data[self.num_IG])
        if setLim:
            self.canvas.axes.set_ylim(self.ylim)
            self.canvas.axes.set_xlim(self.xlim)

        # refresh canvas
        self.canvas.draw()
        
    def plotPC(self):
        '''
        plot phase-corrected spectra

        Returns
        -------
        None.

        '''
        
        # create an axis
        ax = self.figure.add_subplot(111)

        # discards the old graph
        ax.clear()

        # plot data
        if self.axis_add:
            ax.plot(self.x_wvn[30000:],self.s.data[30000:])
            
            # plot lines where pc limits were
            self.line_pclow = self.x_wvn[self.num_pclim1]
            self.line_pchigh = self.x_wvn[self.num_pclim2]
            ax.vlines(self.line_pclow, -5, max(self.s.data[30000:])+5, colors='b',
                      linestyles='dashed')
            ax.vlines(self.line_pchigh, -5, max(self.s.data[30000:])+5, colors='r',
                      linestyles='dashed')
            ax.set_xlabel('Wavenumber (cm-1)')
        else:
            ax.plot(self.fft_points[30000:],self.s.data[30000:])
            
            # plot lines where pc limits were
            ax.vlines(self.num_pclim1, -5, max(self.s.data[30000:])+5, colors='b',
                      linestyles='dashed')
            ax.vlines(self.num_pclim2, -5, max(self.s.data[30000:])+5, colors='r',
                      linestyles='dashed')
            ax.set_xlabel('Wavenumber (cm-1)')
            
        # restrict y limits on graph
        ax.set_ylim(-5,max(self.s.data[30000:])+5)            
        
        # refresh canvas
        self.canvas.draw()
        
    def plotSpec(self):
        '''
        plot phase-corrected spectra

        Returns
        -------
        None.

        '''
        
        # create an axis
        ax = self.figure.add_subplot(111)

        # discards the old graph
        ax.clear()

        # plot data
        self.fft_points = list(range(len(self.s.data[0])))
        ax.plot(self.fft_points[30000:],
                abs(self.s.data[self.num_spec][30000:]))
        
        # plot lines where pc limits were
        ax.vlines(self.num_pclim1, -5, self.max_vline,
                  colors='r', linestyles='dashed')
        ax.vlines(self.num_pclim2, -5, self.max_vline,
                  colors='b', linestyles='dashed')
        
        # restrict plot limits
        ax.set_ylim(-0.5,max(abs(self.s.data[self.num_spec][30000:]))+0.5)
        ax.set_xlim(30000,len(self.s.data[0]))
        
        # refresh canvas
        self.canvas.draw()
        
    def saveConfirm(self):
        '''
        close window upon confirming save

        Returns
        -------
        None.

        '''
        self.close()
        
    def saveData(self):
        '''
        save phase corrected data with wavenumber axis to dir_log

        Returns
        -------
        None.

        '''
        
        # remove irrelevant buttons from phaseCorrectAll
        self.label_PC.setParent(None)
        self.button_PCGood.setParent(None)
        self.button_PCBad.setParent(None)
        
        # combine wavenumber and trans data
        if self.axis_add:
            trans_data = np.column_stack((self.x_wvn, self.s.data))
        else:
            trans_data = self.s.data
        
        # save data and display save message
        try:
            path_save = os.path.join(self.dir_push, self.fn_push)
            np.savetxt(path_save,trans_data)
            self.saved = True
            print(self.fn_push+' SAVED')
        except:
            print('There was an error in trying to save data')
        # display save message
        self.label_save.setText('Data has ben saved to '+path_save)
        self.layout.addWidget(self.label_save)
        self.layout.addWidget(self.button_saveconfirm)
        self.setLayout(self.layout)
        
    def setPCLim(self):
        '''
        let user define where to set pc limits

        Returns
        -------
        None.

        '''
        # clear unrelated widgets
        self.label_findjump.setParent(None)
        self.label_IG.setParent(None)
        self.slider_IG.setParent(None)
        self.button_jump.setParent(None)
        self.button_nojump.setParent(None)
        self.label_PC.setParent(None)
        self.button_PCGood.setParent(None)
        self.button_PCBad.setParent(None)
        
        # set initial values of pc limits on graph
        self.num_pclim1 = round(len(self.s.data[0])*self.pc_lim_low*2)
        self.num_pclim2= round(len(self.s.data[0])*self.pc_lim_high*2)
        
        # slider to look at spectra
        self.slider_spec.setMinimum(0)
        self.slider_spec.setMaximum(len(self.s.data)-1)
        self.slider_spec.setValue(round(len(self.s.data)/2))
        self.slider_spec.valueChanged.connect(self.valueChange_spec)
        self.num_spec = self.slider_spec.value()
        # display IG num
        self.label_spec.setText('Spectrum # '+str(self.num_spec))
        
        # slider to set PC lim 1
        self.slider_pclim1.setMinimum(0)
        self.slider_pclim1.setMaximum(len(self.s.data[0])-1)
        self.slider_pclim1.setValue(self.num_pclim1)
        self.slider_pclim1.valueChanged.connect(self.valueChange_pclim1)
        # display PC lim 1
        self.label_pclim1.setText('PC Lim 1: '+str(self.num_pclim1))
        
        # slider to set PC lim 2
        self.slider_pclim2.setMinimum(0)
        self.slider_pclim2.setMaximum(len(self.s.data[0])-1)
        self.slider_pclim2.setValue(self.num_pclim2)
        self.slider_pclim2.valueChanged.connect(self.valueChange_pclim2)
        # display PC lim 2
        self.label_pclim2.setText('PC Lim 2: '+str(self.num_pclim2))
        
        # set the layout
        self.layout.addWidget(self.label_PCLimInstr)
        self.layout.addWidget(self.label_spec)
        self.layout.addWidget(self.slider_spec)
        self.layout.addWidget(self.label_pclim1)
        self.layout.addWidget(self.slider_pclim1)
        self.layout.addWidget(self.label_pclim2)
        self.layout.addWidget(self.slider_pclim2)
        self.layout.addWidget(self.button_PCAll)
        self.setLayout(self.layout)
        
        # find max in data for vline
        max_trans = []
        for array in self.s.data:
            max_trans.append(max(array[30000:]))
        self.max_vline = max(max_trans)*1.02
        
        # plot spec
        self.plotSpec()
        
    def valueChange_IG(self):
        '''
        Change where to cut out IGs based on slider value

        Returns
        -------
        None.

        '''
        self.num_IG = self.slider_IG.value()
        self.label_IG.setText('IG  # '+str(self.num_IG))
        self.xlim = self.canvas.axes.get_xlim()
        self.ylim = self.canvas.axes.get_ylim()
        self.plotIG(setLim=True)
        
    def valueChange_spec(self):
        '''
        Change which spectrum to display

        Returns
        -------
        None.

        '''
        self.num_spec = self.slider_spec.value()
        self.label_spec.setText('Spectrum # '+str(self.num_spec))
        self.plotSpec()
        
    def valueChange_pclim1(self):
        '''
        Change value of pclim1

        Returns
        -------
        None.

        '''
        self.num_pclim1 = self.slider_pclim1.value()
        self.label_pclim1.setText('PC Lim 2: '+str(self.num_pclim1))
        self.plotSpec()
        
    def valueChange_pclim2(self):
        '''
        Change value of pclim2

        Returns
        -------
        None.

        '''
        self.num_pclim2 = self.slider_pclim2.value()
        self.label_pclim2.setText('PC Lim 2: '+str(self.num_pclim2))
        self.plotSpec()