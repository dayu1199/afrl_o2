"""
FetchHapi

Created by on 2/2/23 by david

Description:

"""
from packfind import find_package
find_package('pldspectrapy')
find_package('pldspectrapy')
import pldspectrapy.O2pldhapi as hapi

import linelist_conversions as lc

hapi.db_begin('')

# hapi.fetch('O2',7,1,13000,13200)

hapi.fetch_by_ids('O2', [36, 37, 38], 13000, 13200, ParameterGroups=['SDVoigt'])

# df_O2Website = lc.par_to_df('O2Website.data', nu_min=13000, nu_max=13200)