"""
PlotCWLockError

Created by on 10/12/22 by david

Description:

"""
import numpy as np
import matplotlib.pyplot as plt

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

fn_error = 'errorlognocontrol022223.txt'
data_error = np.loadtxt(fn_error)
time = data_error[:, 0] - data_error[0, 0]
error = data_error[:, 1]
fig, axs = plt.subplots(3)
avg_error = np.mean(abs(error[160:]))
fig.suptitle('CW Lock Error, Avg. Err. = ' + str(round(avg_error, 2)))

# Plot time resolution
axs[0].axhline(0, linestyle='--')
axs[0].plot(time, error)
axs[0].set_ylabel('Error (Hz)')
axs[0].set_xlabel('Time (s)')
axs[0].set_ylim([-6, 6])
axs[0].set_xlim([0, 500])

# Plot Fourier Transform
dt = time[1] - time[0]
N = len(time)
df = 1/(dt*(N-1))

error_f = np.fft.rfft(error)
freq = np.arange(len(error_f))*df

axs[1].plot(freq, abs(error_f))
axs[1].set_xlabel('Frequency (Hz)')
axs[1].set_ylim([-50, 1200])