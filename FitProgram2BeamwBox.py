# -*- coding: utf-8 -*-
"""
PARAMETERS:
    filename_y:
        Filename of downstream spectra
    filename_p:
        filename of upstream spectra
    dir_y:
        directory of downstream spectra file
    dir_p:
        directory of upstream spectra file
    save_path:
        Where to save data, fit, weight, and report files
    file_tag:
        A special tag to end all saved files with (e.g. height30mm)
    weight_window:
        Time-domain weighting by proportion (e.g. [0.01,0.5])
    weight_flat:
        True for flat weighting, False for exponential weighting
    etalonWindows_y:
        where to cut out windows on downstream spectra
        (e.g. [[500,600],[10000,11000]])
    etalonWindows_p:
        see etalonWindows_p
    angle:
        angle of beams (in deg), single input
    band_fit:
        where to fit spectra (e.g. [6800,7100]) (in cm-1)
    Molecules_to_Fit:
        molecules to fit for (e.g. ['H2O_PaulLF'])
    isotopes:
        isotopes of molecules to fit (e.g. [[1]])
    Pressure_Fit:
        Toggle for fitting pressure, TRUE for fit, FALSE for fix
    Temp_Fit:
        Toggle for fitting temperature
    Velocity_Fit:
        Toggle for fitting velocity
    Shift_Fit:
        Toggle for fitting shift
    Conc_Fit:
        Toggle for fitting concentration, for now not able to toggle for
        individual molecules
    Broadening_Fit:
        Toggle for fitting broadening
    Pressure_Guess:
        Initial Pressure value
    Temp_Guess:
        Initial Temperature value
    Velocity_Guess:
        Initial Velocity value
    Shift_Guess:
        Initial shift value
    Conc_Guess:
        Intiial conc value (for now just takes one for all molecules
    Broadening_Guess:
        Initial broadening guess (just go with 1 if you don't want to mess
                                  with it')
    background_remove:
        Toggles background remove, for now not functional, just input False
    press_back:
        pressure of background, background remove is not functional right now
        so just use 0
    temp_back:
        temp of background, background remove is not functional right now
        so just use 0
    conc_back_y:
        H2O conc of background in downstream, background remove is not 
        functional right now so just use 0
    conc_back_p:
        H2O conc of background in upstream, background remove is not
        functional right now so just use 0
    save_files:
        Toggle to save files
    plot_results:
        Toggle to plot spectral fit
    print_fit_report:
        Toggle to print fit results in command
        
RETURNS: Pretty self-explanatory
    Velocity
    Velocity_err
    Temperature
    Temperature_err
    Pressure
    Pressure_err
    Shift
    Shift_err
    Conc
    Conc_err
    Broadening
    Broadening_err

"""

from FitMFID2BeamwBox import Fitting_Spectra
import matplotlib.pyplot as plt
import numpy as np
from packfind import find_package
find_package('pldspectrapy')
# import pldspectrapy.pldhapi as hapi
import hapi
import td_support as td
import time
import os

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

###############################################################################
##########  Output options for Fitting ########################################

def noise_weight(x_data, eta_center=13096, beta=9):
    center = 13098
    sigma = 98
    subbotin = np.exp(-(abs(x_data - center) / sigma) ** beta)
    # normalize to 0.5
    subbotin *= 0.95 / max(subbotin)
    subbotin = 1 - subbotin
    weight = subbotin.copy()
    # calculate an etalon
    etalon = 0.18 * np.cos((x_data - eta_center) * 2 * np.pi / 11) + 0.8
    weight *= etalon
    # add slight parabola
    parabola = ((x_data - 13100) / 60) ** 2 + 1
    parabola /= min(parabola)
    weight *= parabola
    return weight

def FitProgram(fn_y, fn_p, save_path, file_tag,
               weight_window_y, weight_window_p, weight_flat, etalonWindows_y,
               etalonWindows_p, angle_y, angle_p, band_fit_y, band_fit_p,
               Molecules_to_Fit, isotopes,
               Pressure_Fit, Temp_Fit, Velocity_Fit, Shift_Fit, Conc_Fit,
               Broadening_Fit,
               Pressure_Guess, Temp_Guess, Velocity_Guess, Shift_Guess, 
               Conc_Guess, Broadening_Guess,
               conc_h2o=0,
               conc_box=0, temp_box=0, pres_box=0,
               pathlength=6.81736,
               background_remove=False, pathlength_b=10,
               pl_box=35,
               press_back=0, temp_back=0, conc_back_y=0, conc_back_p=0,
               save_files=True, plot_results=True, print_fit_report=True,
               transUnits=False, noise_add=False, noise_level=0.001,
               noise_flat=False, eta_center=13096, beta=9, db_dir=''):
    ########## Data Specific Parameters #######################################
    print(fn_y)
    print(fn_p)
    
    data_y = np.loadtxt(fn_y)
    data_p = np.loadtxt(fn_p)
    
    theta_y = angle_y/360*2*np.pi #yellow has negative angle
    theta_p = angle_p/360*2*np.pi #purple has positive angle
    
    Pathlength_y = pathlength/np.cos(theta_y) #cm
    Pathlength_p = pathlength/np.cos(theta_p) #cm
    wvnUnits = True # True if frequency axis is in wavenumber, False for THz
                        # absorbance
    conc = [Conc_Guess]*len(Molecules_to_Fit) # Initializes concentration guess
                                                # for each molecule to fit

    Conc_Fit = [Conc_Fit]*len(Molecules_to_Fit) # True for each molecule to fit
    ########################################################################### 
    
    if data_y[0,0]>data_y[-1,0]: # Flips data if it is inverted
        data_y = np.flipud(data_y) 
        print("Flipping FFT")
    if data_p[0,0]>data_p[-1,0]: # Flips data if it is inverted
        data_p = np.flipud(data_p) 
        print("Flipping FFT")
        
    print(len(data_y))
    print(len(data_p))
    if len(data_y) != len(data_p):
        print("yellow and purple are not the same size")

    start_y, stop_y = td.bandwidth_select_td(data_y[:,0], band_fit_y)
    start_p, stop_p = td.bandwidth_select_td(data_p[:,0], band_fit_p)
    if start_y < stop_y:
        if wvnUnits:
            x_data_y = data_y[start_y:stop_y, 0]
            x_data_p = data_p[start_p:stop_p, 0]
        else:
            x_data_y = data_y[start_y:stop_y, 0]/29979245800
            x_data_p = data_p[start_p:stop_p, 0]/29979245800
    else:
        if wvnUnits:
            x_data_y = data_y[start_y:stop_y:-1, 0]
            x_data_p = data_p[start_p:stop_p:-1, 0]
        else:
            x_data_y = data_y[start_y:stop_y:-1, 0]/29979245800
            x_data_p = data_p[start_p:stop_p:-1, 0]/29979245800

    # Imports spectra into y_data, converts to absorbance if in trans units
    y_data_y = np.zeros((len(x_data_y),1))
    y_data_p = np.zeros((len(x_data_p),1))
    if transUnits:
        y_data_y =-np.log(data_y[start_y:stop_y,1])
        y_data_p =-np.log(data_p[start_p:stop_p,1])
    else:
        y_data_y =data_y[start_y:stop_y,1]
        y_data_p =data_p[start_p:stop_p,1]

    if noise_add:
        noise_y = np.random.normal(0, noise_level, len(y_data_y))
        if not noise_flat:
            weight_y = noise_weight(x_data_y, eta_center=eta_center, beta=beta)
            noise_y *= weight_y
        y_data_y += noise_y

        noise_p = np.random.normal(0, noise_level, len(y_data_p))
        if not noise_flat:
            weight_p = noise_weight(x_data_p, eta_center=eta_center, beta=beta)
            noise_p *= weight_p
        y_data_p += noise_p

        # Convert to time domain
    ff_y = (np.fft.irfft(y_data_y))
    ff_p = (np.fft.irfft(y_data_p))
    y_fit_y = np.zeros((ff_y.size,2))
    y_fit_p = np.zeros((ff_p.size,2))
    
    # Builds logic vector to imform fit of parameters to float
    Fit_vector = [Temp_Fit, Pressure_Fit, Shift_Fit, Broadening_Fit,
                  Velocity_Fit]
    Fit_vector.extend(Conc_Fit)
    
    # Builds vector of approximate parameters for starting fits
    Guess_vector = [Temp_Guess, Pressure_Guess, Shift_Guess, Broadening_Guess,
                    Velocity_Guess]
    Guess_vector.extend(conc)
    
    # Loads Molecule Data from HITRAN
    # hapi.db_begin(db_dir)  # Location to store imported HITRAN data
    
    HITRAN_Molecules = ['H2O','CO2','O3','N2O','CO','CH4','O2','NO','SO2',
                        'NO2','NH3','HNO3','OH','HF','HCl','HBr','HI','ClO',
                        'OCS','H2CO','HOCl','N2','HCN','CH3Cl','H2O2','C2H2',
                        'C2H6','PH3','COF2','SF6','H2S','HCOOH','HO2','O',
                        'ClONO2','NO+','HOBr','C2H4','CH3OH','CH3Br','CH3CN',
                        'CF4','C4H2','HC3N','H2','CS','SO3','ArH2O_Labfit']
    molecule_numbers =[0]*(len(Molecules_to_Fit));
    
    ii=0
    for item in Molecules_to_Fit:
        if item not in HITRAN_Molecules:
            print("Listed Molecule is not included in HITRAN", item)
        elif item == 'O2' or item == 'O2_Vgt':
            print('Using preexising O2 HITRAN data in project.')
        else:  
            molecule_numbers[ii] = (HITRAN_Molecules.index(item))+1
            isotope_ids = []
            for jj in range(len(isotopes[ii])):
                isotope_ids.append(hapi.ISO[molecule_numbers[ii],
                                            isotopes[ii][jj]][0]) 
            hapi.fetch_by_ids(item, isotope_ids,min(x_data_y)-1,
                              max(x_data_y)+1)
        ii+=1
        
    if plot_results:
        plt.figure()
        plt.plot(x_data_y,y_data_y)
        # plt.title('Transmission')
        plt.xlabel('Wavenumber (cm-1')
            
    tic= time.time()  # Times each fit iteration


    # This sends data to function which compiles it for the fit
    [result,time_wave,concs,concs_err,weight_wave, time_wave] = \
        Fitting_Spectra(Guess_vector, Fit_vector, x_data_y, y_data_y,
                        x_data_p, y_data_p, Molecules_to_Fit,
                        molecule_numbers, isotopes, Pathlength_y,
                        Pathlength_p, theta_y, theta_p, weight_window_y,
                        weight_window_p, weight_flat,
                        background_remove, pathlength_b,
                        press_back, temp_back, conc_back_y, conc_back_p,
                        conc_box, temp_box, pres_box, pl_box,
                        conc_h2o,
                        etalonWindows_y = etalonWindows_y,
                        etalonWindows_p = etalonWindows_p)

    Velocity = result.best_values['velocity']
    print(Velocity)
    Temperature = result.best_values['Temp']
    Pressure = result.best_values['press']
    Shift = result.best_values['shift']
    Broadening = result.best_values['Broadening']
    Temperature_err = result.params['Temp'].stderr
    Pressure_err = result.params['press'].stderr
    Shift_err = result.params['shift'].stderr
    Velocity_err = result.params['velocity'].stderr
    Broadening_err = result.params['Broadening'].stderr

    Conc = []
    Conc_err = []
    for jj in range(len(Molecules_to_Fit)):
        Conc.append(concs[jj])
        Conc_err.append(concs_err[jj])

    # Stores best fits and corresponding data in y_fit matrix
    # Split back into separate fits for both data sets
        # Stores best fits and corresponding data in y_fit matrix
    cut = len(ff_y)
    time_fit_y = result.best_fit[:cut]
    time_fit_p = result.best_fit[cut:]
    time_data_y = result.data[:cut]
    time_data_p = result.data[cut:]
    time_res_y = time_data_y - time_fit_y
    time_res_p = time_data_p - time_fit_p
    weight_y = weight_wave[:cut]
    weight_p = weight_wave[cut:]

    abs_fit_y = np.real(np.fft.rfft(time_fit_y))
    abs_fit_p = np.real(np.fft.rfft(time_fit_p))
    abs_data_y = np.real(np.fft.rfft(time_data_y))
    abs_data_p = np.real(np.fft.rfft(time_data_p))
    abs_res_y = abs_data_y - abs_fit_y
    abs_res_p = abs_data_p - abs_fit_p
    abs_datanobl_y = np.real(np.fft.rfft(time_data_y-
                                         (1-weight_y)*time_res_y))
    abs_datanobl_p = np.real(np.fft.rfft(time_data_p-
                                         (1-weight_p)*time_res_p))
    abs_resnobl_y = abs_datanobl_y-abs_fit_y
    abs_resnobl_p = abs_datanobl_p-abs_fit_p


    # taking a look at what is actually fitted
    abs_fitweight_y = np.fft.rfft(time_fit_y*weight_y)
    abs_fitweight_p = np.fft.rfft(time_fit_p*weight_p)
    abs_dataweight_y = np.fft.rfft(time_data_y*weight_y)
    abs_dataweight_p = np.fft.rfft(time_data_p*weight_p)
    abs_resweight_y = abs_dataweight_y - abs_fitweight_y
    abs_resweight_p = abs_dataweight_p - abs_fitweight_p

    #%% create background models
    if background_remove:
        SPEEDOFLIGHT = 2.99792458e8
        xx_y = x_data_y - Shift
        xx_p = x_data_p - Shift

        ## outside optics
        # yellow
        step_xx_y = (max(xx_y)-min(xx_y))/(len(xx_y)-1)
        MIDS_b_y = [(7,1,conc_back_y*hapi.abundance(7,1))]
        [nu_b_y, coefs_b_y] = hapi.absorptionCoefficient_SDVoigt(MIDS_b_y,
                'O2', OmegaStep=step_xx_y,
                OmegaRange=[min(xx_y), max(xx_y)],HITRAN_units=False,
                Environment={'p':press_back,'T':temp_back},
                Diluent={'self':conc_back_y,
                         'air':(1-conc_back_y)},
                IntensityThreshold =0)
        abs_b_y = pathlength_b*coefs_b_y
        time_b_y = np.fft.irfft(abs_b_y)
        abs_bweight_y = np.real(np.fft.rfft(time_b_y*weight_y))

        # purple
        step_xx_p = (max(xx_p)-min(xx_p))/(len(xx_p)-1)
        MIDS_b_p = [(7,1,conc_back_p*hapi.abundance(7,1))]
        [nu_b_p, coefs_b_p] = hapi.absorptionCoefficient_SDVoigt(MIDS_b_p,
                'O2', OmegaStep=step_xx_p,
                OmegaRange=[min(xx_p), max(xx_p)],HITRAN_units=False,
                Environment={'p':press_back,'T':temp_back},
                Diluent={'self':conc_back_p,
                         'air':(1-conc_back_p)},
                IntensityThreshold =0)
        abs_b_p = pathlength_b*coefs_b_p
        time_b_p = np.fft.irfft(abs_b_p)
        abs_bweight_p = np.real(np.fft.rfft(time_b_p*weight_p))

        ## box
        # yellow
        MIDS_box = [(7,1,conc_box*hapi.abundance(7,1))]
        [nu_box_y, coefs_box_y] = hapi.absorptionCoefficient_SDVoigt(MIDS_box,
                'O2', OmegaStep=step_xx_y,
                OmegaRange=[min(xx_y), max(xx_y)],HITRAN_units=False,
                Environment={'p':pres_box,'T':temp_box},
                Diluent={'self':conc_box,
                         'air':(1-conc_box)},
                IntensityThreshold =0)
        abs_box_y = pl_box*coefs_box_y
        time_box_y = np.fft.irfft(abs_box_y)
        abs_boxweight_y = np.real(np.fft.rfft(time_box_y*weight_y))

        # purple
        [nu_box_p, coefs_box_p] = hapi.absorptionCoefficient_SDVoigt(MIDS_box,
                'O2', OmegaStep=step_xx_p,
                OmegaRange=[min(xx_p), max(xx_p)],HITRAN_units=False,
                Environment={'p':pres_box,'T':temp_box},
                Diluent={'self':conc_box,
                         'air':(1-conc_box)},
                IntensityThreshold =0)
        abs_box_p = pl_box*coefs_box_p
        time_box_p = np.fft.irfft(abs_box_p)
        abs_boxweight_p = np.real(np.fft.rfft(time_box_p*weight_p))
    else:
        abs_box_y = np.zeros((len(x_data_y), 1))
        abs_b_y = np.zeros((len(x_data_y), 1))
        abs_box_p = np.zeros((len(x_data_p), 1))
        abs_b_p = np.zeros((len(x_data_p), 1))
    abs_fit_y_iso = abs_fit_y - abs_box_y - abs_b_y
    abs_fit_p_iso = abs_fit_p - abs_box_p - abs_b_p

    if print_fit_report:
        print(result.fit_report(result.params))

    # res_y = np.copy(np.fft.rfft(result.best_fit[0:cut]))
    # res_p = np.copy(np.fft.rfft(result.best_fit[cut:]))
    toc = time.time()
    print('Computation Time:', toc - tic)

    # calculate standard deviation around biggest features
    def closest_value(input_list, input_value):
        arr = np.asarray(input_list)
        i = (np.abs(arr - input_value)).argmin()
        return i

    line_pos = [13076.3, 13078.2, 13084.2, 13086.1, 13091.7, 13093.6]
    res_array = []
    fit_array = []
    for center in line_pos:
        start = closest_value(x_data_p, center-0.5)
        stop = closest_value(x_data_p, center+0.5)
        res_array.append(abs_resnobl_p[start:stop])
        fit_array.append(abs_fit_p[start:stop])
    res_array = np.concatenate(res_array)
    fit_array = np.concatenate(fit_array)
    fit_badness = np.linalg.norm(res_array)
    # plt.figure();plt.plot(fit_array);plt.plot(res_array)
    print("Fit Badness ="+str(fit_badness))


    #%% plot
    ffn_y = os.path.basename(fn_y)[:-10]
    ffn_p = os.path.basename(fn_p)[:-10]
    if plot_results:
        # FFT of time domain fits yellow stream
        plt.figure()
        plt.plot(x_data_y,abs_datanobl_y, label='Data', linewidth=1)
        plt.plot(x_data_y,abs_resnobl_y,
                 label='Residual', linewidth=1)
        plt.plot(x_data_y,abs_fit_y, label='Total Fit',
            linewidth=1)
        if background_remove:
            plt.plot(x_data_y,abs_box_y, label='Box Fit', linewidth=1)
            plt.plot(x_data_y,abs_b_y, label='Optics Fit', linewidth=1)
        plt.xlabel('Wavenumber (cm-1)')
        plt.ylabel('Absorbance')
        plt.title("Yellow Stream"+ffn_y)
        # plt.rcParams.update({'font.size': 16})
        plt.legend()
        plt.show(block=False)

        # FFT of time domain fits purple stream
        plt.figure()
        plt.plot(x_data_p,abs_datanobl_p, label='Data', linewidth=1)
        plt.plot(x_data_p,abs_resnobl_p,
                 label='Residual', linewidth=1)
        plt.plot(x_data_p,abs_fit_p, label='Total Fit',
                linewidth=1)
        if background_remove:
            plt.plot(x_data_p,abs_box_p, label='Box Fit', linewidth=1)
            plt.plot(x_data_p,abs_b_p, label='Optics Fit', linewidth=1)
        plt.xlabel('Wavenumber (cm-1)')
        plt.ylabel('Absorbance')
        plt.title("Purple Stream"+ffn_p)
        # plt.rcParams.update({'font.size': 16})
        plt.legend()
        plt.show(block=False)

        # Weighted FFT of time domain fits yellow stream
        plt.figure()
        plt.plot(x_data_y,abs_dataweight_y, label='Data', linewidth=1)
        plt.plot(x_data_y,abs_resweight_y,
                 label='Residual', linewidth=1)
        plt.plot(x_data_y,abs_fitweight_y, label='Total Fit',
            linewidth=1)
        if background_remove:
            plt.plot(x_data_y,abs_boxweight_y, label='Box Fit', linewidth=1)
            plt.plot(x_data_y,abs_bweight_y, label='Optics Fit', linewidth=1)
        plt.xlabel('Wavenumber (cm-1)')
        plt.ylabel('Absorbance')
        plt.title("Yellow Stream"+ffn_y)
        # plt.rcParams.update({'font.size': 16})
        plt.legend()
        plt.show(block=False)

        # Weighted FFT of time domain fits yellow stream
        plt.figure()
        plt.plot(x_data_p,abs_dataweight_p, label='Data', linewidth=1)
        plt.plot(x_data_p,abs_resweight_p,
                 label='Residual', linewidth=1)
        plt.plot(x_data_p,abs_fitweight_p, label='Total Fit',
            linewidth=1)
        if background_remove:
            plt.plot(x_data_p,abs_boxweight_p, label='Box Fit', linewidth=1)
            plt.plot(x_data_p,abs_bweight_p, label='Optics Fit', linewidth=1)
        plt.xlabel('Wavenumber (cm-1)')
        plt.ylabel('Absorbance')
        plt.title("Purple Stream"+ffn_y)
        # plt.rcParams.update({'font.size': 16})
        plt.legend()
        plt.show(block=False)

        # FFT of time domain fits yellow stream
        plt.figure()
        plt.plot(x_data_y,abs_data_y, label='Data', linewidth=1)
        plt.plot(x_data_y,abs_res_y,
                 label='Residual', linewidth=1)
        plt.plot(x_data_y,abs_fit_y, label='Fit',
            linewidth=1)
        plt.xlabel('Wavenumber (cm-1)')
        plt.ylabel('Absorbance')
        plt.title("Yellow Stream"+ffn_y)
        # plt.rcParams.update({'font.size': 16})
        plt.legend()
        plt.show(block=False)

        # FFT of time domain fits purple stream
        plt.figure()
        plt.plot(x_data_p,abs_data_p, label='Data', linewidth=1)
        plt.plot(x_data_p,abs_res_p,
                 label='Residual', linewidth=1)
        plt.plot(x_data_p,abs_fit_p, label='Fit',
                linewidth=1)
        plt.xlabel('Wavenumber (cm-1)')
        plt.ylabel('Absorbance')
        plt.title("Purple Stream"+ffn_p)
        # plt.rcParams.update({'font.size': 16})
        plt.legend()
        plt.show(block=False)

        # Time domain fit plot. Plot vs time_wave for effective time on x axis
        # yellow
        plt.figure()
        plt.plot(np.linspace(0, 1, len(ff_y)),
                 time_data_y, label='Data', linewidth=1)
        plt.plot(np.linspace(0, 1, len(ff_y)),
                 time_res_y,
                 label='Residual', linewidth=1, color='None')
        plt.plot(np.linspace(0, 1, len(ff_y)),
                 time_fit_y,label='Total Fit', linewidth=1)
        if background_remove:
            plt.plot(np.linspace(0, 1, len(ff_y)),
                     time_box_y,label='Box Fit', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff_y)),
                     time_b_y,label='Optics Fit', linewidth=1)
        plt.plot(np.linspace(0, 1, len(ff_y)),
                 result.weights[:cut]*max(time_data_y))
        plt.title("Yellow Stream"+ffn_y)
        plt.xlabel('Index')
        plt.ylabel('Magnitude')
        # plt.rcParams.update({'font.size': 16})
        plt.legend()
        plt.show(block=False)

        # purple
        plt.figure()
        plt.plot(np.linspace(0, 1, len(ff_p)),
                 time_data_p, label='Data', linewidth=1)
        plt.plot(np.linspace(0, 1, len(ff_p)),
                 time_res_p,
                 label='Residual', linewidth=1, color='None')
        plt.plot(np.linspace(0, 1, len(ff_p)),
                 time_fit_p,label='Total Fit', linewidth=1)
        if background_remove:
            plt.plot(np.linspace(0, 1, len(ff_p)),
                     time_box_p,label='Box Fit', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff_p)),
                     time_b_p,label='Optics Fit', linewidth=1)
        plt.plot(np.linspace(0, 1, len(ff_p)),
                 result.weights[cut:]*max(time_data_p))
        plt.title("Purple Stream"+ffn_p)
        plt.xlabel('Index')
        plt.ylabel('Magnitude')
        # plt.rcParams.update({'font.size': 16})
        plt.legend()
        plt.show()

    if save_files:
        filename_report = os.path.join(save_path,
                                       ffn_y+file_tag +'_report.txt')

        f = open(filename_report,'w')
        f.write(result.fit_report())
        f.close()

        # save cepstrum data
        filename_save_y_cep = os.path.join(save_path,
                                           ffn_y+file_tag +'_cep.txt')
        filename_save_p_cep = os.path.join(save_path,
                                           ffn_p+file_tag +'_cep.txt')

        y_cep = np.transpose([time_data_y,time_fit_y])
        p_cep = np.transpose([time_data_p,time_fit_p])
        np.savetxt(filename_save_y_cep,y_cep)
        np.savetxt(filename_save_p_cep,p_cep)


        # save absorbance data
        filename_save_y_abs = os.path.join(save_path,
                                           ffn_y+file_tag +'_abs.txt')
        filename_save_p_abs = os.path.join(save_path,
                                           ffn_p+file_tag +'_abs.txt')

        y_abs = np.transpose([np.real(x_data_y),abs_data_y,abs_datanobl_y])
        p_abs = np.transpose([np.real(x_data_p),abs_data_p,abs_datanobl_p])
        np.savetxt(filename_save_y_abs,y_abs)
        np.savetxt(filename_save_p_abs,p_abs)

        # save background data
        if background_remove:
            filename_save_y_bkgd = os.path.join(save_path,
                                               ffn_y + file_tag + '_bkgd.txt')
            filename_save_p_bkgd = os.path.join(save_path,
                                               ffn_p + file_tag + '_bkgd.txt')

            y_bkgd = np.transpose([np.real(x_data_y), abs_box_y, abs_b_y])
            p_bkgd = np.transpose([np.real(x_data_p), abs_box_p, abs_b_p])
            np.savetxt(filename_save_y_bkgd, y_bkgd)
            np.savetxt(filename_save_p_bkgd, p_bkgd)

        # save res data
        filename_save_y_res = os.path.join(save_path,
                                           ffn_y+file_tag +'_res.txt')
        filename_save_p_res = os.path.join(save_path,
                                           ffn_p+file_tag +'_res.txt')

        y_res = np.transpose([np.real(x_data_y),np.real(np.fft.rfft(
            result.data[0:cut])-np.fft.rfft(result.best_fit[0:cut]))])
        p_res = np.transpose([np.real(x_data_p),np.real(np.fft.rfft(
            result.data[cut:])-np.fft.rfft(result.best_fit[cut:]))])
        np.savetxt(filename_save_y_res,y_res)
        np.savetxt(filename_save_p_res,p_res)

        # save fit data
        filename_save_y_fit = os.path.join(save_path,
                                           ffn_y+file_tag +'_fit.txt')
        filename_save_p_fit = os.path.join(save_path,
                                           ffn_p+file_tag +'_fit.txt')
        if background_remove:
            y_fit = np.transpose([np.real(x_data_y), abs_fit_y, abs_fit_y_iso])
            p_fit = np.transpose([np.real(x_data_p), abs_fit_p, abs_fit_p_iso])
        else:
            y_fit = np.transpose([np.real(x_data_y), abs_fit_y])
            p_fit = np.transpose([np.real(x_data_p), abs_fit_p])
        np.savetxt(filename_save_y_fit,y_fit)
        np.savetxt(filename_save_p_fit,p_fit)

        #save weight vector
        filename_save_y_weight = os.path.join(save_path,
                                              ffn_y+file_tag+'_weight.txt')
        filename_save_p_weight = os.path.join(save_path,
                                              ffn_p+file_tag+'_weight.txt')
        y_weight = np.transpose([time_wave[0:cut],weight_wave[0:cut]])
        p_weight = np.transpose([time_wave[cut:],weight_wave[cut:]])
        np.savetxt(filename_save_y_weight,y_weight)
        np.savetxt(filename_save_p_weight,p_weight)

    print(weight_window_y)
    print(weight_window_p)
    return x_data_y, x_data_p, y_data_y-abs_box_y-abs_b_y, \
           y_data_p-abs_box_p-abs_b_p,Velocity, Velocity_err, \
           Temperature, Temperature_err, Pressure, Pressure_err, \
           Shift, Shift_err, Conc, Conc_err, Broadening, Broadening_err