"""
PlotCWLockError

Created by on 10/12/22 by david

Description:

"""
import numpy as np
import matplotlib.pyplot as plt

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

fn_error = 'errorlognocontrol022223.txt'
data_error = np.loadtxt(fn_error)
time = data_error[:, 0] - data_error[0, 0]
error = data_error[:, 1]
fig, axs = plt.subplots(2)
avg_error = np.mean(error)
fig.suptitle('CW Lock Error, Avg. Err. = ' + str(round(avg_error, 2)))

# Plot time resolution
axs[0].axhline(0, linestyle='--')
axs[0].plot(time, error-avg_error)
axs[0].set_ylabel('Error (Hz)')
axs[0].set_xlabel('Time (s)')
axs[0].set_ylim([-20, 20])
# axs[0].set_xlim([0, 500])

# Plot Fourier Transform
# dt = time[1] - time[0]
# N = len(time)
# df = 1/(dt*(N-1))
#
# error_f = np.fft.rfft(error)
# freq = np.arange(len(error_f))*df
#
# axs[1].plot(freq, abs(error_f))


# # is dt consistent
# dts = []
# for i in np.arange(len(time)-1):
#     dts.append(time[i]-time[i+1])
# axs[1].plot(dts)
# It's somewhat consistent but there is some noise and sometimes random jumps
max_drifts = []
for i in np.arange(6000):
    # had to do it this dumb way because dt is not regular
    for n, t in enumerate(time):
        if t > time[i] + 300:
            stop = n
            break
    max_drifts.append(max([(max(error[i:n]) - error[i]), (error[i] - min(error[i:n]))]))
axs[1].plot(max_drifts)
axs[1].set_ylabel('Drifts over 5 min sets')
# axs[1].set_ylim([-50, 1200])

# find largest drifts in data over five minutes
