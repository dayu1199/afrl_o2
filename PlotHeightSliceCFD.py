# -*- coding: utf-8 -*-
"""
Create colormesh plots of Jan2021 CFD at certain height

Created on Wed Feb 17 11:24:10 2021

@author: David
"""
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

## function to find nearest value in an array to a specified array
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]

height = 0.000

# directory/fileinfo
dr = os.path.join(r'/Users/david/Library/CloudStorage',
                  r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                  r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/CFD',
                  r'DoD SAFE-FRRAspDkPmNCZy8a/RC18_CFDSET1')
fn = 'case01.interp1.dat'

# read in lines and create dictionary
headers = ['X','Y','Z','P','T','U','V','W','R','M','X_O2','X_H2O','X_CH4','X_CO2','X_N2']
cfdvalues = []
f = open(os.path.join(dr,fn))
line_count = 0
for line in f:
    line_count += 1
    if line_count > 16:
        cfdvalues.append(np.asfarray(line.split()))
cfd_df = pd.DataFrame.from_records(cfdvalues, columns=headers)

y = find_nearest(cfd_df['y'],height)
    
slice_y = cfd_df.loc[cfd_df['y']==y]

z = []
z_row = []
i = 0
for val in slice_y['z']:
    i += 1
    z_row.append(val)
    if i == 316:
        z.append(z_row)
        z_row = []
        i = 0
x = []
x_row = []
i = 0
for val in slice_y['x']:
    i += 1
    x_row.append(val)
    if i == 316:
        x.append(x_row)
        x_row = []
        i = 0
p = []
p_row = []
i = 0
for val in slice_y['p']:
    i += 1
    p_row.append(val)
    if i == 316:
        p.append(p_row)
        p_row = []
        i = 0
u = []
u_row = []
i = 0
for val in slice_y['u']:
    i += 1
    u_row.append(val)
    if i == 316:
        u.append(u_row)
        u_row = []
        i = 0
T = []
T_row = []
i = 0
for val in slice_y['T']:
    i += 1
    T_row.append(val)
    if i == 316:
        T.append(T_row)
        T_row = []
        i = 0
z = np.asarray(z)
x = np.asarray(x)
p = np.asarray(p)
u = np.asarray(u)
T = np.asarray(T)
# remove last row (boundary) from array
z = z[:-1,:]
x = x[:-1,:]
p = p[:-1,:]
u = u[:-1,:]
T = T[:-1,:]

# define interpolation points
n_int=40
deg = 12.5
theta = deg*np.pi/180
z_up1 = np.linspace(0,0.05,num=n_int)
z_down1 = np.flip(z_up1)
x_up1 = np.linspace(220e-3,220e-3+0.05*np.tan(theta),num=n_int)
x_down1 = np.linspace(220e-3-0.05*np.tan(theta),220e-3,num=n_int)
x1 = np.append(x_down1[:-1],x_up1[:-1])
z1 = np.append(z_down1[:-1],z_up1[:-1])

z_up2 = np.linspace(0,0.05,num=n_int)
z_down2 = np.flip(z_up2)
x_up2 = np.linspace(240e-3,240e-3+0.05*np.tan(theta),num=n_int)
x_down2 = np.linspace(240e-3-0.05*np.tan(theta),240e-3,num=n_int)
x2 = np.append(x_down2[:-1],x_up2[:-1])
z2 = np.append(z_down2[:-1],z_up2[:-1])

z_up3 = np.linspace(0,0.05,num=n_int)
z_down3 = np.flip(z_up2)
x_up3 = np.linspace(260e-3,260e-3+0.05*np.tan(theta),num=n_int)
x_down3 = np.linspace(260e-3-0.05*np.tan(theta),260e-3,num=n_int)
x3 = np.append(x_down3[:-1],x_up3[:-1])
z3 = np.append(z_down3[:-1],z_up3[:-1])

fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
fig.suptitle(('Height '+str(height)+' m'))
# plot tomographic
h1 = ax1.pcolormesh(z,x,p)
ax1.set_ylabel('Axial (m)')
ax1.set_xlabel('Transverse (m)')
ax1.set_title('Pressure')
ax1.plot(z3,x3,'g.')
ax1.plot(z2,x2,'.',color='orange')
ax1.plot(z1,x1,'b.')
cbar = fig.colorbar(h1, ax=ax1, extend='both',label='Pressure (K)',
                    orientation='horizontal')

h2 = ax2.pcolormesh(z,x,u)
# ax2.set_ylabel('Axial (m)')
ax2.set_xlabel('Transverse (m)')
ax2.set_title('Velocity')
ax2.set(yticklabels=[])
ax2.plot(z3,x3,'g.')
ax2.plot(z2,x2,'.',color='orange')
ax2.plot(z1,x1,'b.')
cbar = fig.colorbar(h2, ax=ax2, extend='both',label='Velocity (m/s)',
                    orientation='horizontal')

h3 = ax3.pcolormesh(z,x,T)
# ax3.set_ylabel('Axial (m)')
ax3.set_xlabel('Transverse (m)')
ax3.set_title('Temperature')
ax3.set(yticklabels=[])
ax3.plot(z3,x3,'g.')
ax3.plot(z2,x2,'.',color='orange')
ax3.plot(z1,x1,'b.')
cbar = fig.colorbar(h3, ax=ax3, extend='both',label='Temperature (K)',
                    orientation='horizontal')