"""
AllanCheck

Created by on 6/23/22 by david

Description: Checking allantools against Igor

"""
#%% import modules
import allantools
import numpy as np
import allantools as allan
import matplotlib.pyplot as plt
import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

#%% import data
fn = 'temp_example.txt'
data = np.loadtxt(fn)

# #%% perform allan deviation
# a = allantools.Dataset(data, data_type='freq', rate=1/3.42426)
# a.compute('mdev')
# print(a.out['stat'])
#
# #%% plot allan deviation
# b = allantools.Plot()
# b.plot(a, errorbars=True, grid=True)
# b.show()

[tau, ad, ade, ns] = allan.mdev(data,
                                         rate=1/3.42426,
                                         data_type='freq')
# plot Velocity Allan
plt.figure()
fig, ax = plt.subplots()
ax.set_yscale('log')
ax.set_xscale('log')
ax.errorbar(tau, ad, yerr=ade)
ax.set_ylabel('mdev')
ax.set_xlabel('Tau (s)')
