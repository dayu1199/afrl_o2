"""
PlotFrepTraceDay1.py

Created by on 4/6/23 by david

Description:

"""
import os
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import matplotlib.transforms as mtransforms
import pandas as pd
from datetime import datetime
import numpy as np

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

d_log = os.path.join(r'/Users/david/Library/CloudStorage/OneDrive-UCB-O365/AFRL',
                 'AFRL ETHOS (DO NOT SYNC)/Mar2023CampaignData - David/LogFiles')

f_log = '20230317.log'
f_key = 'FilenameKey_031623.txt'

time = []
fr1 = []
fr2 = []
dfr = []
pts_per_ig = []
frame_length = []
with open(os.path.join(d_log, f_log), 'r') as f:
    previous_timestamp = []
    for i, line in enumerate(f.readlines()):
        if i == 2:
            header = line.split(',')
        if i > 2:
            contents = line.split(',')
            time.append(contents[0])
            fr1.append(float(contents[1]))
            fr2.append(float(contents[2]))
            dfr.append(float(contents[3]))
            pts_per_ig.append(float(contents[4]))
            frame_length.append(int(contents[5][:-1]))

def gettimestamp(timeprint):
    return int(datetime.timestamp(datetime.strptime(timeprint, "%Y-%m-%dT%H:%M:%S-0400")))

dict_log = {
    'Time': time,
    'Fr1': fr1,
    'Fr2': fr2,
    'dfr': dfr,
    'pts_per_ig': pts_per_ig,
    'frame_length': frame_length
}

df_log = pd.DataFrame.from_dict(dict_log)
df_log['Timestamp'] = df_log['Time'].apply(gettimestamp)

# Plot PPS trace
fig, ax = plt.subplots()
# from 11161 for 31423
# from 24712 for 31623
ax.plot(df_log['Timestamp'][24712:], df_log['Fr1'][24712:], label='Frep1')
# ax.plot(df_log['Timestamp'], df_log['Fr2'], label='Frep2')
ax.set_ylabel('Repetition Rate')
ax.ticklabel_format(style='plain')
formatter = FuncFormatter(lambda x_val, tick_pos: datetime.fromtimestamp(x_val))
ax.xaxis.set_major_formatter(formatter)
ax.set_ylim([200003560, 200003580])
# ax.legend()
# ax.set_ylim([1.1*min(df_log['PPS TI'][7906:]), 1.1*max(df_log['PPS TI'][7906:])])
fig.autofmt_xdate()

# plot magic number
ax2 = ax.twinx()
# from 11161 for 31423
# from 24712 for 31623
ax2.plot(df_log['Timestamp'][24712:], df_log['pts_per_ig'][24712:], 'r')
ax2.set_ylabel('Pts per IG')
ax2.set_ylim([-325000,500000])

# overlay time of measurements
contents = []
with open(f_key, 'r') as f:
    for i, line in enumerate(f.readlines()):
        if i == 0:
            d_up = line[:-1]
        elif i == 1:
            d_up_dist = line[:-1]
        elif i == 2:
            d_down = line[:-1]
        elif i == 3:
            d_down_dist = line[:-1]
        elif i == 4:
            header = line[:-1].split()
        else:
            contents.append(line[:-1].split())
df_key = pd.DataFrame(contents, columns=header)

# find start and end times of filenames
def find_time(row):
    # find time based on upstream information
    if row ['type'] == 'fail':
        time_start = 'N/A'
        time_end = 'N/A'
    else:
        if row['type'] == 'dist':
            d_log = d_up_dist
        else:
            d_log = d_up
        with open(os.path.join(d_log, row['fn_up']+'.log'), 'r') as f:
            for line in f.readlines():
                if 'runtime' in line:
                    runtime = int(float(line.split()[2]))
                if 'start_time_local' in line:
                    time_start = int(datetime.timestamp(datetime.strptime(line.split()[2],
                                                   "%Y%m%d%H%M%S-0400")))
        time_end = int(time_start + runtime)
    return time_start, time_end

df_times = df_key.apply(find_time, axis=1, result_type='expand')
df_times.columns = ['Time Start', 'Time End']
df_key = pd.concat([df_key, df_times], axis=1)

# plot measurement times
colors = iter([plt.cm.tab20(i) for i in range(20)])
for i, row in df_key.iterrows():
    if row['type'] == 'fail':
        continue
    trans = mtransforms.blended_transform_factory(ax.transData, ax.transAxes)
    ax.fill_between(range(row['Time Start'], row['Time End']),
                    200002000, 200004000, facecolor=next(colors), alpha=0.5)
    ax.text(np.mean([row['Time Start'], row['Time End']]), 200003575, row['Run'])


#
# # Plot PPS trace
# fig, ax = plt.subplots()
# ax.plot(df_log['Timer'][7906:], df_log['PPS TI'][7906:])
# ax.set_ylabel('PPS TI (s)')
# formatter = FuncFormatter(lambda x_val, tick_pos: datetime.fromtimestamp(x_val))
# ax.xaxis.set_major_formatter(formatter)
# ax.set_ylim([1.1*min(df_log['PPS TI'][7906:]), 1.1*max(df_log['PPS TI'][7906:])])
# # ax.ticklabel_format(style='plain'
# fig.autofmt_xdate()
#
# # overlay time of measurements
# # first day 3/14/2023 6:07 PM to 7:45 PM
# dt_string1_start = '3/14/2023 18:07:00'
# dt_string1_end = '3/14/2023 19:45:00'
# dt_object1_start = datetime.strptime(dt_string1_start, "%m/%d/%Y %H:%M:%S")
# dt_object1_end = datetime.strptime(dt_string1_end, "%m/%d/%Y %H:%M:%S")
# trans = mtransforms.blended_transform_factory(ax.transData, ax.transAxes)
# ax.fill_between(range(int(datetime.timestamp(dt_object1_start)), int(datetime.timestamp(dt_object1_end))),
#                 -1, 1, facecolor='green', alpha=0.5)
#
# # second day 3/16/2023 5:46 PM to 6:50 PM
# dt_string2_start = '3/16/2023 17:46:00'
# dt_string2_end = '3/16/2023 18:50:00'
# dt_object2_start = datetime.strptime(dt_string2_start, "%m/%d/%Y %H:%M:%S")
# dt_object2_end = datetime.strptime(dt_string2_end, "%m/%d/%Y %H:%M:%S")
# trans = mtransforms.blended_transform_factory(ax.transData, ax.transAxes)
# ax.fill_between(range(int(datetime.timestamp(dt_object2_start)), int(datetime.timestamp(dt_object2_end))),
#                 -1, 1, facecolor='green', alpha=0.5)