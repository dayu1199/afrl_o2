#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code to produce tranmission files from Moose DAQ from AFRL March campaign

Created on Tue Mar 22 15:28:15 2022

@author: david
"""
# Package imports
import os
import time
import numpy as np
import matplotlib.pyplot as plt

from packfind import find_package
find_package("pldspectrapy")
import pldspectrapy as pld
import td_support as td
import pandas as pd

# Plot params
plt.rcParams.update({'font.family': 'Times New Roman',
                     "figure.autolayout": True,
                     "lines.linewidth": 0.8})

# Code switches and params
processone = True
process_raw = True
pclim_low = 0.12
pclim_high = 0.18
savedata = True
plotdata = False
fitallan = True
fromback = True
pc_times = [*range(15, 585, 15)]
ig_num = 1  # how many averaged igs to process, if 0 or greater than available
            #igsjust do all

# directory info
run = '11_2'
d = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                 'Mar2022CampaignData/moose','031122_vis3')
d_key = r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS/MarchCampaign'
fn_key = 'FilenameKey.xlsx'
key_df = pd.read_excel(os.path.join(d_key,fn_key))
row = key_df.loc[key_df['Indexx']==run]
fname = str(row.iloc[0]['vis3 fn'])

# Frequency axis calculation
lockfreq = 32e6
nomfreq = 13333  # Should be within the range specified by band_fit (below)

# open up directory
t0 = time.time()
files = pld.open_dir(d, recursion_levels=2, verbose=2) #######
dt = time.time() - t0
print("Opened %i files in %0.3f seconds" % (len(files), dt))
newest = ""

for pc_time in pc_times:
    # create save directory
    print(str(pc_time) +' s')
    if fromback:
        d_save = os.path.join(d, run + '_pctime_fromback')
    else:
        d_save = os.path.join(d, run+'_pctime')
    if not os.path.isdir(d_save):
        os.mkdir(d_save)
    d_save = os.path.join(d_save, run+'_'+str(pc_time)+'s')
    if not os.path.isdir(d_save):
            os.mkdir(d_save)
    if fitallan:
        d_save = os.path.join(d_save,'time_resolved')
        if not os.path.isdir(d_save):
            os.mkdir(d_save)
        d_save = os.path.join(d_save, run)
        if not os.path.isdir(d_save):
            os.mkdir(d_save)

    # Loop through directory and process data
    for name, daq_file in files.items():

        # Create a couple variables used for determining processing flow
        pass_pc = 0
        pass_raw = 0

        if processone:
            if name != fname:
                continue

        # Make sure there's some data
        if not daq_file.failure:

            # Are there phase corrected IGMs?
            if daq_file.data_pc is not None:
                non_empty_pc = sum(bool(i) for i in daq_file.igs_per_pc)
                pass_pc = 1 if (len(daq_file.data_pc) == non_empty_pc) else 0

            # Are there raw IGMs?
            if daq_file.data_raw is not None:
                non_empty_raw = len(daq_file.data_raw)
                pass_raw = 1 if (non_empty_raw > 0) else 0

            # As long as there's some IGMs go ahead and start processing!
            if pass_pc or pass_raw:
                newest = max(newest, name)
                print(name)

                daq_file.pc_lim_low = pclim_low
                daq_file.pc_lim_high = pclim_high
                # Phase correction and transform to transmission
                # daq_file.data_raw => phase correct and sum raw IGMs
                # daq_file.data_pc => phase correct and sum PC data=
                if fitallan:
                    if process_raw:
                        pc_num = pc_time*daq_file.dfr/daq_file.num_hwavgs
                        if ig_num == 0 or ig_num > len(daq_file.data_raw):
                            n_pc = np.ceil((len(daq_file.data_raw)/pc_num))
                        else:
                            n_pc = ig_num
                    else:
                        pc_num = pc_time/daq_file.pc_time
                        if ig_num == 0 or ig_num > len(daq_file.data_pc):
                            n_pc = np.ceil((len(daq_file.data_pc)/pc_num))
                        else:
                            n_pc = ig_num
                    print('n_pc = ', str(n_pc))
                else:
                    n_pc = 1

                for i in np.arange(n_pc):
                    # check if pc already exists
                    if fitallan:
                        fn_save = name+'_trans'+str(int(i))+'.txt'
                    else:
                        fn_save = name+'_trans.txt'

                    # check if already processed
                    if os.path.exists(os.path.join(d_save, fn_save)):
                        print(fn_save + ' already processed in ' + d_save)
                        continue

                    print(str(int(i+1)) + ' out of ' + str(int(n_pc)) + ' fits\n')
                    if fitallan:
                        if process_raw:
                            if fromback:
                                if i == (n_pc - 1):
                                    if n_pc * pc_num > len(daq_file.data_raw):
                                        data_ig = daq_file.data_raw[0:
                                                              len(daq_file.data_raw) - int(pc_num * i)]
                                    else:
                                        data_ig = daq_file.data_raw[len(daq_file.data_raw) - int(pc_num * (i + 1)):
                                                              len(daq_file.data_raw) - int(pc_num * i)]
                                else:
                                    data_ig = daq_file.data_raw[len(daq_file.data_raw) - int(pc_num * (i + 1)):
                                                          len(daq_file.data_raw) - int(pc_num * i)]
                            else:
                                if i == (n_pc-1):
                                    if n_pc*pc_num > len(daq_file.data_raw):
                                        modulo = len(daq_file.data_raw) % pc_num
                                        data_ig = daq_file.data_raw[int(pc_num*i):int(pc_num*i+modulo)]
                                    else:
                                        data_ig = daq_file.data_raw[int(pc_num*i):int(pc_num*(i+1))]
                                else:
                                    data_ig = daq_file.data_raw[int(pc_num*i):int(pc_num*(i+1))]
                        else:
                            if i == (n_pc-1):
                                if n_pc*pc_num > len(daq_file.data_pc):
                                    modulo = len(daq_file.data_pc) % pc_num
                                    data_ig = daq_file.data_pc[int(pc_num*i):int(pc_num*i+modulo)]
                                else:
                                    data_ig = daq_file.data_pc[int(pc_num*i):int(pc_num*(i+1))]
                            else:
                                data_ig = daq_file.data_pc[int(pc_num*i):int(pc_num*(i+1))]
                    else:
                        if process_raw:
                            data_ig = daq_file.data_raw
                        else:
                            data_ig = daq_file.data_pc

                    data = data_ig.copy()
                    trans = pld.pc_peter(data, pclim_low, pclim_high,
                                         plot=plotdata, zoom=500)
                    pc_ig = np.fft.ifft(trans)

                    # pc_ig = pld.pc_truncated(
                    #     data_ig,
                    #     daq_file.pc_lim_low,
                    #     daq_file.pc_lim_high,
                    #     daq_file.frame_length,
                    # )

                    # p2p.append((np.max(pc_ig)-np.min(pc_ig))/(pc_num*daq_file.num_hwavgs))

                    # trans = np.abs(np.fft.fft(pc_ig)) #######

                    # Create frequency axis
                    x_wvn_full = pld.mobile_axis2(
                        daq_file, f_opt=lockfreq, wvn_spectroscopy=nomfreq
                    )
                    lambda_full = 1e7/x_wvn_full
                    if plotdata:
                        fig,ax = plt.subplots(2,1)
                        ax[0].plot(pc_ig)
                        ax[0].set_xlabel('Time step')
                        ax[0].set_ylabel('Power')
                        ax[1].plot(lambda_full[:len(trans)],trans)
                        ax[1].set_xlabel('Wavelength (cm-1)')
                        ax[1].set_ylabel('Transmission')
                        plt.suptitle(name)
                    # save file
                    save_data = np.vstack((x_wvn_full[:len(trans)],trans))
                    save_data = np.transpose(save_data)

                    np.savetxt(os.path.join(d_save,fn_save),save_data)
            else:
                print('No data ')


