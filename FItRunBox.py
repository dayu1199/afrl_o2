#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code to fit data from frequency doubling box

Created on Thu Mar 31 11:41:39 2022

@author: david
"""

import os
from openpyxl import Workbook, load_workbook
import pandas as pd
import pickle
from datetime import datetime

import FitProgram1Beam as fithit

##################################Fitting Parameters###########################

# Fitting Options
save_files = True # saves fits and fitted parameters in text files.
plot_results = True # Plots time domain and TD fit converted to the frequency
                    # domain. Turn this off for many datafiles.
print_fit_report = True # Prints each fit report to command window 
back_remove = False # Remove background from spectra before fitting
pull_bandweight = True # get band_fit and weight start from FittingInfo.xlsx
# Directory Info
d_data = os.path.join(r'/Users/david/Library/CloudStorage/OneDrive-UCB-O365',
                      'AFRL/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/cubig',
                      '031122/pc_peter_ig')
d_save = os.path.join(r'/Users/david/Library/CloudStorage/OneDrive-UCB-O365',
                      'AFRL/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits/box')
file_tag = '' # remember to add a _ before

# indexes = {5,6,7,8,9,10,'11_2',12,13,14,15,16,17,18,19}
indexes = {'boxall'}

# MFID Parameters
if pull_bandweight:
    fn_fitinfo = os.path.join(r'/Users/david/Library/CloudStorage/OneDrive-UCB-O365',
                              'AFRL ETHOS/MarchCampaign/FittingInfo.xlsx')
    df_fitinfo = pd.read_excel(fn_fitinfo, sheet_name='PC_Peter')
else:
    weight_window = [0.009,0.5] # default [0.005,0.5]
    band_fit = [13015.1, 13172.4] 
    weight_flat = True # Otherwise will do an exponential weighting
etalon_windows = [[510,530],[568,576]] #    # etalon_windows = [[510,530],[566,576]]
weight_flat = True # Otherwise will do an exponential weighting
# Fitting Parameters
angle = 0
molec_to_fit = ['O2'] # must be a list
isotopes = [[1]] # format [[1],[2]]
press_guess = 1 # atm
temp_guess = 295
vel_guess = 0
shift_guess = 0
conc_guess = 0.201
broad_guess = 1
pathlength_guess = 35 # theta correction happens in FitProgram
temp_fit = True
press_fit = False
shift_fit = True
vel_fit = False
conc_fit = True
broad_fit = False
pl_fit = False

# Background Fitting Parameters
temp_back = 296.00000
shift_back = 0
press_back = 0
label_back = ''

if not(back_remove):
    conc_back = 0
    shift_back = 0
    press_back = 0
    
# organization of fits
if save_files:
    notes = input('Type down notes for this fit:')

    # workbook name
    name_tabulate = 'BoxFits.xlsx'
    fn_tabulate = os.path.join(d_save,name_tabulate)
    
    # load/create workbook
    if os.path.isfile(fn_tabulate):
        wb = load_workbook(fn_tabulate)
    else:
        wb = Workbook()
    
    # get time for labeling fit
    now = datetime.now()
    timestamp = now.strftime('%d_%m_%Y_%H%M%S')
    
    # folder to save data
    d_save_0 = os.path.join(d_save,timestamp)
    if not(os.path.isdir(d_save_0)):
        os.mkdir(d_save_0)
    
###############################Port in data###################################
# get files from directory
files = os.listdir(d_data)
 
# prepare arrays for dataframe
fn = []
vel = []
vel_unc = []
temp = []
temp_unc = []
press = []
press_unc = []
shift = []
shift_unc = []
conc = []
conc_unc = []
pl = []
pl_unc = []

# get files from directory
# import filename key to get filenames
d_key = os.path.join(r'/Users/david/Library/CloudStorage',
                     r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                     r'AFRL ETHOS/MarchCampaign')
fn_key = 'FilenameKey.xlsx'
df_key = pd.read_excel(os.path.join(d_key,fn_key))
 
for index in indexes:
    # only fit the one file if fit_one is switched on
    # Grab filenames from key
    row = df_key.loc[df_key['Indexx']==index]
    fn_box = str(row.iloc[0]['box fn'])
    file = fn_box + '_trans.txt'

    # make sure that this file wasn't fit before
    if os.path.exists(os.path.join(d_save,file[:-10]+file_tag+'_report.txt')):
        print('Fit file already exists.')
        continue
    # file = index+'_trans.txt'

    fn.append(file)
    
    # directory to save results
    if save_files:
        d_save_1 = os.path.join(d_save_0,str(index))
        if not(os.path.isdir(d_save_1)):
            os.mkdir(d_save_1)
    else:
        d_save_1 = ''
    
    if pull_bandweight:
        if index == '11_2':
            row = df_fitinfo.loc[df_fitinfo['Indexx']==11]
        else:
            row = df_fitinfo.loc[df_fitinfo['Indexx']==index]
        bd_start = row.iloc[0]['box start']
        bd_end = row.iloc[0]['box end']
        bl_start = row.iloc[0]['box BL start']
        band_fit = [bd_start, bd_end]
        weight_window = [bl_start,0.5]


    vel_val, vel_err, temp_val, temp_err, press_val, press_err, shift_val,\
        shift_err, conc_val, conc_err, broad_val, broad_err, pl_val, pl_err = \
            fithit.FitProgram(file, d_data, d_save_1, file_tag,
                              weight_window, weight_flat, etalon_windows,
                              angle, band_fit,
                              molec_to_fit, isotopes, 
                              press_fit, temp_fit, vel_fit,
                              shift_fit, conc_fit, broad_fit, pl_fit,
                              press_guess, temp_guess, vel_guess,
                              shift_guess, conc_guess, broad_guess,
                              pathlength_guess,
                              save_files=save_files,
                              plot_results=plot_results,
                              background_remove=back_remove)
    vel.append(vel_val)
    vel_unc.append(vel_err)
    temp.append(temp_val)
    temp_unc.append(temp_err)
    press.append(press_val)
    press_unc.append(press_err)
    shift.append(shift_val)
    shift_unc.append(shift_err)
    conc.append(conc_val[0])
    conc_unc.append(conc_err[0])
    pl.append(pl_val)
    pl_unc.append(pl_err)
    
    if save_files:
        # load/create worksheet
        try:
            ws = wb[file]
        except:
            ws = wb.create_sheet(file)
            # create header
            header = ['INDEX','TIME','DAQ','Fn',
                      'WEIGHT_FLAT','WEIGHT_WIN','ETA','BW',
                      'MOLECULES',
                      'PL_FIT','PL_VAL','PL_ERR','PL_GUES',
                      'VEL_FIT','VEL_VAL','VEL_ERR','VEL_GUES',
                      'TEMP_FIT','TEMP_VAL','TEMP_ERR','TEMP_GUES',
                      'PRES_FIT','PRES_VAL','PRES_ERR','PRES_GUES',
                      'SHIFT_FIT','SHIFT_VAL','SHIFT_ERR','SHIFT_GUES',
                      'CONC_FIT','CONC_VAL','CONC_ERR','CONC_GUES',
                      'BROAD_FIT','BROAD_VAL','BROAD_ERR','BROAD_GUES',
                      'BACK_REMOVE','BACK_LABEL','BACK_PRESS','BACK_TEMP',
                      'BACK_CONC','NOTES']
            ws.append(header)
        # add data from current run
        datawrite = [index, timestamp, 'cubig',
                     file, str(weight_flat),
                     str(weight_window), str(etalon_windows),
                     str(band_fit), str(molec_to_fit),
                     str(pl_fit), pl_val, pl_err, pathlength_guess,
                     str(vel_fit), vel_val, vel_err, vel_guess,
                     str(temp_fit), temp_val, temp_err, temp_guess,
                     str(press_fit), press_val, press_err, press_guess,
                     str(shift_fit), shift_val, shift_err, shift_guess,
                     str(conc_fit), str(conc_val), str(conc_err),
                     str(conc_guess),
                     str(broad_fit), broad_val, broad_err, broad_guess,
                     str(back_remove), label_back, press_back, temp_back,
                     conc_back, notes]
        ws.append(datawrite)
        # save workbook
        wb.save(fn_tabulate)
        
    
# Create dict of results
d_fit = {'Fn': fn,
     'PL (cm)': pl,
     'PL Unc.': pl_unc,
     'Vel. (m/s)': vel,
     'Vel. Unc.': vel_unc,
     'Temp. (K)': temp,
     'Temp. Unc.': temp_unc,
     'Pres. (atm)': press,
     'Pres. Unc.': press_unc,
     'Shift (cm-1)': shift,
     'Shift Unc.': shift_unc,
     'Conc.': conc,
     'Conc. Unc': conc_unc}

df_fit = pd.DataFrame.from_dict(d_fit)
if save_files:
    pickle.dump(df_fit, open(os.path.join(d_save_0,'df_fit.p'),'wb'))

print('Fitting Done!')