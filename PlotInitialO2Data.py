"""
PlotInitialO2Data.py

Created by on 8/26/22 by david

Description:

"""

import os
import numpy as np
import matplotlib.pyplot as plt

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

d_data = r'/Volumes/UCFDataMac/011722_LongDistortionTest/pc'

data_1 = np.loadtxt(os.path.join(d_data, '1min',
                                 'fitdata_freq_20220117161157.txt'))
wvn_1 = data_1[:, 2]
abs_1 = data_1[:, 0]
fit_1 = data_1[:, 1]

data_5 = np.loadtxt(os.path.join(d_data, '5min',
                                 'fitdata_freq_20220117161157.txt'))
wvn_5 = data_5[:, 2]
abs_5 = data_5[:, 0]
fit_5 = data_5[:, 1]

data_30 = np.loadtxt(os.path.join(d_data, '30min',
                                 'fitdata_freq_20220117161157.txt'))
wvn_30 = data_30[:, 2]
abs_30 = data_30[:, 0]
fit_30 = data_30[:, 1]

plt.figure()
plt.plot(wvn_1, abs_1, label='1 min data')
plt.plot(wvn_5, abs_5, label='5 min data')
plt.plot(wvn_30, abs_30, label='30 min data')
plt.plot(wvn_30, fit_30, label='30 min fit')
# plt.legend()
# plt.ylabel('Absorbance')
# plt.xlabel('Wavenumber ($\mathregular{cm^{-1}}$) for $\mathregular{H_{2}O}$')