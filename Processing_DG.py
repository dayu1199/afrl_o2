# -*- coding: utf-8 -*-
"""
Created on Friday March 12, 2021

@author: CoburnS

Purpose: Creating a generic type processing file that will eventually be turned
into a standard option.

This will be built from loop_fitting_test.py and fitting_test_single.py
    - The core processing code will be from fitting_test_single.py because that
        was used to tune the fitting parameters.
    - The outer loop for reading through the data repository will come from
        loop_fitting_test.py since that was been verified with field data
        collected at PAO.

This code assumes that the appropriate HAPI database (local) is already available
    - Needs to be configured for molecules being fits

"""

#   Package imports

import os
import time
from copy import copy
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from packfind import find_package
find_package("pldspectrapy")
import pldspectrapy as pld
import td_support as td

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()
#   End package imports

#   Configurable variables -> eventually want to pull all of these out
#   and create a configuration file that sets these parameters


## fitting function
def FitFunc(x_wvn, y_td, bl, etalons, db_name1,
            modfull, pars, p2p, name, x_wvn_full, trans_orig, trans, daq_file,
            savedata=True, savename='', savename_td='', savename_freq='', savepath='',
            band_fit=[6810, 7180], plot_fit_td=True, plot_fit_freq=True):
    # Create the weighting function for time domain fitting
    weight = td.weight_func(len(x_wvn), bl, etalons)
    Fit = modfull.fit(
        y_td, xx=x_wvn, params=pars, weights=weight, name=db_name1
    )
    # This section sets up data for plotting so may not be needed

    # Pull time domain fit data
    y_datai = Fit.data
    fit_datai = Fit.best_fit
    weight = Fit.weights

    # Pull frequency domain fit data
    data_lessbl = np.real(
        np.fft.rfft(y_datai - (1 - weight) * (y_datai - fit_datai))
    )
    model = np.real(np.fft.rfft(fit_datai))
    residual = data_lessbl - model
                                 
    print(Fit.fit_report())
    
    print('Noise level is '+str(np.std(residual[1942:2391])))
    print('P2P is '+str(p2p[-1]))


    if plot_fit_td:
        # And plot time domain
        plt.figure()
        plt.plot(y_datai, label='Data')
        plt.plot(fit_datai, label= 'Model')
        plt.plot(y_datai - fit_datai, label='Residual')
        plt.plot(weight)
        plt.legend()
        plt.title(str(i)+' '+name)
        plt.show()
    if plot_fit_freq:
        # And plot frequency domain
        fig, axs = plt.subplots(2, 1, sharex="col")
        axs[0].plot(x_wvn, data_lessbl, x_wvn, model)
        axs[1].plot(x_wvn, residual)
        axs[0].set_title(str(i))
        plt.figure()
        plt.plot(1/x_wvn_full*1e7, trans_orig[:len(x_wvn_full)])
        plt.title(str(i))
        plt.figure()
        plt.plot(x_wvn, trans)
        plt.title(str(i)+' '+name)
        plt.show()
        # axs[0].set_ylabel('Absorbance'); #axs[0].legend(['data','fit'])
        # axs[1].set_ylabel('Residual'); axs[1].set_xlabel('Wavenumber ($cm^{-1}$)')

    # End the plotting section

    # Section for saving data -> will probably be replaced

    if savedata:
        if not os.path.isdir(savepath):
            os.mkdir(savepath)
        # Create an output string with file information
        outinfo = (
            "====================================================" + "\n"
        )
        outinfo += "[[Data Info]]\n"
        outinfo += "\tData file\t\t\t = " + name + "\n"
        outinfo += "\tTotal P2P\t\t\t = " + str(daq_file.p2p_total) + "\n"
        outinfo += "\tFit region\t\t\t = " + str(band_fit) + "\n"
        outinfo += "\tBaseline\t\t\t = " + str(bl) + "\n"
        outinfo += "\tEtalons\t\t\t\t = " + str(etalons) + "\n"

        # DY - In order to save fit results to a textfile I use the following lines:
        f = open(os.path.join(savepath, savename), "a")
        if fitallan:
            f.write('pc '+str(int(i))+'\n')
        f.write(outinfo)
        f.write(Fit.fit_report() + "\n")
        # f.write('===================================================='+'\n')
        f.close()

        # Save the time domain fit arrays
        fit_outdata_td = np.array([y_datai, fit_datai, weight])
        fit_outdata_td = fit_outdata_td.T
        if fitallan:
            np.savetxt(
                os.path.join(savepath, savename_td + "_" + name + "_" + str (int(i)) + ".txt"),
                fit_outdata_td,
                delimiter="\t",
            )
        else:
            np.savetxt(
                os.path.join(savepath, savename_td + "_" + name + ".txt"),
                fit_outdata_td,
                delimiter="\t",
            )

        # Save the frequency domain fit arrays
        fit_outdata_freq = np.array([data_lessbl, model, x_wvn])
        fit_outdata_freq = fit_outdata_freq.T
        if not(fitallan):
            np.savetxt(
                os.path.join(savepath, savename_freq + "_" + name + ".txt"),
                fit_outdata_freq,
                delimiter="\t",
            )
        else:
            np.savetxt(
                os.path.join(savepath, savename_freq + "_" + name + "_" + str (int(i)) + ".txt"),
                fit_outdata_freq,
                delimiter="\t",
            )

        f = open(os.path.join(savepath, savename), "a")
        f.write(
            "====================================================" + "\n"
        )
        f.close()
        
    return Fit

def FitOneBeam(fname, savepath, files, molecule='H2O_PaulLF2',
               band_fit = [6810, 7188], plot_fit_td = True, plot_fit_freq = True,
               lockfreq = -32e6, nomfreq = 7000, bl = 200, etalons = [],
               importdata=False,
               importwvn=False, d_import='', fname_import=''):
    savename = "fitresults.txt"
    savename_td = "fitdata_td"
    savename_freq = "fitdata_freq"
    if not os.path.exists(savepath):
        os.mkdir(savepath)

    # HAPI/fitting parameters - will need this section for each molecule
    if fitdata:
        db_name1 = molecule
        mod, pars = td.spectra_single_lmfit("h2o")
        pars["h2omol_id"].value = 1
        pars["h2omolefraction"].set(value=0.05, vary=True)  # (molefraction)
        # pars['h2opressure'].set(value = (634.7/760), vary = False)  # (atm)
        pars["h2opressure"].set(value=0.83, vary=True)  # (atm), 20201125 Exp
        # pars['h2opressure'].set(value = (0.8351), vary = False)  # (atm), 20201203 Exp
        pars["h2otemperature"].set(value=296, vary=True)  # (K)
        pars["h2opathlength"].set(value=8.3, vary=False)  # (cm)
        pars["h2oshift"].var = True  # (cm-1, but always float)

        modfull = mod

    newest = ""
    # Create a couple variables used for determining processing flow
    pass_pc = 0
    pass_raw = 0

    temps = []
    concs = []
    press = []
    pathlengths = []
    shifts = []
    p2p = []
    # Loop through directory and process data
    if not (importdata):
        for name, daq_file in files.items():

            if processone:
                if name != fname:
                    continue
                    print('yes')

            # Make sure there's some data
            if not daq_file.failure:

                # Are there phase corrected IGMs?
                if daq_file.data_pc is not None:
                    non_empty_pc = sum(bool(i) for i in daq_file.igs_per_pc)
                    pass_pc = 1 if (len(daq_file.data_pc) == non_empty_pc) else 0

                # Are there raw IGMs?
                if daq_file.data_raw is not None:
                    non_empty_raw = len(daq_file.data_raw)
                    pass_raw = 1 if (non_empty_raw > 0) else 0

                # As long as there's some IGMs go ahead and start processing!
                if pass_pc or pass_raw:
                    if not (importdata):
                        newest = max(newest, name)
                        print(name)

                        # pc_lim_low = 0.2
                        # pc_lim_high = 0.25
                        pc_lim_low = 0.2
                        pc_lim_high = 0.4
                        # Phase correction and transform to transmission
                        # daq_file.data_raw => phase correct and sum raw IGMs
                        # daq_file.data_pc => phase correct and sum PC data=
                        if fitallan:
                            if process_raw:
                                pc_num = pc_time * daq_file.dfr / daq_file.num_hwavgs
                                if ig_num == 0 or ig_num > len(daq_file.data_raw):
                                    n_pc = np.ceil((len(daq_file.data_raw) / pc_num))
                                else:
                                    n_pc = ig_num
                            elif process_pc:
                                pc_num = pc_time / daq_file.pc_time
                                if ig_num == 0 or ig_num > len(daq_file.data_pc):
                                    n_pc = np.ceil((len(daq_file.data_pc) / pc_num))
                                else:
                                    n_pc = ig_num
                        else:
                            n_pc = 1

                        for i in np.arange(n_pc):
                            print(str(int(i + 1)) + ' out of ' + str(int(n_pc)) + ' fits\n')
                            if fitallan:
                                if process_raw:
                                    if i == (n_pc - 1):
                                        if n_pc * pc_num > len(daq_file.data_raw):
                                            modulo = len(daq_file.data_raw) % pc_num
                                            data_ig = daq_file.data_raw[int(pc_num * i):int(pc_num * i + modulo)]
                                        else:
                                            data_ig = daq_file.data_raw[int(pc_num * i):int(pc_num * (i + 1))]
                                    else:
                                        data_ig = daq_file.data_raw[int(pc_num * i):int(pc_num * (i + 1))]
                                if process_pc:
                                    if i == (n_pc - 1):
                                        if n_pc * pc_num > len(daq_file.data_pc):
                                            modulo = len(daq_file.data_pc) % pc_num
                                            data_ig = daq_file.data_pc[int(pc_num * i):int(pc_num * i + modulo)]
                                        else:
                                            data_ig = daq_file.data_pc[int(pc_num * i):int(pc_num * (i + 1))]
                                    else:
                                        data_ig = daq_file.data_pc[int(pc_num * i):int(pc_num * (i + 1))]
                            else:
                                if process_raw:
                                    data_ig = daq_file.data_raw
                                elif process_pc:
                                    data_ig = daq_file.data_pc

                            p2p.append((np.max(data_ig[0]) - np.min(data_ig[0])) / (daq_file.num_hwavgs))

                            # make sure ig is aligned with center
                            peak = np.argmax(abs(data_ig[0]))
                            data_ig = np.roll(data_ig, int(len(data_ig[0])/2)-peak)
                            data = data_ig.copy()
                            trans = pld.pc_peter(data, pc_lim_low, pc_lim_high, plot=True,
                                                 zoom=500)
                            pc_ig = np.fft.ifft(trans)
                            # data = data_ig.copy()
                            # pc_ig = pld.pc_truncated(data, pc_lim_low, pc_lim_high)
                            # trans = np.abs(np.fft.fft(pc_ig))
                            plt.figure()
                            plt.plot(pc_ig)
                            # p2p.append((np.max(pc_ig)-np.min(pc_ig))/(pc_num*daq_file.num_hwavgs))

                            # trans = np.abs(np.fft.fft(pc_ig))

                            # Create frequency axis
                            if importwvn:
                                data_import = np.loadtxt(
                                    os.path.join(d_import, fname_import+ '_' + str(int(i)) + '_trans.txt'))
                                x_wvn_full = data_import[:,0]
                            else:
                                x_wvn_full = pld.mobile_axis2(
                                    daq_file, f_opt=lockfreq, wvn_spectroscopy=nomfreq)
                                # Convert interesting portion of spectrum to time-domain
                            start_pnt, stop_pnt = td.bandwidth_select_td(x_wvn_full, band_fit)
                            trans_orig = copy(trans)

                            data_stop = min([len(x_wvn_full), len(trans)])
                            if savedata:
                                if len(x_wvn_full) > len(trans_orig):
                                    np.savetxt(os.path.join(savepath, name + '_' + str(int(i)) + '_trans.txt'),
                                               np.array([x_wvn_full[:len(trans_orig)], trans_orig]).T)
                                else:
                                    np.savetxt(os.path.join(savepath, name + '_' + str(int(i)) + '_trans.txt'),
                                               np.array([x_wvn_full, trans_orig[:len(x_wvn_full)]]).T)
                            # if savedata:
                            # np.savetxt(os.path.join(savepath, name+'_trans.txt'),
                            #            np.array([x_wvn_full[:len(trans_orig)], trans_orig]).T)

                            plt.figure();
                            plt.plot(x_wvn_full[:data_stop], trans[:data_stop])
                            plt.title(fname)
                            if start_pnt < stop_pnt:
                                # Normal setup
                                trans = trans[start_pnt:stop_pnt]
                                x_wvn = x_wvn_full[start_pnt:stop_pnt]
                                y_td = np.fft.irfft(-np.log(trans))
                            else:
                                # DCS in 0.5-1.0 portion of Nyquist window, need to flip x-axis to fit
                                trans_flipped = trans[int((daq_file.frame_length / 2)):: -1]
                                x_wvn_flipped = x_wvn_full[::-1]
                                start_pnt, stop_pnt = td.bandwidth_select_td(
                                    x_wvn_flipped, band_fit
                                )
                                x_wvn = x_wvn_flipped[start_pnt:stop_pnt]
                                trans = trans_flipped[start_pnt:stop_pnt]
                                y_td = np.fft.irfft(-np.log(trans))

                            plt.figure();
                            plt.plot(x_wvn, trans)
                            plt.title(fname)
                            if fitdata:
                                # fit
                                tic = time.time()  # time-domain-specific fitting starts here
                                Fit = FitFunc(x_wvn, y_td, bl, etalons, db_name1,
                                              modfull, pars, p2p, name, x_wvn_full,
                                              trans_orig, trans, daq_file,
                                              savedata=savedata, savename=savename,
                                              savename_td=savename_td,
                                              savename_freq=savename_freq,
                                              savepath=savepath,
                                              band_fit=band_fit,
                                              plot_fit_td=plot_fit_td, plot_fit_freq=plot_fit_freq)
                                toc = time.time()  # time-domain-specific fitting starts here
                                print('Fitting time = ' + str(toc - tic))
                                # store fits in arrays
                                concs.append(Fit.best_values['h2omolefraction'])
                                temps.append(Fit.best_values['h2otemperature'])
                                press.append(Fit.best_values['h2opressure'])
                                pathlengths.append(Fit.best_values['h2opathlength'])
                                shifts.append(Fit.best_values['h2oshift'])
    else:
        i = 0
        data_import = np.loadtxt(os.path.join(savepath, 'fitdata_freq_' + fname + '_' + str(i) + '.txt'))
        data_abs = data_import[:, 0]
        trans = np.exp(-data_abs)
        data_ig = np.fft.ifft(trans)
        p2p.append((np.max(data_ig[0]) - np.min(data_ig[0])) / 50)

        x_wvn_full = data_import[:, 2]

        # Convert interesting portion of spectrum to time-domain
        start_pnt, stop_pnt = td.bandwidth_select_td(x_wvn_full, band_fit)
        trans_orig = copy(trans)
        if start_pnt < stop_pnt:
            # Normal setup
            trans = trans[start_pnt:stop_pnt]
            x_wvn = x_wvn_full[start_pnt:stop_pnt]
            y_td = np.fft.irfft(-np.log(trans))
        else:
            # DCS in 0.5-1.0 portion of Nyquist window, need to flip x-axis to fit
            trans_flipped = trans[::-1]
            x_wvn_flipped = x_wvn_full[::-1]
            start_pnt, stop_pnt = td.bandwidth_select_td(
                x_wvn_flipped, band_fit
            )
            x_wvn = x_wvn_flipped[start_pnt:stop_pnt]
            trans = trans_flipped[start_pnt:stop_pnt]
            y_td = np.fft.irfft(-np.log(trans))

        tic = time.time()  # time-domain-specific fitting starts here
        Fit = FitFunc(x_wvn, y_td, bl, etalons, mod, db_name1,
                      savedata=savedata, savename=savename,
                      savename_td=savename_td,
                      savename_freq=savename_freq,
                      plot_fit_td=plot_fit_td, plot_fit_freq=plot_fit_freq)
        toc = time.time()  # time-domain-specific fitting starts here
        print('Fitting time = ' + str(toc - tic))
        # store fits in arrays
        concs.append(Fit.best_values['h2omolefraction'])
        temps.append(Fit.best_values['h2otemperature'])
        press.append(Fit.best_values['h2opressure'])
        pathlengths.append(Fit.best_values['h2opathlength'])
        shifts.append(Fit.best_values['h2oshift'])

    if fitdata and savedata:
        np.savetxt(os.path.join(savepath, name + '_xwvnfull.txt'), x_wvn_full)
        np.savetxt(os.path.join(savepath, name + '_conc.txt'), concs)
        np.savetxt(os.path.join(savepath, name + '_pathlength.txt'), pathlengths)
        np.savetxt(os.path.join(savepath, name + '_temp.txt'), temps)
        np.savetxt(os.path.join(savepath, name + '_pres.txt'), press)
        np.savetxt(os.path.join(savepath, name + '_shift.txt'), shifts)

if __name__ == "__main__":
    day = '031623'  # can be either 031423 or 031623 (MMDDYY)
    importdata = False
    processone = True
    fitdata = False

    # Save info: flag, file name, directory
    savedata = True  # flag (0 = no save; 1 = save)
    process_raw = 1
    process_pc = 0
    # fit individual igs for allan deviation calculation later
    fitallan = True  # will work with only raw data
    pc_time = 0.1  # how much time (s) to fit for fit allan
    ig_num = 0  # how many averaged igs to process, if 0 or greater than available igs just do all
    # import information from key file
    contents = []
    with open('FilenameKey_'+day+'.txt', 'r') as f:
        for i, line in enumerate(f.readlines()):
            if i==1:
                d_up = line[:-1]
                d_up_save = os.path.join(d_up, 'fit', 'dg')
            elif i==3:
                d_down = line[:-1]
                d_down_save = os.path.join(d_down, 'fit', 'dg')
            elif i==4:
                header = line.split()
            elif i>4:
                contents.append(line.split())

    df_key = pd.DataFrame(data=contents, columns=header)

    pld.db_begin('')  # point HAPI (nested function in fit) to linelist file

    t0 = time.time()
    files_up = pld.open_dir(d_up, recursion_levels=2, verbose=2)
    files_down = pld.open_dir(d_down, recursion_levels=2, verbose=2)
    dt = time.time() - t0
    print("Opened %i files in %0.3f seconds" % (len(files_up)+len(files_down), dt))
    newest = ""

    for i, row in df_key.iterrows():
        if row['type'] == 'dist':
            # fit upstream
            FitOneBeam(row['fn_up'], d_up_save, files_up, molecule='H2O_PaulLF2',
                       band_fit=[6810, 7188], bl=200, etalons=[],
                       importdata=False, importwvn=False)
            # fit downstream
            FitOneBeam(row['fn_down'], d_down_save, files_down, molecule='H2O_PaulLF2',
                       band_fit=[6810, 7188], bl=200, etalons=[],
                       importdata=False,
                       importwvn=True, d_import=d_up_save, fname_import=row['fn_up'])
        else:
            continue

    print("Total total runtime %d seconds" % (time.time() - t0))