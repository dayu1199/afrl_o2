"""
PlotAllanDev

Created by on 6/23/22 by david

Description: Takes in a dataframe of fits and performs an allan deviation

"""
#%% import modules
# external
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
plt.rc('axes.formatter', useoffset=False)
import seaborn as sns
import pandas as pd
import scipy

# internal
import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

#%% parameters
runs = ['AP','AQ']

fn_allan = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                        r'Mar2022CampaignData/Fits/retro_allan',
                        r'AllanDeviationsRetro.xlsx')

for label in runs:
    #%% pull in time-resolved info
    df_time = pd.read_excel(fn_allan, sheet_name=label + ' Resolved')

    # time as a column
    time = 10*np.array(range(len(df_time['Temp. (K)'])))
    time = list(time)
    df_time['Time (s)'] = time

    #%% plot time-resolved data
    fig, axes = plt.subplots(2, 2)
    fig.suptitle(label)

    # time-resolved velocity
    sns.regplot(ax=axes[0, 0], data=df_time, x='Time (s)', y='Vel. (m/s)')
    r, p = scipy.stats.pearsonr(df_time['Time (s)'], df_time['Vel. (m/s)'])
    axes[0, 0].text(50, df_time['Vel. (m/s)'].max()-300, '$r^2$ = '+str(round(r**2, 3)))

    # time-resolved temperature
    b = sns.regplot(ax=axes[0, 1], data=df_time, x='Time (s)', y='Temp. (K)')
    r, p = scipy.stats.pearsonr(df_time['Time (s)'], df_time['Temp. (K)'])
    axes[0, 1].text(50, df_time['Temp. (K)'].max()-200, '$r^2$ = ' + str(round(r ** 2, 3)))
    #%% pull in allan deviation
    df_vel_allan = pd.read_excel(fn_allan,
                                  sheet_name=label + ' Allan',
                                  usecols='F:I',
                                  header=1,
                                 nrows=4)
    tau_vel = df_vel_allan['Tau (s).1'].values
    sigma_vel = df_vel_allan['Sigma.1'].values
    sigmaCI_vel = [df_vel_allan['Sigma Min..1'].values,
                df_vel_allan['Sigma Max..1'].values]


    df_temp_allan = pd.read_excel(fn_allan,
                                  sheet_name=label+' Allan',
                                  usecols='A:D',
                                  header=1,
                                  nrows=4)
    tau_temp = df_temp_allan['Tau (s)'].values
    sigma_temp = df_temp_allan['Sigma'].values
    sigmaCI_temp = [df_temp_allan['Sigma Min.'].values,
                   df_temp_allan['Sigma Max.'].values]

    #%% calculate expected precision at
    #%% Plot allan deviations
    # plot  Allan
    axes[1, 0].set_yscale('log')
    axes[1, 0].set_xscale('log')
    axes[1, 0].errorbar(tau_vel, sigma_vel, yerr=sigmaCI_vel)
    axes[1, 0].set_ylabel('Vel. Allan (m/s)')
    axes[1, 0].set_xlabel('Tau (s)')
    axes[1, 0].xaxis.set_minor_formatter(mticker.ScalarFormatter())

    # plot Velocity Allan
    axes[1, 1].set_yscale('log')
    axes[1, 1].set_xscale('log')
    axes[1, 1].errorbar(tau_temp, sigma_temp, yerr=sigmaCI_temp)
    axes[1, 1].set_ylabel('Temp. Allan (K)')
    axes[1, 1].set_xlabel('Tau (s)')
    axes[1, 1].xaxis.set_minor_formatter(mticker.ScalarFormatter())