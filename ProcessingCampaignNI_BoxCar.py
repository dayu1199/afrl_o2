#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code to produce tranmission files from NI DAQs from AFRL March campaign

Created on Thu Mar 24 17:18:59 2022

@author: david
"""

#   Package imports

import os
import time
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from packfind import find_package

find_package("pldspectrapy")
import pldspectrapy as pld

find_package("pldspectrapy1")
import pldspectrapy1 as pld1
import td_support as td

# Plot params
plt.rcParams.update({'font.family': 'Times New Roman',
                     "figure.autolayout": True,
                     "lines.linewidth": 0.8})
# Code switches and params
processone = True
pull_axis = True
pclim_low = 0.12
pclim_high = 0.18
savedata = True
plotdata = False
fitallan = True
ig_num = 0  # how many averaged igs to process, if 0 or greater than available igsjust do all
pc_time = 200

# directory info
run = '11_2'
daq_name = 'nist'  # nist, cu small, or cu big
d = r'/Volumes/UCFDataMac/O2March2022Campaign/nist/031122_vis2'
dsub_moose = 'pc_peter' #subfolder to get freq axis from moose files

# Frequency axis function
def freq_axis_mobile_comb(v_CW, frc, fr2, n_pnts, f_opt, wvn_spectroscopy):
    """
    Calculates frequency axis from f-2f self-referenced mobile frequency combs.

    INPUTS:
        v_CW          = (THz) frequency of MyRio CW reference laser
        frep_Clocked  = (Hz)  pulse repetition rate of comb into DAQ
        frep_Other    = (Hz)  pulse repetition rate of unclocked comb
        Nyq           =       distance in 1/2-integers from CW laser to edge of Nyquist window
                              eg if v_CW = 200THz and window from 205-206THz, then Nyq = 2.5
    OUTPUTS:
       x_freq         = (cm-1) frequency of each comb tooth

       !!!!!!!!!!!
       Nate is not sure that this calculation is correct
       !!!!!!!!!!!
    """
    SPEED_OF_LIGHT = 299792458
    M_2_CM = 100
    # Calculate CW laser frequency from individual comb f_reps, IG length, optical beat offset
    f_cw_approx = v_CW  # approximate CW reference laser frequency (Hz)
    df_tooth = 0.5 * (frc + fr2)
    df_nyq = n_pnts * fr2
    nyq_cw = np.argmin(np.abs(f_cw_approx - df_nyq * np.asarray(range(20))))
    f_cw = df_nyq * nyq_cw + f_opt
    print("CW laser =", f_cw, "Hz")

    # determine Nyquist window of spectroscopy
    f_spectroscopy = wvn_spectroscopy * SPEED_OF_LIGHT * M_2_CM
    nyq, sign = divmod(f_spectroscopy, df_nyq)
    if sign / df_nyq < 0.5:
        flip_spectrum = False
    else:
        nyq += 1
        flip_spectrum = True

    # Last make frequency axis array
    x_hz = np.arange(n_pnts / 2 + 1)
    if flip_spectrum:
        x_hz *= -1
    x_hz = x_hz * df_tooth
    x_hz += nyq * df_nyq - f_opt
    x_wvn = x_hz / SPEED_OF_LIGHT / M_2_CM

    return x_wvn

# open up directory #this is just to get the file names
t0 = time.time()
files = pld.open_dir(d, recursion_levels=2, verbose=2)
dt = time.time() - t0
print("Opened %i files in %0.3f seconds" % (len(files), dt))
newest = ""

# get filename key info
d_key = r'/Users/david/Library/CloudStorage/GoogleDrive-dayu1199@colorado.edu/My Drive/AFRL ETHOS/MarchCampaign'
fn_key = 'FilenameKey.xlsx'
key_df = pd.read_excel(os.path.join(d_key,fn_key))
d_moose = r'/Users/david/Library/CloudStorage/GoogleDrive-dayu1199@colorado.edu/My Drive/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/moose'

# create save directory
print(str(pc_time) + ' s')

d_save = os.path.join(d, run + '_boxcar')
if not os.path.isdir(d_save):
    os.mkdir(d_save)
d_save = os.path.join(d_save, run + '_' + str(pc_time) + 's')
if not os.path.isdir(d_save):
    os.mkdir(d_save)
if fitallan:
    d_save = os.path.join(d_save, 'time_resolved')
    if not os.path.isdir(d_save):
        os.mkdir(d_save)
    d_save = os.path.join(d_save, run)
    if not os.path.isdir(d_save):
        os.mkdir(d_save)

for name, daq_file in files.items():
    pass_raw = 0
    if processone:
        if name != run:
            continue
    print(name)

    # savename
    fn_save = name+'_trans.txt'

    # check if already processed
    if os.path.exists(os.path.join(d_save, fn_save)):
        print('IG already processed in ' + d_save)
        continue

    IG = pld1.open_daq_files(os.path.join(d, name))

    # Are there raw IGMs?
    if IG.data_raw is not None:
        non_empty_raw = len(IG.data_raw)
        pass_raw = 1 if (non_empty_raw > 0) else 0

    # As long as there's some IGMs go ahead and start processing!
    if pass_raw:
        newest = max(newest, name)

        # create wavenumber axis
        if pull_axis:
            # search for column with both correct daq and filename
            row = key_df.loc[(key_df['vis2 DAQ']==daq_name) &\
                             (key_df['vis2 fn']==name)]
            if len(row) == 0:
                row = key_df.loc[(key_df['box DAQ']==daq_name) &\
                                 (key_df['box fn']==name)]
            if len(row) == 0:
                print('No moose axis found for this file. Used default axis')
                fr_correct = -200
                v_CW = 191.5014207
                fr1 = 200010295 + fr_correct
                fr2 = 200010921.693 + fr_correct
                f_opt = 32e6
                n_pnts = 319152
                wvn_spectroscopy = 13333
                x_wvn_full = freq_axis_mobile_comb(v_CW, fr1, fr2, n_pnts, f_opt,
                                                   wvn_spectroscopy)
            else:
                fn_axis = str(row.iloc[0]['vis3 fn']) + '_trans.txt'
                axis_found = False
                # search through moose directory for freq axis
                for folder in os.listdir(d_moose):
                    if os.path.isdir(os.path.join(d_moose,folder)):
                        for sub_folder in os.listdir(os.path.join(d_moose,folder)):
                            if os.path.isdir(os.path.join(d_moose,folder,sub_folder)):
                                for file in os.listdir(os.path.join(d_moose,folder,
                                                                    sub_folder)):
                                    if file == fn_axis:
                                        data_trans = np.loadtxt(os.path.join(d_moose,
                                                                             folder,
                                                                             sub_folder,
                                                                             file))
                                        x_wvn_full = data_trans[:,0]
                                        axis_found = True
                                        break
                            if axis_found:
                                break
                        if axis_found:
                            break
                if not axis_found:
                    print('Axis not Found!!')
        else:
            fr_correct = -200
            v_CW = 191.5014207
            fr1 = 200010295 + fr_correct
            fr2 = 200010921.693 + fr_correct
            f_opt = 32e6
            n_pnts = 319152
            wvn_spectroscopy = 13333
            x_wvn_full = freq_axis_mobile_comb(v_CW, fr1, fr2, n_pnts, f_opt,
                                               wvn_spectroscopy)

        if fitallan:
            pc_num = pc_time * daq_file.dfr / daq_file.num_hwavgs
            if ig_num == 0 or ig_num > len(daq_file.data_raw):
                n_pc = np.ceil((len(daq_file.data_raw) / (pc_num/2)))
            else:
                n_pc = ig_num
            print('n_pc = ', str(n_pc))
        else:
            n_pc = 1

        for i in np.arange(n_pc):
            # check if pc already exists
            if fitallan:
                fn_save = name + '_trans' + str(int(i)) + '.txt'
            else:
                fn_save = name + '_trans.txt'

            # check if already processed
            if os.path.exists(os.path.join(d_save, fn_save)):
                print('IG already processed in ' + d_save)
                continue

            print(str(int(i + 1)) + ' out of ' + str(int(n_pc)) + ' fits\n')
            if fitallan:
                if i == (n_pc - 1):
                    if n_pc * pc_num > len(daq_file.data_raw):
                        continue
                        # modulo = len(IG.data_raw) % pc_num
                        # data_ig = IG.data_raw[int(pc_num * i):
                                              # int(pc_num * i + modulo)]
                    else:
                        data_ig = IG.data_raw[int(pc_num * i/2):
                                              int(pc_num * (i + 2)/2)]
                else:
                    data_ig = IG.data_raw[int(pc_num * i/2):
                                          int(pc_num * (i + 2)/2)]
            else:
                data_ig = IG.data_raw
            # # phase correct IGs
            # pc_ig = pld1.Spectra(IG,
            #                      num_pcs=len(IG),
            #                      pc_lim_low=pclim_low,
            #                      pc_lim_high=pclim_high)
            # trans = np.abs(pc_ig.data[0])
            data = data_ig.copy()
            trans = pld1.pc_peter(data, pclim_low, pclim_high, plot=plotdata,
                                         zoom=500)

            lambda_full = 1e7/x_wvn_full
            if plotdata:
                fig,ax = plt.subplots(2,1)
                ax[0].plot(np.fft.irfft(trans))
                ax[0].set_xlabel('Time step')
                ax[0].set_ylabel('Power')
                ax[1].plot(lambda_full[:len(trans)],trans)
                ax[1].set_xlabel('Wavelength (cm-1)')
                ax[1].set_ylabel('Transmission')
                plt.suptitle(name)
            # save file
            save_data = np.vstack((x_wvn_full[:len(trans)],trans))
            save_data = np.transpose(save_data)
            np.savetxt(os.path.join(d_save,fn_save),save_data)
    else:
        print('No raw data')

print('Phase Correction Done!')