#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code to fit data in house data sets from before March Campaign

Created on Thu Mar 31 11:41:39 2022

@author: david
"""

import os
from openpyxl import Workbook, load_workbook
import pandas as pd
import pickle
from datetime import datetime

import FitProgram1Beam as fithit

##################################Fitting Parameters###########################

# Fitting Options
save_files = False # saves fits and fitted parameters in text files.
plot_results = True # Plots time domain and TD fit converted to the frequency
                    # domain. Turn this off for many datafiles.
print_fit_report = True # Prints each fit report to command window 
back_remove = False # Remove background from spectra before fitting
pull_bandweight = True # get band_fit and weight start from FittingInfo.xlsx
# Directory Info
d_data = os.path.join(r'/Users/david/Library/CloudStorage',
                      r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                      r'AFRL ETHOS (DO NOT SYNC)/VoigtvsSD')
d_save = os.path.join(r'/Users/david/Library/CloudStorage',
                      r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                      r'AFRL ETHOS (DO NOT SYNC)/VoigtvsSD/Voigt')
file_tag = '' # remember to add a _ before

# indexes = {5,6,7,8,9,10,'11_2',12,13,14,15,16,17,18,19}
indexes = {'20220117161157'}

# MFID Parameters
weight_window = [0.01,0.5] # default [0.005,0.5]
band_fit = [13010, 13180]
weight_flat = True # Otherwise will do an exponential weighting
etalon_windows = [] #    # etalon_windows = [[510, 530]]
weight_flat = True # Otherwise will do an exponential weighting
# Fitting Parameters
angle = 0
molec_to_fit = ['O2_Vgt'] # must be a list
isotopes = [[1]] # format [[1],[2]]
press_guess = 0.83 # atm
temp_guess = 290
vel_guess = 0
shift_guess = 0
conc_guess = 0.201
broad_guess = 1
pathlength_guess = 35 # theta correction happens in FitProgram
temp_fit = True
press_fit = False
shift_fit = True
vel_fit = True
conc_fit = False
broad_fit = False
pl_fit = True

# Background Fitting Parameters
temp_back = 296.00000
shift_back = 0
press_back = 0
label_back = ''

if not(back_remove):
    conc_back = 0
    shift_back = 0
    press_back = 0

###############################Port in data###################################
# get files from directory
files = os.listdir(d_data)
 
# prepare arrays for dataframe
fn = []
vel = []
vel_unc = []
temp = []
temp_unc = []
press = []
press_unc = []
shift = []
shift_unc = []
conc = []
conc_unc = []
pl = []
pl_unc = []

for index in indexes:
    file = index + '_trans.txt'

    fn.append(file)


    vel_val, vel_err, temp_val, temp_err, press_val, press_err, shift_val,\
        shift_err, conc_val, conc_err, broad_val, broad_err, pl_val, pl_err = \
            fithit.FitProgram(file, d_data, d_save, file_tag,
                              weight_window, weight_flat, etalon_windows,
                              angle, band_fit,
                              molec_to_fit, isotopes, 
                              press_fit, temp_fit, vel_fit,
                              shift_fit, conc_fit, broad_fit, pl_fit,
                              press_guess, temp_guess, vel_guess,
                              shift_guess, conc_guess, broad_guess,
                              pathlength_guess,
                              save_files=save_files,
                              plot_results=plot_results,
                              background_remove=back_remove)
    vel.append(vel_val)
    vel_unc.append(vel_err)
    temp.append(temp_val)
    temp_unc.append(temp_err)
    press.append(press_val)
    press_unc.append(press_err)
    shift.append(shift_val)
    shift_unc.append(shift_err)
    conc.append(conc_val[0])
    conc_unc.append(conc_err[0])
    pl.append(pl_val)
    pl_unc.append(pl_err)

# Create dict of results
d_fit = {'Fn': fn,
     'PL (cm)': pl,
     'PL Unc.': pl_unc,
     'Vel. (m/s)': vel,
     'Vel. Unc.': vel_unc,
     'Temp. (K)': temp,
     'Temp. Unc.': temp_unc,
     'Pres. (atm)': press,
     'Pres. Unc.': press_unc,
     'Shift (cm-1)': shift,
     'Shift Unc.': shift_unc,
     'Conc.': conc,
     'Conc. Unc': conc_unc}

df_fit = pd.DataFrame.from_dict(d_fit)
if save_files:
    pickle.dump(df_fit, open(os.path.join(d_save,'df_fit.p'),'wb'))

print('Fitting Done!')