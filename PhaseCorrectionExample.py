import numpy as np
import matplotlib.pyplot as plt

#%% inputs

data = np.load('IGs for David.npy')
ppIG = 16161

#%% phase correction

N_zoom = 200 # how many points to look at for phase correction

ref = data[0, ppIG // 2 - N_zoom: ppIG // 2 + N_zoom + 1]
ftref = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(ref)))
ftref = np.pad(ftref, ([2 ** 11, 2 ** 11]), constant_values=0)

omega = np.fft.fftfreq(ppIG) * 2 * np.pi

shift = np.zeros((len(data[:,0])))

for n, data_n in enumerate(data[::]):
        
    # zoom in and fft 400 points around the centerburst
    zoom = data_n[ppIG // 2 - N_zoom: ppIG // 2 + N_zoom + 1]
    zoom = zoom - np.mean(zoom)
    fft = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(zoom)))

    # filter out f0
    fft[np.argmax(fft[:len(fft) // 2].__abs__())] = 0.0
    fft[np.argmax(fft[len(fft) // 2:].__abs__()) + len(fft) // 2] = 0.0

    # calculate the cross-correlation and its maximum
    fft = np.pad(fft, ([2 ** 11, 2 ** 11]), constant_values=0)
    fft *= ftref.conj()
    fft = np.fft.fftshift(np.fft.ifft(np.fft.ifftshift(fft)))
    shift[n] = (np.argmax(fft.real) - len(fft) // 2) * len(zoom) / len(fft)

    # shift the data using fft
    fft = np.fft.fft(np.fft.ifftshift(data[n]))
    fft *= np.exp(1j * omega * shift[n])
    data[n] = np.fft.fftshift(np.fft.ifft(fft)).real

    # progress update
    if n % 100 == 0:
        print(f'{n} / {len(data)}')
        
IG_avg = np.mean(data, 0)
meas_avg = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(IG_avg))).__abs__()
i_center = int((len(meas_avg)-1)/2)
meas_avg = meas_avg[i_center:] # remove reflected portion
        
# %% figures that would be helpful for visualization

plt.figure()
plt.plot(shift)

plt.figure()
plt.plot(IG_avg)
plt.figure()
plt.plot(meas_avg) # our CEO beats are huge


