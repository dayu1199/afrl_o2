"""
PlotFacilityData

Created by on 7/8/22 by david

Description: Plot data from facility spreadsheets

"""

#%% import modules
import matplotlib.pyplot as plt
import seaborn as sns
import os
import pandas as pd
import numpy as np

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

#%% pull data
d_facility = os.path.join(r'/Volumes/GoogleDrive/My Drive',
                          r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData',
                          r'F22070_Steven/excel')
fn = 'F22070_AK_Steven.xlsx'

df_facility = pd.read_excel(os.path.join(d_facility, fn))

#%% cut dataframe
start = 465
end = 3466

df_cut = df_facility.iloc[range(start, end)]
df_cut = df_cut[[' UTILITY::LOGGING_REC_NUM',
                 ' PSI::PSI_OC_-5.250_045',
                 ' PSI::PSI_OC_-5.250_225',
                 ' PSI::PSI_OC_-4.000_045',
                 ' PSI::PSI_OC_-4.000_225',
                 ' PSI::PSI_OC_-2.750_045',
                 ' PSI::PSI_OC_-2.750_225']]
df_cut['Time (s)'] = (df_cut[' UTILITY::LOGGING_REC_NUM']-start)*0.2
df_cut = df_cut.drop(' UTILITY::LOGGING_REC_NUM', axis=1)
df_cut = df_cut.set_index('Time (s)')

# calculate overall average
pres_mean = df_cut.mean(axis=1)
df_cut['Pressure Average'] = pres_mean

# calculate rolling average
N = len(df_cut['Pressure Average'])
pres_roll = []
for i in range(N):
    pres_roll.append(df_cut['Pressure Average'].iloc[range(i)].mean(0))
df_cut['Pressure Rolling Average'] = pres_roll

#%% plot pressure values
plt.figure()
sns.scatterplot(data=df_cut)

# plot just average
plt.figure()
sns.scatterplot(data=df_cut, x='Time (s)', y='Pressure Average')
sns.scatterplot(data=df_cut, x='Time (s)', y='Pressure Rolling Average')