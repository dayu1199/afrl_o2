#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Fit simulation of box data

Created on Tue May 10 11:12:14 2022

@author: david
"""

import os
import numpy as np
import pandas as pd
import pickle
from datetime import datetime

import FitProgram1Beam as fithit

import matplotlib.pyplot as plt
import seaborn as sns
import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()
##################################Fitting Parameters###########################

# Fitting Options
save_files = True # saves fits and fitted parameters in text files.
plot_results = False # Plots time domain and TD fit converted to the frequency domain. Turn this off for many datafiles.
print_fit_report = True # Prints each fit report to command window 
back_remove = False # Remove background from spectra before fitting

noise_add = True
noise_level = 0.0025
iterations = 100

# Directory Info
# Directory Info
d_data = os.path.join(r'/Users/david/Library/CloudStorage',
                      r'GoogleDrive-dayu1199@colorado.edu/My Drive'
                      r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/cubig/031122/pc_conjugate')
d_save = os.path.join(r'/Users/david/Library/CloudStorage',
                      r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                      r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits/sim/box')
file_tag = ''

# MFID Parameters
weight_window = [0.00874,0.5] # default [0.005,0.5]
etalon_windows = [] # format [[0,100],[200,300]]
band_fit = [13015.1, 13172.4]
weight_flat = True # Otherwise will do an exponential weighting
    
# Fitting Parameters
angle = 0
molec_to_fit = ['O2'] # must be a list
isotopes = [[1]] # format [[1],[2]]
press_guess = 1 # atm
temp_guess = 250
vel_guess = 0
shift_guess = 0
conc_guess = 0.201
broad_guess = 1
pathlength_guess = 35 # theta correction happens in FitProgram
temp_fit = True
press_fit = False
shift_fit = True
vel_fit = False
conc_fit = True
broad_fit = False
pl_fit = False

# Background Fitting Parameters
temp_back = 296.00000
shift_back = 0
press_back = 0
label_back = ''

if not(back_remove):
    conc_back = 0
    shift_back = 0
    press_back = 0
    
# Prepare save folder
now = datetime.now()
timestamp = now.strftime('%d_%m_%Y_%H%M%S')
save_path_0 = os.path.join(d_save,timestamp)
if not(os.path.isdir(save_path_0)):
    os.mkdir(save_path_0)
    
###############################Port in data###################################
 
# prepare arrays for dataframe
fn = []
vel = []
vel_unc = []
temp = []
temp_unc = []
press = []
press_unc = []
shift = []
shift_unc = []
conc = []
conc_unc = []
pxlt = []
# broad = []
# broad_unc = []
# pl = []
# pl_unc = []

# organization of fits
for i in range(iterations):
    print('iteration',i)
    file = 'O2simulation_box.txt'
    save_path = os.path.join(save_path_0,str(i))
    if not(os.path.isdir(save_path)):
        os.mkdir(save_path)
    save_path = save_path
    
    vel_val, vel_err, temp_val, temp_err, press_val, press_err, shift_val,\
        shift_err, conc_val, conc_err, broad_val, broad_err, pl_val, pl_err = \
            fithit.FitProgram(file, '', save_path, file_tag,
                              weight_window, weight_flat, etalon_windows,
                              angle, band_fit,
                              molec_to_fit, isotopes, 
                              press_fit, temp_fit, vel_fit,
                              shift_fit, conc_fit, broad_fit, pl_fit,
                              press_guess, temp_guess, vel_guess,
                              shift_guess, conc_guess, broad_guess,
                              pathlength_guess,
                              save_files=save_files,
                              plot_results=plot_results,
                              background_remove=back_remove,
                              transUnits=False,
                              noise_add=noise_add, noise_level=noise_level)
    vel.append(vel_val)
    vel_unc.append(vel_err)
    temp.append(temp_val)
    temp_unc.append(temp_err)
    press.append(press_val)
    press_unc.append(press_err)
    shift.append(shift_val)
    shift_unc.append(shift_err)
    conc.append(conc_val[0])
    conc_unc.append(conc_err[0])
    pxlt.append(conc_val[0]*pathlength_guess*press_val/temp_val)

# Create dict of results
d_fit = {'Vel. (m/s)': vel,
         'Vel. Unc.': vel_unc,
         'Temp. (K)': temp,
         'Temp. Unc.': temp_unc,
         'Pres. (atm)': press,
         'Pres. Unc.': press_unc,
         'Shift (cm-1)': shift,
         'Shift Unc.': shift_unc,
         'Conc.': conc,
         'Conc. Unc': conc_unc,
         'PXL': pxlt}

df_fit = pd.DataFrame.from_dict(d_fit)
if save_files:
    with open(os.path.join(save_path_0, 'df_fit.p'), 'wb') as f:
        pickle.dump(df_fit, f)
    if iterations > 1:
        if vel_fit:
            vel_std = np.std(vel)
            vel_ave = np.mean(vel)
            print('Velocity mean is '+str(vel_ave))
            print('Velocity uncertainty is '+str(vel_std))
            np.savetxt(os.path.join(save_path_0,'vel.txt'),vel)
        if conc_fit:
            conc_std = np.std(conc)
            conc_ave = np.mean(conc)
            print('Concentration mean is '+str(conc_ave))
            print('Concentration uncertainty is '+str(conc_std))
            np.savetxt(os.path.join(save_path_0,'conc.txt'),conc)
        if press_fit:
            press_std = np.std(press)
            press_ave = np.mean(press)
            print('Pressure mean is '+str(press_ave))
            print('Pressure uncertainty is '+str(press_std))
            np.savetxt(os.path.join(save_path_0,'press.txt'),press)
        if temp_fit:
            temp_std = np.std(temp)
            temp_ave = np.mean(temp)
            print('Temperature mean is '+str(temp_ave))
            print('Temperature uncertainty is '+str(temp_std))
            np.savetxt(os.path.join(save_path_0,'temp.txt'),temp)
        if shift_fit:
            shift_std = np.std(shift)
            shift_ave = np.mean(shift)
            print('Shift mean is '+str(shift_ave))
            print('Shift uncertainty is '+str(shift_std))
            np.savetxt(os.path.join(save_path_0,'shift.txt'),shift)
        if conc_fit or press_fit:
            pxlt_std = np.std(pxlt)
            pxlt_ave = np.mean(pxlt)
            print('PXLT mean is '+str(pxlt_ave))
            print('PXLT uncertainty is '+str(pxlt_std))
            np.savetxt(os.path.join(save_path_0,'pxlt.txt'),pxlt)


print('Fitting Done!')

#%% plot
# plot temperature and concentration trend
plt.figure()
sns.regplot(x=temp, y=conc)
plt.xlabel('Temperature')
plt.ylabel('Concentration')

# plot pxlt time trace
plt.figure()
plt.plot(pxlt)
plt.ylabel('PXLT')
