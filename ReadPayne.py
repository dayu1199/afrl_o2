"""
ReadPayne.py

Created by on 2/9/23 by david

Description:

"""
import pandas as pd
import pickle

fn = 'O2PayneNoSD.txt'

header = ['quanta', 'nu', 'nu_unc', 'sw', 'sw_unc',
          'gamma_air', 'gamma_air_unc', 'n_air', 'n_air_unc',
          'gamma_self', 'gamma_self_unc', 'n_self', 'n_self_unc',
          'delta_air', 'delta_air_unc', 'deltap_air', 'deltap_air_unc',
          'delta_self', 'delta_self_unc', 'deltap_self', 'deltap_self_unc']

def pull_unc(entry):
    split_dec = entry.find('.')
    split_unc = entry.find('(')
    split_unc_end = entry.find(')')
    split_sci = entry.find('E')
    unc_string = entry[split_unc+1:split_unc_end]
    exponent = -(split_unc - split_dec + len(unc_string) - 1)
    if split_sci == -1:
        if 'f' in unc_string:
            uncertainty = 'f'
        else:
            uncertainty = float(unc_string) * 10**exponent
        value = float(entry[:split_unc])
    else:
        exponent_2 = int(entry[split_sci+1:])
        if 'f' in unc_string:
            uncertainty = 'f'
        else:
            uncertainty = float(unc_string) * 10**(exponent + exponent_2)
        value = float(entry[:split_unc]) * 10**exponent_2
    return value, uncertainty

text1 = []
with open(fn, 'r') as f:
    for i, line in enumerate(f.readlines()):
        line = line.replace(' f', 'f')
        line = line.replace(' f', 'f')
        text1.append(line)

table = []
for line in text1:
    entries = line.split()
    # quanta
    quanta = entries[0] + entries[1] + entries[2]
    # nu
    nu, nu_unc = pull_unc(entries[4])
    # sw
    sw, sw_unc = pull_unc(entries[6])
    # gamma_air
    gamma_air, gamma_air_unc = pull_unc(entries[8])
    # n_air
    n_air, n_air_unc = pull_unc(entries[10])
    # gamma_self
    gamma_self, gamma_self_unc = pull_unc(entries[12])
    # n_self
    n_self_air, n_self_unc = pull_unc(entries[14])
    # delta_air
    delta_air, delta_air_unc = pull_unc(entries[16])
    # deltap_air
    deltap_air, deltap_air_unc = pull_unc(entries[18])
    # delta_self
    delta_self, delta_self_unc = pull_unc(entries[20])
    # deltap_self
    deltap_self, deltap_self_unc = pull_unc(entries[22])
    # combine into table
    table.append([quanta, nu, nu_unc, sw, sw_unc, gamma_air, gamma_air_unc,
                  n_air, n_air_unc, gamma_self, gamma_self_unc,
                  n_self_air, n_self_unc, delta_air, delta_air_unc,
                  deltap_air, deltap_air_unc, delta_self, delta_self_unc,
                  deltap_self, deltap_self_unc])

df = pd.DataFrame(table, columns=header)

with open('Payne_df.pkl', 'wb') as file:
    pickle.dump(df, file)