#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code to remove bl,etalon from data using chebyshev

Created on Tue Apr 26 11:41:38 2022

@author: david
"""

import os
import numpy as np
import matplotlib.pyplot as plt

from packfind import find_package
find_package('pldspectrapy')
import Fit_wChebBL_functions as che
import td_support as td

# import data
d = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                 'Mar2022CampaignData/cubig/031122/pc_conjugate')
fn = '6_trans.txt'
data = np.loadtxt(os.path.join(d,fn))
wvn = data[:,0]
tra = data[:,1]

# cut to fit bandwidth
band_fit = [13045, 13155] 
start, stop = td.bandwidth_select_td(wvn, band_fit)
tra_cut = tra[start:stop]
wvn_cut = wvn[start:stop]

# fit transmission with chebyshev
n_chunks = 1
n_deg = 50
bl_coefs = che.baseline_chunks_estimate(n_chunks,n_deg,tra_cut,wvn_cut)
bl = che.baseline_chunks(bl_coefs,n_chunks,n_deg,wvn_cut)

tra_nobl = tra_cut/bl
abs_nobl = -np.log(tra_nobl)
abs_nobl_cep = np.fft.irfft(abs_nobl)

# fit absorbance with chebyshev
abs0 = -np.log(tra_cut)
bl_coefs0 = che.baseline_chunks_estimate(n_chunks,n_deg,abs0,wvn_cut)
bl0 = che.baseline_chunks(bl_coefs0,n_chunks,n_deg,wvn_cut)

abs_nobl0 = abs0 - bl0
abs_nobl_cep0 = np.fft.irfft(abs_nobl0)

## plot comparison

# baselines
plt.figure()
plt.plot(wvn_cut,tra_cut,label='Transmission')
plt.plot(wvn_cut,bl,label='Baseline')
plt.ylabel('Transmission')
plt.xlabel('Wavenumber (cm-1)')
plt.legend()

plt.figure()
plt.plot(wvn_cut,abs0,label='Absorbance')
plt.plot(wvn_cut,bl0,label='Baseline')
plt.ylabel('Absorbance')
plt.xlabel('Wavenumber (cm-1)')
plt.legend()

# spectrum
plt.figure()
plt.plot(wvn_cut,abs_nobl,label='Cheb. Trans.')
plt.plot(wvn_cut,abs_nobl0,label='Cheb. Abs.')
plt.xlabel('Wavenumber (cm-1)')
plt.ylabel('Absorbance')
plt.legend()

# cepstrum
plt.figure()
plt.plot(abs_nobl_cep,label='Cheb. Trans.')
plt.plot(abs_nobl_cep0,label='Cheb. Abs.')
plt.xlabel('Time')
plt.ylabel('Cep.')
plt.legend()