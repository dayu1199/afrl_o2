"""
PlotPCTime

Created by on 7/5/22 by david

Description: Plot results of one run at different averaging times

"""

#%% import modules
import matplotlib.pyplot as plt
import os
import pickle
import seaborn as sns
import numpy as np
from scipy import stats

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

#%% pull data
# directory info
dr_fit = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
        r'Mar2022CampaignData/Fits/crossed')
time_stamp = '07_07_2022_144705'
time_stamp_fromback = '03_08_2022_191756'



d_fit = os.path.join(dr_fit, time_stamp)
d_fit_fromback = os.path.join(dr_fit, time_stamp_fromback)

runs = ['5', '6', '7', '10', '11_2']
for run in runs:
    # fill in arrays with data
    time = []
    vel = []
    temp = []

    for fn in os.listdir(os.path.join(d_fit, run)):
        if '.p' in fn:
            try:
                time.append(int(fn[-12:-9]))
            except:
                time.append(int(fn[-11:-9]))

            with open(os.path.join(d_fit, run, fn), 'rb') as f:
                df_fit = pickle.load(f)
                # plot time-resolved data if more than one fit
                # if len(df_fit.index) > 1:
                #     t = np.arange(len(df_fit.index))*time[-1]
                #     df_fit['Time (s)'] = t + time[-1]/2
                #     fig, axes = plt.subplots(1, 2)
                #     fig.suptitle(str(time[-1]) +' s')
                #     sns.regplot(ax=axes[0], x='Time (s)', y='Vel. (m/s)',
                #                     data=df_fit)
                #     sns.regplot(ax=axes[1], x='Time (s)', y='Temp. (K)',
                #                     data=df_fit)

                # take the first row and append lists
                temp.append(df_fit.iloc[0]['Temp. (K)'])
                vel.append(df_fit.iloc[0]['Vel. (m/s)'])
    fig, axes = plt.subplots(1, 2)
    fig.suptitle(run+' Averaging Time Comparison')

    sns.regplot(ax=axes[0], x=time, y=vel)
    # axes[1].scatter(time, vel)
    axes[0].set_xlabel(run + ' Averaging Time (s)')
    axes[0].set_ylabel('Velocity (m/s)')
    axes[0].set_ylim([300, 1300])
    axes[0].set_xlim([0, 600])
    sns.regplot(ax=axes[1], x=time, y=temp)
    # axes[0].scatter(time, temp)
    axes[1].set_xlabel('Averaging Time (s)')
    axes[1].set_ylabel('Temperature (K)')
    axes[1].set_ylim([200, 800])
    axes[1].set_xlim([0, 600])

    # linear regression
    m_vel, b_vel, r_vel, p_vel, std_err_vel = stats.linregress(time, vel)
    m_temp, b_temp, r_temp, p_temp, std_err_temp = stats.linregress(time, temp)

    # add equation to plot
    vel_str = 'U =' + str(round(m_vel, 2)) + 't + ' + str(round(b_vel, 2))+' m/s'
    vel_stat = 'r^2=' + str(round(r_vel**2, 3)) + ', p=' + str(round(p_vel, 4))
    axes[0].text(30, 400, vel_str, color='blue')
    axes[0].text(30, 350, vel_stat, color='blue')

    temp_str = 'T =' + str(round(m_temp, 2)) + 't + ' + str(round(b_temp, 2)) +' K'
    temp_stat = 'r^2=' + str(round(r_temp**2, 3)) + ', p=' + str(round(p_temp, 4))
    axes[1].text(30, 300, temp_str, color='blue')
    axes[1].text(30, 250, temp_stat, color='blue')

    # print('Temp. intercept is ' + str(b_temp))
    # print('Vel. intercept is ' + str(b_vel))


    ## fromback
    # fill in arrays with data
    time = []
    vel = []
    temp = []

    for fn in os.listdir(os.path.join(d_fit, run)):
        if '.p' in fn:
            try:
                time.append(int(fn[-12:-9]))
            except:
                time.append(int(fn[-11:-9]))

            with open(os.path.join(d_fit_fromback, run, fn), 'rb') as f:
                df_fit = pickle.load(f)
                # plot time-resolved data if more than one fit
                # if len(df_fit.index) > 1:
                #     t = np.arange(len(df_fit.index))*time[-1]
                #     df_fit['Time (s)'] = t + time[-1]/2
                #     fig, axes = plt.subplots(1, 2)
                #     fig.suptitle(str(time[-1]) +' s')
                #     sns.regplot(ax=axes[0], x='Time (s)', y='Vel. (m/s)',
                #                     data=df_fit)
                #     sns.regplot(ax=axes[1], x='Time (s)', y='Temp. (K)',
                #                     data=df_fit)

                # take the first row and append lists
                temp.append(df_fit.iloc[0]['Temp. (K)'])
                vel.append(df_fit.iloc[0]['Vel. (m/s)'])

    sns.regplot(ax=axes[0], x=time, y=vel, color='red')
    # axes[1].scatter(time, vel)
    axes[0].set_xlabel(run + ' Averaging Time (s)')
    axes[0].set_ylabel('Velocity (m/s)')
    axes[0].set_ylim([300, 1300])
    axes[0].set_xlim([0, 600])
    sns.regplot(ax=axes[1], x=time, y=temp, color='red')
    # axes[0].scatter(time, temp)
    axes[1].set_xlabel('Averaging Time (s)')
    axes[1].set_ylabel('Temperature (K)')
    axes[1].set_ylim([200, 800])
    axes[1].set_xlim([0, 600])

    # linear regression
    m_vel, b_vel, r_vel, p_vel, std_err_vel = stats.linregress(time, vel)
    m_temp, b_temp, r_temp, p_temp, std_err_temp = stats.linregress(time, temp)

    # add equation to plot
    vel_str = 'U =' + str(round(m_vel, 2)) + 't + ' + str(round(b_vel, 2)) + ' m/s'
    vel_stat = 'r^2=' + str(round(r_vel ** 2, 3)) + ', p=' + str(round(p_vel, 4))
    axes[0].text(300, 950, vel_str, color='red')
    axes[0].text(300, 900, vel_stat, color='red')

    temp_str = 'T =' + str(round(m_temp, 2)) + 't + ' + str(round(b_temp, 2)) + ' K'
    temp_stat = 'r^2=' + str(round(r_temp ** 2, 3)) + ', p=' + str(round(p_temp, 4))
    axes[1].text(300, 750, temp_str, color='red')
    axes[1].text(300, 700, temp_stat, color='red')
