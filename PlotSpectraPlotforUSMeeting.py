"""
Port in H2O and O2 for figure in presentation

PlotSpectraPlotforUSMeeting

Created by on 3/7/23 by david

Description:

"""

import matplotlib.pyplot as plt
import os
import pandas as pd

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

dir = os.path.join(r'/Users/david/Library/CloudStorage/OneDrive-UCB-O365',
                   'Presentations/2023_USCombustion/Figures/spectraplot')

# port in h2o
df_h2o = pd.read_csv(os.path.join(dir,
                                  'H2O_HITRAN_X=0.05_T=800K_P=0.8atm_L=6cm.csv'))

df_o2 = pd.read_csv(os.path.join(dir,
                                  'O2_HITRAN_X=0.05_T=800K_P=0.8atm_L=6cm.csv'))

plt.figure(figsize=(10,5))
plt.plot(df_h2o['nu'], df_h2o['absorbance'], color='blue', label='$H_{2}O$')
plt.plot(df_o2['nu'], df_o2['absorbance']*4, color='red', label='$O_{2}$')

plt.ylabel('Absorbance')
plt.xlabel('$Wavenumber (cm^{-1})$')
# plt.xlim([6600,7600])
plt.xlim([min(df_h2o['nu']), max(df_h2o['nu'])])
# plt.legend()