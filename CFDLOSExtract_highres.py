# -*- coding: utf-8 -*-
"""
Extract CFD LOS values from high-res CFD

Created on Tue Mar  9 16:38:29 2021

@author: David
"""
import sys
import os
import numpy as np
import pandas as pd
import pickle

import CFDTools as cfdt
import FitProgram2BeamwBox as fithit

dr = os.path.join(r'/Users/david/Library/CloudStorage',
                  r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                  r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/CFD',
                  r'DoD SAFE-FRRAspDkPmNCZy8a/RC18_CFDSET1')
dr_sim = os.path.join(r'/Users/david/Library/CloudStorage',
                  r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                  r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/CFD/Sim')
dr_fit = os.path.join(r'/Users/david/Library/CloudStorage',
                  r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                  r'AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/CFD/Fit')

fns = ['case01.interp2.dat',
       'case02.interp2.dat',
       'case03.interp2.dat',
       'case04.interp2.dat',
       'case05.interp2.dat']


uniform = False
fit = False
label = ''
if uniform:
    label += '_Uni'

# Parameters for CFD Extraction
deg = 35
n_int = 200
mol_to_use = ['O2']

# MFID Params
isotopes = [[1]]
weight_windows_y = [[0.008, 0.5],
                    [0.0084, 0.5],
                    [0.008, 0.5],
                    [0.008, 0.5],
                    [0.0082, 0.5]]  # default [0.005,0.5]
weight_windows_p = [[0.0081, 0.5],
                    [0.0083, 0.5],
                    [0.008, 0.5],
                    [0.008, 0.5],
                    [0.0085, 0.5]]  # default [0.005,0.5]
etalon_windows_y = [[[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]]]  # format [[[0.1,0.15]],[[0.22,0.25]]]
etalon_windows_p = [[[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]]]
band_fits_y = [[13016.1, 13166.7],
               [13015.2, 13168],
               [13015.2, 13169],
               [13015.2, 13170.5],
               [13015.9, 13168]]
band_fits_p = [[13016.1, 13166.9],
               [13014.3, 13167.8],
               [13014.3, 13172],
               [13015.9, 13166.1],
               [13015.1, 13168]]
weight_flat = True


# guess paramet
Pres_Guesss = [1.170, 1.371, 0.926, 1.176, 1.189]  # atm
Temp_Guess = 720
Velocity_Guess = 800
Shift_Guess = 0
Conc_Guess = 0.21
Broadening_Guess = 1

# Float paramaters
Temp_Fit = True
Pressure_Fit = False
Shift_Fit = True
Velocity_Fit = True
Conc_Fit = False
Broadening_Fit = False

height = 0
axial = -0.0986

if fit:
    # set up variables for dict
    fn_dict = []
    vel_dict = []
    velerr_dict = []
    temp_dict = []
    temperr_dict = []
    conc_dict = []
    concerr_dict = []
    pres_dict = []
    preserr_dict = []
    shift_dict = []
    shifterr_dict = []

for i in range(len(fns)):
    # parameters for specific cfd
    print(i)
    fn = fns[i]
    weight_window_y = weight_windows_y[i]
    weight_window_p = weight_windows_p[i]
    etalonWindows_y = etalon_windows_y[i]
    etalonWindows_p = etalon_windows_p[i]
    band_fit_y = band_fits_y[i]
    band_fit_p = band_fits_p[i]
    Pressure_Guess = Pres_Guesss[i]

    fn_p = fn[:6] + label + '_CFD_p.txt'
    fn_y = fn[:6] + label + '_CFD_y.txt'
    print(fn_p)
    print(fn_y)
    # # Extract Axial Slice
    cfdt.ExtractHeightSlice(height, dr, fn, label, axial)

    # Extract CFD info and create simulation
    if uniform:
        trans_p, trans_y = cfdt.LOSSpectralSimulation_Uniform(dr, fn, height, axial,
                                                      deg=deg,
                                                      v_start=13015, v_stop=13170,
                                                      n_int=n_int,
                                                      mol_to_sim=mol_to_use[0])
    else:
        trans_p, trans_y = cfdt.LOSSpectralSimulation(dr, fn, height, axial,
                                                     deg=deg,
                                                     v_start=13015, v_stop=13170,
                                                     n_int=n_int,
                                                     mol_to_sim=mol_to_use[0])
    np.savetxt(os.path.join(dr_sim, fn_p), trans_p)
    np.savetxt(os.path.join(dr_sim, fn_y), trans_y)

    if fit:
        ####################### Fit simulated spectra ###############################
        x_data_y, x_data_p, y_data_y_justflow, y_data_p_justflow, vel, velerr,\
        temp, temperr, pres, preserr, shift, shifterr, conc, concerr, \
        broad, broaderr = fithit.FitProgram(
                                os.path.join(dr_sim, fn_y), os.path.join(dr_sim, fn_p),
                                dr_fit, label,
                                weight_window_y, weight_window_p, weight_flat,
                                etalonWindows_y, etalonWindows_p, -deg, deg,
                                band_fit_y, band_fit_p, mol_to_use, isotopes,
                                Pressure_Fit, Temp_Fit, Velocity_Fit, Shift_Fit,
                                Conc_Fit, Broadening_Fit,
                                Pressure_Guess, Temp_Guess,
                                Velocity_Guess, Shift_Guess, Conc_Guess,
                                Broadening_Guess,
                                background_remove=False,
                                save_files=False, plot_results=True,
                                print_fit_report=True, transUnits=True)

        fn_dict.append(fn)
        vel_dict.append(vel)
        velerr_dict.append(velerr)
        temp_dict.append(temp)
        temperr_dict.append(temperr)
        pres_dict.append(pres)
        preserr_dict.append(preserr)
        shift_dict.append(shift)
        shifterr_dict.append(shifterr)
        conc_dict.append(conc[0])
        concerr_dict.append(concerr)

if fit:
    data_dict = {
        'Filename': fn_dict,
        'velocity (m/s)': vel_dict,
        'velocity err (m/s)': velerr_dict,
        'temperature (K)': temp_dict,
        'temperature err (K)': temperr_dict,
        'pressure (atm)': pres_dict,
        'pressure err (atm)': preserr_dict,
        'shift (cm^-1)': shift_dict,
        'shift err (cm-1)': shifterr_dict,
        'concentration': conc_dict,
        'concentration err': concerr_dict
        }

    data_df = pd.DataFrame.from_dict(data_dict)
    pickle.dump(data_df,
                open(os.path.join(dr_sim, 'simfitresults' + label+'.p'), 'wb'))