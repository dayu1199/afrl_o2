#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code to get bkgd from retro bkgd runs

Created on Mon Apr 11 10:03:31 2022

@author: david
"""
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from openpyxl import Workbook, load_workbook
import pandas as pd
import pickle
from datetime import datetime

import FitProgramBkgdRetrowBox as fithit

##################################Fitting Parameters###########################

# Fitting Options
save_files = True  # saves fits and fitted parameters in text files.
plot_results = True  # Plots time domain and TD fit converted to the frequency
# domain. Turn this off for many datafiles.
print_fit_report = True  # Prints each fit report to command window
label_back = ''
fit_box = True

# Directory Info
d_data = r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/'
d_save = r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)/Mar2022CampaignData/Fits/retro/bkgd'
d_pc = 'pc_peter'
file_tag = ''  # remember to add a _ before

# which indexes to fit, just test runs
indexes = [14, 15, 19]  # vacuum [14,19] warm [15]

# MFID Parameters
weight_windows = [[0.0093, 0.5],
                  [0.0082, 0.5],
                  [0.0081, 0.5]]  # vacuum
# weight_windows = [[0.0081,0.5]]
etalon_windows = [] # format [[0,100],[200,300]]
band_fits = [[13016.9, 13171.8],
             [13014.1, 13168.2],
             [13016.9, 13169.6]]  # vacuum
# band_fits = [[13016,13171.2]]
weight_flat = True  # Otherwise will do an exponential weighting
    
# Fitting Parameters
angle = 16.69
molec_to_fit = ['O2'] # must be a list
n_pass = 2
isotopes = [[1]] # format [[1],[2]]

pres_guess_ins = [0.188, 0.873, 0.194]  # atm vacuum
# pres_guess_ins = [0.8733]
temp_guess_ins = [300, 290, 300]  # K vacuum
# temp_guess_ins = [290]
vel_guess_ins = [0, 625, 0]  # m/s vacuum
# vel_guess_ins = [625]
conc_guess_in = 0.2095
pres_guess_out = 1  # atm
temp_guess_out = 250  # K
conc_guess_out = 0.1
broad_guess = 1
shift_guess = 0

pres_fit_in = False
temp_fit_in = False
vel_fit_in = False
conc_fit_in = False
pres_fit_out = False
temp_fit_out = True
conc_fit_out = True
pl_fit_out = False   
shift_fit = True
broad_fit = False
    
conc_guess_box = 0.00501
temp_guess_box = 281.7
pres_guess_box = 1
if fit_box:
    pl_guess_out = 38.6
    pl_box = 35
else:
    pl_box = 0
    pl_guess_out = 73.6
conc_fit_box = False
temp_fit_box = False
pres_fit_box = False
    
###############################Port in data###################################
# get files from directory
# import filename key to get filenames
d_key = r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS/MarchCampaign'
fn_key = 'FilenameKey.xlsx'
key_df = pd.read_excel(os.path.join(d_key,fn_key))
 
# prepare arrays for dataframe
fn = []
temp_in = []
temp_in_unc = []
temp_out = []
temp_out_unc = []
pres_in = []
pres_in_unc = []
pres_out = []
pres_out_unc = []
shift = []
shift_unc = []
broad = []
broad_unc = []
vel_in = []
vel_in_unc = []
pl_out = []
pl_out_unc = []
conc_in = []
conc_in_unc = []
conc_out = []
conc_out_unc = []
temp_box = []
temp_box_unc = []
pres_box = []
pres_box_unc = []
conc_box = []
conc_box_unc = []

if save_files:
    # organization of fits
    notes = input('Type down notes for this fit:')
    
    # workbook name
    name_tabulate = 'RetroFitsBkgd_v2.xlsx'
    fn_tabulate = os.path.join(d_save,name_tabulate)

    # load/create workbook
    if os.path.isfile(fn_tabulate):
        wb = load_workbook(fn_tabulate)
    else:
        wb = Workbook()
    
    # get time for labeling fit
    now = datetime.now()
    timestamp = now.strftime('%d_%m_%Y_%H%M%S')
    
    # folder to save data
    d_save_0 = os.path.join(d_save,timestamp)
    if not(os.path.isdir(d_save_0)):
        os.mkdir(d_save_0)

i = 0
for index in indexes:
    print(index)
    # Grab filenames from key
    row = key_df.loc[key_df['Indexx']==index]
    vis3_daq = str(row.iloc[0]['vis3 DAQ'])
    vis3_fn = str(row.iloc[0]['vis3 fn'])
    d_vis3 = os.path.join(d_data,vis3_daq)

    # find vis 3 data
    vis3_found = False
    # search through moose directory for freq axis
    for folder in os.listdir(d_vis3):
        for sub_folder in os.listdir(os.path.join(d_vis3,folder)):
            if os.path.isdir(os.path.join(d_vis3,folder,sub_folder)) and \
                sub_folder == d_pc:
                for file in os.listdir(os.path.join(d_vis3,folder,
                                                    sub_folder)):
                    if file == vis3_fn+'_trans.txt':
                        fn_retro = os.path.join(d_vis3,folder,sub_folder,file)
                        vis3_found = True
                        print(os.path.join(d_vis3,folder,sub_folder,file))
                        break
            if vis3_found:
                break
        if vis3_found:
            break
    if not vis3_found:
        print('vis3 file not found!')
        continue
    
    fn.append(fn_retro)
    
    # directory to save results
    if save_files:
        d_save_1 = os.path.join(d_save_0,str(index))
        if not(os.path.isdir(d_save_1)):
            os.mkdir(d_save_1)
    else:
        d_save_1 = ''
        
    #%% run Fit
    weight_window = weight_windows[i]
    band_fit = band_fits[i]
    pres_guess_in = pres_guess_ins[i]
    temp_guess_in = temp_guess_ins[i]
    vel_guess_in = vel_guess_ins[i]
    
    return_array = fithit.FitProgram(fn_retro, d_save_1, file_tag,
                              weight_window, weight_flat, etalon_windows,
                              angle, band_fit, molec_to_fit, isotopes, 
                              shift_fit, shift_guess,
                              pres_fit_in, temp_fit_in, vel_fit_in,
                              conc_fit_in,
                              pres_fit_out, temp_fit_out, conc_fit_out,
                              pl_fit_out,
                              pres_guess_in, temp_guess_in, vel_guess_in,
                              conc_guess_in,
                              pres_guess_out, temp_guess_out, conc_guess_out,
                              pl_guess_out,
                              broad_fit, broad_guess,
                              conc_guess_box, temp_guess_box, pres_guess_box,
                              n_pass=n_pass,
                              conc_fit_box=conc_fit_box,
                              temp_fit_box=temp_fit_box,
                              pres_fit_box=pres_fit_box,
                              pl_box = pl_box,
                              save_files=save_files, plot_results=plot_results,
                              print_fit_report=print_fit_report,
                              transUnits=True)
    
    temp_in.append(return_array[0])
    temp_in_unc.append(return_array[1])
    temp_out.append(return_array[2])
    temp_out_unc.append(return_array[3])
    pres_in.append(return_array[4])
    pres_in_unc.append(return_array[5])
    pres_out.append(return_array[6])
    pres_out_unc.append(return_array[7])
    shift.append(return_array[8])
    shift_unc.append(return_array[9])
    broad.append(return_array[10])
    broad_unc.append(return_array[11])
    vel_in.append(return_array[12])
    vel_in_unc.append(return_array[13])
    pl_out.append(return_array[14])
    pl_out_unc.append(return_array[15])
    conc_in.append(return_array[16])
    conc_in_unc.append(return_array[17])
    conc_out.append(return_array[18])
    conc_out_unc.append(return_array[19])
    
    temp_box.append(return_array[20])
    temp_box_unc.append(return_array[21])
    pres_box.append(return_array[22])
    pres_box_unc.append(return_array[23])
    conc_box.append(return_array[24])
    conc_box_unc.append(return_array[25])
    
    if save_files:
        # load/create worksheet
        try:
            ws = wb[str(index)]
        except:
            ws = wb.create_sheet(str(index))
            # create header
            header = ['INDEX','TIME','DAQ','Fn',
                      'WEIGHT_FLAT','WEIGHT_WIN','ETA','BW','MOLECULES',
                      'PL_OUT_FIT','PL_OUT_VAL','PL_OUT_ERR','PL_OUT_GUES',
                      'TEMP_OUT_FIT','TEMP_OUT_VAL','TEMP_OUT_ERR','TEMP_OUT_GUES',
                      'PRES_OUT_FIT','PRES_OUT_VAL','PRES_OUT_ERR','PRES_OUT_GUES',
                      'CONC_OUT_FIT','CONC_OUT_VAL','CONC_OUT_ERR','CONC_OUT_GUES',
                      'VEL_IN_FIT','VEL_IN_VAL','VEL_IN_ERR','VEL_IN_GUES',
                      'TEMP_IN_FIT','TEMP_IN_VAL','TEMP_IN_ERR','TEMP_IN_GUES',
                      'PRES_IN_FIT','PRES_IN_VAL','PRES_IN_ERR','PRES_IN_GUES',
                      'CONC_IN_FIT','CONC_IN_VAL','CONC_IN_ERR','CONC_IN_GUES',
                      'SHIFT_FIT','SHIFT_VAL','SHIFT_ERR','SHIFT_GUES',
                      'BROAD_FIT','BROAD_VAL','BROAD_ERR','BROAD_GUES',
                      'TEMP_BOX_FIT','TEMP_BOX_VAL','TEMP_BOX_ERR','TEMP_BOX_GUES',
                      'PRES_BOX_FIT','PRES_BOX_VAL','PRES_BOX_ERR','PRES_BOX_GUES',
                      'CONC_BOX_FIT','CONC_BOX_VAL','CONC_BOX_ERR','CONC_BOX_GUES',
                      'PL_BOX_VAL',
                      'NOTES']
            ws.append(header)
        # add data from current run
        datawrite = [str(index), timestamp, vis3_daq, vis3_fn, str(weight_flat),
                     str(weight_window), str(etalon_windows),
                     str(band_fit), str(molec_to_fit),
                     str(pl_fit_out), pl_out[-1], pl_out_unc[-1], pl_guess_out,
                     str(temp_fit_out), temp_out[-1], temp_out_unc[-1],
                     temp_guess_out,
                     str(pres_fit_out), pres_out[-1], pres_out_unc[-1],
                     pres_guess_out,
                     str(conc_fit_out), conc_out[-1][0], conc_out_unc[-1][0],
                     conc_guess_out,
                     str(vel_fit_in), vel_in[-1], vel_in_unc[-1], vel_guess_in,
                     str(temp_fit_in), temp_in[-1], temp_in_unc[-1], temp_guess_in,
                     str(pres_fit_in), pres_in[-1], pres_in_unc[-1], pres_guess_in,
                     str(conc_fit_in), conc_in[-1][0], conc_in_unc[-1][0],
                     conc_guess_in,
                     str(shift_fit), shift[-1], shift_unc[-1], shift_guess,
                     str(broad_fit), broad[-1], broad_unc[-1], broad_guess,
                     str(temp_fit_box), temp_box[-1], temp_box_unc[-1], temp_guess_box,
                     str(pres_fit_box), pres_box[-1], pres_box_unc[-1], pres_guess_box,
                     str(conc_fit_box), conc_box[-1], conc_box_unc[-1],
                     notes]
        ws.append(datawrite)
        # save workbook
        wb.save(fn_tabulate)
        i += 1
        
# Create dict of results
d_fit = {'Fn': fn,
         'PL Out (cm)': pl_out,
         'PL Out Unc.': pl_out_unc,
         'Temp. In (K)': temp_in,
         'Temp. In Unc.': temp_in_unc,
         'Temp. Out (K)': temp_out,
         'Temp. Out Unc.': temp_out_unc,
         'Temp. Box (K)': temp_box,
         'Temp. Box Unc.': temp_box_unc,
         'Pres. In (atm)': pres_in,
         'Pres. In Unc.': pres_in_unc,
         'Pres. Out (atm)': pres_out,
         'Pres. Out Unc.': pres_out_unc,
         'Pres. Box (atm)': pres_box,
         'Pres. Box Unc.': pres_box_unc,
         'Vel. In (m/s)': vel_in,
         'Vel. In Unc.': vel_in_unc,
         'Conc. In': conc_in,
         'Conc. In Unc': conc_in_unc,
         'Conc. Out': conc_out,
         'Conc. Out Unc': conc_out_unc,
         'Conc. Box': conc_box,
         'Conc. Box Unc': conc_box_unc,
         'Shift (cm-1)': shift,
         'Shift Unc.': shift_unc,
         'Broad (cm-1)': broad,
         'Broad Unc.': broad_unc}

df_fit = pd.DataFrame.from_dict(d_fit)
if save_files:
    pickle.dump(df_fit, open(os.path.join(d_save_0,'df_fit.p'),'wb'))

print('Fitting Done!')
