#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Program for preparing retro data for BACKGROUND fitting

Created on Mon Apr 11 10:38:35 2022

@author: david
"""
import os
import matplotlib.pyplot as plt
import numpy as np
import time

from FitMFIDBkgdRetrowBox import Fitting_Spectra
import td_support as td
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

def FitProgram(filename, save_path, file_tag, weight_window, weight_flat,
               etalon_windows, angle, band_fit, molecules_to_fit, isotopes,
               shift_fit, shift_guess,
               pressure_fit_in, temp_fit_in, velocity_fit_in, conc_fit_in,
               pressure_fit_out, temp_fit_out, conc_fit_out, pathlength_fit_out,
               pressure_guess_in, temp_guess_in, velocity_guess_in,
               conc_guess_in,
               pressure_guess_out, temp_guess_out, conc_guess_out,
               pathlength_guess_out,
               broadening_fit, broadening_guess,
               conc_guess_box, temp_guess_box, pres_guess_box,
               conc_fit_box=False, temp_fit_box=False, pres_fit_box=False,
               pathlength_in=6.84276, n_pass=2, pl_box=35,
               save_files=True, plot_results=True, print_fit_report=True,
               wvnUnits=True, transUnits=True):
    
    ###########################################################################
    theta = angle/360*2*np.pi # convert deg to rad

    pathlength_in = n_pass*pathlength_in/np.cos(theta)

    conc_in = [conc_guess_in]*len(molecules_to_fit) # Initializes concentration
                                            # guess for each molecule to fit
    conc_out = [conc_guess_out]*len(molecules_to_fit)
    
    conc_fit_in = [conc_fit_in]*len(molecules_to_fit) # True for each molecule
                                                        # to fit
    conc_fit_out = [conc_fit_out]*len(molecules_to_fit)
    
    # ------------- Time Domain Specific Parameters ---------------------------    
    ###########################################################################    
    print(filename)
    
    # Loads in file
    data = np.loadtxt(filename)
    
    if data[0,0]>data[-1,0]: # Flips data if it is inverted
        data = np.flipud(data) 
        print("Flipping FFT")
    
    print(len(data))
    if (len(data)!=len(data)):
            print('Data have inconsistent sizes')
    
    # Imports frequency wave for data, converts to wavenumber if in frequency
    # units
    start, stop = td.bandwidth_select_td(data[:,0], band_fit)
    if start < stop:
        if wvnUnits:
            x_data = data[start:stop, 0]
        else:
            x_data = data[start:stop, 0]/29979245800
    else:
        if wvnUnits:
            x_data = data[start:stop:-1, 0]
        else:
            x_data = data[start:stop:-1, 0]/29979245800
    
    # Imports spectra into y_data, converts to absorbance if in transmission
    # units
    y_data = np.zeros((len(x_data),1))
    if transUnits:
        y_data[:,0] = -np.log(data[start:stop,1])
    else:
        y_data[:,0] = data[start:stop,1]
    
    # Convert to time domain
    ff = (np.fft.irfft(y_data[:,0]))
    y_fit = np.zeros((ff.size,(len(data[0])-1)*2))
    
    # Builds logic vector to imform fit of parameters to float
    fit_vector = [temp_fit_in, temp_fit_out, pressure_fit_in, pressure_fit_out, 
        shift_fit, broadening_fit, velocity_fit_in, pathlength_fit_out,
        conc_fit_box, temp_fit_box, pres_fit_box]
    fit_vector.extend(conc_fit_in)
    fit_vector.extend(conc_fit_out)
    
    # Builds vector of approximate parameters for starting fits
    guess_vector = [temp_guess_in, temp_guess_out, pressure_guess_in,
        pressure_guess_out, shift_guess,
        broadening_guess, velocity_guess_in, pathlength_guess_out,
        conc_guess_box, temp_guess_box, pres_guess_box]
    guess_vector.extend(conc_in)
    guess_vector.extend(conc_out)
    
    # Loads Molecule Data from HITRAN
    hapi.db_begin("")  # Location to store imported HITRAN data
    
    HITRAN_Molecules = ['H2O','CO2','O3','N2O','CO','CH4','O2','NO','SO2',
                        'NO2','NH3','HNO3','OH','HF','HCl','HBr','HI','ClO',
                        'OCS','H2CO','HOCl','N2','HCN','CH3Cl','H2O2','C2H2',
                        'C2H6','PH3','COF2','SF6','H2S','HCOOH','HO2','O',
                        'ClONO2','NO+','HOBr','C2H4','CH3OH','CH3Br','CH3CN',
                        'CF4','C4H2','HC3N','H2','CS','SO3','ArH2O_Labfit']
    molecule_numbers =[0]*(len(molecules_to_fit))
    
    ii=0
    for item in molecules_to_fit:
        if item not in HITRAN_Molecules:
            print("Listed Molecule is not included in HITRAN", item)
        elif item == 'O2':
            print('Using preexising O2 HITRAN data in project.')
        else:  
            molecule_numbers[ii] = (HITRAN_Molecules.index(item))+1
            isotope_ids = []
            for jj in range(len(isotopes[ii])):
                isotope_ids.append(hapi.ISO[molecule_numbers[ii],
                                            isotopes[ii][jj]][0]) 
                
            hapi.fetch_by_ids(item, isotope_ids,min(x_data)-1,
                              max(x_data)+1) 
        ii+=1

    tic= time.time()  # Times each fit iteration
    if not y_data[10]:  # checks from empty FFTs and nans in data
        print("Empty FFT")
    else:
        # This sends data to function which compiles it for the fit
        [result, time_wave, conc_in, conc_err_in, conc_out,
         conc_err_out, weight_wave] =\
            Fitting_Spectra(guess_vector,
                fit_vector, x_data, y_data[:,0],
                molecules_to_fit, molecule_numbers, isotopes,
                pathlength_in, theta, pl_box,
                weight_window, weight_flat,
                etalon_windows=etalon_windows)
                                      
                                      
        # Stores fitted parameters and one sigma uncertainties
        Temperature_In = result.best_values['temp_in']
        Temperature_Out = result.best_values['temp_out']
        Temp_Box = result.best_values['temp_box']
        Pressure_In = result.best_values['press_in']
        Pressure_Out = result.best_values['press_out']
        Pres_Box = result.best_values['pres_box']
        Shift = result.best_values['shift']
        Shift_err = result.params['shift'].stderr
        Broadening = result.best_values['broadening']
        Broadening_err = result.params['broadening'].stderr
        Temperature_In_err = result.params['temp_in'].stderr
        Temperature_Out_err = result.params['temp_out'].stderr
        Temp_Box_err = result.params['temp_box'].stderr
        Pressure_In_err = result.params['press_in'].stderr
        Pressure_Out_err = result.params['press_out'].stderr
        Pres_Box_err = result.params['pres_box'].stderr
        Velocity_In = result.best_values['velocity_in']
        Velocity_In_err = result.params['velocity_in'].stderr
        Pathlength_Out = result.best_values['pathlength_out']
        Pathlength_Out_err = result.params['pathlength_out'].stderr
        
        Conc_In = conc_in
        Conc_In_err = conc_err_in
        Conc_Out = conc_out
        Conc_Out_err = conc_err_out

        Conc_Box = result.best_values['conc_box']
        Conc_Box_err = result.params['conc_box'].stderr
    
        # Stores best fits and corresponding data in y_fit matrix          
        time_fit = result.best_fit
        time_data = result.data
        time_res = time_data - time_fit
        weight = weight_wave
        
        abs_fit = np.real(np.fft.rfft(time_fit))
        abs_data = np.real(np.fft.rfft(time_data))
        abs_datanobl = np.real(np.fft.rfft(time_data-
                                             (1-weight_wave)*time_res))
        abs_resnobl = abs_datanobl-abs_fit
        
        # taking a look at what is actually fitted
        abs_fitweight = np.real(np.fft.rfft(time_fit*weight))
        abs_dataweight = np.real(np.fft.rfft(time_data*weight))
        abs_resweight = abs_dataweight - abs_fitweight
        
        #%% create models for plotting
        # Spectral fitting Section ###########################
        # shift xx, don't velocity_in scale yet
        SPEEDOFLIGHT = 2.99792458e8
        
        xx = x_data - Shift
        step_out = (max(xx)-min(xx))/(len(xx)-1)
        
        # box Section
        MIDS = [(7,1,Conc_Box*hapi.abundance(7,1))]
        [nu_box,coefs_box] = hapi.absorptionCoefficient_Voigt(
            MIDS,(molecules_to_fit),
            OmegaStep=step_out, OmegaRange=[min(xx), max(xx)],
            HITRAN_units=False, Environment={'p':Pres_Box,'T':Temp_Box},
            Diluent={'self':Conc_Box-0.21/0.79*(1-Conc_Box),
                     'air':(1-Conc_Box)/0.79},
            IntensityThreshold =0)
    
        abs_box = pl_box*coefs_box
        time_box = np.fft.irfft(abs_box)
        abs_boxweight = np.real(np.fft.rfft(time_box*weight))

        # out Section
        MIDS = []
        # Wavenumber resolution of data       
        for jj in range(len(molecules_to_fit)):
    #            MIDS = []
            for kk in range(len(isotopes[jj])):
                aa = ()
                aa = aa+(molecule_numbers[jj],)
                aa = aa+(isotopes[jj][kk],)
                aa = aa+(Conc_Out[jj]*hapi.abundance(molecule_numbers[jj],
                                                       isotopes[jj][kk]),)
                MIDS.append(aa)
                # print("out_p:", conc_out_p, temp_out, press_out, shift,
                #     pathlength_out_p)
                
        [nu_out,coefs_out] = hapi.absorptionCoefficient_Voigt(
            MIDS,(molecules_to_fit),
            OmegaStep=step_out, OmegaRange=[min(xx), max(xx)],
            HITRAN_units=False, Environment={'p':Pressure_Out,
                                             'T':Temperature_Out},
            Diluent={'self':Conc_Out[jj]-0.21/0.79*(1-Conc_Out[jj]),
                     'air':(1-Conc_Out[jj])/0.79},
            IntensityThreshold =0)
    
        abs_out = Pathlength_Out*coefs_out
        time_out = np.fft.irfft(abs_out)
        abs_outweight = np.real(np.fft.rfft(time_out*weight))
    
        # in Section Yellow
        MIDS = []
        slope_doppler_in_y = Velocity_In*np.sin(-theta)/SPEEDOFLIGHT+1
        xx_in_y = xx*slope_doppler_in_y
        step_in_y = (max(xx_in_y)-min(xx_in_y))/(len(xx_in_y)-1)      # Wavenumber resolution of data       
        for jj in range(len(molecules_to_fit)):
    #            MIDS = []
            for kk in range(len(isotopes[jj])):
                aa = ()
                aa = aa+(molecule_numbers[jj],)
                aa = aa+(isotopes[jj][kk],)
                aa = aa+(Conc_In[jj]*hapi.abundance(molecule_numbers[jj],
                                                    isotopes[jj][kk]),)
                MIDS.append(aa)
                # print("in_y:", conc_in, temp_in, press_in, shift, pathlength_in_y,
                      # velocity_in)
                
        [nu_in_y,coefs_in_y] = hapi.absorptionCoefficient_Voigt(
            MIDS,(molecules_to_fit),
            OmegaStep=step_in_y,OmegaRange=[min(xx_in_y), max(xx_in_y)],
            HITRAN_units=False, Environment={'p':Pressure_In,
                                             'T':Temperature_In},
            Diluent={'self':Conc_In[jj]-0.21/0.79*(1-Conc_In[jj]),
                     'air':(1-Conc_In[jj])/0.79}, #*1.5095245023408206
            IntensityThreshold =0)
    
        abs_in_y = pathlength_in*coefs_in_y
        time_in_y = np.fft.irfft(abs_in_y)
        abs_inweight_y = np.real(np.fft.rfft(time_in_y*weight))
    
        # in Section Purple
        MIDS = []
        slope_doppler_in_p = Velocity_In*np.sin(theta)/SPEEDOFLIGHT+1
        xx_in_p = xx*slope_doppler_in_p
        step_in_p = (max(xx_in_p)-min(xx_in_p))/(len(xx_in_p)-1)      # Wavenumber resolution of data       
        for jj in range(len(molecules_to_fit)):
    #            MIDS = []
            for kk in range(len(isotopes[jj])):
                aa = ()
                aa = aa+(molecule_numbers[jj],)
                aa = aa+(isotopes[jj][kk],)
                aa = aa+(Conc_In[jj]*hapi.abundance(molecule_numbers[jj],
                                                    isotopes[jj][kk]),)
                MIDS.append(aa)
                # print("in_p:", conc_in, temp_in, press_in, shift, pathlength_in_p,
                #       velocity_in)
                
        [nu_in_p,coefs_in_p] = hapi.absorptionCoefficient_Voigt(
            MIDS,(molecules_to_fit),
            OmegaStep=step_in_p,OmegaRange=[min(xx_in_p), max(xx_in_p)],
            HITRAN_units=False, Environment={'p':Pressure_In,
                                             'T':Temperature_In},
            Diluent={'self':Conc_In[jj]-0.21/0.79*(1-Conc_In[jj]),
                     'air':(1-Conc_In[jj])/0.79}, #*1.5095245023408206
            IntensityThreshold =0)
    
        abs_in_p = pathlength_in*coefs_in_p
        time_in_p = np.fft.irfft(abs_in_p)
        abs_inweight_p = np.real(np.fft.rfft(time_in_p*weight))
            
        if print_fit_report:
            print(result.fit_report(result.params))
    
        # res_y = np.copy(np.fft.rfft(result.best_fit[0:cut]))
        # res_p = np.copy(np.fft.rfft(result.best_fit[cut:]))
        toc = time.time()     
        print('Computation Time:', toc - tic)
        
        #%% plot
        ffn = os.path.basename(filename)[:-10]
        if plot_results:
            # FFT of time domain fits yellow stream
            plt.figure() 
            plt.plot(x_data,abs_datanobl, label='Data', linewidth=1)
            plt.plot(x_data,abs_resnobl,
                      label='Residual', linewidth=1, color='none')
            plt.plot(x_data,abs_fit, label='Total Fit',
                linewidth=1)
            plt.plot(x_data,abs_box, label='Box Fit', linewidth=1)
            plt.plot(x_data,abs_in_y, label='In Fit Down', linewidth=1)
            plt.plot(x_data,abs_in_p, label='In Fit Up', linewidth=1)
            plt.plot(x_data,abs_out, label='Out Fit', linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title("No BL Fits"+ffn)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
    
            # Weighted FFT of time domain fits yellow stream
            plt.figure() 
            plt.plot(x_data,abs_dataweight, label='Data', linewidth=1)
            plt.plot(x_data,abs_resweight,
                     label='Residual', linewidth=1, color='none')
            plt.plot(x_data,abs_fitweight, label='Total Fit',
                linewidth=1)
            plt.plot(x_data,abs_boxweight, label='Box Fit', linewidth=1)
            plt.plot(x_data,abs_inweight_y, label='In Fit Down', linewidth=1)
            plt.plot(x_data,abs_inweight_p, label='In Fit Up', linewidth=1)
            plt.plot(x_data,abs_outweight, label='Out Fit', linewidth=1)
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Absorbance')
            plt.title("Weighted Fits"+ffn)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)
            
            # Time domain fit plot. Plot vs time_wave for effective time on x axis 
            # yellow
            plt.figure()
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_data, label='Data', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_res,
                     label='Residual', linewidth=1, color='None')
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_fit,label='Total Fit', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_box,label='Box Fit', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_in_y,label='In Fit Down', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_in_p,label='In Fit Up', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff)),
                     time_out,label='Out Fit', linewidth=1)
            plt.plot(np.linspace(0, 1, len(ff)),
                     result.weights*max(time_data))
            # plt.title()
            plt.xlabel('Index')
            plt.ylabel('Magnitude')
            plt.title("Time Domain"+ffn)
            # plt.rcParams.update({'font.size': 16})
            plt.legend()
            plt.show(block=False)

    
        if save_files:  
            filename_report = os.path.join(save_path,
                                           ffn+file_tag +'_report.txt')
            
            f = open(filename_report,'w')
            f.write(result.fit_report())
            f.close()
            
            # save cepstrum data
            filename_save_cep = os.path.join(save_path,
                                               ffn+file_tag +'_cep.txt')   
        
            cep = np.transpose([time_data,time_fit])
            np.savetxt(filename_save_cep,cep)
        
            # save absorbance data
            filename_save_abs = os.path.join(save_path,
                                               ffn+file_tag +'_abs.txt')   
        
            abs_data = np.transpose([np.real(x_data),abs_data,abs_datanobl])
            np.savetxt(filename_save_abs,abs_data)
        
            # save res data
            filename_save_res = os.path.join(save_path,
                                               ffn+file_tag +'_res.txt')   
        
            res = np.transpose([np.real(x_data),np.real(np.fft.rfft(
                result.data)-np.fft.rfft(result.best_fit))])
            np.savetxt(filename_save_res,res)
        
            # save fit data
            filename_save_fit = os.path.join(save_path,
                                               ffn+file_tag +'_fit.txt')   
        
            fit = np.transpose([np.real(x_data),np.real(np.fft.rfft(
                result.best_fit))])
            np.savetxt(filename_save_fit,fit)
            np.savetxt(filename_save_fit,fit)
            
            #save weight vector
            filename_save_weight = os.path.join(save_path,
                                                  ffn+file_tag+'_weight.txt')   
            weight = np.transpose([time_wave,weight_wave])
            np.savetxt(filename_save_weight,weight)
    
        print(weight_window)
        return_array = [Temperature_In, Temperature_In_err, Temperature_Out, \
            Temperature_Out_err, Pressure_In, Pressure_In_err, \
            Pressure_Out, Pressure_Out_err, Shift, Shift_err, \
            Broadening, Broadening_err,\
            Velocity_In, Velocity_In_err, 
            Pathlength_Out, Pathlength_Out_err,\
            Conc_In, Conc_In_err, Conc_Out, Conc_Out_err,
            Temp_Box, Temp_Box_err, Pres_Box, Pres_Box_err,
            Conc_Box, Conc_Box_err]
        return return_array

