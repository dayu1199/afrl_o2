"""
FreqDoubIntensity.py

Created by on 8/26/22 by david

Description:

"""
import os
import numpy as np
import matplotlib.pyplot as plt

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

from packfind import find_package
find_package('pldspectrapy')
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
from pldspectrapy.constants import *
import td_support as td


d = r'/Volumes/UCFDataMac/011722_LongDistortionTest/pc'
data = np.loadtxt(os.path.join(d, '20220117161157_trans.txt'))
wvnumspace = data[:,0]
trans = data[:,1]

y_O2 = 0.21  # mole fraction O2
P = 0.79  # pressure (atm)
T = 294 # Temperature (K)
L = 95 # cm

#%% Simmulate Upstream
# adjust frequency axis to account for velocity doppler shift
nsteps = len(wvnumspace)-1
windowend_u = np.amax(wvnumspace) # new bandwidth end
windowstart_u = np.amin(wvnumspace) # new bandiwdth start
dv_u = (wvnumspace[-1]-wvnumspace[0])/nsteps # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend_u = windowend_u.item()
windowstart_u = windowstart_u.item()
dv_u = dv_u.item()

hapi.db_begin()

MIDS = [(7, 1, y_O2*hapi.abundance(7, 1))]
# create absorbance model using hapi
[nu_u,coefs_u] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv_u, OmegaRange=[windowstart_u, windowend_u],
            HITRAN_units=False, Environment={'p': P, 'T': T},
            Diluent={'self': y_O2-0.21/0.79*(1-y_O2),
                     'air': (1-y_O2)/0.79},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2_u = coefs_u*L  # get asorption (absorbance * pathlength)

plt.figure()
plt.plot(wvnumspace, trans/max(trans[100:]))
plt.plot(nu_u, -abs_O2_u*8)
plt.ylabel('Normalized Laser Intensity')
plt.xlabel('Wavenumber ($\mathregular{cm^{-1}}$)')