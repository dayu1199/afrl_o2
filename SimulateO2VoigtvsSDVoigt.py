"""
SimulateO2VoigtvsSDVoigt

Created by on 8/2/22 by david

Description: Compare the voigt vs SD voigt profile.
Apparently brian's broadening parameters are only in SDVoigt

"""
#%% import modules
import numpy as np
import matplotlib.pyplot as plt
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as pldhapi
from pldspectrapy.constants import *
import td_support as td

# import hapi

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

#%% Params
add_noise = True
noise_level = 0.014
label = 'AJ'
savedata = False
# add O2 spectra
pldhapi.db_begin('')  # initialize hapi database
# hapi.fetch_by_ids('O2', [36,37,38], 13000, 13200, ParameterGroups=['''SDVoigt'])

y_O2 = 0.21  # mole fraction O2
P = 0.83  # pressure (atm)
T = 600  # Temperature (K)
V = 918  # velocity, m/s
theta = np.deg2rad(35.1)  # degree of beam, rad
windowstart = 13000  # bandwidth start (cm-1)
windowend = 13200  # bandwidth end (cm-1)
dv = 0.00667  # frequency axis step size (cm-1)
L = 6.81736/np.cos(theta)  # path length of measurement (cm)

# hapi.fetch_by_ids('O2', [36, 37, 38], windowstart, windowend,
#                   ParameterGroups=['160-char', 'SDVoigt'])
#
# hapi.fetch_by_ids('O2', [36, 37, 38], windowstart, windowend,
                  # ParameterGroups=['160-char', 'Voigt'])

# hapi.fetch('O2', 7, 1, windowstart, windowend)

#%% Simmulate Upstream
# adjust frequency axis to account for velocity doppler shift
wvnumspace = np.arange(windowstart,windowend,dv) # create array of frequency axis
start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,windowend]) # optimal start stop of bandwidth for fourier transfrom
wvnumspace = wvnumspace[start:stop]
nsteps = len(wvnumspace)-1
slope_doppler = V*np.sin(theta)/SPEED_OF_LIGHT+1  # doppler scaling due to velocity
new_wvw_num = wvnumspace*slope_doppler  # apply to frequency axis
windowend = np.amax(new_wvw_num) # new bandwidth end
windowstart = np.amin(new_wvw_num)  # new bandiwdth start
dv = (new_wvw_num[-1]-new_wvw_num[0])/nsteps  # new freq. axis step size
# get only values from variables (this step may be unccessary)
windowend = windowend.item()
windowstart = windowstart.item()
dv = dv.item()


MIDS = [(7, 1, y_O2*pldhapi.abundance(7, 1))]
# create absorbance model using hapi
[nu_voigt,coefs_voigt] = pldhapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv, OmegaRange=[windowstart, windowend],
            HITRAN_units=False, Environment={'p': P, 'T': T},
            Diluent={'self': y_O2-0.21/0.79*(1-y_O2),
                     'air': (1-y_O2)/0.79},
            IntensityThreshold =0)#*1.5095245023408206
abs_voigt = coefs_voigt*L  # get asorption (absorbance * pathlength)

# hapi.db_begin('')
# MIDS = [(7, 1, y_O2*pldhapi.abundance(7, 1))]
[nu_sd,coefs_sd] = pldhapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv, OmegaRange=[windowstart, windowend],
            HITRAN_units=False, Environment={'p': P, 'T': T},
            Diluent={'self': y_O2,
                     'air': (1-y_O2)},
            IntensityThreshold =0)#*1.5095245023408206
abs_sdvoigt = coefs_sd*L  # get asorption (absorbance * pathlength)

plt.plot(wvnumspace, abs_voigt, label='wrong collisional')
plt.plot(wvnumspace, abs_sdvoigt, label='correct collisional')
plt.plot(wvnumspace, abs_voigt-abs_sdvoigt, label='Difference')

plt.legend() # add legend
