" Test out if I can access OneDrive"

import os

dir = r'/Users/david/Library/CloudStorage/OneDrive-UCB-O365/Python_Test'

# with open(os.path.join(dir, 'string.txt'), 'w') as f:
#     f.write('Hello There')

with open(os.path.join(dir, 'string.txt'), 'r') as f:
    for line in f.readlines():
        print(line)