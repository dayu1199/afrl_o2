# -*- coding: utf-8 -*-
"""
phase correcting several IG files with 
Created on Wed Mar 25 11:04:47 2020

@author: David
"""
import sys
import os
from PhaseCorrectGUI import *

dir_raw = os.path.join(r'/Volumes/AFRL1Beam2/031723/kcell')
fn_raw = '20230317084519'
dir_log = dir_raw
fn_log = ''
dir_push = dir_raw
fn_push = 'poop.txt'

app = QtGui.QApplication(sys.argv)
main = Window(app, dir_raw, fn_raw, dir_push, fn_push, dir_log, fn_log)
main.show()
app.exec_()
print('Program Done')