"""
PlotAllanSim

Created by on 6/30/22 by david

Description: Compare Allan from real data to simulation data

"""

#%% import modules
import os
import pandas as pd
import matplotlib.pyplot as plt
# import matplotlib.ticker as mticker
# plt.rc('axes.formatter', useoffset=False)

import clipboard_and_style_sheet
clipboard_and_style_sheet.style_sheet()

#%% parameters and pull data
# real data
fn_real = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                        r'Mar2022CampaignData/Fits/crossed_allan',
                        r'AllanDeviationsCrossed.xlsx')
df_vel_real = pd.read_excel(fn_real,
                             sheet_name='AK Allan',
                             usecols='F:I',
                             header=1,
                             nrows=4)
tau_vel_real = df_vel_real['Tau (s).1'].values
sigma_vel_real = df_vel_real['Sigma.1'].values
sigmaCI_vel_real = [df_vel_real['Sigma Min..1'].values,
                    df_vel_real['Sigma Max..1'].values]

df_temp_real = pd.read_excel(fn_real,
                              sheet_name='AK Allan',
                              usecols='A:D',
                              header=1,
                              nrows=4)
tau_temp_real = df_temp_real['Tau (s)'].values
sigma_temp_real = df_temp_real['Sigma'].values
sigmaCI_temp_real = [df_temp_real['Sigma Min.'].values,
                     df_temp_real['Sigma Max.'].values]

# sim data
fn_sim = os.path.join(r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS (DO NOT SYNC)',
                        r'Mar2022CampaignData/Fits/crossed_allan',
                        r'AllanDeviationsSim.xlsx')
df_vel_sim = pd.read_excel(fn_sim,
                           sheet_name='7.14.22 AK',
                           usecols='D:G',
                           header=1,
                           nrows=7)
tau_vel_sim = df_vel_sim['Tau (s)'].values
sigma_vel_sim = df_vel_sim['Sigma'].values
sigmaCI_vel_sim = [df_vel_sim['Sigma Min.'].values,
                   df_vel_sim['Sigma Max.'].values]

df_temp_sim = pd.read_excel(fn_sim,
                            sheet_name='7.14.22 AK',
                            usecols='I:L',
                            header=1,
                            nrows=7)
tau_temp_sim = df_temp_sim['Tau (s).1'].values
sigma_temp_sim = df_temp_sim['Sigma.1'].values
sigmaCI_temp_sim = [df_temp_sim['Sigma Min..1'].values,
                    df_temp_sim['Sigma Max..1'].values]

#%% plot comparison
fig, axes = plt.subplots(1, 2)

# plot temperature  Allan
axes[0].set_yscale('log')
axes[0].set_xscale('log')
axes[0].errorbar(tau_vel_real, sigma_vel_real, yerr=sigmaCI_vel_real,
                    label='Real Data (AK)')
axes[0].errorbar(tau_vel_sim, sigma_vel_sim, yerr=sigmaCI_vel_sim,
                    label='Simulated Data')
axes[0].set_ylabel('Vel. Allan (m/s)')
axes[0].set_xlabel('Tau (s)')
axes[0].legend()
# axes[0].xaxis.set_minor_formatter(mticker.ScalarFormatter())

# plot velocity allan
axes[1].set_yscale('log')
axes[1].set_xscale('log')
axes[1].errorbar(tau_temp_real, sigma_temp_real, yerr=sigmaCI_temp_real,
                    label='Real Data (AK)')
axes[1].errorbar(tau_temp_sim, sigma_temp_sim, yerr=sigmaCI_temp_sim,
                    label='Simulated Data')
axes[1].set_ylabel('Temp. Allan (K)')
axes[1].set_xlabel('Tau (s)')
axes[1].legend()
# axes[1].xaxis.set_minor_formatter(mticker.ScalarFormatter())

#%% Add simulated precision number
# vel
axes[0].scatter([600], [90.9], label='Simulated Precision', color='red')
axes[0].legend()
# temp
axes[1].scatter([600], [21.9], label='Simulated Precision', color='red')
axes[1].legend()