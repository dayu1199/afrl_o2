"""
FormatBrianO2.py

Created by on 1/11/23 by david

Description:
Format text copied from Brian's O2 paper

"""
import pandas as pd
import linelist_conversions as lc
import numpy as np

format_uncertainties = True

text1 = []
with open('BrianO2Unformatted.txt', 'r') as f:
    for i, line in enumerate(f.readlines()):
        if i != 0:
            line = line.replace('- ', '-')
            line = line.replace(' (', '(')
            line = line.replace('\t', ' ')
        text1.append(line)

# with open('BrianO2Formatted.txt', 'w') as f:
#     f.writelines(text)

# format further
# pull the uncertainty numbers
if format_uncertainties:
    text2 = []
    for i, line in enumerate(text1):
        line_new = ''
        uncertainties = ''
        if i != 0:
            x = line.split()
            uncertainty_indices = [2, 5, 6, 7, 8, 9, 10, 11, 12, 13]
            for j, value in enumerate(x):
                if j in uncertainty_indices:
                    start = x[j].find('(')
                    if start != -1:
                        uncertainty = x[j][start+1: -1]
                        if uncertainty == '0':
                            print('There is a 0 uncertainty!!')
                        if len(uncertainty) == 1:
                            uncertainty = '0' + uncertainty
                        elif len(uncertainty) > 2:
                            uncertainty = round(int(uncertainty), -(len(uncertainty)-2))
                            uncertainty = str(uncertainty)[:2]
                        line_new += x[j][:start] + ' '
                    else:
                        uncertainty = '00'
                        line_new += x[j] + ' '
                    uncertainties += uncertainty
                else:
                    line_new += x[j] + ' '
            line_new = line_new + uncertainties + '\n'
        else:
            line_new = line[:-1] + ' uncertainty\n'
        text2.append(line_new)

    # write to file
    with open('BrianO2Formatted.txt', 'w') as f:
        f.writelines(text2)

    # write to pandas dataframe
    text3 = []
    for line in text2:
        text3.append(line.split())
    df = pd.DataFrame(text3[1:], columns=text3[0])

    ## add molecular number and isotopologue
    mol_num = np.ones((len(text3)-1, 1)).astype('int64') * 7
    iso = np.ones((len(text3)-1, 1)).astype('int64')
    df['molec_id'] = mol_num
    df['local_iso_id'] = iso

    # convert datatype
    df['nu'] = pd.to_numeric(df['nu'])
    df['sw'] = pd.to_numeric(df['sw'])
    df['elower'] = pd.to_numeric(df['elower'])
    df['gamma_n2'] = pd.to_numeric(df['gamma_n2'])
    df['n_n2'] = pd.to_numeric(df['n_n2'])
    df['gamma_self'] = pd.to_numeric(df['gamma_self'])
    df['n_self'] = pd.to_numeric(df['n_self'])
    df['delta_n2'] = pd.to_numeric(df['delta_n2'])
    df['n_delta_n2'] = pd.to_numeric(df['n_delta_n2'])
    df['delta_self'] = pd.to_numeric(df['delta_self'])
    df['n_delta_self'] = pd.to_numeric(df['n_delta_self'])
    df['sd_air'] = pd.to_numeric(df['sd_air'])

    # air parameters
    df['gamma_air'] = 0.79*df['gamma_n2'] + 0.21*df['gamma_self']
    df['n_air'] = 0.79 * df['n_n2'] + 0.21 * df['n_self']
    df['delta_air'] = 0.79 * df['delta_n2'] + 0.21 * df['delta_self']
    df['n_delta_air'] = 0.79 * df['n_delta_n2'] + 0.21 * df['n_delta_self']

    extra_params = {"gamma_n2": "%8.5f",
                    "n_n2": "%6.3f",
                    "n_self": "%6.3f",
                    "delta_n2": "%9.5f",
                    "n_delta_n2": "%5.1e",
                    "delta_self": "%9.5f",
                    "n_delta_self": "%5.1e",
                    "sd_air": "%6.3f",
                    "n_delta_air": "%5.1e",
                    "uncertainty": "%21s"}

    lc.df_to_par(df, 'O2Drouin', extra_params=extra_params)
else:
    with open('BrianO2Formatted_1.txt', 'w') as f:
        f.writelines(text1)
