#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot O2 Isotopes

Created on Tue Apr 12 09:37:50 2022

@author: david
"""

import os
import matplotlib.pyplot as plt
from packfind import find_package
find_package('pldspectrapy')
import td_support as td
import pldspectrapy.pldhapi as hapi
import numpy as np

from pldspectrapy.constants import *

def SimSpec(T,P,y,V,wvnumstart,wvnumend,
            angle=35,molec_to_sim='O2',wavelength=False):
    hapi.db_begin('')
    d_save = 'SimulationFiles'
    ## Constants
    # isolator conditions, no combustion
    # R = 83.144598 #cm3*bar/molK, universal gas constant
    # T = 700 #K
    # P =  0.85 # atm
    # angle = 35
    # y_H2O = 0.07
    # V = 1040
    # molec_to_sim = 'H2O_PaulLF'
    # Na = 6.02E23 #Avogadro's number
    
    theta = angle/180*np.pi
    L = 6.84/np.cos(theta) #cm
    wvnum_start = 13000
    wvnum_end = 13170
    wvnum_step = 0.00667
    wvnumspace = np.arange(wvnum_start,wvnum_end,wvnum_step)
    start, stop = td.bandwidth_select_td(wvnumspace, [wvnum_start,wvnum_end])
    wvnumspace = wvnumspace[start:stop]
    
    nsteps = len(wvnumspace)-1
    
    # isotope 1
    slope_doppler = V*np.sin(theta)/SPEED_OF_LIGHT+1
    new_wvw_num = wvnumspace*slope_doppler
    windowend = np.amax(new_wvw_num)
    windowstart = np.amin(new_wvw_num)
    dv = (new_wvw_num[-1]-new_wvw_num[0])/nsteps
    windowend = windowend.item()
    windowstart = windowstart.item()
    dv = dv.item()
    
    MIDS = [(7,1,y*hapi.abundance(7,1))]
    
    [nu_1,coefs_1] = hapi.absorptionCoefficient_SDVoigt(MIDS,
                (molec_to_sim), OmegaStep=dv,OmegaRange=[windowstart, windowend],
                HITRAN_units=False, Environment={'p':P,'T':T}, 
                Diluent={'self':y,'air':(1-y)},
                IntensityThreshold =0)#*1.5095245023408206
    
    absorbance_1 = L*coefs_1
    
    # isotope 2
    MIDS = [(7,2,y*hapi.abundance(7,2))]
    
    [nu_2,coefs_2] = hapi.absorptionCoefficient_SDVoigt(MIDS,
                (molec_to_sim), OmegaStep=dv,OmegaRange=[windowstart, windowend],
                HITRAN_units=False, Environment={'p':P,'T':T}, 
                Diluent={'self':y,'air':(1-y)},
                IntensityThreshold =0)#*1.5095245023408206
    
    absorbance_2 = L*coefs_2
    
    # isotope 3
    MIDS = [(7,3,y*hapi.abundance(7,3))]
    
    [nu_3,coefs_3] = hapi.absorptionCoefficient_SDVoigt(MIDS,
                (molec_to_sim), OmegaStep=dv,OmegaRange=[windowstart, windowend],
                HITRAN_units=False, Environment={'p':P,'T':T}, 
                Diluent={'self':y,'air':(1-y)},
                IntensityThreshold =0)#*1.5095245023408206
    
    absorbance_3 = L*coefs_3
    
    if wavelength:
        lambda_O2 = 1e7/wvnumspace
        plt.figure()
        plt.plot(lambda_O2,absorbance_1,label='Isotope 1')
        plt.plot(lambda_O2,absorbance_2,label='Isotope 2')
        plt.plot(lambda_O2,absorbance_3,label='Isotope 3')
        plt.legend()
    else:
        plt.figure()
        plt.plot(wvnumspace,absorbance_1,label='Isotope 1')
        plt.plot(wvnumspace,absorbance_2,label='Isotope 2')
        plt.plot(wvnumspace,absorbance_3,label='Isotope 3')
        plt.legend()

if __name__ == "__main__":
    # T = 700 #K
    # P =  0.85 # atm
    # angle = 35
    # y_H2O = 0.07
    # V = 1040
    wvnumstart = 12950
    wvnumend = 13170
    # hapi.fetch_by_ids('O2',[36,37,38],wvnumstart, wvnumend, ParameterGroups=['160-char', 'SDVoigt'])
    SimSpec(500,0.85,0.201,0, wvnumstart, wvnumend,
            angle=35,molec_to_sim='O2',wavelength=True)
    
    