

from lmfit import Model, Parameters
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
import numpy as np
from math import floor
import matplotlib.pyplot as plt


def spectra(xx_y, xx_p, press_in, press_out, temp_in, temp_out, shift, velocity_in,
    pathlength_in_y, pathlength_in_p, pathlength_out, broadening,
    conc_box, temp_box, pres_box, pathlength_box,
    theta_y, theta_p, **kwargs):
    ''' This is the actual fitting function. in this configuration this function only works for HITRAN molecules
    inputs:
        xx: xscaling wave in wavenumber
        press: pressure in atm
        temp: temperature in K
        shift: wavenumber shift in cm-1
        pathlength: cm
        broadening: multiple of gamma air
        **kwards: Concentrations and HITRAN molecules    '''
    
    print('Fitting...')
    SPEEDOFLIGHT = 2.99792458e8
    conc_in=[]; conc_out=[];
    molecules_to_fit =[]
    molecule_number=[]; isotopes=[]
    
    for key, value in kwargs.items(): # Unpacks keyword arguments into molecules to fit and concentrations
        if key[:2] == 'x_':
            isos = []
            if key.find('in')>0:
                i = key.find('in')
                conc_in.extend([value])
                molecules_to_fit.append(key[2:i])   
                molecule_number.extend([getMoleculeNumber(key[2:i])])
                while i<len(key)-2:
                    isos.extend([int(key[i+2])])
                    i+=1
                isotopes.append(isos)
            elif key.find('out')>0:
                conc_out.extend([value])
        else:
            print('Unexpected keyword argument')

    # Spectral fitting Section ###########################
    # shift xx, don't velocity_in scale yet
    xx_y = xx_y - shift
    xx_p = xx_p - shift
    
    step_out_y = (max(xx_y)-min(xx_y))/(len(xx_y)-1)
    step_out_p = (max(xx_p)-min(xx_p))/(len(xx_p)-1)

    # box Section Yellow
    MIDS = [(7,1,conc_box*hapi.abundance(7,1))]
    [nu_box_y,coefs_box_y] = hapi.absorptionCoefficient_Voigt(
        MIDS,(molecules_to_fit),
        OmegaStep=step_out_y, OmegaRange=[min(xx_y), max(xx_y)],
        HITRAN_units=False, Environment={'p':pres_box,'T':temp_box},
        Diluent={'self':conc_box,
                 'air':(1-conc_box)},
        IntensityThreshold =0)

    absorbance_box_y = pathlength_box*coefs_box_y
    
    # box Section Purple
    MIDS = [(7,1,conc_box*hapi.abundance(7,1))]
    [nu_box_p,coefs_box_p] = hapi.absorptionCoefficient_Voigt(
        MIDS,(molecules_to_fit),
        OmegaStep=step_out_p, OmegaRange=[min(xx_p), max(xx_p)],
        HITRAN_units=False, Environment={'p':pres_box,'T':temp_box},
        Diluent={'self':conc_box,
                 'air':(1-conc_box)},
        IntensityThreshold =0)

    absorbance_box_p = pathlength_box*coefs_box_p

    # out Section Yellow
    MIDS = []
    # Wavenumber resolution of data       
    for jj in range(len(molecules_to_fit)):
#            MIDS = []
        for kk in range(len(isotopes[jj])):
            aa = ()
            aa = aa+(molecule_number[jj],)
            aa = aa+(isotopes[jj][kk],)
            aa = aa+(conc_out[jj]*hapi.abundance(molecule_number[jj],
                                                   isotopes[jj][kk]),)
            MIDS.append(aa)
            # print("out_y:", conc_out, temp_out, press_out, shift,
                # pathlength_out)
            
    [nu_out_y,coefs_out_y] = hapi.absorptionCoefficient_Voigt(
        MIDS,(molecules_to_fit),
        OmegaStep=step_out_y, OmegaRange=[min(xx_y), max(xx_y)],
        HITRAN_units=False, Environment={'p':press_out,'T':temp_out},
        Diluent={'self':conc_out[jj],
                 'air':(1-conc_out[jj])},
        IntensityThreshold =0)

    absorbance_out_y = pathlength_out*coefs_out_y

    # out Section Purple
    MIDS = []
    # Wavenumber resolution of data       
    for jj in range(len(molecules_to_fit)):
#            MIDS = []
        for kk in range(len(isotopes[jj])):
            aa = ()
            aa = aa+(molecule_number[jj],)
            aa = aa+(isotopes[jj][kk],)
            aa = aa+(conc_out[jj]*hapi.abundance(molecule_number[jj],
                                                   isotopes[jj][kk]),)
            MIDS.append(aa)
            # print("out_p:", conc_out_p, temp_out, press_out, shift,
            #     pathlength_out_p)
            
    [nu_out_p,coefs_out_p] = hapi.absorptionCoefficient_Voigt(
        MIDS,(molecules_to_fit),
        OmegaStep=step_out_p, OmegaRange=[min(xx_p), max(xx_p)],
        HITRAN_units=False, Environment={'p':press_out,'T':temp_out},
        Diluent={'self':conc_out[jj],
                 'air':(1-conc_out[jj])},
        IntensityThreshold =0)

    absorbance_out_p = pathlength_out*coefs_out_p

    # in Section Yellow
    MIDS = []
    slope_doppler_in_y = velocity_in*np.sin(theta_y)/SPEEDOFLIGHT+1
    xx_in_y = xx_y*slope_doppler_in_y
    step_in_y = (max(xx_in_y)-min(xx_in_y))/(len(xx_in_y)-1)      # Wavenumber resolution of data       
    for jj in range(len(molecules_to_fit)):
#            MIDS = []
        for kk in range(len(isotopes[jj])):
            aa = ()
            aa = aa+(molecule_number[jj],)
            aa = aa+(isotopes[jj][kk],)
            aa = aa+(conc_in[jj]*hapi.abundance(molecule_number[jj],
                                                isotopes[jj][kk]),)
            MIDS.append(aa)
            # print("in_y:", conc_in, temp_in, press_in, shift, pathlength_in_y,
                  # velocity_in)
            
    [nu_in_y,coefs_in_y] = hapi.absorptionCoefficient_Voigt(
        MIDS,(molecules_to_fit),
        OmegaStep=step_in_y,OmegaRange=[min(xx_in_y), max(xx_in_y)],
        HITRAN_units=False, Environment={'p':press_in,'T':temp_in},
        Diluent={'self':conc_in[jj],
                 'air':(1-conc_in[jj])}, #*1.5095245023408206
        IntensityThreshold =0)

    absorbance_in_y = pathlength_in_y*coefs_in_y

    # in Section Purple
    MIDS = []
    slope_doppler_in_p = velocity_in*np.sin(theta_p)/SPEEDOFLIGHT+1
    xx_in_p = xx_p*slope_doppler_in_p
    step_in_p = (max(xx_in_p)-min(xx_in_p))/(len(xx_in_p)-1)      # Wavenumber resolution of data       
    for jj in range(len(molecules_to_fit)):
#            MIDS = []
        for kk in range(len(isotopes[jj])):
            aa = ()
            aa = aa+(molecule_number[jj],)
            aa = aa+(isotopes[jj][kk],)
            aa = aa+(conc_in[jj]*hapi.abundance(molecule_number[jj],
                                                isotopes[jj][kk]),)
            MIDS.append(aa)
            # print("in_p:", conc_in, temp_in, press_in, shift, pathlength_in_p,
            #       velocity_in)
            
    [nu_in_p,coefs_in_p] = hapi.absorptionCoefficient_Voigt(
        MIDS,(molecules_to_fit),
        OmegaStep=step_in_p,OmegaRange=[min(xx_in_p), max(xx_in_p)],
        HITRAN_units=False, Environment={'p':press_in,'T':temp_in},
        Diluent={'self':conc_in[jj],
                 'air':(1-conc_in[jj])}, #*1.5095245023408206
        IntensityThreshold =0)

    absorbance_in_p = pathlength_in_p*coefs_in_p

    # Sums up absorption for all molecules in molecules to fit 
    absorbance_y = absorbance_in_y + absorbance_out_y + absorbance_box_y
    absorbance_y = np.fft.irfft(absorbance_y) # inverse Fourier transform for fitting

    absorbance_p = absorbance_in_p + absorbance_out_p + absorbance_box_p
    absorbance_p = np.fft.irfft(absorbance_p) # inverse Fourier transform for fitting
    
    absorbance = np.concatenate((absorbance_y,absorbance_p))
    return absorbance

def Fitting_Spectra(guess_vector, fit_vector, x_data_y, y_data_y, x_data_p,
    y_data_p, molecules_to_fit, molecule_numbers, isotopes,
    pathlength_in_y, pathlength_in_p, theta_y, theta_p, pl_box,
    weight_window_y, weight_window_p, weight_flat,
    etalon_windows_y={}, etalon_windows_p={}):
    '''
    Calculates weighting wave and initializes fit parameters. Returns fit object and effective time wave
    '''
    
    # Decompose guess and fit vectors to initialize parameters for fit
    temp_in_guess = guess_vector[0]
    temp_in_fit = fit_vector[0]
    temp_out_guess = guess_vector[1]
    temp_out_fit = fit_vector[1]
    
    pressure_in_guess = guess_vector[2]
    pressure_in_fit = fit_vector[2]
    pressure_out_guess = guess_vector[3]
    pressure_out_fit = fit_vector[3]
    
    shift_guess = guess_vector[4]
    shift_fit = fit_vector[4]
    
    broadening_guess = guess_vector[5]
    broadening_fit = fit_vector[5]

    velocity_in_guess = guess_vector[6]
    velocity_in_fit = fit_vector[6]
    
    pathlength_out_guess = guess_vector[7]
    pathlength_out_fit = fit_vector[7]
    
    conc_box_guess = guess_vector[8]
    conc_box_fit = fit_vector[8]
    
    temp_box_guess = guess_vector[9]
    temp_box_fit = fit_vector[9] 
    
    pres_box_guess = guess_vector[10]
    pres_box_fit = fit_vector[10] 
    
    ii = 0 # initializes concentration parameters
    conc_fit_in = [0]*len(molecules_to_fit)
    conc_in = [0]*len(molecules_to_fit)
    conc_err_in = [0]*len(molecules_to_fit)
    conc_fit_out = [0]*len(molecules_to_fit)
    conc_out = [0]*len(molecules_to_fit)
    conc_err_out = [0]*len(molecules_to_fit)
    
    params = Parameters()

    molecule_info=[]
    for ii in range(len(molecules_to_fit)):
        conc_fit_in[ii] = fit_vector[11+ii]
        conc_in[ii] = guess_vector[11+ii]
        molecule_info.append('x_'+molecules_to_fit[ii]+'in')
        for iso in isotopes[ii]:
            molecule_info[ii] += str(iso)
        params.add(molecule_info[ii], value = conc_in[ii],
            min=0, max=1, vary=conc_fit_in[ii])
        print(molecule_info[ii])
    for ii in range(len(molecules_to_fit)):
        conc_fit_out[ii] = fit_vector[11+ii+len(molecules_to_fit)]
        conc_out[ii] = guess_vector[11+ii+len(molecules_to_fit)]
        molecule_info.append('x_'+molecules_to_fit[ii]+'out')
        for iso in isotopes[ii]:
            molecule_info[ii+len(molecules_to_fit)] += str(iso)
        params.add(molecule_info[ii+len(molecules_to_fit)], value = 
            conc_out[ii],
            min=0, max=1, vary=conc_fit_out[ii])
        print(molecule_info[ii+len(molecules_to_fit)])
 
    
    #Defines bounds of fit. Make sure to change these to reflect environmental conditions
    params.add('temp_in', value=temp_in_guess, min=100, max=350,
        vary=temp_in_fit)
    params.add('temp_out', value=temp_out_guess, min=250, max=400,
        vary=temp_out_fit)
    params.add('press_in', value=pressure_in_guess, min=pressure_in_guess*0.1,
        max=pressure_in_guess*5, vary=pressure_in_fit)
    params.add('press_out', value=pressure_out_guess,
        min=pressure_out_guess*0.1, max=pressure_out_guess*5,
        vary=pressure_out_fit)
    params.add('shift', value=shift_guess, min=-.1, max=.1, vary=shift_fit)
    params.add('velocity_in', value=velocity_in_guess, 
               min=-2000, max=2000,
               vary=velocity_in_fit)
    params.add('pathlength_in_y', value = pathlength_in_y, vary=False)
    params.add('pathlength_in_p', value = pathlength_in_p, vary=False)
    params.add('pathlength_out', value = pathlength_out_guess,
               vary=pathlength_out_fit)
    params.add('broadening', value = broadening_guess, vary=broadening_fit)
    params.add('theta_y', value = theta_y, vary=False)
    params.add('theta_p', value = theta_p, vary=False)
    
    params.add('conc_box', value=conc_box_guess, min=0, max=0.5,
        vary=conc_box_fit)
    params.add('temp_box', value=temp_box_guess, min=250, max=500,
        vary=temp_box_fit)
    params.add('pres_box', value=pres_box_guess, min=0.1, max=2,
               vary=pres_box_fit)
    params.add('pathlength_box', value=pl_box, vary=False)

    ## Start edits here, how to fit for pathlength out
    
    # Defines weighting window for fit
    percent_start_y = weight_window_y[0] 
    percent_stop_y = weight_window_y[1]
    percent_start_p = weight_window_p[0] 
    percent_stop_p = weight_window_p[1]
    y_datai_y = np.fft.irfft(y_data_y)
    y_datai_p = np.fft.irfft(y_data_p) 
    
    # Calculates weighting wave for fit
    [weight_y, time_y] =  weightWave(x_data_y, temp_out_guess, pressure_out_guess,
        conc_out, molecules_to_fit,molecule_numbers, percent_start_y,
        percent_stop_y)
    [weight_p, time_p] =  weightWave(x_data_p, temp_out_guess, pressure_out_guess,
        conc_out, molecules_to_fit,molecule_numbers, percent_start_p,
        percent_stop_p)

    if weight_flat:
        for i in range(len(weight_y)):
            if weight_y[i] != 0:
                weight_y[i] = 1
        for i in range(len(weight_p)):
            if weight_p[i] != 0:
                weight_p[i] = 1

    # Sets weighting wave to zero over etalon sections
    for a in etalon_windows_y:
        weight_y[a[0]:a[1]] = [0 for i in range(a[0],a[1])]
        weight_y[weight_y.size-a[1]:weight_y.size-a[0]] = [0 for i in range(weight_y.size-a[1],weight_y.size-a[0])]
    for a in etalon_windows_p:
        weight_p[a[0]:a[1]] = [0 for i in range(a[0],a[1])]
        weight_p[weight_y.size-a[1]:weight_p.size-a[0]] = [0 for i in range(weight_p.size-a[1],weight_p.size-a[0])]

    # concatenate both streams
    y_datai = np.concatenate((y_datai_y,y_datai_p))
    weight = np.concatenate((weight_y,weight_p))
    time = np.concatenate((time_y,time_p))

    # Builds fit model
    gmodel = Model(spectra, independent_vars=['xx_y','xx_p'])
    
    # Runs fit, result is fit model object containing results of fit
    result = gmodel.fit(y_datai, params=params, weights=weight,
                        xx_y=x_data_y, xx_p=x_data_p)
    for ii in range(len(molecules_to_fit)):
        conc_in[ii] = result.best_values[molecule_info[ii]]
        conc_err_in[ii] = result.params[molecule_info[ii]].stderr
        conc_out[ii] = result.best_values[molecule_info\
            [ii+len(molecules_to_fit)]]
        conc_err_out[ii] = result.params[molecule_info\
            [ii+len(molecules_to_fit)]].stderr

    print(result.fit_report)
      
    return result, time, conc_in, conc_err_in, conc_out, conc_err_out, weight

    
def weightWave(fwave1,T,P,molefractions, molecules, moleculenum, percent_start,percent_stop):   
    ''' Calculates exponential decay weighting wave for all molecules. Add together exponential decays weighted by each molecules concentration
    Exponential decay is calculated from the envelope portion of Ian's 2010 Time Domain paper. An average broadening (weighted by linestrength)
    is calculated from HITRAN parameters in window. Returns weighting wave and effective time wave
    '''
    
    zero_start = int(floor(fwave1.size*percent_start*2))
    zero_stop = int(floor(fwave1.size*percent_stop*2))

    fwave = fwave1*2.99792458*10**10
    c = 299792458 #m/s
    Na = 6.02E-23   #mol-1
    k = 1.38064852E-23  #m^2kgs^-2K^-1
    c2 = (6.62606957E-27)*(2.99792458E10)/(1.3806488E-16)  #cm*K
    
    ff = (np.fft.irfft(fwave))
    n = ff.size
    dstep = (fwave[1]-fwave[0])
    times =np.fft.fftfreq(n, dstep)
    times = np.fft.fftshift(times)
    time_abs = (times-times[0])
    
    ii=0
    for molecule in molecules:
        
        envelope = np.zeros((len(time_abs)))
        temp_env = np.zeros((len(time_abs)))
        chi = molefractions[ii]
        
        M = molarMass(molecule) #g/mol
        S = np.asarray(hapi.getColumn(molecule,"sw"))  #cm-1/(molec cm-2)
        gamma_self = np.asarray(hapi.getColumn(molecule,"gamma_self")) #cm-1 atm-1
        gamma_air = np.asarray(hapi.getColumn(molecule,"gamma_air")) #cm-1 atm-1
        n_air = np.asarray(hapi.getColumn(molecule,"n_air")) #unitless
        ls = np.asarray(hapi.getColumn(molecule,"nu")) #cm-1
        elower = np.asarray(hapi.getColumn(molecule,"elower")) #cm-1
        
        Q_296 = PartitionFunction(molecule,296)[0]
        QT = PartitionFunction(molecule,T)[0]
              
        linecenter = 299792458*100*ls
        gamma = 299792458*100*(gamma_air*(1-chi)*P + gamma_self*chi*P)*(296/T)**n_air
        doppler = 299792458*100*(linecenter/c)*np.sqrt(2*Na*k*T*np.log(2)/(M/1000))
        linestrength = c*100*S*(Q_296/QT)*(np.exp(-c2*elower/T)/np.exp(-c2*elower/296))*((1-np.exp(-c2*linecenter/(T*c*100))/(1-np.exp(-c2*linecenter/(296*c*100)))))
        
        #Linestrength weighted average gamma
        gamma_avg = np.average(gamma,weights=linestrength)
        doppler_avg = np.average(doppler,weights = linestrength)
        temp_env = np.exp((-1*((2*np.pi*doppler_avg*time_abs)**2)/(4*np.log(2)))+(-2.0*np.pi*gamma_avg*time_abs))
        
        envelope += temp_env/temp_env[zero_start]*chi
        
        ii+=1
                
        envelope[zero_stop:envelope.size-zero_stop] = [0 for i in range(zero_stop,envelope.size-zero_stop)]
        envelope[0:zero_start] = [0 for i in range(0,zero_start)]
        envelope[int(envelope.size/2):]=[0 for i in range(int(envelope.size/2),envelope.size)]
        
        ienvelope = np.flipud(envelope)
        envelope = envelope+ienvelope
        
        maxval = max(envelope)
        envelope = envelope/maxval

    
    return envelope, time_abs
    
    
def PartitionFunction(molecule,temperature):
    '''
    Calculates the partition sum accoring to the TIPS code released on HITRAN.org
    
    Call to TIPS is via HAPI 10/23
    
    Gamache et al. Total internal partition sums for 166 isotopologues of 51 molecules 
    important in planetary atmospheres: Application to HITRAN2016 and beyond, 
    Journal of Quantitative Spectroscopy and Radiative Transfer, 2017.
    
    Assumes isotope  =  1

    '''
    
    
    molecules = {
    'H2O':1,
    'H2O_Paul':1,
    'H2O_PaulLF':1,
    'CO2':2,
    'O3':3,
    'N2O':4,
    'CO':5,
    'CH4':6,
    'O2':7,
    'NO':8,
    'SO2':9,
    'NO2':10,
    'NH3':11,
    'HNO3':12,
    'OH':13,
    'HF':14,
    'HCl':15,
    'HBr':16,
    'HI':17,
    'ClO':18,
    'OCS':19,
    'H2CO':20,
    'HOCl':21,
    'N2':22,
    'HCN':22,
    'CH3Cl':23,
    'H2O2':24,
    'C2H2':25,
    'C2H6':26,
    'PH3':27,
    'COF2':28,
    'SF6':29,
    'H2S':30,
    'HCOOH':31,
    'HO2':32,
    'O':33,
    'ClONO2':34,
    'NO+':35,
    'HOBr':36,
    'C2H4':37,
    'CH3OH':38,
    'CH3Br':39,
    'CH3CN':40,
    'CF4':41,
    'C4H2':42,
    'HC3N':43,
    'H2':44,
    'CS':45,
    'SO3':46,
    'ArH2O_Labfit':1
    }
    
    moleculenum = molecules[molecule]
    temp = [temperature]
    QT =hapi.partitionSum(moleculenum,1,temp)
    return QT
     
    
def molarMass(molecule):
    '''
    Returns the molar mass (g/mol) of molecule (string)
    '''
    masses = {
        'H2O':18.01528,
        'H2O_Paul':18.01528,
        'H2O_PaulLF':18.01528,
        'CO2':44.01,
        'O3':48,
        'N2O':44.013,
        'CO':28.01,
        'CH4':16.04,
        'O2':15.999,
        'NO':30.006,
        'SO2':64.066,
        'NO2':46.0055,
        'NH3':17.031,
        'HNO3':63.01,
        'OH':17.008,
        'HF':20.01,
        'HCl':36.46,
        'HBr':80.91,
        'HI':127.911,
        'ClO':51.4521,
        'OCS':60.07,
        'H2CO':30.031,
        'HOCl':52.46,
        'N2':28.0134,
        'HCN':27.0253,
        'CH3Cl':50.49,
        'H2O2':34.0147,
        'C2H2':26.04,
        'C2H6':30.07,
        'PH3':33.99758,
        'COF2':96.93,
        'SF6':146.06,
        'H2S':34.1,
        'HCOOH':46.03,
        'HO2':33.0,
        'O':16.0,
        'ClONO2':97.46,
        'NO+':30.006,
        'HOBr':96.91,
        'C2H4':28.05,
        'CH3OH':32.04,
        'CH3Br':94.94,
        'CH3CN':41.05,
        'CF4':88.0043,
        'C4H2':50.0587,
        'HC3N':51.048,
        'H2':2.01588,
        'CS':44.07,
        'SO3':80.062,
        'ArH2O_Labfit':18.01528
        }
    
    molarmass = masses[molecule]
    return molarmass
    
    
    
def savethings(name, mattosave):
    
    with open(name + '.txt', 'w') as f:
        jj=0
        for item in mattosave: 
            for element in item:
               f.write('{0:.20f}, '.format(element) + ' ') 
            jj+=1
            f.write('\n')   
        f.close()

    
def getMoleculeNumber(Name):
    
    HITRAN_molecules = ['H2O','CO2','O3','N2O','CO','CH4','O2','NO','SO2','NO2','NH3','HNO3','OH','HF','HCl','HBr','HI','ClO','OCS','H2CO','HOCl','N2','HCN','CH3Cl','H2O2','C2H2','C2H6','PH3','COF2','SF6','H2S','HCOOH','HO2','O','ClONO2','NO+','HOBr','C2H4','CH3OH','CH3Br','CH3CN','CF4','C4H2','HC3N','H2','CS','SO3','ArH2O_Labfit']
    if Name == 'H2O_Paul':
        molecule_number = 1
    if Name == 'H2O_PaulLF':
        molecule_number = 1
    else:
        try:
            molecule_number = (HITRAN_molecules.index(Name))+1
        except ValueError:
            molecule_number=111
    
    return molecule_number