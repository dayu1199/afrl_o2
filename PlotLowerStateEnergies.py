#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot of Lower State Energies

Created on Wed Apr 13 09:27:39 2022

@author: david
"""

import os
import matplotlib.pyplot as plt
from packfind import find_package
find_package('pldspectrapy')
import td_support as td
import pldspectrapy.pldhapi as hapi
import numpy as np

# %% import data/fit
d_data = os.path.join(r'/Users/david/Library/CloudStorage',
                      r'GoogleDrive-dayu1199@colorado.edu/My Drive',
                      r'AFRL ETHOS (DO NOT SYNC)',
                      r'Mar2022CampaignData/Fits/retro/04_05_2022_144049/16')
fn_data = '20220311234509_abs.txt'
fn_fit = '20220311234509_fit.txt'

hapi.db_begin('')
# hapi.fetch_by_ids('O2', 36, 13000, 13170, ParameterGroups=['160-char', 'SDVoigt?'])

data = np.loadtxt(os.path.join(d_data,fn_data))
x_data = data[:,0]
abs_nobl = data[:,2]

fit = np.loadtxt(os.path.join(d_data,fn_fit))
x_fit = fit[:,0]
abs_fit = fit[:,1]

# %% pull O2 E" data
sw = []
nu = []
E = []
with open('O2.data') as file:
    for line in file:
        sw_temp = float(line[16:25])
        if sw_temp > 1e-25:
            sw.append(sw_temp)
            nu.append(float(line[3:15]))
            E.append(float(line[47:55]))
            
# %% Plot
fig, ax1 = plt.subplots()
ax1.plot(x_data,abs_nobl,label='Data')
ax1.plot(x_fit,abs_fit,label='Fit')
ax1.set_xlabel('Wavenumber ($cm^{-1}$)')
ax1.set_ylabel('Absorbance')
ax1.set_ylim([-0, 0.014])

ax2 = ax1.twinx()
ax2.plot(nu,E,'r')
ax2.yaxis.label.set_color('red')
ax2.tick_params(axis='y', colors='red')
ax2.set_ylim([0,950])
ax2.set_ylabel('E"')

def tick_function(wvnum_array):
    lambda_array = 1e7/wvnum_array
    return ['%5.1f' % z for z in lambda_array]

wvnum = np.array([13020,13040,13060,13080,13100,13120,13140,13160])
ax3 = ax1.twiny()
ax3.set_xlim(ax1.get_xlim())
ax3.set_xticks(wvnum)
ax3.set_xticklabels(tick_function(wvnum))
ax3.set_xlabel('Wavelength (nm)')