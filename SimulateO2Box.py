#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Simulate spectra in box
Created on Tue May 10 10:32:24 2022

@author: david
"""
import numpy as np
import matplotlib.pyplot as plt
from packfind import find_package
find_package('pldspectrapy')
import pldspectrapy.pldhapi as hapi
from pldspectrapy.constants import *
import td_support as td

#%% params
plt.rcParams.update({"figure.autolayout": True, "lines.linewidth": 0.8,
                     'font.size': 14, 'font.family':'serif',
                     'font.serif':'Times'})

add_noise = True
noise_level = 0.0025
# add O2 spectra
hapi.db_begin('') # initialize hapi database

#%% simulate absorbance
y_O2 = 0.00421491 # mole fraction O2
P = 1 # pressure (atm)
T = 295 # Temperature (K)
windowstart = 13010 # bandwidth start (cm-1)
windowend = 13180 # bandwidth end (cm-1)
dv = 0.00667 # frequency axis step size (cm-1)
L = 35

## Upstream
# adjust frequency axis to account for velocity doppler shift
wvnumspace = np.arange(windowstart,windowend,dv) # create array of frequency axis
start, stop = td.bandwidth_select_td(wvnumspace, [windowstart,windowend]) # optimal start stop of bandwidth for fourier transfrom
wvnumspace = wvnumspace[start:stop]
nsteps = len(wvnumspace)-1
windowstart = np.amin(wvnumspace) # new bandiwdth start
windowend = np.amax(wvnumspace) # new bandwidth end
dv = (wvnumspace[-1]-wvnumspace[0])/nsteps # new freq. axis step size

MIDS = [(7,1,y_O2*hapi.abundance(7,1))]
# create absorbance model using hapi
[nu,coefs] = hapi.absorptionCoefficient_SDVoigt(MIDS,
            ('O2'), OmegaStep=dv,OmegaRange=[windowstart, windowend],
            HITRAN_units=False, Environment={'p':P,'T':T},
            Diluent={'self': y_O2 - 0.21 / 0.79 * (1 - y_O2),
                     'air': (1 - y_O2) / 0.79},
            IntensityThreshold =0)#*1.5095245023408206
abs_O2 = coefs*L # get asorption (absorbance * pathlength)

#%% calculate noise
noise = np.random.normal(0,noise_level,len(abs_O2))
beta = 9
center = 13092
sigma = 100
subbotin = np.exp(-(abs(wvnumspace-center)/sigma)**beta)
# normalize to 0.5
subbotin *= 0.95/max(subbotin)
subbotin = 1 - subbotin
weight = subbotin.copy()
# calculate an etalon
etalon = 0.05*np.cos((wvnumspace-13105)*2*np.pi/11) + 0.8
weight *= etalon
# add slight parabola
parabola = ((wvnumspace-13100)/60)**2+1
parabola /= min(parabola)
weight *= parabola
# weight = ((wvnumspace-13100)/38)**2+1
noise *= weight

#%% plot absorption model
lambda_O2 = 1e7/wvnumspace # transform to wavelength units
plt.figure() # initialize plotting figure
# plt.plot(nu,abs_O2+noise,label='O2') #plot absorption
if add_noise:
  plt.plot(wvnumspace,abs_O2 + noise)
plt.plot(wvnumspace,abs_O2)
plt.legend() # add legend

time_O2 = np.fft.irfft(abs_O2)
time_noise = np.fft.irfft(noise)

plt.figure()
plt.plot(time_noise+time_O2)
plt.plot(time_O2)

#%% save data
data = np.vstack((wvnumspace,abs_O2))
data = np.transpose(data)
np.savetxt('O2simulation_box.txt',data)

print(np.std(noise[15700:16100]))
