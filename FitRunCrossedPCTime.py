# -*- coding: utf-8 -*-
"""
Run Fitting for Crossed Beams for Mar2020 Campaign
vis2 downstream yellow
vis3 upstream purple

Created on Tue Sep 24 13:28:11 2019
@author: David
"""
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from openpyxl import Workbook, load_workbook
import pandas as pd
import pickle
from datetime import datetime

import FitProgram2BeamwBox as fithit
from OptimizeBL import OptimizeBL

##################################Fitting Parameters###########################

# Fitting Options
save_files = True  # saves fits and fitted parameters in text files.
plot_results = False  # Plots time domain and TD fit converted to the frequency domain. Turn this off for many datafiles.
print_fit_report = True  # Prints each fit report to command window
back_remove = True  # Remove background from spectra before fitting
label_back = ''

# Directory Info
pc_times = pc_times = [*range(15, 570, 15)]
d_data_y = os.path.join(r'/Users/david/Library/CloudStorage',
                        r'GoogleDrive-dayu1199@colorado.edu',
                        r'My Drive/AFRL ETHOS (DO NOT SYNC)',
                        r'Mar2022CampaignData/nist/031122_vis2')
d_data_p = os.path.join(r'/Users/david/Library/CloudStorage',
                        r'GoogleDrive-dayu1199@colorado.edu',
                        r'My Drive/AFRL ETHOS (DO NOT SYNC)',
                        r'Mar2022CampaignData/moose/031122_vis3')
d_save = os.path.join(r'/Users/david/Library/CloudStorage',
                      r'GoogleDrive-dayu1199@colorado.edu',
                      r'My Drive/AFRL ETHOS (DO NOT SYNC)',
                      r'Mar2022CampaignData/Fits/crossed')
file_tag = ''  # remember to add a _ before

# which indexes to fit, just test runs
indexes = [5, 6, 7, 10, '11_2']  # {5,6,7,10,'11_2'}

# MFID Parameters
# weight_windows_y = [[0.00872,0.5]]
# weight_windows_p = [[0.00845,0.5]]
# weight_windows_y = [[0.008, 0.5],
#                     [0.0084, 0.5],
#                     [0.008, 0.5],
#                     [0.008, 0.5],
#                     [0.0082, 0.5]]  # default [0.005,0.5]
# weight_windows_p = [[0.0081, 0.5],
#                     [0.0083, 0.5],
#                     [0.008, 0.5],
#                     [0.008, 0.5],
#                     [0.0084, 0.5]]  # default [0.005,0.5]
etalon_windows_y = [[[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]]]  # format [[[0.1,0.15]],[[0.22,0.25]]]
etalon_windows_p = [[[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]],
                    [[0.0109, 0.01117]]]
# band_fits_y = [[13011,13166]]
# band_fits_p = [[13017,13167]]
# band_fits_y = [[13016.1, 13166.7],
#                [13015.2, 13168],
#                [13015.2, 13169],
#                [13015.2, 13170.5],
#                [13015.9, 13168]]
# band_fits_p = [[13016.1, 13166.9],
#                [13014.3, 13167.8],
#                [13014.3, 13172],
#                [13015.9, 13166.1],
#                [13015.1, 13167.8]]
weight_flat = True  # Otherwise will do an exponential weighting
    
# Fitting Parameters
angle_p = 34.87
angle_y = -35.1
molec_to_fit = ['O2']  # must be a list
isotopes = [[1]] # format [[1],[2]]
press_guesss = [1.170, 1.371, 0.926, 1.176, 1.189]  # atm
# press_guesss = [1.189]
temp_guess = 600
vel_guess = 1500
shift_guess = 0
conc_guess = 0.21
broad_guess = 1
temp_fit = True
press_fit = False
shift_fit = True
vel_fit = True
conc_fit = False
broad_fit = False

# Background Fitting Parameters
conc_back = 5.1e-8
temp_back = 376.7
press_back = 4.5
pl_back = 10
label_back = '13 061522'

# Box Fitting Parameters
conc_box = 0.00501
temp_box = 281.7
pres_box = 1
pl_box = 35

if not(back_remove):
    conc_back_y = 0
    conc_back_p = 0
    shift_back = 0
    press_back = 0
    conc_box = 0
    
###############################Port in data###################################
# get files from directory
# import filename key to get filenames
d_key = r'/Volumes/GoogleDrive/My Drive/AFRL ETHOS/MarchCampaign'
fn_key = 'FilenameKey.xlsx'
key_df = pd.read_excel(os.path.join(d_key,fn_key))

# get time for labeling fit
now = datetime.now()
timestamp = now.strftime('%d_%m_%Y_%H%M%S')

# folder to save data
d_save_0 = os.path.join(d_save,timestamp)
if save_files:
    if not(os.path.isdir(d_save_0)):
        os.mkdir(d_save_0)

for index in indexes:
    print(index)

    # weight_window_y = weight_windows_y[i]
    # weight_window_p = weight_windows_p[i]
    etalon_window_y = etalon_windows_y[i]
    etalon_window_p = etalon_windows_p[i]
    # band_fit_y = band_fits_y[i]
    # band_fit_p = band_fits_p[i]
    press_guess = press_guesss[i]
    i += 1
    # Grab filenames from key

    # if index != 7:
    #     continue

    row = key_df.loc[key_df['Indexx']==index]
    vis2_daq = str(row.iloc[0]['vis2 DAQ'])
    vis2_fn = str(row.iloc[0]['vis2 fn'])
    d_vis2 = os.path.join(d_data_y,
                          str(index)+'_pctime_fromback',
                          str(index)+'_'+str(pc_time)+'s',
                          'time_resolved', str(index))
    vis3_daq = str(row.iloc[0]['vis3 DAQ'])
    vis3_fn = str(row.iloc[0]['vis3 fn'])
    d_vis3 = os.path.join(d_data_p,
                          str(index) + '_pctime_fromback',
                          str(index) + '_' + str(pc_time) + 's',
                          'time_resolved', str(index))

    # check limiting number of files
    n_allan = min([len(os.listdir(d_vis2)), len(os.listdir(d_vis3))])
    print('Fitting ' + str(n_allan) + ' spectra for ' + str(index))

    # prepare arrays for dataframe
    iteration = []
    fn_up = []
    fn_down = []
    vel = []
    vel_unc = []
    temp = []
    temp_unc = []
    press = []
    press_unc = []
    shift = []
    shift_unc = []
    conc = []
    conc_unc = []
    # broad = []
    # broad_unc = []
    # pl = []
    # pl_unc = []

    d_save_1 = os.path.join(d_save_0, str(index))
    if save_files:
        if not(os.path.isdir(d_save_1)):
            os.mkdir(d_save_1)

    for j in range(n_allan):
        print('Fitting ' + str(j+1) + ' out of ' + str(n_allan) + ' spectra')
        # perform fits
        # find vis 2 data
        vis2_found = False

        fn_y = vis2_fn + '_trans' + str(int(j)) + '.txt'
        fn_y = os.path.join(d_vis2, fn_y)
        fn_p = vis3_fn + '_trans' + str(int(j)) + '.txt'
        fn_p = os.path.join(d_vis3, fn_p)

        # make sure fn_y exists
        if not os.path.isfile(fn_y):
            print('vis2 file not found!')
            continue
        if not os.path.isfile(fn_p):
            print('vis3 file not found!')
            continue

        fn_down.append(fn_y)
        fn_up.append(fn_p)

        # get optimal bd with and bl
        data_y = np.loadtxt(fn_y)
        band_fit_y, bl_start_y = OptimizeBL(data_y[:, 0], data_y[:, 1],
                                            thres=5e-5, thres_range=20,
                                            weight=0.6
                                            )
        weight_window_y = [bl_start_y, 0.5]

        data_p = np.loadtxt(fn_p)
        band_fit_p, bl_start_p = OptimizeBL(data_p[:, 0], data_p[:, 1],
                                            thres=5e-5, thres_range=20,
                                            weight=0.6)
        weight_window_p = [bl_start_p, 0.5]
        print(band_fit_y, band_fit_p, weight_window_y, weight_window_p)

        vel_val, vel_err, temp_val, temp_err, press_val, press_err, shift_val,\
            shift_err, conc_val, conc_err, broad_val, broad_err = \
                fithit.FitProgram(fn_y, fn_p, d_save_1, file_tag,
                                  weight_window_y, weight_window_p, weight_flat, etalon_window_y,
                                  etalon_window_p,
                                  angle_y, angle_p, band_fit_y,band_fit_p,
                                  molec_to_fit, isotopes,
                                  press_fit, temp_fit, vel_fit,
                                  shift_fit, conc_fit, broad_fit,
                                  press_guess, temp_guess, vel_guess,
                                  shift_guess, conc_guess, broad_guess,
                                  conc_box=conc_box, temp_box=temp_box,
                                  pres_box=pres_box, pl_box=pl_box,
                                  background_remove=back_remove,
                                  pathlength_b=pl_back,
                                  press_back=press_back, temp_back=temp_back,
                                  conc_back_y=conc_back, conc_back_p=conc_back,
                                  save_files=False,
                                  plot_results=plot_results,
                                  transUnits=True)
        iteration.append(j)
        vel.append(vel_val)
        vel_unc.append(vel_err)
        temp.append(temp_val)
        temp_unc.append(temp_err)
        press.append(press_val)
        press_unc.append(press_err)
        shift.append(shift_val)
        shift_unc.append(shift_err)
        conc.append(conc_val[0])
        conc_unc.append(conc_err[0])

    # Create dict of results
    d_fit = {'Iteration': iteration,
             'Fn Down': fn_down,
             'Fn Up': fn_up,
             'Vel. (m/s)': vel,
             'Vel. Unc.': vel_unc,
             'Temp. (K)': temp,
             'Temp. Unc.': temp_unc,
             'Pres. (atm)': press,
             'Pres. Unc.': press_unc,
             'Shift (cm-1)': shift,
             'Shift Unc.': shift_unc,
             'Conc.': conc,
             'Conc. Unc': conc_unc}

    df_fit = pd.DataFrame.from_dict(d_fit)
    if save_files:
        pickle.dump(df_fit, open(os.path.join(d_save_1,
                                              str(index)+'_'+str(pc_time)+
                                              '_df_fit.p'), 'wb'))

    print('Fitting for '+str(index)+' Done!')
print('Fitting Done!')